(cl:defpackage #:zombie-raptor/core/sdl2
  (:documentation "Processes SDL2 input and provides a window abstraction.")
  (:use #:cl
        #:zombie-raptor/core/constants
        #:zombie-raptor/core/settings
        #:zombie-raptor/core/window
        #:zombie-raptor/render/gl
        #:zr-utils)
  (:import-from #:alexandria
                #:once-only
                #:with-gensyms)
  (:import-from #:autowrap
                #:basic-foreign-type
                #:enum-value
                #:find-record-field
                #:find-type
                #:foreign-record-field
                #:foreign-type
                #:frf-bit-offset
                #:ptr)
  (:import-from #:cffi
                #:foreign-pointer
                #:foreign-type-size
                #:with-foreign-objects)
  (:import-from #:cffi-sys
                #:%mem-ref
                #:%mem-set)
  (:import-from #:sdl2
                #:create-window
                #:destroy-window
                #:gl-set-attr
                #:gl-set-swap-interval
                #:in-main-thread
                #:init
                #:quit
                #:scancode-key-to-value
                #:set-relative-mouse-mode
                #:set-window-fullscreen
                #:with-sdl-event)
  (:import-from #:sdl2-ffi
                #:sdl-event
                #:sdl-event-type)
  (:import-from #:sdl2-ffi.functions
                #:sdl-get-window-size
                #:sdl-gl-create-context
                #:sdl-gl-delete-context
                #:sdl-gl-make-current
                #:sdl-gl-swap-window
                #:sdl-poll-event
                #:sdl-push-event
                #:sdl-warp-mouse-in-window)
  (:export #:+sdl-scancode-size+
           #:actual-window-dimensions!
           #:do-events
           #:input-state
           #:make-input-state
           #:quit-sdl
           #:request-quit
           #:reset-input-state!
           #:scancode-key-to-value
           #:sdl-input!
           #:sdl-warp-mouse
           #:sdl-window
           #:set-relative-mouse-mode
           #:setup-sdl
           #:with-input-state
           #:with-sdl-event-pointer
           #:with-sdl-rendering
           #:with-sdl-thread))

(cl:in-package #:zombie-raptor/core/sdl2)

;;;; Input state

(defconstant +sdl-scancode-size+ 283
  "The size of arrays whose indexes represent SDL scancodes.")

(defconstant +sdl-mouse-buttons+ 5
  "These are the mouse buttons that SDL supports.")

(define-array-and-type keyboard-state-array      (bit `(,+sdl-scancode-size+)))
(define-array-and-type mouse-click-state-array   (bit `(,(1+ +sdl-mouse-buttons+))))
(define-array-and-type mouse-click-counter-array (fixnum `(,(1+ +sdl-mouse-buttons+))))
(define-array-and-type click-location-array      (fixnum '(4)))
(define-array-and-type mouse-motion-array        (fixnum '(3)))
(define-array-and-type mouse-wheel-array         (fixnum '(2)))

(define-simple-struct (input-state (:conc-name nil))
  (key-presses    keyboard-state-array)
  (keys-active    keyboard-state-array)
  (mouse-clicks   mouse-click-state-array)
  (click-counter  mouse-click-counter-array)
  (click-location click-location-array)
  (mouse-motion   mouse-motion-array)
  (mouse-wheel    mouse-wheel-array))

(define-accessor-macro with-input-state)

;;;; Start and stop SDL

(defun setup-sdl (settings)
  "
Starts SDL and then creates and returns an sdl window and an SDL GL
context.
"
  (init :everything)
  (with-settings (title
                  width
                  height
                  vsync
                  msaa
                  fullscreen)
      settings
    (in-main-thread (:no-event t)
      (gl-set-attr :context-profile-mask 1)
      (gl-set-attr :context-major-version +gl-major-version+)
      (gl-set-attr :context-minor-version +gl-minor-version+)
      (gl-set-attr :multisamplebuffers (if (zerop msaa) 0 1))
      (gl-set-attr :multisamplesamples msaa)
      (let* ((sdl-window (create-window :title title
                                        :x :centered
                                        :y :centered
                                        :w width
                                        :h height
                                        :flags '(:shown :opengl)))
             (gl-context (sdl-gl-create-context sdl-window)))
        (sdl-gl-make-current sdl-window gl-context)
        (gl-set-swap-interval (if vsync 1 0))
        (set-window-fullscreen sdl-window (if (eq fullscreen t)
                                              :windowed
                                              fullscreen))
        (actual-window-dimensions! settings sdl-window)
        (set-relative-mouse-mode 1)
        (values sdl-window gl-context)))))

(defun quit-sdl (gl-context sdl-window)
  "
Deletes the SDL GL context, destroys the SDL window, and then quits
SDL.
"
  (sdl-gl-delete-context gl-context)
  (destroy-window sdl-window)
  (quit))

;;;; Misc. macros

(defmacro with-sdl-thread ((&key standard-output (background t)) &body body)
  `(in-main-thread (:background ,background)
     (let ((*standard-output* (or ,standard-output *standard-output*)))
       ,@body)))

(defmacro with-sdl-rendering ((sdl-window (red green blue alpha)) &body body)
  "Runs the basic SDL+GL rendering code on sdl-window."
  `(with-gl-rendering (,red ,green ,blue ,alpha :swap-window (sdl-gl-swap-window ,sdl-window))
     ,@body))

;;;; SDL Event pointers

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun sdl-event-pointer-type (fields)
    (let* ((sdl-event (foreign-type (find-type 'sdl-event)))
           (foreign-item sdl-event)
           (byte-offset 0))
      (dolist (field-name fields (values foreign-item byte-offset))
        (let ((foreign-field (find-record-field foreign-item field-name)))
          (when (typep foreign-field 'foreign-record-field)
            (incf byte-offset (truncate (frf-bit-offset foreign-field) 8)))
          (setf foreign-item (basic-foreign-type foreign-field))))))

  (define-function (sdl-event-pointer-type* :inline t) (name)
    (sdl-event-pointer-type (short-name-to-fields-list name)))

  (defun short-name-to-fields-list (short-name)
    (ecase short-name
      (:event-type '(:type))
      (:button-button '(:button :button))
      (:button-clicks '(:button :clicks))
      (:button-x '(:button :x))
      (:button-y '(:button :y))
      (:motion-xrel '(:motion :xrel))
      (:motion-yrel '(:motion :yrel))
      (:motion-state '(:motion :state))
      (:keysym-sym '(:key :keysym :sym))
      (:keysym-scancode '(:key :keysym :scancode))
      (:wheel-x '(:wheel :x))
      (:wheel-y '(:wheel :y))
      (:wheel-direction '(:wheel :direction)))))

(defmacro with-event-accessor (slots event &body body)
  (once-only (event)
    (flet ((symbol-macrolet-slot (binding accessor)
             (multiple-value-bind (type offset)
                 (sdl-event-pointer-type* (symbol-to-keyword accessor))
               `(,binding (%mem-ref ,event ,type ,offset)))))
      (let ((slots* (mapcar (lambda (slot)
                              (etypecase slot
                                (list (destructuring-bind (binding accessor)
                                          slot
                                        (check-type binding symbol)
                                        (check-type accessor symbol)
                                        (symbol-macrolet-slot binding accessor)))
                                (symbol (symbol-macrolet-slot slot slot))))
                            slots)))
        `(symbol-macrolet ,slots*
           ,@body)))))

;;;; Events

(defmacro enum-case ((keyform foreign-enum) &body cases)
  "
Acts like `case' on a foreign enum, where the cases are the high-level
keywords translated to their numeric values by this macro.
"
  `(case= ,keyform
     ,@(mapcar (destructuring-lambda (enum-keyword &rest implicit-progn)
                 (check-type enum-keyword keyword)
                 `(,(enum-value foreign-enum enum-keyword) ,@implicit-progn))
               cases)))

(defmacro do-events ((input-state event-pointer) &body cases)
  "
Iterates over the events with an `enum-case' calling the provided
function name for a matched case. The called function is called like
this: (function-name input-state event-pointer)
"
  (with-gensyms (event? quit)
    (once-only (event-pointer input-state)
      (let ((cases* (mapcar (destructuring-lambda (enum-keyword function-name)
                              `(,enum-keyword (,function-name ,input-state ,event-pointer)))
                            cases)))
        (multiple-value-bind (type offset) (sdl-event-pointer-type* :event-type)
          `(do ((,event? (sdl-poll-event ,event-pointer) (sdl-poll-event ,event-pointer))
                ,quit)
               ((zerop ,event?) ,quit)
             (declare (int32 ,event?))
             (enum-case ((%mem-ref ,event-pointer ,type ,offset)
                         sdl-event-type)
               ,@cases*
               (:quit (setf ,quit t)))))))))

(defmacro with-sdl-event-pointer ((event-pointer) &body body)
  (with-gensyms (event)
    `(with-sdl-event (,event)
       (let ((,event-pointer (ptr ,event)))
         ,@body))))

(defconstant +quit+ (enum-value 'sdl-event-type :quit))

(defun request-quit ()
  (with-sdl-event (event :quit)
    (setf (event :type) +quit+)
    (sdl-push-event event)))

(defmacro define-sdl-input-function (name input-state-accessors pointers &body body)
  (with-gensyms (input-state event-pointer)
    `(define-function (,name :inline t)
         ((,input-state input-state)
          ,@(list-if pointers `(,event-pointer foreign-pointer)))
       (with-input-state ,input-state-accessors
           ,input-state
         ,@(if pointers
               `((with-event-accessor ,pointers
                     ,event-pointer
                   ,@body))
               body)))))

(define-sdl-input-function %key-down (key-presses keys-active)
    ((scancode keysym-scancode))
  (setf (bit key-presses scancode) 1
        (bit keys-active scancode) 1))

(define-sdl-input-function %key-up (key-presses keys-active)
    ((scancode keysym-scancode))
  (setf (bit key-presses scancode) 0
        (bit keys-active scancode) 0))

(define-sdl-input-function %mouse-button-down (mouse-clicks click-counter click-location)
    ((button button-button)
     (clicks button-clicks)
     (x button-x)
     (y button-y))
  (let ((button button))
    (setf (bit mouse-clicks button) 1
          (aref click-counter button) clicks
          (aref click-location 0) x
          (aref click-location 1) y)))

(define-sdl-input-function %mouse-button-up (mouse-clicks click-location)
    ((button button-button)
     (x button-x)
     (y button-y))
  (setf (bit mouse-clicks button) 0
        (aref click-location 2) x
        (aref click-location 3) y))

(define-sdl-input-function %mouse-motion (mouse-motion)
    ((x-rel motion-xrel)
     (y-rel motion-yrel)
     (state motion-state))
  (incf (aref mouse-motion 0) x-rel)
  (incf (aref mouse-motion 1) y-rel)
  (setf (aref mouse-motion 2) state))

(define-sdl-input-function %mouse-wheel (mouse-wheel)
    ((x wheel-x)
     (y wheel-y)
     (direction wheel-direction))
  (declare (ignore direction))
  (incf (aref mouse-wheel 0) x)
  (incf (aref mouse-wheel 1) y))

(define-sdl-input-function reset-input-state! (mouse-motion mouse-wheel)
    ()
  (fill mouse-motion 0)
  (fill mouse-wheel 0))

(define-function sdl-input! ((input-state input-state)
                             (event-pointer foreign-pointer))
  "
Handles SDL events, which are mostly input-related. This function
modifies input-state and returns t if the quit event was received.
"
  (declare (optimize (speed 3)))
  (do-events (input-state event-pointer)
    (:keydown         %key-down)
    (:keyup           %key-up)
    (:mousebuttondown %mouse-button-down)
    (:mousebuttonup   %mouse-button-up)
    (:mousemotion     %mouse-motion)
    (:mousewheel      %mouse-wheel)))

;;;; Misc. functions

(defun actual-window-dimensions! (settings sdl-window)
  (with-settings (height width)
      settings
    (with-foreign-objects ((width* :int)
                           (height* :int))
      (sdl-get-window-size sdl-window width* height*)
      (let ((width* (%mem-ref width* :int))
            (height* (%mem-ref height* :int)))
        (psetf width width*
               height height*)
        (values width* height*)))))

(define-function (sdl-warp-mouse :inline t) (sdl-window x y)
  (sdl-warp-mouse-in-window sdl-window x y))

;;;; Window object

(defclass sdl-window (window)
  ((%window
    :accessor window
    :documentation "An SDL window foreign object"))
  (:documentation "
Holds the state that the SDL library needs. Its initialization
correctly starts SDL for a single-window application. There is a
`cleanup' method that correctly closes SDL and the graphics context it
creates.
"))

(defmethod initialize-instance :after ((object sdl-window) &key settings)
  (setf (values (window object) (graphics-context object)) (setup-sdl settings)))

(defmethod cleanup ((object sdl-window))
  (with-accessors* (graphics-context window)
      object
    (quit-sdl graphics-context window)))
