(cl:defpackage #:zombie-raptor/core/times
  (:documentation "Tracks time for the game engine.")
  (:use #:cl
        #:zombie-raptor/core/constants
        #:zr-utils)
  (:export #:times
           #:update-times!
           #:with-times
           #:with-times-accessors))

(cl:in-package #:zombie-raptor/core/times)

(define-function (%current-second :inline t) ()
  "Converts internal CL time to a double-float second."
  (* (get-internal-real-time) +seconds-per-time-unit+))

;;; Stores time state that the game loop needs
(defstruct (times (:type (vector double-float))
                  (:conc-name nil))
  (current-time  0d0 :type double-float)
  (old-time-diff 0d0 :type double-float)
  (time-diff     0d0 :type double-float)
  (elapsed-time  0d0 :type double-float)
  (missed-time   0d0 :type double-float)
  (new-time      0d0 :type double-float)
  (current-spf   0d0 :type double-float))

(deftype times ()
  '(simple-array double-float (7)))

(define-accessor-macro with-times-accessors)

(defmacro with-times (name slots &body body)
  "Initializes the time state and provides accessors to its contents"
  `(let ((,name (make-array 7
                            :element-type 'double-float
                            :initial-element 0d0)))
     (declare (dynamic-extent ,name))
     (setf (current-time ,name) (%current-second))
     (with-times-accessors ,slots
         ,name
       ,@body)))

(define-function update-times! ((times times))
  (declare (optimize (speed 3)))
  "Updates the time state each game loop iteration"
  (with-accessors* (current-spf
                    current-time
                    missed-time
                    new-time
                    old-time-diff
                    time-diff)
      times
    (setf new-time (%current-second)
          old-time-diff time-diff
          time-diff (- new-time current-time)
          current-time new-time
          current-spf (* 0.5d0 (+ time-diff old-time-diff)))
    (incf missed-time time-diff)
    times))

;;; Calculate the time to pass into the rendering system
(define-function (%missed-time-per-step :inline t) ((times times))
  (float* (* (missed-time times) +time-steps-per-second+)))
