(defpackage #:zombie-raptor/core/types
  (:documentation "Common types used by the engine.")
  (:use #:cl
        #:zombie-raptor/core/constants
        #:zombie-raptor/math/boolean-set
        #:zr-utils)
  (:export #:entity-id
           #:entity-ids
           #:entity-set-array
           #:gl-id
           #:gl-ids
           #:make-entity-id
           #:make-entity-ids
           #:make-entity-index
           #:make-entity-indices
           #:make-entity-set-array
           #:make-gl-ids
           #:make-single-floats
           #:make-vecs
           #:single-floats
           #:tick
           #:vecs))

(in-package #:zombie-raptor/core/types)

(deftype entity-id ()
  `(mod ,+max-entity-id+))

(deftype entity-index ()
  `(mod ,+max-entities+))

(deftype tick ()
  `(unsigned-byte ,+tick-size+))

(deftype gl-id ()
  'fixnum)

(define-function (make-entity-id :inline t) (&key (initial-element 0 entity-id))
  initial-element)

(define-function (make-entity-index :inline t) (&key (initial-element 0 entity-index))
  initial-element)

(define-array-and-type entity-set-array (boolean-set `(,size) :arguments (size)))
(define-array-and-type entity-ids (entity-id `(,size) :arguments (size) :initial-element 0))
(define-array-and-type entity-indices (entity-index `(,size) :arguments (size) :initial-element 0))
(define-array-and-type gl-ids (fixnum `(,size) :arguments (size) :initial-element 1))
(define-array-and-type single-floats (single-float `(,size) :arguments (size)))
(define-array-and-type vecs (single-float `(,storage-size ,vec-size) :arguments (vec-size storage-size)))
