(defpackage #:zombie-raptor/core/settings
  (:documentation "Stores settings used by the game engine.")
  (:use #:cl
        #:zr-utils)
  (:export #:copy-settings
           #:make-settings
           #:settings
           #:with-settings))

(in-package #:zombie-raptor/core/settings)

(deftype fullscreen-options ()
  "
There are four valid options: T, NIL, :fullscreen, and :windowed. T is
equivalent to :windowed, which is the borderless windowed fullscreen
instead of the 'real' fullscreen.
"
  `(or boolean (member :fullscreen :windowed)))

;;; Note: uint16 is probably not enough for multiple 16K displays, so
;;; it's not future proof.
(deftype resolution ()
  'uint32)

(deftype sensitivity ()
  'single-float)

(deftype fov ()
  'single-float)

(defstruct (settings (:copier %copy-settings))
  "
Stores the settings that are used to customize the user's experience.
"
  (title               "Zombie Raptor" :type string)
  (width               1280            :type resolution)
  (height              720             :type resolution)
  (fullscreen          nil             :type fullscreen-options)
  (app-name            "zombie-raptor" :type string)
  (org-name            nil             :type (maybe string))
  (mouse-sensitivity-x 0.002f0         :type sensitivity)
  (mouse-sensitivity-y 0.002f0         :type sensitivity)
  (invert-x            nil             :type boolean)
  (invert-y            nil             :type boolean)
  ;; TODO: Update the FOV when the FOV is changed
  (fov                 100f0           :type fov)
  (vsync               t               :type boolean)
  (msaa                0               :type uint32)
  (debug               nil             :type boolean)
  (background          nil             :type (maybe (simple-array single-float (3)))))

(psetf (documentation 'settings-title 'function)
       "The title for the created window."
       (documentation 'settings-width 'function)
       "The width of the created window."
       (documentation 'settings-height 'function)
       "The height of the created window."
       (documentation 'settings-fullscreen 'function)
       "Create the window as windowed, fullscreen, or windowed fullscreen."
       (documentation 'settings-app-name 'function)
       "The name of the app in created directories for things like settings."
       (documentation 'settings-org-name 'function)
       "The parent ('organization') directory of the app-name, if present."
       (documentation 'settings-mouse-sensitivity-x 'function)
       "The rotation speed of the camera, if controlled by the mouse's x-axis."
       (documentation 'settings-mouse-sensitivity-y 'function)
       "The movement speed of looking up and down via the camera, if controlled by the mouse's y-axis."
       (documentation 'settings-invert-x 'function)
       "Determines if the x-axis movement is 'inverted' from the default behavior."
       (documentation 'settings-invert-y 'function)
       "Determines if the y-axis movement is 'inverted' from the default behavior."
       (documentation 'settings-fov 'function)
       "The field of view of the camera."
       (documentation 'settings-vsync 'function)
       "Sets whether or not the vsync feature is enabled. If true, the frame rate will be locked."
       (documentation 'settings-msaa 'function)
       "The amount of MSAA (multisample anti-aliasing). It is disabled with 0."
       (documentation 'settings-debug 'function)
       "Enables the (potentially verbose and slow) debug functionality of various features.")

;;; A deep copy of the settings struct
(define-function (copy-settings :inline t) ((settings settings))
  (let ((copy (%copy-settings settings)))
    (psetf (settings-title copy) (copy-seq (settings-title settings))
           (settings-app-name copy) (copy-seq (settings-app-name settings))
           (settings-org-name copy) (copy-seq (settings-org-name settings)))
    copy))

(define-accessor-macro with-settings #:settings-)
