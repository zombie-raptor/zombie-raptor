(cl:defpackage #:zombie-raptor/core/constants
  (:documentation "Constants used by the game engine.")
  (:use #:cl
        #:zr-utils)
  (:import-from #:alexandria
                #:define-constant)
  (:export #:+gl-major-version+
           #:+gl-minor-version+
           #:+gl-version-string+
           #:+glsl-version+
           #:+max-entities+
           #:+max-entity-id+
           #:+max-geometry+
           #:+max-texture-depth+
           #:+max-texture-dimension+
           #:+max-texture-size+
           #:+min-texture-dimension+
           #:+seconds-per-time-unit+
           #:+semantic-version-string+
           #:+tick-size+
           #:+ticks-per-second+
           #:+time-step+
           #:+time-steps-per-second+
           #:+version-epoch+
           #:+version-major+
           #:+version-minor+
           #:+version-patch+
           #:+version-string+))

(cl:in-package #:zombie-raptor/core/constants)

(defconstant +time-step+ 0.01d0
  "
The time step (in seconds) for everything that is not related to
rendering.")

(defconstant +time-steps-per-second+ 100.0d0
  "The number of time steps in a second.")

(defconstant +ticks-per-second+ 100
  "The number of integer ticks in a second.")

(defconstant +seconds-per-time-unit+ (double-float* (/ internal-time-units-per-second))
  "The number of time units in a second.")

(defconstant +tick-size+ 60)

(defconstant +version-epoch+ 0)
(defconstant +version-major+ 0)
(defconstant +version-minor+ 0)
(defconstant +version-patch+ 0)
(define-constant +version-string+
    (format nil
            "~D.~D.~D.~D"
            +version-epoch+
            +version-major+
            +version-minor+
            +version-patch+)
  :test 'string=
  :documentation "A string representing the version of the game engine.")
(define-constant +semantic-version-string+
    (format nil
            "~D.~D.~D"
            +version-major+
            +version-minor+
            +version-patch+)
  :test 'string=
  :documentation "A string representing the semantic version of the game engine.")

(defconstant +glsl-version+ 4.6)
(defconstant +gl-major-version+ 4)
(defconstant +gl-minor-version+ 6)
(define-constant +gl-version-string+
    (format nil
            "~D.~D"
            +gl-major-version+
            +gl-minor-version+)
  :test 'string=
  :documentation "A string representing the supported OpenGL version.")

(defconstant +max-entities+ 4096
  "This is the number of entities that can be created.")

(defconstant +max-entity-id+ 4096 ;; todo: (expt 2 16)
  "
This is the highest possible entity ID. There are far more entity IDs
than possible entities.
")

(defconstant +max-geometry+ 512)

(defconstant +max-texture-depth+ 255)
(defconstant +min-texture-dimension+ 1)
(defconstant +max-texture-dimension+ 3)
(defconstant +max-texture-size+ 4096)
