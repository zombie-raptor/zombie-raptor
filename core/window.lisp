(defpackage #:zombie-raptor/core/window
  (:documentation "Describes a 3D-accelerated application window.")
  (:use #:cl
        #:zombie-raptor/core/constants
        #:zr-utils)
  (:export #:%exit-function
           #:%seconds-per-frame
           #:active?
           #:application
           #:engine-version
           #:exit-function
           #:fps
           #:graphics-context
           #:graphics-data
           #:seconds-per-frame
           #:window
           #:with-window))

(in-package #:zombie-raptor/core/window)

(defgeneric fps (object)
  (:documentation "
Returns the measured Frames Per Second (FPS) of an object's renderer."))

(defclass window ()
  ((%graphics-context
    :accessor graphics-context
    :documentation "
A graphics context, which holds the state for a graphics API such as
OpenGL"))
  (:documentation "
A window obtained from a library that handles windows and input"))

(defclass application ()
  ((%name
    :reader name
    :initarg :name
    :initform :application
    :documentation "The internal name of the application.")
   (%active?
    :reader active?
    :accessor %active?
    :initform nil
    :documentation "Is the application currently running?")
   (%seconds-per-frame
    :reader seconds-per-frame
    :accessor %seconds-per-frame
    :initform nil
    :documentation "
Returns how many seconds, on average, that it takes to render a frame.
This is the more useful inverse of the more common frames-per-second
measurement.")
   (%window
    :reader window
    :accessor %window
    :initform nil
    :documentation "
A window obtained from a library that handles windows and input.")
   (%graphics-data
    :accessor graphics-data
    :initform nil
    :documentation "
Holds the graphics data that the game engine needs to persist.")
   (%engine-version
    :initform +version-string+
    :reader engine-version
    :documentation "
Returns the current EPOCH.MAJOR.MINOR.PATCH version string of the game
engine.")
   (%exit-function
    :reader exit-function
    :accessor %exit-function
    :initform nil
    :documentation "
If provided and not NIL, then this optional function is called when
the application exits."))
  (:documentation "A single-window, graphically-accelerated application"))

(defmethod print-object ((app application) stream)
  (print-unreadable-object (app stream :type t :identity t)
    (format stream
            "~A ~A"
            (name app)
            (window app)))
  app)

(defmethod fps ((app application))
  (with-accessors* (seconds-per-frame) app
    (if seconds-per-frame (/ seconds-per-frame) nil)))

;;; Note: Both window and graphics-data need cleanup methods defined!
(defmethod cleanup ((app application))
  (with-accessors* (%active? %exit-function %window graphics-data)
      app
    (when %exit-function
      (funcall %exit-function)
      (setf %exit-function nil))
    (when %window
      (cleanup %window)
      (setf %window nil))
    (when graphics-data
      (cleanup graphics-data)
      (setf graphics-data nil))
    (setf %active? nil)))

(defmacro with-initialized-window ((app graphics-data) &body body)
  "
Ensures that cleanup is run on all of the stored contents of an
app, which will free CFFI data and handle any other closing
issues.

Note: This should be run after the window slot of app is
initialized.
"
  `(unwind-protect
        (progn
          (setf (graphics-data ,app) ,graphics-data
                (%active? ,app) t)
          ,@body)
     (cleanup ,app)))

(defmacro with-window ((app &key standard-output window graphics-data with-thread-macro) &body body)
  "
Stores graphics-data and window. Runs everything in the macro provided
by with-thread-macro, using a *standard-output* rebound to
standard-output if it is provided.

Note: If graphics-data needs to be initialized in that thread, then
the constructor code to be run should be provided instead of an
already initialized window object.
"
  `(progn (setf (%window ,app) ,window)
          (,with-thread-macro (:standard-output ,standard-output)
            (with-initialized-window (,app ,graphics-data)
              ,@body))))
