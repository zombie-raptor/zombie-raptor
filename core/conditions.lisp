(defpackage #:zombie-raptor/core/conditions
  (:documentation "The common conditions (e.g. errors) used in the game engine.")
  (:use #:cl)
  (:export #:invalid-dimension-for-texture-error
           #:invalid-glsl-version
           #:invalid-texture-data-error
           #:model-vertex-count-error
           #:opengl-shader-error
           #:spir-v-type-error
           #:texture-dimension-error
           #:zombie-raptor-error))

(in-package #:zombie-raptor/core/conditions)

(define-condition zombie-raptor-error (error) ())

;;; TODO: This should be a "graphics shader" error to be API agnostic.
(define-condition opengl-shader-error (zombie-raptor-error)
  ((%action
    :initarg :action
    :reader error-in-action)
   (%message
    :initarg :message
    :reader error-message))
  ;; TODO: Handle the case of unbound arguments
  (:report (lambda (condition stream)
             (format stream
                     "Graphics shader error in ~A:~%~A"
                     (error-in-action condition)
                     (error-message condition)))))

;;; Note: SPIR-V can run in a compute kernel or on the CPU, so a
;;; SPIR-V error should not be considered a graphics shader error.
(define-condition spir-v-type-error (zombie-raptor-error)
  ((%binding
    :initarg :binding
    :reader error-binding)
   (%given-type
    :initarg :given-type
    :reader error-given-type)
   (%expected-type
    :initarg :expected-type
    :reader error-expected-type))
  (:report (lambda (condition stream)
             (format stream
                     "SPIR-V type error: ~A is of type ~A, but needs to be of type ~A"
                     (error-binding condition)
                     (error-given-type condition)
                     (error-expected-type condition)))))

(define-condition invalid-glsl-version (zombie-raptor-error)
  ((%invalid-version
    :initarg :invalid-version
    :reader invalid-version))
  (:report (lambda (condition stream)
             (let ((version-bound? (slot-boundp condition '%invalid-version)))
               (format stream "Invalid GLSL version")
               (when version-bound?
                 (let ((version (invalid-version condition)))
                   (format stream ": ~S~%" version)
                   (format stream
                           "I don't know how to turn this ~A into a valid GLSL version.~%"
                           (typecase version
                             (fixnum 'fixnum)
                             (integer 'integer)
                             (t (type-of version)))))))))
  (:documentation "
An error for when the GLSL version provided is not a valid GLSL
version.
"))

(define-condition model-vertex-count-error (zombie-raptor-error)
  ((%vertices-expected
    :initarg :vertices-expected
    :reader vertices-expected)
   (%vertices-given
    :initarg :vertices-given
    :reader vertices-given))
  (:report (lambda (condition stream)
             (let ((expected-vertices-bound? (slot-boundp condition '%vertices-expected))
                   (given-vertices-bound? (slot-boundp condition '%vertices-given)))
               (if (or expected-vertices-bound? given-vertices-bound?)
                   (progn
                     (when expected-vertices-bound?
                       (format stream
                               "~A vertices were expected"
                               (vertices-expected condition)))
                     (when given-vertices-bound?
                       (format stream
                               "~:[~;, but ~]~A vertices were given"
                               expected-vertices-bound?
                               (vertices-given condition))))
                   (format stream "Mismatch between expected and given vertices")))))
  (:documentation "
An error for when the model read by the engine does not have the
expected vertex count.
"))

(define-condition texture-dimension-error (zombie-raptor-error)
  ((%dimension
    :initarg :dimension
    :reader dimension))
  (:report (lambda (condition stream)
             (format stream
                     "Invalid texture dimension")
             (when (slot-boundp condition '%dimension)
               (format stream ": ~A" (dimension condition))))))

(define-condition invalid-dimension-for-texture-error (zombie-raptor-error)
  ((%texture-name
    :initarg :texture-name
    :reader texture-name)
   (%dimension
    :initarg :dimension
    :reader dimension)
   (%texture-problem
    :initarg :texture-problem
    :reader texture-problem)
   (%size
    :initarg :size
    :reader dimension-size))
  ;; TODO: Handle the case of unbound arguments
  (:report (lambda (condition stream)
             (format stream
                     "The texture ~S for ~S must be ~A, but is ~D"
                     (dimension condition)
                     (texture-name condition)
                     (texture-problem condition)
                     (dimension-size condition)))))

(define-condition invalid-texture-data-error (zombie-raptor-error)
  ((%texture-name
    :initarg :texture-name
    :reader texture-name)
   (%texture-length
    :initarg :texture-length
    :reader texture-length)
   (%expected-length
    :initarg :expected-length
    :reader expected-length))
  ;; TODO: Handle the case of unbound arguments
  (:report (lambda (condition stream)
             (format stream
                     "Texture data for ~S is of an invalid length ~D, when ~D was expected"
                     (texture-name condition)
                     (texture-length condition)
                     (expected-length condition)))))
