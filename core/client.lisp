(defpackage #:zombie-raptor/core/client
  (:documentation "Defines the game client and core game loop.")
  (:use #:cl
        #:zombie-raptor/core/constants
        #:zombie-raptor/core/input
        #:zombie-raptor/core/sdl2
        #:zombie-raptor/core/settings
        #:zombie-raptor/core/times
        #:zombie-raptor/core/types
        #:zombie-raptor/core/window
        #:zombie-raptor/data/model
        #:zombie-raptor/data/shader
        #:zombie-raptor/data/texture
        #:zombie-raptor/entity/entity
        #:zombie-raptor/entity/physics
        #:zombie-raptor/entity/render
        #:zombie-raptor/render/gl-data
        #:zr-utils)
  (:import-from #:bordeaux-threads
                #:lock
                #:make-lock
                #:with-lock-held)
  (:import-from #:cffi
                #:foreign-pointer)
  (:import-from #:zombie-raptor/math/matrix
                #:update-fov)
  (:export #:ecs
           #:ecs-labels
           #:fov
           #:hud-ecs
           #:game
           #:make-game
           #:script-state))

(in-package #:zombie-raptor/core/client)

(defgeneric sdl-window (object)
  (:documentation "Returns the SDL window that an object contains."))

(defclass game (application)
  ((%ecs
    :accessor ecs
    :documentation "An entity component system")
   (%hud-ecs
    :accessor hud-ecs
    :documentation "Another entity component system")
   (%ecs-labels
    :accessor ecs-labels
    :documentation "An object that names various entity IDs")
   (%settings
    :accessor settings
    :documentation "The game client and window settings.")
   (%script-state
    :accessor script-state
    :documentation "State used by the script function"))
  (:documentation "
A game window that stores all the state that needs to be stored while
also running a game loop in a background process"))

(defmethod sdl-window ((game game))
  "The SDL window attached to the game."
  (window (window game)))

(defmethod foreign-matrices ((game game))
  (foreign-matrices (graphics-data game)))

(defmethod fov ((game game))
  "The main horizontal field-of-view of the main camera in the game."
  (with-settings (fov) (settings game)
    fov))

(defmethod (setf fov) (new-value (game game))
  (check-type new-value single-float)
  (with-settings (fov width height) (settings game)
    (setf fov (update-fov (foreign-matrices game)
                          new-value
                          (float* (/ height width))))))

(defmethod meshes ((game game))
  (meshes (graphics-data game)))

(defun update-window-dimensions (game)
  (lambda (settings)
    (actual-window-dimensions! settings (sdl-window game))))

(define-function setup-ecs ((init-function function)
                            (graphics-data gl-data)
                            (settings settings))
  "Creates two ECSes, one for the 3D world and one for the 2D HUD."
  (with-settings (height width)
      settings
    (let ((ecs (make-entity-component-system))
          (hud-ecs (make-entity-component-system)))
      (multiple-value-bind (ecs-labels script-state exit-function)
          (funcall init-function
                   :ecs ecs
                   :hud-ecs hud-ecs
                   :mesh-keys (mesh-keys graphics-data)
                   :width width
                   :height height
                   :allow-other-keys t)
        (values ecs hud-ecs ecs-labels script-state exit-function)))))

(define-function (setup-ecs* :inline t) (game init-function settings)
  (with-accessors* (ecs graphics-data hud-ecs ecs-labels script-state %exit-function)
      game
    (setf (values ecs hud-ecs ecs-labels script-state %exit-function)
          (setup-ecs (if (symbolp init-function)
                         (fdefinition init-function)
                         init-function)
                     graphics-data
                     settings))))

(define-function (%make-game :check-type t)
    (game
     &key
     (settings        nil settings)
     (controls        nil controls)
     (shader-data     nil shaders)
     (textures        nil textures)
     (models          nil models)
     (init-function   nil (and (or symbol function) (not null)))
     (script-function nil (and (or symbol function) (not null)))
     (standard-output nil (maybe stream)))
  (with-window (game :standard-output standard-output
                     :window (make-instance 'sdl-window :settings settings)
                     :graphics-data (make-gl-data :shaders shader-data
                                                  :textures textures
                                                  :models models
                                                  :settings settings
                                                  :verify t
                                                  :update-window-dimensions (update-window-dimensions game))
                     :with-thread-macro with-sdl-thread)
    (setup-ecs* game init-function settings)
    (setf (settings game) settings)
    (with-sdl-event-pointer (event-pointer)
      (game-loop game
                 event-pointer
                 (import-controls controls settings)
                 (if (symbolp script-function)
                     (fdefinition script-function)
                     script-function)
                 (make-input-state)
                 (make-input-action-state :settings settings
                                          :ecs (ecs game)
                                          :hud-ecs (hud-ecs game)
                                          :mesh-keys (mesh-keys (graphics-data game)))
                 (with-settings (background)
                     settings
                   (or background
                       (make-array 3 :element-type 'single-float :initial-element 0f0))))))
  game)

(defmethod initialize-instance :after ((game game)
                                       &key
                                         settings
                                         controls
                                         shader-data
                                         textures
                                         models
                                         init-function
                                         script-function
                                         standard-output
                                       &allow-other-keys)
  (%make-game game
              :settings settings
              :controls controls
              :shader-data shader-data
              :textures textures
              :models models
              :init-function init-function
              :script-function script-function
              :standard-output standard-output))

(define-function (make-game :inline t) (&key (name :game keyword)
                                             settings
                                             controls
                                             shader-data
                                             textures
                                             models
                                             (init-function (constantly nil))
                                             (script-function (constantly nil))
                                             standard-output)
  "
Makes a game object and starts a game loop running in a background
thread.
"
  (make-instance 'game :name name
                       :settings settings
                       :controls controls
                       :shader-data shader-data
                       :textures textures
                       :models models
                       :init-function init-function
                       :script-function script-function
                       :standard-output standard-output))

;;; Note: It will take millions of years for tick to become a bignum,
;;; but the compiler doesn't know this. Let's help it know it's a
;;; fixnum by using incf-mod instead of incf. Running the game engine
;;; for millions of years without restarting is not supported.
(defmacro do-inner-game-loop ((missed-time elapsed-time tick) &body body)
  "
Handles the time and tick updating in the inner game loop, which is
run multiple times to keep the physics and other simulations
independent from the rendering.
"
  `(do ()
       ((< ,missed-time +time-step+))
     ,@body
     (incf-mod ,tick (expt 2 +tick-size+))
     (incf ,elapsed-time +time-step+)
     (decf ,missed-time +time-step+)))

(defmacro with-game-readers (readers game &body body)
  "
Provides read-only access to slots from the game object. Most macros
like this use `with-accessors' or `symbol-macrolet' and always call
the accessor. This uses `let' and only calls the accessor once. This
essentially caches things, making slots in an instance of a `defclass'
object usable in a hot loop, such as the game loop.
"
  `(let ,(mapcar (lambda (reader)
                   `(,reader (,reader ,game)))
          readers)
     ,@body))

(define-function store-old-states* ((ecs entity-component-system)
                                    (hud-ecs entity-component-system))
  "Stores the old ECS states for interpolation."
  (store-old-states ecs)
  (store-old-states hud-ecs))

(define-function physics* ((ecs entity-component-system)
                           (hud-ecs entity-component-system)
                           (elapsed-time single-float))
  "Runs the physics simulations on both ECSes."
  (let ((step (float* +time-step+)))
    (physics ecs elapsed-time step)
    (physics hud-ecs elapsed-time step)))

(define-function game-loop ((game game)
                            (event-pointer foreign-pointer)
                            (controls internal-controls)
                            (script-function function)
                            (input-state input-state)
                            (input-action-state input-action-state)
                            (background-color (simple-array single-float (3))))
  (declare (optimize (speed 3)))
  "
The game loop updates the timer, then reads the input, then calls the
script function, then updates the physics, then renders. The
render-foo functions prepare the renderer and then the draw-foo
functions actually tell OpenGL to draw. This is currently all done in
order, but it will be multithreaded eventually.

Each iteration of the loop increments the tick, which can then be used
by the scripting to time things. The input reading, physics updating,
and renderer do not care about the tick, but might act based on the
actual time elapsed.

The renderer's timing is based on the screen's refresh rate with vsync
or is unlimited... there probably should be a very high limit
eventually. Everything else is tied to the +time-step+. Each logic
time step should correspond to a tick. The renderer shouldn't know
about ticks.
"
  (with-times times (current-spf elapsed-time missed-time %missed-time-per-step)
    (with-game-readers (ecs hud-ecs ecs-labels script-state sdl-window foreign-matrices meshes)
        game
      (let ((mesh-ids (make-array +max-entities+ :element-type 'gl-id :initial-element 0))
            (hud-mesh-ids (make-array +max-entities+ :element-type 'gl-id :initial-element 0))
            (render-bits (make-array +max-entities+ :element-type 'bit :initial-element 0))
            (hud-render-bits (make-array +max-entities+ :element-type 'bit :initial-element 0))
            (changed-bits (make-array +max-entities+ :element-type 'bit :initial-element 0))
            (hud-changed-bits (make-array +max-entities+ :element-type 'bit :initial-element 0))
            (render-lock (make-lock))
            (input-lock (make-lock))
            (camera-id 0)
            (input? nil))
        (declare (type boolean input?)
                 (type lock render-lock input-lock))
        (do ((tick 0)
             (quit nil))
            ((with-lock-held (input-lock) quit))
          (declare (type tick tick))
          ;; The times are updated before everything else.
          (update-times! times)
          (setf (%seconds-per-frame game) (float* current-spf))
          (with-lock-held (input-lock)
            ;; SDL input handles the SDL events. It copies the
            ;; input-relevant information into the Lisp-native
            ;; input-state struct.
            (setf quit (sdl-input! input-state event-pointer)
                  input? t)
            (when input?
              ;; Calls the higher order functions created by the
              ;; controls with input based on the input-state struct.
              ;; All of the state required by the actions are stored
              ;; in input-action-state, including the ECSes.
              (input input-state
                     input-action-state
                     controls
                     sdl-window)
              (setf input? nil)))
          (let ((skipped? (< missed-time +time-step+)))
            ;; The inner game loop is used to keep the simulation
            ;; framerate independent from the rendering framerate. It
            ;; can run multiple times per rendering frame.
            (do-inner-game-loop (missed-time elapsed-time tick)
              (store-old-states* ecs hud-ecs)
              ;; The physics simulation runs before the script. This
              ;; includes the input processing and the realistic
              ;; physics.
              (physics* ecs hud-ecs (float* elapsed-time))
              ;; Calls the script function with all of the things that
              ;; it needs. It can act on either ECS and might depend
              ;; on the current time (as a tick). The script-state is
              ;; arbitrary and user-defined. The labels associate a
              ;; name with an ECS ID.
              (funcall script-function
                       :ecs ecs
                       :hud-ecs hud-ecs
                       :tick tick
                       :state script-state
                       :entity-labels ecs-labels
                       :allow-other-keys t))
            (with-lock-held (render-lock)
              ;; The render-foo functions update the matrices based on
              ;; the ECSes.
              (render-2D-hud hud-ecs
                             foreign-matrices
                             (if skipped? 1f0 %missed-time-per-step)
                             hud-mesh-ids
                             hud-render-bits
                             hud-changed-bits)
              (render-entities ecs
                               foreign-matrices
                               (if skipped? 1f0 %missed-time-per-step)
                               camera-id
                               mesh-ids
                               render-bits
                               changed-bits)
              ;; The draw functions use the matrices to call OpenGL
              ;; with the latest visual information.
              (with-sdl-rendering (sdl-window ((aref background-color 0)
                                               (aref background-color 1)
                                               (aref background-color 2)
                                               1f0))
                (draw-2D-hud hud-mesh-ids hud-render-bits meshes foreign-matrices)
                (draw-entities mesh-ids render-bits meshes camera-id foreign-matrices)))))))))
