(cl:defpackage #:zombie-raptor/core/input
  (:documentation "Handles the game input.")
  (:use #:cl
        #:zombie-raptor/core/sdl2
        #:zombie-raptor/core/settings
        #:zombie-raptor/data/game-data
        #:zombie-raptor/entity/entity
        #:zr-utils)
  (:import-from #:alexandria
                #:doplist
                #:with-gensyms)
  (:export #:controls
           #:define-input-action
           #:define-input-action-group
           #:define-input-action-range
           #:define-input-action-table
           #:define-key-bindings
           #:define-mouse-actions
           #:define-mouse-input-action
           #:import-controls
           #:input
           #:input-action-state
           #:internal-controls
           #:make-controls
           #:make-input-action-state))

(cl:in-package #:zombie-raptor/core/input)

;;;; Input actions

(define-array-and-type key-bindings-array (t `(,+sdl-scancode-size+) :initial-element nil))

(defstruct (input-action-state (:constructor %make-input-action-state))
  "Stores state needed for the input actions."
  (entity-id           0                              :type fixnum)
  (rotation-quantity   (* 0.0025f0 (float* pi))       :type single-float)
  (ecs                 (make-entity-component-system) :type entity-component-system)
  (hud-ecs             (make-entity-component-system) :type entity-component-system)
  (mesh-keys           (make-hash-table)              :type hash-table)
  (action-set          0                              :type uint4)
  (height/width        (float 9/16)                   :type single-float)
  (width               1280                           :type uint32)
  (height              720                            :type uint32)
  (mouse-sensitivity-x 0.002f0                        :type single-float)
  (mouse-sensitivity-y 0.002f0                        :type single-float)
  (mouse-rotation      t                              :type boolean)
  (toggle-mouse        nil                            :type boolean)
  (warp-mouse          nil                            :type boolean)
  (quit                nil                            :type boolean))

(define-function (make-input-action-state :inline t) (&key ecs hud-ecs mesh-keys settings)
  (with-settings (height
                  invert-x
                  invert-y
                  mouse-sensitivity-x
                  mouse-sensitivity-y
                  width)
      settings
    (%make-input-action-state :ecs ecs
                              :hud-ecs hud-ecs
                              :mesh-keys mesh-keys
                              :height/width (/ (float* height)
                                               (float* width))
                              :width width
                              :height height
                              :mouse-sensitivity-x (if invert-x
                                                       mouse-sensitivity-x
                                                       (- mouse-sensitivity-x))
                              :mouse-sensitivity-y (if invert-y
                                                       mouse-sensitivity-y
                                                       (- mouse-sensitivity-y)))))

(define-accessor-macro with-input-action-state #:input-action-state-)

(defmacro define-input-action (name-and-options hold slots &body body)
  "
Defines an input-action function that acts on symbol macro accessors
for the given slots from the input-action-state struct. The variable
hold is the return value and should be either t or nil. This describes
whether the key is expected to be held or not.
"
  (with-gensyms (input-action-state)
    `(define-function ,name-and-options ((,input-action-state input-action-state))
       ,@(if slots
             `((with-input-action-state ,slots
                   ,input-action-state
                 ,@body
                 ,hold))
             `(,@body
               ,hold)))))

(defmacro define-input-action-range (name (index-name start stop) hold slots &body body)
  "
Defines input-action functions just like in `define-input-action'. The
difference is that it adds suffixed integers to the end of each name
and generates one input-action for each version from start to
stop. This integer can be accessed internally with the variable
index-name.
"
  `(progn ,@(loop :for i :from start :to stop
                  :collect `(define-input-action ,(suffix-symbol name (format nil "-~A" i))
                                ,hold
                                ,slots
                              (let ((,index-name ,i))
                                ,@body)))))

(defmacro define-input-action-group ((replacement-variable) hold slots unique-parts &body body)
  "
Defines a group of input-action functions like in
`define-input-action'. The difference is that multiple names are
provided, one for each input-action, as the first item in the list in
unique-parts. The second element of the list substitutes
replacement-variable with that substitution.
"
  `(progn ,@(mapcar (destructuring-lambda (name substitution)
                      `(define-input-action ,name ,hold ,slots
                         (symbol-macrolet ((,replacement-variable ,substitution))
                           ,@body)))
                    unique-parts)))

(defmacro define-input-action-table (name &body function-symbols)
  "
Takes in a list of all of the valid input-action functions as
function-symbols and stores them in a function with the provided
name. This is so the engine knows which functions should be treated as
valid input-actions.
"
  (labels ((plist-from-symbol (symbol)
             (check-type symbol (and symbol (not keyword)))
             (list (symbol-to-keyword symbol) `',symbol))
           (plist-from-symbols (symbol-list)
             (mapcan #'plist-from-symbol symbol-list)))
    `(defun ,name ()
       (hash ,@(plist-from-symbols function-symbols)))))

(defmacro define-mouse-input-action (name-and-options distance slots &body body)
  "Defines a mouse-input-action that handles mouse drag input."
  (with-gensyms (input-action-state)
    `(define-function ,name-and-options ((,input-action-state input-action-state)
                                         (,distance fixnum))
       (declare (ignorable ,input-action-state ,distance))
       ,@(if slots
             `((with-input-action-state ,slots
                   ,input-action-state
                 ,@body
                 nil))
             `(,@body
               nil)))))

(defmacro define-mouse-actions (name &body function-symbols)
  `(defun ,name ()
     (vector ,@(mapcar (lambda (x) `',x) function-symbols))))

(define-input-action (%toggle-mouse! :inline t) nil (mouse-rotation toggle-mouse warp-mouse)
  (when toggle-mouse
    (if mouse-rotation
        (progn (set-relative-mouse-mode 0)
               (setf warp-mouse t))
        (set-relative-mouse-mode 1))
    (setf mouse-rotation (not mouse-rotation)
          toggle-mouse (not toggle-mouse))))

;;;; Key bindings

(defmacro define-key-bindings (name bindings-plists)
  "
Takes in a list of plists that associates a given scancode with an
input-action keyword.
"
  `(defun ,name ()
     ',bindings-plists))

(define-function setup-key-bindings ((key-bindings-list list)
                                     (input-action-table hash-table))
  "
Takes a plist indexed by scancode keywords and converts it into an
array indexed by scancode integer values.
"
  (let ((controls (make-key-bindings-array)))
    (doplist (key val key-bindings-list controls)
      (let ((key-action (gethash val input-action-table))
            (index (scancode-key-to-value key)))
        ;; TODO: replace with custom-defined conditions
        (error-unless key-action "The key action ~S doesn't exist!" val)
        (error-when (aref controls index) "The key ~S was already bound!" key)
        (setf (aref controls index) key-action)))))

(define-function setup-key-bindings* ((settings settings) key-bindings key-actions)
  "
Turns the definition of the key bindings into the internal format that
the input part of the game loop uses.
"
  (with-settings (app-name org-name)
      settings
    (create-key-bindings-in-directory key-bindings app-name org-name)
    ;; (find-key-bindings app-name org-name)
    (let ((bindings-table (make-hash-table)))
      (do-destructuring-bind ((s-exp-type bindings-name &rest bindings-plist) key-bindings)
        ;; TODO: replace with custom-defined condition
        (error-unless (eql s-exp-type :key-bindings)
                      "Syntax error in parsing key bindings. ~S was expected, but ~S was received!"
                      :key-bindings
                      s-exp-type)
        (check-type bindings-name keyword)
        (insert-unique-hash-table-value bindings-table
                                        bindings-name
                                        (setup-key-bindings bindings-plist key-actions)
                                        "Duplicate key bindings group names are not allowed, but ~S is a duplicate."))
      bindings-table)))

;;;; Controls

(defstruct controls
  "
Stores the three kinds of controls that the game engine uses to
generate its input handling.
"
  (key-actions
   (error 'required-input-error
          :missing-input-name "key-actions"
          :printable-class-name "controls")
   :type hash-table)
  (key-bindings
   (error 'required-input-error
          :missing-input-name "key-bindings"
          :printable-class-name "controls")
   :type list)
  (mouse-actions
   (error 'required-input-error
          :missing-input-name "mouse-actions"
          :printable-class-name "controls")
   :type simple-vector))

(define-accessor-macro with-controls-accessors #:controls-)

(defstruct internal-controls
  (active-key-bindings
   (error 'required-input-error
          :missing-input-name "active-key-bindings"
          :printable-class-name "internal-controls")
   :type key-bindings-array)
  (all-key-bindings
   (error 'required-input-error
          :missing-input-name "all-key-bindings"
          :printable-class-name "internal-controls")
   :type hash-table)
  (mouse-actions
   (error 'required-input-error
          :missing-input-name "mouse-actions"
          :printable-class-name "internal-controls")
   :type simple-vector))

(define-accessor-macro with-internal-controls #:internal-controls-)

(define-function import-controls ((controls controls) (settings settings))
  "
Turns the definitions of the controls into the internal format that
the input part of the game loop uses. This handles both key bindings
and mouse actions.
"
  (with-controls-accessors (key-bindings key-actions mouse-actions)
      controls
    (let ((all-key-bindings (setup-key-bindings* settings key-bindings key-actions)))
      (make-internal-controls :active-key-bindings (gethash :debug all-key-bindings)
                              :all-key-bindings all-key-bindings
                              :mouse-actions mouse-actions))))

;;;; Input

(define-function update-sdl-window! ((input-action-state input-action-state)
                                     sdl-window)
  "
Any action that the controls can do that modifies the SDL window state
goes here. Right now, that's just mouse warping.
"
  (with-input-action-state (warp-mouse width height)
      input-action-state
    (when warp-mouse
      (sdl-warp-mouse sdl-window (round width 2) (round height 2))
      (setf warp-mouse nil))))

(define-function run-controls! ((input-state input-state)
                                (input-action-state input-action-state)
                                (controls internal-controls))
  "
The input-state says which controls are active, e.g. keys that are
pressed. The controls are defined for the mouse and the keyboard. The
controls are functions that are called that do actions on the
input-action-state.
"
  (with-input-action-state (action-set mouse-rotation)
      input-action-state
    (with-input-state (keys-active key-presses mouse-motion)
        input-state
      (with-internal-controls ((key-bindings active-key-bindings) mouse-actions)
          controls
        ;; First, the key presses.
        (dotimes (i +sdl-scancode-size+)
          (when (and (= (bit key-presses i) 1)
                     (= (bit keys-active i) 1)
                     (aref key-bindings i))
            (unless (funcall (aref key-bindings i) input-action-state)
              (setf (bit keys-active i) 0))))
        ;; Next, the mouse rotation.
        (when mouse-rotation
          (let ((x-rel (aref mouse-motion 0))
                (y-rel (aref mouse-motion 1)))
            (unless (zerop x-rel)
              (funcall (elt mouse-actions (* 2 action-set)) input-action-state x-rel))
            (unless (zerop y-rel)
              (funcall (elt mouse-actions (1+ (* 2 action-set))) input-action-state y-rel))))))))

(define-function input ((input-state input-state)
                        (input-action-state input-action-state)
                        (controls internal-controls)
                        sdl-window)
  "
Handles the input (key and mouse) state by calling the functions that
are created by the key bindings. For instance, examples/controls.lisp
sets the examples' controls.

The interaction with the ECS is done indirectly through these higher
order functions.

The input-state struct is the input, copied over from SDL, that tells
this function what to do. The input-action-state struct contains all
of the information that the input higher order functions might need to
read from and write to, including the ECSes themselves. The controls
are the data structures that contain the higher order functions, e.g.
which function to call when a key is pressed. The sdl-window object is
needed for a few of the input actions.

At the end, the input-state is cleared for the next iteration and any
request to quit the app is processed.
"
  (run-controls! input-state input-action-state controls)
  (%toggle-mouse! input-action-state)
  (update-sdl-window! input-action-state sdl-window)
  (reset-input-state! input-state)
  (with-input-action-state (quit) input-action-state
    (when quit
      (request-quit))))
