;;;; Controls

;;;; This file defines the controls for the example programs. This is
;;;; the high-level implementation of the controls, where action
;;;; functions are called based on input events detected by the SDL
;;;; library.
;;;;
;;;; This file is currently divided into two main parts. The first
;;;; part is the key bindings, associating scancode key presses with
;;;; the key actions. The second part is the input actions, which
;;;; define corresponding functions for each key-bindable action as
;;;; well as the messier mouse actions that need to be improved.

(defpackage #:zombie-raptor/examples/controls
  (:documentation "Controls for the example programs.")
  (:use #:cl
        #:zombie-raptor/core/input
        #:zombie-raptor/core/types
        #:zombie-raptor/entity/entity
        #:zombie-raptor/entity/entity-action
        #:zombie-raptor/examples/scripting)
  (:export #:key-actions
           #:key-bindings
           #:mouse-actions))

(in-package #:zombie-raptor/examples/controls)

;;;; Key bindings (configuration plists)

;;; Key bindings are lists of plists. The first two elements of each
;;; plist are the name of the plist. The rest are pairs of keywords,
;;; where the key is an SDL scancode and the value is the name of an
;;; input-action that is defined below.
(define-key-bindings key-bindings
    ((:key-bindings :debug
      :scancode-escape :quit
      ;; :scancode-1 :use-entity-1
      ;; :scancode-2 :use-entity-2
      ;; :scancode-3 :use-entity-3
      ;; :scancode-4 :use-entity-4
      ;; :scancode-5 :use-entity-5
      ;; :scancode-c :use-entity-0
      :scancode-w :move-forwards
      :scancode-s :move-backwards
      :scancode-a :move-strafe-left
      :scancode-d :move-strafe-right
      :scancode-g :toggle-gravity
      :scancode-space :move-y+
      :scancode-lctrl :move-y-
      ;; :scancode-q :yaw-rotate+
      ;; :scancode-e :yaw-rotate-
      ;; :scancode-r :pitch-rotate+
      ;; :scancode-f :pitch-rotate-
      ;; :scancode-z :roll-rotate+
      ;; :scancode-x :roll-rotate-
      :scancode-m :toggle-mouse)
     (:key-bindings :fps-motion
      :scancode-escape :quit
      :scancode-w :move-forwards
      :scancode-s :move-backwards
      :scancode-a :move-strafe-left
      :scancode-d :move-strafe-right
      :scancode-space :move-y+
      ;; :scancode-e :interact
      ;; :scancode-q :use-last-item
      ;; :scancode-r :reload
      ;; :scancode-1 :use-item-1
      ;; :scancode-2 :use-item-2
      ;; :scancode-3 :use-item-3
      ;; :scancode-4 :use-item-4
      ;; :scancode-5 :use-item-5
      ;; :scancode-lshift :toggle-sprint
      ;; :scancode-lctrl :toggle-crouch
      ;; :scancode-tab :match-info
      )
     (:key-bindings :overhead
      :scancode-escape :quit
      :scancode-w :move-forwards
      :scancode-s :move-backwards
      :scancode-a :move-strafe-left
      :scancode-d :move-strafe-right
      :scancode-m :toggle-mouse)
     ;; (:key-bindings :menu
     ;;  :scancode-escape :back
     ;;  :scancode-w :menu-up
     ;;  :scancode-s :menu-down
     ;;  :scancode-a :menu-back
     ;;  :scancode-d :menu-okay
     ;;  :scancode-return :menu-okay
     ;;  :scancode-backspace :menu-back)
     ))

;;;; Input action functions

;;;; The following are functions that are either defined by
;;;; `defined-input-action' or by related macros that create multiple
;;;; functions via `define-input-action'. These functions all operate
;;;; on the input-action-state struct and are called when a key press
;;;; is recognized. The struct accessors are accessed by symbol
;;;; macros, similar to the ones produced by `with-accessors'.
;;;;
;;;; The symbols referring to the functions are all placed into a
;;;; hash-table with `define-input-action-table'. This table is used
;;;; to create the actual responses to the key presses.
;;;;
;;;; With the exception of `key-actions' and `mouse-actions', all of
;;;; these functions can be recompiled while the program is running!
;;;;
;;;; `define-input-action-group' looks a lot like `let', but it's
;;;; actually creating multiple new functions at the top level with
;;;; the names, not local bindings!

(define-input-action quit t (quit)
  (setf quit t))

(define-input-action-range use-entity (i 0 5) nil (entity-id)
  (setf entity-id i))

(define-input-action-group (direction) t (ecs entity-id)
    ((move-strafe-left  :x-)
     (move-strafe-right :x+)
     (move-backwards    :z+)
     (move-forwards     :z-))
  (move-entity ecs entity-id direction))

(define-input-action-group (axis) t (ecs entity-id rotation-quantity)
    ((yaw-rotate- :yaw)
     (pitch-rotate- :pitch)
     (roll-rotate- :roll))
  (rotate-entity ecs entity-id axis (- rotation-quantity)))

(define-input-action-group (axis) t (ecs entity-id rotation-quantity)
    ((yaw-rotate+ :yaw)
     (pitch-rotate+ :pitch)
     (roll-rotate+ :roll))
  (rotate-entity ecs entity-id axis rotation-quantity))

(define-input-action move-y+ t (ecs entity-id)
  (entity-jump ecs entity-id))

(define-input-action move-y- t (ecs entity-id)
  (entity-crouch ecs entity-id))

(define-input-action toggle-mouse nil (toggle-mouse)
  (setf toggle-mouse t))

(define-input-action toggle-gravity nil (ecs)
  (with-selection ecs
      (id :id 0)
      ((velocity (velocity.y velocity :elt 1))
       (physics (allow-falling? allow-falling?)
                (acceleration.y acceleration :elt 1)
                (gravity? gravity?)))
    (setf allow-falling? (if (= 1 allow-falling?) 0 1))
    (when (zerop allow-falling?)
      (psetf gravity? 0
             velocity.y 0f0
             acceleration.y 0f0))))

;;; This creates the function key-actions, which is really just a
;;; hash-table of the symbols referring to functions that are created
;;; above.
(define-input-action-table key-actions
  quit
  use-entity-0
  use-entity-1
  use-entity-2
  use-entity-3
  use-entity-4
  use-entity-5
  toggle-gravity
  move-strafe-left
  move-strafe-right
  move-backwards
  move-forwards
  move-y-
  move-y+
  yaw-rotate-
  yaw-rotate+
  pitch-rotate-
  pitch-rotate+
  roll-rotate-
  roll-rotate+
  toggle-mouse)

(define-mouse-input-action mouse-drag-action-x distance (ecs entity-id mouse-sensitivity-x)
  (rotate-entity ecs entity-id :yaw (* distance mouse-sensitivity-x)))

(define-mouse-input-action mouse-drag-action-y distance (ecs entity-id height/width mouse-sensitivity-y)
  (rotate-entity-camera-pitch ecs entity-id (* distance height/width mouse-sensitivity-y)))

(define-mouse-actions mouse-actions
  mouse-drag-action-x
  mouse-drag-action-y
  mouse-drag-action-x
  mouse-drag-action-y)
