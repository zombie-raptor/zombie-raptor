(defpackage #:zombie-raptor/examples/example
  (:documentation "The core of the example programs.")
  (:use #:cl
        #:zombie-raptor/core/client
        #:zombie-raptor/core/input
        #:zombie-raptor/core/settings
        #:zombie-raptor/data/model
        #:zombie-raptor/data/terrain
        #:zombie-raptor/data/texture
        #:zombie-raptor/entity/entity
        #:zombie-raptor/entity/entity-action
        #:zombie-raptor/examples/controls
        #:zombie-raptor/examples/scripting
        #:zombie-raptor/examples/shaders
        #:zombie-raptor/examples/textures
        #:zombie-raptor/examples/world
        #:zombie-raptor/math/vector
        #:zr-utils)
  (:export #:launch-example-window
           #:launch-terrain-test
           #:launch-2D-hud-test
           #:make-example-game))

(in-package #:zombie-raptor/examples/example)

(define-function (make-default-controls :inline t) ()
  (make-controls :key-actions (key-actions)
                 :key-bindings (key-bindings)
                 :mouse-actions (mouse-actions)))

(define-function (default-background-color
                  :inline t
                  :return vec3)
    ()
  (vec3 0.483f0 0.701f0 0.836f0))

(define-function (2D-background-color
                  :inline t
                  :return vec3)
    ()
  (vec3 0.1f0 0.1f0 0.1f0))

(declaim (inline make-example-game))
(defun make-example-game (&key
                            (name :example)
                            (width 1280)
                            (height 720)
                            (fov nil)
                            fullscreen
                            (title "Zombie Raptor")
                            (models (make-models (list (square :red))))
                            (init-function #'make-simple-script-entities)
                            (script-function #'simple-script)
                            (texture nil)
                            (textures (make-textures (list (generate-square-texture texture))))
                            debug
                            standard-output
                            background-color)
  "Creates an example game with reasonable defaults."
  (let ((settings (make-settings :title title
                                 :width width
                                 :height height
                                 :fov (or fov 100f0)
                                 :fullscreen fullscreen
                                 :app-name "example"
                                 :org-name "zombie-raptor"
                                 :msaa 4
                                 :debug debug
                                 :background (or background-color
                                                 (default-background-color)))))
    (make-game :name name
               :settings settings
               :shader-data (shader-data)
               :textures textures
               :models models
               :controls (make-default-controls)
               :init-function init-function
               :script-function script-function
               :standard-output standard-output)))

(deftype color ()
  "Valid names for colors."
  `(member :red :green :blue :white :black :gray :orange :cyan))

(define-function (split-color-integer :inline t
                                      :return (octet octet octet))
    ((integer (unsigned-byte 24)))
  (declare (optimize (speed 3)))
  (values (ldb (byte 8 16) integer)
          (ldb (byte 8 8) integer)
          (ldb (byte 8 0) integer)))

;;; Note: This is a borderline function to inline because it creates
;;; three floats, but it's very common to have a literal color like
;;; #xFF9900. When that happens, this will become three constant
;;; single-floats. Having this inline function is better than a
;;; compiler macro because the real compiler can spot more occurrences
;;; than the compiler macro can. The solution is thus to have two
;;; versions, one inline and one not.
(define-function (%color-integer-to-floats :inline t
                                          :return (single-float single-float single-float))
    ((integer (unsigned-byte 24)))
  (declare (optimize (speed 3)))
  (multiple-value-bind (r g b)
      (split-color-integer integer)
    (values (* r #.(/ 255f0))
            (* g #.(/ 255f0))
            (* b #.(/ 255f0)))))

(define-function (color-integer-to-floats :return (single-float single-float single-float))
    ((integer (unsigned-byte 24)))
  (%color-integer-to-floats integer))

;;; Note: This probably should be replaced by a file of constants that
;;; implements all of https://en.wikipedia.org/wiki/Web_colors with
;;; the possibility of alternate files for X11 colors, IRC colors,
;;; etc.
(define-function (color-to-color-values :inline t
                                        :return (single-float single-float single-float))
    ((color color))
  (declare (optimize (speed 3)))
  "Converts from a valid color name to its RGB values."
  (ecase color
    (:red    (%color-integer-to-floats #xFF0000))
    (:green  (%color-integer-to-floats #x008000))
    (:blue   (%color-integer-to-floats #x0000FF))
    (:white  (%color-integer-to-floats #xFFFFFF))
    (:black  (%color-integer-to-floats #x000000))
    (:gray   (%color-integer-to-floats #x808080))
    (:orange (%color-integer-to-floats #xFFA500))
    (:cyan   (%color-integer-to-floats #x00FFFF))))

;;; If provided, literal colors should just be the three single float
;;; values instead of inlining the function. Compilers aren't that
;;; smart yet.
(define-compiler-macro color-to-color-values (&whole form arg)
  (if (atom arg)
      (case arg
        (:red    `(%color-integer-to-floats #xFF0000))
        (:green  `(%color-integer-to-floats #x008000))
        (:blue   `(%color-integer-to-floats #x0000FF))
        (:white  `(%color-integer-to-floats #xFFFFFF))
        (:black  `(%color-integer-to-floats #x000000))
        (:gray   `(%color-integer-to-floats #x808080))
        (:orange `(%color-integer-to-floats #xFFA500))
        (:cyan   `(%color-integer-to-floats #x00FFFF))
        (t form))
      form))

(define-function square ((color color))
  (multiple-value-bind (r g b) (color-to-color-values color)
    (make-square :name :square
                 :color-r r
                 :color-g g
                 :color-b b
                 :program :hud
                 :texture :square)))

;;;; The main test game

(defun generate-models (world)
  (make-models (list* (square :red)
                      (make-cube 1f0
                                 :name :cube
                                 :program :default
                                 :texture :square)
                      (make-cuboid 0.65f0 1.85f0 0.65f0
                                   :name :tall-box
                                   :program :default
                                   :texture :square)
                      (make-cuboid 1f0 1f0 2f0
                                   :name :long-box
                                   :program :default
                                   :texture :square)
                      ;; (make-cuboid 3f0 0.25f0 3f0
                      ;;              :name :elevator-platform
                      ;;              :program :default
                      ;;              :texture :square)
                      world)))

(defun launch-example-window (&key (width 1280) (height 720) fov fullscreen debug standard-output texture)
  "
Launches a window that runs a simple game built on the Zombie Raptor
engine.
"
  (multiple-value-bind (world-models world-coords world)
      (make-level :name :world
                  :program :default
                  :texture :square)
    (make-example-game :width width
                       :height height
                       :fov fov
                       :fullscreen fullscreen
                       :title "Zombie Raptor Example Program"
                       :models (generate-models world-models)
                       :init-function (make-test-entities world-coords world)
                       :script-function #'script
                       :debug debug
                       :texture texture
                       :standard-output standard-output)))

;;;; Terrain

(define-function (make-random-state-with-seed :inline t) (&optional seed)
  "
Makes a random state with the given seed, if a seed is provided.

Note: The seed is implementation-specific and is currently only
supported under SBCL.
"
  #-sbcl
  (when seed
    (warn "Ignoring the random seed."))

  #+sbcl
  (if seed
      (sb-ext:seed-random-state seed)
      (make-random-state t))

  #-sbcl
  (make-random-state t))

(define-function make-generated-terrain (&optional (seed nil (maybe (unsigned-byte 32))))
  "
Randomly generates terrain using a simple algorithm: A new tile is
very likely to be similar to one of its neighbors that has already
been generated.
"
  (let* ((terrain-types 4)
         (border-weight 15)
         (row-size (isqrt +chunk-size+))
         (terrain (make-array +chunk-size+
                              :element-type 'octet
                              :initial-element 0))
         (random-state (make-random-state-with-seed seed)))
    (flet ((random-terrain ()
             (random terrain-types random-state)))
      (dotimes (y row-size)
        (dotimes (x row-size)
          (setf (aref terrain (flat-index 1 row-size x y))
                (cond ((and (zerop x) (zerop y))
                       (round (+ (random-terrain)
                                 (random-terrain))
                              2))
                      ((zerop (random border-weight random-state))
                       (random-terrain))
                      ((zerop x)
                       (aref terrain (flat-index 1 row-size x (1- y))))
                      ((zerop y)
                       (aref terrain (flat-index 1 row-size (1- x) y)))
                      (t
                       (ecase= (random 5 random-state)
                         (0 (aref terrain (flat-index 1 row-size (1- x) (1- y))))
                         ((1 2) (aref terrain (flat-index 1 row-size (1- x) y)))
                         ((3 4) (aref terrain (flat-index 1 row-size x (1- y)))))))))))
    (make-terrain terrain
                  :model-name :world
                  :program :terrain
                  :texture :terrain)))

(defun generate-terrain-models (&optional seed)
  "Generates the models used in the terrain test."
  (let* ((terrain (make-generated-terrain seed))
         (terrain-model (list (terrain-model terrain)))
         (terrain-map (terrain-map-texture terrain)))
    (values (make-models (list* (square :white)
                                (make-cuboid 1f0 1f0 2f0
                                             :name :long-box
                                             :program :default
                                             :texture :square)
                                terrain-model))
            terrain-map)))

#+(or)
(defun launch-terrain-test (&key (width 1280) (height 720) fullscreen debug standard-output seed)
  "
Launches a window that demonstrates the terrain capabilities of the
engine.
"
  (multiple-value-bind (models terrain-map)
      (generate-terrain-models seed)
    (make-example-game :width width
                       :height height
                       :fullscreen fullscreen
                       :title "Zombie Raptor Terrain Test"
                       :models models
                       :init-function #'make-terrain-test-entities
                       :textures (make-textures (list (generate-square-texture)
                                                      (generate-terrain-textures)
                                                      terrain-map))
                       :debug debug
                       :standard-output standard-output)))

;;;; A 2D example

(defun generate-2D-models ()
  (make-models (list (square :red)
                     (multiple-value-bind (r g b) (color-to-color-values :orange)
                       (make-hexagon :name :hexagon
                                     :color-r r
                                     :color-g g
                                     :color-b b
                                     :program :hud
                                     :texture :square))
                     (multiple-value-bind (r g b) (color-to-color-values :cyan)
                       (make-equilateral-triangle :name :triangle
                                                  :color-r r
                                                  :color-b g
                                                  :color-g b
                                                  :program :hud
                                                  :texture :square)))))

(defun launch-2D-hud-test (&key (width 1280) (height 720) fullscreen debug standard-output)
  "Launches a completely 2D environment to demonstrate a fancier HUD."
  (make-example-game :width width
                     :height height
                     :fullscreen fullscreen
                     :title "Zombie Raptor 2D HUD Test"
                     :models (generate-2D-models)
                     :init-function #'make-2D-hud-test-entities
                     :debug debug
                     :standard-output standard-output
                     :background-color (2D-background-color)))
