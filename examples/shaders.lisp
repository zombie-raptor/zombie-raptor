(defpackage #:zombie-raptor/examples/shaders
  (:documentation "Example uses of the define-shader macro.")
  (:import-from #:zombie-raptor/data/shader)
  (:import-from #:zombie-raptor/zrvl)
  (:use #:zrvl)
  (:export #:shader-data))

(in-package #:zombie-raptor/examples/shaders)

;;;; Note: The `zrvl:define-shader' macro is incredibly complicated,
;;;; but is primarily achieved by expanding macros or applying
;;;; functions. This means that there is very little magic going on
;;;; because IDEs like SLIME can expose the API of VEC3 because it is
;;;; simply a call to `zrvl:vec3'. It also means that you can debug
;;;; macros with `cl:macroexpand-1' and see a full call stack.
;;;; Alternatively, you can also set `cl:*break-on-signals*' to true
;;;; to debug these macros.
;;;;
;;;; The cost of this power is that the most convenient way to write
;;;; shaders is to (:use #:zrvl) in the shader packages and then
;;;; `cl:in-package' into the new package and have its functions like
;;;; `zrvl:+' replace CL functions like `cl:+' while in the part of
;;;; the file that is writing the shader. ZRVL still uses
;;;; `cl:defpackage' and `cl:in-package' so you can always leave
;;;; mid-file without having to prefix those symbols.

;;;; For the most part, ZRVL attempts to be a subset of Common Lisp
;;;; using GPU types. There are some differences.
;;;;
;;;; First, static types are inferred instead of using dynamic typing
;;;; because there is no runtime environment on the GPU. This means
;;;; you cannot set a variable that is using a different type that was
;;;; inferred from the previous value. In practice, that means that
;;;; setting variables instead of defining new bindings is even more
;;;; discouraged than normal.
;;;;
;;;; Second, shader global variables do something fairly unique here.
;;;; Unlike Common Lisp's globals, these are true global variables
;;;; rather than variables dynamic scope. ZRVL chooses to expose these
;;;; globals at the entry point to the shader, as if they are function
;;;; arguments. However, these globals are still implicitly available
;;;; to any external functions that the shader calls, unlike other
;;;; shader languages that choose to make the globals into variables
;;;; to the "main" function. In practice, that makes them almost like
;;;; dynamic scope special variables in CL, but you cannot rebind them
;;;; dynamically, only lexically, which limits any redefinition to a
;;;; local scope.
;;;;
;;;; Third, this setup necessitates an implicit local LET*, similar to
;;;; what Scheme does. Here, this is using the name `zrvl:define'.
;;;; This creates a variable scope that lets you use those bindings
;;;; when setting the output variables. Without this, those bindings
;;;; would be contained within the LET or LET* and end in the body of
;;;; the shader, before the output variables are set. This is because
;;;; `zrvl:define-shader' uses a DO-style structure where the output
;;;; variables can be set next to their definitions rather than
;;;; needing the explicit use of `zrvl:setq'. This is also why output
;;;; variables are in their own list, after the other globals: any
;;;; code run next to them assumes that the body below, if it exists,
;;;; has already run.

;;;; 2D shaders

;;;; First, here are some simple shaders suitable for a 2D HUD
;;;; overlay. Notice how they have no body at all, just outputs
;;;; calculated from inputs. The outputs of the vertex shader are the
;;;; inputs of the fragment shader.

;;; The vertex shader is the first stage and the input from the game
;;; engine itself. It operates on the simple geometric vertices. It's
;;; a simple matter of taking inputs and transforming them into
;;; outputs.
;;;
;;; Note: At the moment, this shader takes in 3D vectors (instead of
;;; 2D vectors) and takes in an unused normal vector because all
;;; shaders use the same vertex shader interface.
(define-shader (simple-2D-vert :vertex-shader)
               ((:in position vec3 :location 0)
                (:in normal vec3 :location 1)
                (:in color vec3 :location 2)
                (:in texcoord vec2 :location 3)
                (:uniform view-matrix mat4 :location 1)
                (:uniform model-matrix mat4 :location 2)
                (:uniform projection-matrix mat4 :location 3))
               ((:out frag-position vec3 :location 0 := (vec3 (* model-matrix (vec4 position 1f0))))
                (:out frag-color vec3 :location 1 := color)
                (:out frag-texcoord vec2 :location 2 := texcoord)
                (:out gl-position vec4 := (* projection-matrix
                                             view-matrix
                                             model-matrix
                                             (vec4 position 1f0)))))

;;; The fragment, or pixel, shader is the final stage and takes
;;; fragment data from the vertex shader and turns them into the
;;; actual output pixels. Texturing usually happens here. Again, it
;;; takes inputs and turns them into outputs.
(define-shader (simple-2D-frag :fragment-shader)
               ((:in frag-position vec3 :location 0)
                (:in frag-color vec3 :location 1)
                (:in frag-texcoord vec2 :location 2)
                (:uniform default-texture sampler-2D :location 0))
               ((:out out-color vec4 :location 0 := (vec4 (.* (vec3 (texture default-texture frag-texcoord))
                                                              frag-color)
                                                          1f0))))

;;;; 3D shaders, with lighting

;;;; Now, here's some simple 3D Phong shading. The vertex shader still
;;;; doesn't need a body, but the fragment shader does intermediate
;;;; calculations with the `define' form, which creates the variables
;;;; expected by the fragment shader's out-color at the end.
;;;;
;;;; Some alternate colors are commented out if you want to try
;;;; different colors with recompilation.

(define-shader (simple-vert :vertex-shader)
               ((:in position vec3 :location 0)
                (:in normal vec3 :location 1)
                (:in color vec3 :location 2)
                (:in texcoord vec2 :location 3)
                (:uniform view-matrix mat4 :location 1)
                (:uniform model-matrix mat4 :location 2)
                (:uniform projection-matrix mat4 :location 3)
                (:uniform normal-matrix mat4 :location 4))
               ((:out frag-position vec3 :location 0 := (vec3 (* model-matrix
                                                                 (vec4 position 1f0))))
                (:out frag-normal vec3 :location 1 := (* (mat3<- normal-matrix)
                                                         normal))
                (:out frag-color vec3 :location 2 := color)
                (:out frag-texcoord vec2 :location 3 := texcoord)
                ;; Temporarily hard coded light position and color.
                (:out light-position vec3 :location 4 := (vec3 10f0 10f0 0f0))
                (:out light-color vec3 :location 5 := (vec3 1.0f0 0.7f0 0.3f0)
                      #+(or)
                      (vec3 (/ 61f0 255f0) (/ 162f0 255f0) (/ 153f0 255f0)))
                ;; View space variations of the position and normal
                ;; vectors, for the specular light. Passing in a
                ;; separate view normal matrix might be a more
                ;; efficient way to calculate v-light-position.
                (:out v-frag-position vec3 :location 6 := (vec3 (* view-matrix
                                                                   (vec4 frag-position 1f0))))
                (:out v-frag-normal vec3 :location 7 := (* (mat3<- (* (transpose (inverse view-matrix))
                                                                      normal-matrix))
                                                           normal))
                (:out v-light-position vec3 :location 8 := (vec3 (* view-matrix
                                                                    (vec4 light-position 1f0))))
                (:out gl-position vec4 := (* projection-matrix
                                             view-matrix
                                             model-matrix
                                             (vec4 position 1f0)))))

(define-shader (simple-frag :fragment-shader)
               ((:in frag-position vec3 :location 0)
                (:in frag-normal vec3 :location 1)
                (:in frag-color vec3 :location 2)
                (:in frag-texcoord vec2 :location 3)
                (:in light-position vec3 :location 4)
                (:in light-color vec3 :location 5)
                (:in v-frag-position vec3 :location 6)
                (:in v-frag-normal vec3 :location 7)
                (:in v-light-position vec3 :location 8)
                (:uniform default-texture sampler-2D :location 0))
               ((:out out-color vec4 :location 0 := (vec4 (.* (+ ambient-light diffuse specular)
                                                              object-color)
                                                          1f0)))
  (define surface-normal (normalize frag-normal))
  (define v-surface-normal (normalize v-frag-normal))
  (define texture-color (vec3 (texture default-texture frag-texcoord)))
  (define object-color (.* frag-color texture-color))
  #+(or)
  (define object-color (/ (* (+ frag-color (* 0.5f0 (+ 1f0 surface-normal)))
                             texture-color)
                          2f0))
  (define light-direction (normalize (- light-position frag-position)))
  (define v-light-direction (normalize (- v-light-position v-frag-position)))
  ;; Ambient light, which is the same strength everywhere
  (define ambient-light (let ((ambient-strength 0.5f0)
                              (ambient-color (vec3 1.0f0 0.7f0 0.3f0)
                                             ;; (vec3 (/ 61f0 255f0) (/ 162f0 255f0) (/ 153f0 255f0))
                                             ))
                          (* ambient-strength ambient-color)))
  ;; Diffuse light, based on the light source's position
  (define diffuse (let ((diffuse-strength 0.5f0)
                        (diffuse-scalar (max (dot surface-normal light-direction) 0f0)))
                    (* diffuse-strength diffuse-scalar light-color)))
  ;; Specular light, which makes things "shine" relative to the viewer
  (define specular (let* ((specular-strength 0.5f0)
                          (view-direction (normalize (- v-frag-position)))
                          (reflection (reflect (- v-light-direction) v-surface-normal))
                          (specular-scalar (expt (max (dot view-direction reflection) 0f0) 32f0)))
                     (* specular-strength specular-scalar light-color))))

;;;; 3D shaders with terrain textures

;;;; This is just a duplication of the above with a slight difference
;;;; to allow for a 2D-array for terrain textures. This duplication
;;;; will motivate shader functions later on in development.

#+(or)
(define-shader (terrain-vert :vertex-shader)
               ((:in position vec3 :location 0)
                (:in normal vec3 :location 1)
                (:in weight vec3 :location 2)
                (:in texcoord-1 vec2 :location 3)
                (:in texcoord-2 vec2 :location 4)
                (:in texcoord-3 vec2 :location 5)
                (:uniform view-matrix mat4 :location 1)
                (:uniform model-matrix mat4 :location 2)
                (:uniform projection-matrix mat4 :location 3)
                (:uniform normal-matrix mat4 :location 4))
               ((:out frag-position vec3 :location 0 := (vec3 (* model-matrix
                                                                 (vec4 position 1f0))))
                (:out frag-normal vec3 :location 1 := (* (mat3<- normal-matrix)
                                                         normal))
                (:out frag-weight vec3 :location 2 := weight)
                (:out frag-texcoord-1 vec2 :location 3 := texcoord-1)
                (:out frag-texcoord-2 vec2 :location 4 := texcoord-2)
                (:out frag-texcoord-3 vec2 :location 5 := texcoord-3)
                (:out light-position vec3 :location 6 := (vec3 10f0 50f0 0f0))
                (:out light-color vec3 :location 7 := (vec3 1f0 1f0 1f0))
                (:out v-frag-position vec3 :location 8 := (vec3 (* view-matrix
                                                                   (vec4 frag-position 1f0))))
                (:out v-frag-normal vec3 :location 9 := (* (mat3<- (* (transpose (inverse view-matrix))
                                                                      normal-matrix))
                                                           normal))
                (:out v-light-position vec3 :location 10 := (vec3 (* view-matrix
                                                                     (vec4 light-position 1f0))))
                (:out gl-position vec4 := (* projection-matrix
                                             view-matrix
                                             model-matrix
                                             (vec4 position 1f0)))))

#+(or)
(define-shader (terrain-frag :fragment-shader)
               ((:in frag-position vec3 :location 0)
                (:in frag-normal vec3 :location 1)
                (:in frag-weight vec3 :location 2)
                (:in frag-texcoord-1 vec2 :location 3)
                (:in frag-texcoord-2 vec2 :location 4)
                (:in frag-texcoord-3 vec2 :location 5)
                (:in light-position vec3 :location 6)
                (:in light-color vec3 :location 7)
                (:in v-frag-position vec3 :location 8)
                (:in v-frag-normal vec3 :location 9)
                (:in v-light-position vec3 :location 10)
                (:uniform terrain sampler-2D-array :location 0)
                (:uniform terrain-map sampler-2D :location 5))
               ((:out out-color vec4 :location 0 := (vec4 (.* (+ ambient-light diffuse specular)
                                                              texture)
                                                          1f0)))
  ;; (define (weight :vec3) (normalize frag-weight))
  (define texture (let ((terrain-type (* 255f0 (swizzle (texture terrain-map frag-texcoord-1) :r))))
                    (vec3 (texture terrain (vec3 frag-texcoord-1 terrain-type)))))
  ;; Lighting
  (define surface-normal (normalize frag-normal))
  (define v-surface-normal (normalize v-frag-normal))
  (define light-direction (normalize (- light-position frag-position)))
  (define v-light-direction (normalize (- v-light-position v-frag-position)))
  ;; Ambient light, which is the same strength everywhere
  (define ambient-light (let ((ambient-strength 0.8f0)
                              (ambient-color (vec3 1f0 1f0 1f0)))
                          (* ambient-strength ambient-color)))
  ;; Diffuse light, based on the light source's position
  (define diffuse (let ((diffuse-strength 0.5f0)
                        (diffuse-scalar (max (dot surface-normal light-direction) 0f0)))
                    (* diffuse-strength diffuse-scalar light-color)))
  ;; Specular light, which makes things "shine" relative to the viewer
  (define specular (let* ((specular-strength 0.5f0)
                          (view-direction (normalize (- v-frag-position)))
                          (reflection (reflect (- v-light-direction) v-surface-normal))
                          (specular-scalar (expt (max (dot view-direction reflection) 0f0) 32f0)))
                     (* specular-strength specular-scalar light-color))))

;;;; Exporting the shaders

;;; This macro defines a function containing the shader information.
;;; The shaders above were all compiled at macro expansion time into
;;; functions containing the SPIR-V bytecode.
;;;
;;; Here, shader programs are defined as the program's name followed
;;; by which shaders are in the program. Finally, the macro's body is
;;; a list of shaders, which are defined above with `define-shader'.
;;;
;;; Note: Only `shader-data' needs to be exported.
(define-shader-data shader-data
    ((default () :vertex simple-vert :fragment simple-frag)
     (hud () :vertex simple-2D-vert :fragment simple-2D-frag)
     #+(or)
     (terrain (:texture-names #(:terrain :terrain-map)) :vertex terrain-vert :fragment terrain-frag))
  simple-frag
  simple-vert
  ;; terrain-frag
  ;; terrain-vert
  simple-2D-frag
  simple-2D-vert)
