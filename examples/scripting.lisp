(defpackage #:zombie-raptor/examples/scripting
  (:documentation "Scripting for the engine's example programs.")
  (:use #:cl
        #:zombie-raptor/core/types
        #:zombie-raptor/data/terrain
        #:zombie-raptor/entity/entity
        #:zombie-raptor/entity/entity-action
        #:zombie-raptor/math/vector
        #:zr-utils)
  (:export #:make-2D-hud-test-entities
           #:make-simple-script-entities
           #:make-simple-test-hud
           #:make-terrain-test-entities
           #:make-test-entities
           #:script
           #:simple-script))

(in-package #:zombie-raptor/examples/scripting)

(define-simple-struct simple-hud-labels
  (dot entity-id))

(define-accessor-macro with-simple-hud-labels #:simple-hud-labels-)

(define-simple-struct fancy-hud-labels
  (dot      entity-id)
  (square   entity-id)
  (hexagon  entity-id)
  (triangle entity-id))

(define-accessor-macro with-fancy-hud-labels #:fancy-hud-labels-)

(define-simple-struct example-world-labels
  (platform-1 entity-id)
  (platform-2 entity-id)
  (goal       entity-id)
  (hud-labels simple-hud-labels))

(define-accessor-macro with-example-world-labels #:example-world-labels-)

;;; HUDs

;;; Note: the shapes themselves must be generated or imported

(defun make-test-hud (&key ecs hud-ecs mesh-keys width height hud-entity-labels)
  (declare (ignore ecs))
  "Makes a fancy test HUD with multiple different 2D shapes."
  (with-fancy-hud-labels (dot square hexagon triangle)
      hud-entity-labels
    (setf dot
          (make-basic-entity hud-ecs
                             mesh-keys
                             :square
                             :scale (vec3 2f0 2f0 1f0)
                             :falling? nil)
          square
          (make-basic-entity hud-ecs
                             mesh-keys
                             :square
                             :location (vec3 (- (/ width 2.5f0)) (/ height 2.75f0) 0f0)
                             :scale (vec3 (/ height 10f0) (/ height 10f0) 1f0))
          hexagon
          (make-basic-entity hud-ecs
                             mesh-keys
                             :hexagon
                             :location (vec3 (- (/ width 2.5f0)) (- (/ height 2.75f0)) 0f0)
                             :scale (vec3 (/ height 10f0) (/ height 10f0) 1f0))
          triangle
          (make-basic-entity hud-ecs
                             mesh-keys
                             :triangle
                             :location (vec3 (/ width 2.5f0) (- (/ height 2.75f0)) 0f0)
                             :scale (vec3 (/ height 10f0) (/ height 10f0) 1f0)))
    hud-entity-labels))

(defun make-simple-test-hud (&key ecs hud-ecs mesh-keys width height hud-entity-labels)
  (declare (ignore ecs width height))
  "Makes a HUD that only contains a center dot."
  (with-simple-hud-labels (dot)
      hud-entity-labels
    (setf dot (make-basic-entity hud-ecs
                                 mesh-keys
                                 :square
                                 :scale (vec3 2f0 2f0 1f0)
                                 :falling? nil)))
  hud-entity-labels)

;;; Other init scripts

(define-function boidify ((ecs entity-component-system)
                          (entity-id entity-id)
                          (goal-id entity-id)
                          &optional
                          (floors nil (maybe (simple-array single-float (* 4)))))
  (with-selection ecs
      (i :id entity-id :add (boid) :changed? t)
      ((location (x location :elt 0)
                 (z location :elt 2))
       (boid (_ goal := goal-id)
             (zone zone)
             (in-zone? in-zone?)))
    (if floors
        (multiple-value-bind (in? zone-id)
            ;; TODO: replace with ECS lookup
            (boid-zone floors x z)
          (setf in-zone? (if in? 1 0))
          (unless in?
            (format t "Boid ~D (~A, ~A) not in zone.~%" entity-id x z))
          (if in? (setf zone zone-id)))
        (setf in-zone? 0))))

(define-function initialize-boids ((ecs entity-component-system))
  (with-selection ecs
      (i)
      ((boid (goal goal)
             (waypoint waypoint :row-of 3)))
    (with-selection ecs
        (j :id goal)
        ((location (location location :row-of 3)))
      (setf waypoint location))))

(defun make-test-entities (world-coords world)
  "Initializes all of the entities for the main example."
  (lambda (&key ecs hud-ecs mesh-keys width height)
    (let ((entity-labels (make-example-world-labels)))
      (with-world-data (floors zone-IDs) world
        (with-example-world-labels (hud-labels goal)
            entity-labels
          (make-simple-test-hud :hud-ecs hud-ecs
                                :mesh-keys mesh-keys
                                :width width
                                :height height
                                :hud-entity-labels hud-labels)
          (make-fps-camera-entity ecs
                                  :location (vec3 -14.5f0 1.75f0 16f0)
                                  :rotation (vec3 (* -0.25f0 (float* pi)) 0f0 0f0))
          (make-basic-entity ecs
                             mesh-keys
                             :cube
                             :location (vec3 0f0 0.5f0 -8f0))
          (setf goal (make-location-entity ecs))
          (dotimes (i (array-dimension floors 0))
            (multiple-value-bind (x1 x2 z1 z2) (array-row-of-4 floors i)
              (setf (aref zone-IDs i)
                    (make-zone-entity ecs
                                      :corner-a (vec3 x1 0f0 z1)
                                      :corner-b (vec3 x2 2.75f0 z2)))))
          (loop :for vec :in (list (vec3 -6f0 1.0f0 15f0)
                                   (vec3 -13.5f0 1.0f0 -2f0)
                                   (vec3 -4.5f0 1.0f0 -4f0)
                                   (vec3 -1.5f0 1.0f0 -1f0)
                                   (vec3 -13.5f0 1.0f0 1f0)
                                   (vec3 -4.5f0 1.0f0 -2f0)
                                   (vec3 -1.5f0 1.0f0 1f0))
                ;; TODO: look up the floors in the ECS, as created above
                :do (boidify ecs (make-basic-entity ecs mesh-keys :tall-box :location vec) goal floors)))
        (loop :for i :from 0
              :for coords :of-type vec4 :in world-coords
              :do
                 (make-world-entity ecs
                                    mesh-keys
                                    (intern (format nil "~A~D" (symbol-name :world) i) :keyword)
                                    :location (multiple-value-call #'vec3 (array-of-3 coords))
                                    :rotation (vec3 (aref coords 3) 0f0 0f0)))
        (values entity-labels world)))))

(defun make-terrain-test-entities (&key ecs hud-ecs mesh-keys width height)
  "Initializes the camera and terrain for the terrain example."
  (make-simple-test-hud :hud-ecs hud-ecs
                        :mesh-keys mesh-keys
                        :width width
                        :height height
                        :hud-entity-labels (make-simple-hud-labels))
  (make-overhead-camera-entity ecs
                               :location (vec3 0f0 20f0 0f0)
                               :rotation (vec3 0f0 -0.85f0 0f0))
  #+(or)
  (make-basic-entity ecs
                     mesh-keys
                     :long-box
                     :location (vec3 0f0 0.5f0 0f0)
                     :scale (vec3 0.5f0 0.5f0 0.5f0))
  (make-world-entity ecs
                     mesh-keys
                     :world
                     :location (vec3 0f0 0f0 0f0))
  nil)

(defun make-2D-hud-test-entities (&key ecs hud-ecs mesh-keys width height)
  "Initializes the '3D' part of the fancy 2D HUD."
  (let ((entity-labels (make-fancy-hud-labels)))
    (make-test-hud :hud-ecs hud-ecs
                   :mesh-keys mesh-keys
                   :width width
                   :height height
                   :hud-entity-labels entity-labels)
    (make-fps-camera-entity ecs :location (vec3 0f0 0f0 0f0))
    entity-labels))

(defun make-simple-script-entities (&key ecs hud-ecs mesh-keys width height)
  "The default initialization: just a simple HUD and a simple camera."
  (let ((entity-labels (make-simple-hud-labels)))
    (make-simple-test-hud :hud-ecs hud-ecs
                          :mesh-keys mesh-keys
                          :width width
                          :height height
                          :hud-entity-labels entity-labels)
    (make-fps-camera-entity ecs :location (vec3 0f0 0f0 0f0))
    entity-labels))

;;; Scripts

(defun simple-script (&key &allow-other-keys)
  "The default script, which does nothing."
  nil)

;;; TODO: temporary; replace with proper collision later
(define-function simple-collision ((ecs entity-component-system)
                                   (world-data world-data))
  (with-world-data (walls) world-data
    (with-selection ecs
        (i)
        ((location (location.x location :elt 0)
                   (location.z location :elt 2)
                   (old-location.x old-location :elt 0)
                   (old-location.z old-location :elt 2))
         (velocity (velocity.x velocity :elt 0)
                   (velocity.z velocity :elt 2))
         (physics (acceleration.x acceleration :elt 0)
                  (acceleration.z acceleration :elt 2)))
      (loop :for k :from 0 :below (array-dimension walls 0)
            :for size := (aref walls k 0)
            :for x    := (aref walls k 1)
            :for z    := (aref walls k 2)
            :for dir  := (= 1 (aref walls k 3))
            :with max-distance := (if (zerop i) 0.25f0 0.5f0)
            :do (when (zerop i)
                  (if dir
                      (when (and (< (abs (- location.x x)) max-distance)
                                 (< (- z (* 0.5f0 max-distance)) location.z (+ z size (* (if (zerop i) 0.5f0 0.75f0) max-distance))))
                        (psetf location.x old-location.x
                               velocity.x 0f0
                               acceleration.x 0f0)
                        (when (or (and (< old-location.z location.z) (< (abs (- location.z z)) 0.1f0))
                                  (and (> old-location.z location.z) (< (abs (- location.z (+ z size))) 0.1f0)))
                          (psetf location.z old-location.z
                                 velocity.z 0f0
                                 acceleration.z 0f0)))
                      (when (and (< (abs (- location.z z)) max-distance)
                                 (< (- x (* 0.5f0 max-distance)) location.x (+ x size (* (if (zerop i) 0.5f0 0.75f0) max-distance))))
                        (psetf location.z old-location.z
                               velocity.z 0f0
                               acceleration.z 0f0)
                        (when (or (and (< old-location.x location.x) (< (abs (- location.x x)) 0.1f0))
                                  (and (> old-location.x location.x) (< (abs (- location.x (+ x size))) 0.1f0)))
                          (setf location.x old-location.x
                                velocity.x 0f0
                                acceleration.x 0f0))))))))
  nil)

(define-function script (&key (ecs nil entity-component-system)
                              (hud-ecs nil entity-component-system)
                              (tick 0 tick)
                              (state nil world-data)
                              (entity-labels nil example-world-labels))
  (declare (ignore hud-ecs))
  (simple-collision ecs state)
  (with-example-world-labels (goal) entity-labels
    (when (= tick 300)
      (update-boid-goal ecs 0 goal)
      (initialize-boids ecs))
    (let ((update-interval 25))
      (when (> tick 300)
        ;; Have a delayed reaction time to updating the goal.
        (when (zerop (mod tick update-interval))
          (update-boid-goal ecs 0 goal))
        (flock-boids ecs tick state :update-interval update-interval))))
  nil)
