(cl:defpackage #:zombie-raptor/examples/textures
  (:documentation "Handles OpenGL textures.")
  (:use #:cl
        #:zombie-raptor/data/texture
        #:zombie-raptor/text/font
        #:zr-utils)
  (:export #:generate-square-texture
           #:generate-terrain-textures))

(cl:in-package #:zombie-raptor/examples/textures)

(defun generate-text-texture ()
  (let ((row-size 1024)
        (texel-size 4))
    (flet* (((index :inline t) (i j)
             (flat-index texel-size row-size i j)))
      (with-texture-data-make-texture (texture :square)
          (2 4 1024 :initial-element 255)
        ;; Draws some text
        (let ((overlay (generate-glyph-coordinates)))
          (dotimes (column row-size)
            (dotimes (row row-size)
              (when (= 1 (aref overlay (flat-index 1 row-size row column)))
                (with-array-accessors ((texel texture :range 3))
                    (index row column)
                  (setf texel (values 0 0 0)))))))
        ;; Draws four lines, where offset is the distance from the edge
        (let ((offset 3))
          (do ((i offset (1+ i)))
              ((= i (- row-size offset)))
            (with-array-accessors ((texel texture :range 3))
                (index i offset)
              (setf texel (values 0 0 0))))
          (do ((i offset (1+ i)))
              ((= i (- row-size offset)))
            (with-array-accessors ((texel texture :range 3))
                (index i (- row-size (1+ offset)))
              (setf texel (values 0 0 0))))
          (do ((i offset (1+ i)))
              ((= i (- row-size offset)))
            (with-array-accessors ((texel texture :range 3))
                (index offset i)
              (setf texel (values 0 0 0))))
          (do ((i offset (1+ i)))
              ((= i (- row-size offset)))
            (with-array-accessors ((texel texture :range 3))
                (index (- row-size (1+ offset)) i)
              (setf texel (values 0 0 0)))))))))

(define-function (orange :inline t) ()
  (values #xff #x9f #x31))

(define-function (white :inline t) ()
  (values #xff #xff #xff))

(defun generate-default-texture ()
  (let ((row-size 1024)
        (texel-size 4))
    (flet* (((line-color :inline t) ()
             (values #x90 #x90 #x90))
            ((in-range? :inline t) (number range-center thickness)
             (<= (- range-center (floor (- thickness 1) 2))
                 number
                 (+ range-center (floor (- thickness 1) 2)))))
      (with-texture-data-make-texture (texture :square)
          (2 4 1024 :initial-element 255)
        ;; Draws a grid by dividing the texture into a 4x4 square
        ;; pattern. Note that the outermost thickness is actually
        ;; doubled (from 3 to 6) if the texture is tiled.
        (loop :for index :from 0 :by 4 :below (* row-size row-size texel-size)
              :for row := (floor index (* row-size texel-size))
              :for col := (mod (floor index texel-size) row-size)
              :with size-1/4 := (- (round row-size 4) 1)
              :with size-1/2 := (- (round row-size 2) 1)
              :with size-3/4 := (+ size-1/4 size-1/2 1)
              :do (with-array-accessors ((texel texture :range 3))
                      index
                    (setf texel
                          (if (or (in-range? row 1 3)
                                  (= row size-1/4)
                                  (in-range? row size-1/2 3)
                                  (= row size-3/4)
                                  (in-range? row (- row-size 2) 3)
                                  (in-range? col 1 3)
                                  (= col size-1/4)
                                  (in-range? col size-1/2 3)
                                  (= col size-3/4)
                                  (in-range? col (- row-size 2) 3))
                              (line-color)
                              (white)))))))))

(defun generate-square-texture (&optional path)
  (if path
      (read-file-into-texture :square path)
      (generate-default-texture)))

(define-function (%texel :inline t) (texture i r g b)
  (with-array-accessors ((texel texture :range 3)) i
    (setf texel (values r g b))))

(defmacro do-y-x ((y y-times) (x x-times) &body body)
  `(dotimes (,y ,y-times)
     (dotimes (,x ,x-times)
       ,@body)))

(defun generate-terrain-textures ()
  (let* ((texel-size 4)
         (width 32))
    (flet* (((index :inline t) (x y layer)
              (+ (* texel-size width width layer)
                 (flat-index texel-size width x y))))
      (with-texture-data-make-texture (texture :terrain)
          (2 4 32 :depth 4 :initial-element 255 :array? t)
        (do-y-x (y width) (x width) (%texel texture (index x y 0) #x0c #x2b #x63))
        (do-y-x (y width) (x width) (%texel texture (index x y 1) #x63 #x2b #x0c))
        (do-y-x (y width) (x width) (%texel texture (index x y 2) #x2b #x63 #x0c))
        (do-y-x (y width) (x width) (%texel texture (index x y 3) #x2b #x7a #x2b))))))
