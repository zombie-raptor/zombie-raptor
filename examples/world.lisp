(defpackage #:zombie-raptor/examples/world
  (:documentation "Generates an example world.")
  (:use #:cl
        #:zombie-raptor/core/types
        #:zombie-raptor/data/model
        #:zombie-raptor/entity/entity-action
        #:zombie-raptor/math
        #:zr-utils)
  ;; Needed to infer the dependency on Alexandria
  (:import-from #:alexandria)
  (:import-from #:static-vectors
                #:make-static-vector)
  (:export #:make-level))

(in-package #:zombie-raptor/examples/world)

;;; Rotate in one of four directions

(alexandria:define-constant +rotation-0+
    (identity-matrix)
  :test #'matrix=)

(alexandria:define-constant +rotation-1+
    (yaw-rotation-matrix (float* (/ pi 2)))
  :test #'matrix=)

(alexandria:define-constant +rotation-2+
    (yaw-rotation-matrix (float* pi))
  :test #'matrix=)

(alexandria:define-constant +rotation-3+
    (yaw-rotation-matrix (float* (* 3 (/ pi 2))))
  :test #'matrix=)

(deftype direction ()
  `(mod 4))

(deftype vertex ()
  `(mod 4))

(define-function (rotation :return matrix :inline t) ((direction direction))
  (ecase= direction
    (0 +rotation-0+)
    (1 +rotation-1+)
    (2 +rotation-2+)
    (3 +rotation-3+)))

;;; Make an element array for a certain number of quads
(defun make-element-array (quads)
  (loop :with element-array :of-type (simple-array uint16 (*))
          := (make-static-vector (* 2 3 quads)
                                 :element-type 'uint16
                                 :initial-element 0)
        :for i :from 0 :below (length element-array) :by 6
        :for k :from 0 :by 4
        :do (with-array-accessors ((triangle-1 element-array
                                               :range 3
                                               := (values (+ k 2) (+ k 3) (+ k 1)))
                                   (triangle-2 element-array
                                               :range 3
                                               :offset 3
                                               := (values (+ k 0) (+ k 2) (+ k 1))))
                i)
        :finally (return element-array)))

;;; Rotate the normal in one of four directions; this can be hard coded
(define-function (determine-normal :return (single-float single-float single-float))
    ((direction direction))
  (ecase= direction
    (0 (values -1f0 0f0 0f0))
    (2 (values +1f0 0f0 0f0))
    (3 (values 0f0 0f0 -1f0))
    (1 (values 0f0 0f0 +1f0))))

;;; Determine which texcoord is valid for a given vertex
(define-function (determine-texcoord :return (single-float single-float))
    ((vertex vertex)
     (length single-float)
     (height single-float))
  (values (if (oddp vertex) length 0f0)
          (if (< vertex 2) height 0f0)))

;;; Square vertices for creating rectangles that will be scaled,
;;; rotated, and translated later on. This is for walls that are
;;; rotated by yaw so the x is 0, the z is centered, and the y has a
;;; floor and a ceiling.
(define-function (original-square-vertices
                  :return (single-float single-float single-float)
                  :inline t)
    ((vertex vertex))
  (values 0f0
          (if (< vertex 2) 1f0 0f0)
          (if (evenp vertex) -0.5f0 0.5f0)))

;;; Find the valid vertex for a given wall quad
(define-function (determine-vertex
                  :default-type single-float
                  :return (single-float single-float single-float))
    ((vertex vertex)
     x
     z
     offset-l
     offset-r
     length
     ceiling-height
     wall-thickness
     (world-offset-translation matrix)
     (rotation matrix)
     (long-side? boolean))
  (declare (optimize (speed 3)))
  (let* ((scale (scale 1f0
                       ceiling-height
                       (if long-side?
                           (- length offset-l offset-r)
                           wall-thickness)))
         (matrix scale)
         (local-translation (translate (* -0.5f0 (if long-side?
                                                     wall-thickness
                                                     length))
                                       0f0
                                       (* 0.5f0 (- offset-l offset-r))))
         (global-translation (translate x 0f0 z)))
    (declare (dynamic-extent scale local-translation global-translation))
    ;; Apply the matrices in the correct order and then apply them to
    ;; the vertex. Note that the first argument is the destination
    ;; matrix.
    (matrix** matrix
              world-offset-translation
              global-translation
              rotation
              local-translation
              scale)
    ;; Note: This intermediate binding is used to return 3, instead of
    ;; 4, values and to allow these variables to exist for debugging.
    (multiple-value-bind (x* y* z*)
        (multiple-value-call #'matrix*vector/mv
          matrix
          (original-square-vertices vertex))
      (values x* y* z*))))

;; Shifts the wall coordinates that refer to one of the edges of the
;; wall to refer to the center of the wall instead. This allows them
;; to be used on the generated geometry.
(define-function (shift-wall-coordinates :return (single-float single-float))
    ((x single-float)
     (z single-float)
     (length single-float)
     (direction direction)
     (long-side? boolean))
  (if (or (and (evenp direction) long-side?)
          (and (oddp direction) (not long-side?)))
      (values x
              (+ z (* 0.5f0 length)))
      (values (+ x (* 0.5f0 length))
              z)))

(define-function (write-quad :default-type single-float)
    ((vertex-array (simple-array single-float (*)))
     (stride alexandria:positive-fixnum)
     (index alexandria:array-index)
     (direction direction)
     x
     z
     ceiling-height
     wall-thickness
     (world-offset-translation matrix)
     &optional
     (length 0f0)
     (truncate-l 0f0)
     (truncate-r 0f0)
     (long-side? t boolean))
  (let ((offset-l (* truncate-l (* 0.5f0 wall-thickness)))
        (offset-r (* truncate-r (* 0.5f0 wall-thickness))))
    (multiple-value-bind (x z)
        (shift-wall-coordinates x z length direction long-side?)
      (loop :for vertex-array-index :from (* 4 stride index) :by stride
            :for vertex-number :of-type (integer 0 4) :from 0 :below 4
            ;; Create one of the four sides of the wall, i.e. a quad,
            ;; one vertex at a time.
            :do (with-array-accessors ((vertex vertex-array
                                               :range 3
                                               := (determine-vertex vertex-number
                                                                    x
                                                                    z
                                                                    offset-l
                                                                    offset-r
                                                                    length
                                                                    ceiling-height
                                                                    wall-thickness
                                                                    world-offset-translation
                                                                    (rotation direction)
                                                                    long-side?))
                                       (normal vertex-array
                                               :range 3
                                               :offset (* 3 1)
                                               := (determine-normal direction))
                                       (color vertex-array
                                              :range 3
                                              :offset (* 3 2)
                                              := (values 1f0 1f0 1f0))
                                       (texcoord vertex-array
                                                 :range 2
                                                 :offset (* 3 3)
                                                 := (determine-texcoord vertex-number
                                                                        (if long-side?
                                                                            (- length offset-l offset-r)
                                                                            wall-thickness)
                                                                        ceiling-height)))
                    vertex-array-index
                  #+(or)
                  (format t
                          "~D ~A ~A ~A ~A~%"
                          vertex-array-index
                          (multiple-value-list vertex)
                          (multiple-value-list normal)
                          (multiple-value-list color)
                          (multiple-value-list texcoord)))))))

(define-function make-map-connections (map-nodes
                                       (x-offset single-float)
                                       (z-offset single-float)
                                       connections)
  (loop :for node :across map-nodes
        :for i :from 0
        :do (setf (map-node-adjacent-nodes node)
                  (loop :for j :from 0 :below (length map-nodes)
                        :for adjacency? := (= 1 (aref (map-node-adjacency node) j))
                        :when adjacency?
                          :collect (multiple-value-bind (x z) (funcall connections i j)
                                     (check-type x single-float)
                                     (check-type z single-float)
                                     (make-map-node-connection :id j
                                                               :connected-node (aref map-nodes j)
                                                               :points (list (vec2 (+ x x-offset)
                                                                                   (+ z z-offset))))))))
  map-nodes)

(defun make-wall (x-size y-size z-size n name program texture)
  (make-cuboid x-size
               y-size
               z-size
               :name (intern (format nil "~A~D" (symbol-name name) n) :keyword)
               :program program
               :texture texture))

(define-function (make-wall-coords :default-type single-float)
    (x z dir size ceiling-height)
  (declare (optimize (speed 3)))
  (flet ((rotate-coords (x z theta)
           (values (- (* x (cos theta))
                      (* z (sin theta)))
                   (+ (* x (sin theta))
                      (* z (cos theta))))))
    (declare (inline rotate-coords))
    ;; Translate the 0 to 1 direction into a 0 to -pi/2 rotation
    (let ((dir* (* dir (float* (- (/ pi 2f0))))))
      ;; Offset the wall by half of the wall length rotated in the
      ;; opposite direction so the points specify one of two edges of
      ;; the line segment instead of the center.
      (multiple-value-bind (x-offset z-offset)
          (rotate-coords (* 0.5f0 size) 0f0 (- dir*))
        (vec4 (+ x x-offset)
              (* 0.5f0 ceiling-height)
              (+ z z-offset)
              dir*)))))

(define-function (make-floor-coords :default-type single-float)
    (x-center z-center floor-thickness)
  (vec4 x-center
        (* -0.5f0 floor-thickness)
        z-center
        0f0))

;;; Marks the center and the four corners, for debugging
(define-function make-markers ((width single-float)
                               (length single-float)
                               (start (integer 0 *))
                               name
                               program
                               texture)
  (values (loop :for i :from start
                :repeat 5
                :collect (make-cube 1
                                    :name (intern (format
                                                   nil
                                                   "~A~D"
                                                   (symbol-name name)
                                                   i)
                                                  :keyword)
                                    :program program
                                    :texture texture))
          (list (vec4 0f0 -0.375f0 0f0 (float* (/ pi 4d0)))
                (vec4 (* +0.5f0 width) 0f0 (* +0.5f0 length) 0f0)
                (vec4 (* +0.5f0 width) 0f0 (* -0.5f0 length) 0f0)
                (vec4 (* -0.5f0 width) 0f0 (* +0.5f0 length) 0f0)
                (vec4 (* -0.5f0 width) 0f0 (* -0.5f0 length) 0f0))))

(defmacro make-map-nodes ((x-offset z-offset) (&rest adjacencies) (&rest nodes) &body body)
  (alexandria:once-only ((x-offset* x-offset)
                         (z-offset* z-offset))
    `(make-map-connections (vector ,@(loop :for adjacency :in adjacencies
                                           :for i :from 0
                                           :collect `(make-map-node :id ,i
                                                                    :adjacency ,adjacency
                                                                    :adjacent-nodes (list))))
                           ,x-offset*
                           ,z-offset*
                           (lambda (,@nodes) ,@body))))

(defun make-level (&key (name :world) program texture)
  (let* ((wall-thickness 0.125f0)
         ;; (door-width 2.1f0)
         ;; (door-height 2.4f0)
         (ceiling-height 2.75f0)
         (world (make-array '(22 4)
                            :element-type 'single-float
                            ;; the data is (size x z direction)
                            :initial-contents '((19.935f0 30f0      00.0625f0 1f0)
                                                (04f0     30f0      20f0      1f0)
                                                (10f0     30f0      24f0      1f0)
                                                (03.935f0 10f0      24.0625f0 1f0)
                                                (03.935f0 10f0      30f0      1f0)
                                                (03.935f0 20f0      24.0625f0 1f0)
                                                (03.935f0 20f0      30f0      1f0)
                                                (19.935f0 00f0      00.0625f0 1f0)
                                                (04f0     00f0      20f0      1f0)
                                                (10f0     00f0      24f0      1f0)
                                                (30f0     00f0      00f0      0f0)
                                                (13.935f0 00.0625f0 20f0      0f0)
                                                (13.935f0 16f0      20f0      0f0)
                                                (03.935f0 00.0625f0 24f0      0f0)
                                                (04f0     06f0      24f0      0f0)
                                                (04f0     10f0      24f0      0f0)
                                                (04f0     16f0      24f0      0f0)
                                                (04f0     20f0      24f0      0f0)
                                                (03.935f0 26f0      24f0      0f0)
                                                (09.935f0 00.0625f0 34f0      0f0)
                                                (10f0     10f0      34f0      0f0)
                                                (09.935f0 20f0      34f0      0f0))))
         (floors (make-array '(5 4)
                             :element-type 'single-float
                             ;; the data is (x1 x2 z1 z2)
                             :initial-contents '((20f0 30f0 24f0 34f0)
                                                 (10f0 20f0 24f0 34f0)
                                                 (00f0 10f0 24f0 34f0)
                                                 (00f0 30f0 20f0 24f0)
                                                 (00f0 30f0 00f0 20f0))))
         (world-width (loop :for k :from 0 :below (array-dimension floors 0)
                            :maximize (aref floors k 1)))
         (world-length (loop :for k :from 0 :below (array-dimension floors 0)
                             :maximize (aref floors k 3))))
    ;; Shift from map format coords to world coords
    (loop :for k :from 0 :below (array-dimension world 0)
          :do (progn
                (incf (aref world k 1) (* -0.5f0 world-width))
                (incf (aref world k 2) (* -0.5f0 world-length))))
    (loop :for k :from 0 :below (array-dimension floors 0)
          :do (progn
                (incf (aref floors k 0) (* -0.5f0 world-width))
                (incf (aref floors k 1) (* -0.5f0 world-width))
                (incf (aref floors k 2) (* -0.5f0 world-length))
                (incf (aref floors k 3) (* -0.5f0 world-length))))
    ;; Generate the walls and floor shapes.
    (labels ((make-walls (height thickness)
               (loop :for i :from 0
                     :for k :from 0 :below (array-dimension world 0)
                     :for size := (aref world k 0)
                     :for dir := (aref world k 3)
                     :for x := (aref world k 1)
                     :for z := (aref world k 2)
                     :collect (make-wall size height thickness i name program texture)
                       :into models
                     :collect (make-wall-coords x
                                                z
                                                dir
                                                size
                                                height)
                       :into locations
                     :finally (return (values models locations))))
             (make-floors (start thickness)
               (loop :for i :from start
                     :for k :from 0 :below (array-dimension floors 0)
                     :for x1 := (aref floors k 0)
                     :for x2 := (aref floors k 1)
                     :for z1 := (aref floors k 2)
                     :for z2 := (aref floors k 3)
                     :for x-center := (* 0.5f0 (+ x1 x2))
                     :for z-center := (* 0.5f0 (+ z1 z2))
                     :for x-size := (- x2 x1)
                     :for z-size := (- z2 z1)
                     :collect (make-wall x-size thickness z-size i name program texture)
                       :into models
                     :collect (make-floor-coords x-center z-center thickness) :into locations
                     :finally (return (values models locations))))
             (make-walls-and-floors ()
               (multiple-value-bind (models locations) (make-walls ceiling-height wall-thickness)
                 (multiple-value-bind (models* locations*) (make-floors (length models) wall-thickness)
                   (multiple-value-bind (models** locations**) (make-markers world-width
                                                                             world-length
                                                                             (+ (length models)
                                                                                (length models*))
                                                                             name
                                                                             program
                                                                             texture)
                     (values (append models models* models**)
                             (append locations locations* locations**)))))))
      ;; Generate the connections between the floor-based navigation zones
      (let ((map-nodes (make-map-nodes
                           ;; offsets
                           ((* -0.5f0 world-width)
                            (* -0.5f0 world-length))
                           ;; connections
                           (#*01010 #*10110 #*01010 #*11101 #*00010)
                           ;; bindings
                           (node0 node1)
                         ;; connection points
                         ;;
                         ;; todo: turn each of these into a new zone
                         ;; and shrink the navigable area of the
                         ;; floors
                         (ecase= node0
                           (0 (ecase= node1
                                (1 (values 20f0 29f0))
                                (3 (values 25f0 24f0))))
                           (1 (ecase= node1
                                (0 (values 20f0 29f0))
                                (2 (values 10f0 29f0))
                                (3 (values 15f0 24f0))))
                           (2 (ecase= node1
                                (1 (values 10f0 29f0))
                                (3 (values 05f0 24f0))))
                           (3 (ecase= node1
                                (0 (values 25f0 24f0))
                                (1 (values 15f0 24f0))
                                (2 (values 05f0 24f0))
                                (4 (values 15f0 20f0))))
                           (4 (ecase= node1
                                (3 (values 15f0 20f0))))))))
        (multiple-value-bind (models locations) (make-walls-and-floors)
          (values models
                  locations
                  (make-world-data :walls world
                                   :floors floors
                                   :map-nodes map-nodes
                                   :zone-IDs (make-array (array-dimension floors 0)
                                                         :element-type 'entity-id
                                                         :initial-element 0))))))))
