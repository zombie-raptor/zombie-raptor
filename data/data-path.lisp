(cl:defpackage #:zombie-raptor/data/data-path
  (:documentation "
An abstraction over Common Lisp pathnames so that finding various
application directories is trivial.
")
  (:use #:cl)
  (:import-from #:asdf
                #:system-source-directory)
  (:import-from #:uiop/configuration
                #:xdg-cache-home
                #:xdg-config-home
                #:xdg-data-home)
  (:import-from #:uiop/pathname
                #:subpathname)
  (:import-from #:zr-utils
                #:define-function)
  (:export #:app-directory-path
           #:local-cache-path
           #:local-config-path
           #:local-data-path
           #:main-data-directory
           #:main-data-directory*))

(cl:in-package #:zombie-raptor/data/data-path)

(define-function (relative-path :inline t) (&rest path-strings)
  "Generates an absolute pathname object from directories as strings."
  (make-pathname :directory `(:relative ,@path-strings)))

(define-function (absolute-path :inline t) (&rest path-strings)
  "Generates an absolute pathname object from directories as strings."
  (make-pathname :directory `(:absolute ,@path-strings)))

(define-function (app-directory-path :inline t) (lookup-function org-name app-name)
  "
Generates a full path to a directory given a lookup-function that
provides the base path, an app-name directory, and an optional
org-name directory. This is designed to be used with the XDG functions
or with functions that use a similar API.
"
  (funcall lookup-function
           (if org-name
               (relative-path org-name app-name)
               (relative-path app-name))))

(defun local-data-path (app-name &optional org-name)
  "
Gets the OS-specific idiomatic directory location for the local,
user-specific data directory.
"
  (app-directory-path #'xdg-data-home org-name app-name))

(defun local-config-path (app-name &optional org-name)
  "
Gets the OS-specific idiomatic directory location for the local,
user-specific configuration directory.
"
  (app-directory-path #'xdg-config-home org-name app-name))

(defun local-cache-path (app-name &optional org-name)
  "
Gets the OS-specific idiomatic directory location for the local,
user-specific cache directory.
"
  (app-directory-path #'xdg-cache-home org-name app-name))

(defun main-data-directory (system-name &rest path-strings)
  "Gets the main (non-local) data directory for the app."
  (subpathname (system-source-directory system-name)
               (apply #'relative-path path-strings)
               :type :directory))

(defun main-data-directory* (system-name subpath)
  "
Gets the main (non-local) data directory for the app, using a string
or relative path for the subpath.
"
  (subpathname (system-source-directory system-name)
               subpath
               :type :directory))
