(defpackage #:zombie-raptor/data/shader
  (:documentation "High-level abstractions over OpenGL shaders, including a shader language.")
  (:use #:cl
        #:zombie-raptor/core/conditions
        #:zombie-raptor/core/constants
        #:zombie-raptor/data/shader-core
        #:zombie-raptor/zrvl/spir-v
        #:zr-utils)
  (:import-from #:alexandria
                #:curry
                #:rcurry)
  (:import-from #:cffi
                #:foreign-enum-value
                #:with-foreign-string)
  (:import-from #:cl-opengl
                #:attach-shader
                #:compile-shader
                #:create-program
                #:create-shader
                #:detach-shader
                #:get-program
                #:get-program-info-log
                #:get-shader
                #:get-shader-info-log
                #:link-program
                #:shader-source)
  (:import-from #:static-vectors
                #:static-vector-pointer)
  (:export #:check-shaders
           #:find-location-in-program
           #:load-shaders
           #:make-gl-shader-programs
           #:matrix-names
           #:shader
           #:shader-binary
           #:shader-program
           #:shader-programs
           #:shader-stage
           #:shaders
           #:texture-names))

(in-package #:zombie-raptor/data/shader)

(defgeneric shaders (object)
  (:documentation "Retrieves the shaders stored in the object."))

(defconstant +spir-v+ (foreign-enum-value '%gl:enum :shader-binary-format-spir-v))

;;;; Shader class

(defclass shader ()
  ((%name
    :initarg :name
    :accessor name
    :checked-type keyword
    :initform (error 'required-input-error
                     :printable-class-name "shader"
                     :missing-input-name "name")
    :documentation "The name of the shader.")
   (%shader-stage
    :initarg :stage
    :accessor shader-stage
    :checked-type shader-stage
    :initform (error 'required-input-error
                     :printable-class-name "shader"
                     :missing-input-name "shader stage")
    :documentation "The OpenGL stage of the shader.")
   (%data
    :initarg :data
    :accessor shader-binary
    :checked-type (array (unsigned-byte 32) (*))
    :initform (error 'required-input-error
                     :printable-class-name "shader"
                     :missing-input-name "shader SPIR-V binary")
    :documentation "The compiled SPIR-V binary form of a shader.")
   (%inputs
    :initarg :inputs
    :accessor inputs
    :checked-type shader-input-variable-list
    :initform nil
    :documentation "A list of shader-input-variables.")
   (%outputs
    :initarg :outputs
    :accessor outputs
    :checked-type shader-output-variable-list
    :initform nil
    :documentation "A list of shader-output-variables."))
  (:documentation "
A shader class provides information about a shader to the game engine,
which then compiles the shader in OpenGL.")
  (:metaclass checked-types))

(define-typed-list shader)

(define-simple-slot-saving-for-class shader)

(define-function shader-stage-nickname ((shader-stage shader-stage))
  "
Returns the short name of a shader stage for a given long name if the
nickname exists.
"
  (case shader-stage
    (:vertex-shader          :vert)
    (:tess-control-shader    :tesc)
    (:tess-evaluation-shader :tese)
    (:geometry-shader        :geom)
    (:fragment-shader        :frag)
    (:compute-shader         :comp)
    (t shader-stage)))

(defmethod print-object ((shader shader) stream)
  (print-unreadable-object (shader stream :type t)
    (format stream
            "~A ~A"
            (shader-stage-nickname (shader-stage shader))
            (name shader)))
  shader)

(define-function check-shader-existence ((shaders hash-table)
                                         (shader-name keyword))
  (error-unless (hash-table-value-present? shader-name shaders)
                'missing-object-error
                :printable-type-name "shader"
                :object-name shader-name))

;;;; Shader pipeline class

;;; TODO: actually use the new shaders
(defclass shader-pipeline ()
  ((%vertex-shader
    :initarg :vertex-shader
    :accessor vertex-shader
    :checked-type keyword
    :initform (error 'required-input-error
                     :printable-class-name "shader pipeline"
                     :missing-input-name "vertex shader")
    :documentation "The name of the vertex shader.")
   (%tessellation-control-shader
    :initarg :tessellation-control-shader
    :accessor tessellation-control-shader
    :checked-type (maybe keyword)
    :initform nil
    :documentation "The name of the tessellation control shader, if it exists.")
   (%tessellation-evaluation-shader
    :initarg :tessellation-evaluation-shader
    :accessor tessellation-evaluation-shader
    :checked-type (maybe keyword)
    :initform nil
    :documentation "The name of the tessellation evaluation shader, if it exists.")
   (%geometry-shader
    :initarg :geometry-shader
    :accessor geometry-shader
    :checked-type (maybe keyword)
    :initform nil
    :documentation "The name of the geometry shader, if it exists.")
   (%fragment-shader
    :initarg :fragment-shader
    :accessor fragment-shader
    :checked-type keyword
    :initform (error 'required-input-error
                     :printable-class-name "shader pipeline"
                     :missing-input-name "fragment shader")
    :documentation "The name of the fragment shader."))
  (:documentation "
A shader pipeline represents the parts of the OpenGL rendering
pipeline that are programmable via shaders. Vertex and
fragment (pixel) shaders are required. Tessellation shaders (control
and evaluation) are optional. Geometry shaders are also optional.
Compute shaders are not part of the pipeline.
")
  (:metaclass checked-types))

(define-simple-slot-saving-for-class shader-pipeline)

(defmethod print-object ((pipeline shader-pipeline) stream)
  (print-unreadable-object (pipeline stream :type t)
    (format stream
            "~A ~A ~A ~A ~A"
            (vertex-shader pipeline)
            (tessellation-control-shader pipeline)
            (tessellation-evaluation-shader pipeline)
            (geometry-shader pipeline)
            (fragment-shader pipeline)))
  pipeline)

;;;; Shader program class

;;; Note: The matrix order in matrix-names matches the matrix order of
;;; the matrix arguments to draw (and draw-2D) in render/gl.lisp
(defclass shader-program ()
  ((%name
    :initarg :name
    :accessor name
    :checked-type keyword
    :initform (error 'required-input-error
                     :printable-class-name "shader program"
                     :missing-input-name "name")
    :documentation "The name of the shader program.")
   (%shader-pipeline
    :initarg :shader-pipeline
    :accessor shader-pipeline
    :checked-type shader-pipeline
    :initform (error 'required-input-error
                     :printable-class-name "shader program"
                     :missing-input-name "shader pipeline")
    :documentation "The programmable shader pipeline.")
   (%inputs
    :accessor inputs
    :checked-type (maybe shader-input-variable-list)
    :initform nil
    :documentation "A list of shader-input-variables.")
   (%outputs
    :accessor outputs
    :checked-type (maybe shader-output-variable-list)
    :initform nil
    :documentation "A list of shader-output-variables.")
   (%texture-names
    :initarg :texture-names
    :accessor texture-names
    :initform (vector :default-texture)
    :documentation "The name of the texture that the program uses.")
   (%matrix-names
    :initarg :matrix-names
    :accessor matrix-names
    :initform (vector :projection-matrix
                      :view-matrix
                      :model-matrix
                      :normal-matrix)
    :documentation "A vector of string names for the program's matrices."))
  (:documentation "
A shader program class provides information about a shader program to
the game engine, which is used to make the shader program in OpenGL.")
  (:metaclass checked-types))

(define-typed-list shader-program)

(define-simple-slot-saving-for-class shader-program)

(defmethod vertex-shader ((program shader-program))
  (vertex-shader (shader-pipeline program)))

(defmethod fragment-shader ((program shader-program))
  (fragment-shader (shader-pipeline program)))

(defmethod print-object ((program shader-program) stream)
  (print-unreadable-object (program stream :type t)
    (format stream
            "~A ~A"
            (name program)
            (shader-pipeline program)))
  program)

(define-function (find-location-in-program :inline t) ((variable-name symbol)
                                                       (program shader-program))
  (find-shader-variable-location variable-name (inputs program)))

(define-function make-spir-v-shader ((binary (simple-array uint32 (*)))
                                     (shader-type shader-stage)
                                     (name symbol))
  (declare (ignorable name))
  (let ((shader-count 1)
        (binary-byte-length (* 4 (length binary)))
        (shader (gl:create-shader shader-type)))
    (static-vectors:with-static-vector (a shader-count
                                          :element-type 'uint32
                                          :initial-element shader)
      (%gl:shader-binary shader-count
                         (static-vector-pointer a)
                         +spir-v+
                         (static-vector-pointer binary)
                         binary-byte-length))
    #+(or)
    (static-vectors:with-static-vector (loaded-binary 1 :element-type 'int32 :initial-element 0)
      (%gl:get-shader-iv shader :spir-v-binary (static-vector-pointer loaded-binary))
      (format t "~A ~A ~A~%" name shader-type (aref loaded-binary 0)))
    ;; "main" can be cached instead of creating and freeing a new
    ;; foreign string every call.
    (with-foreign-string (main "main")
      (%gl:specialize-shader shader
                             main
                             0
                             (cffi:null-pointer)
                             (cffi:null-pointer))
      ;; TODO: complete the error handling to get the status
      (static-vectors:with-static-vector (compiled 1 :element-type 'int32 :initial-element 0)
        (%gl:get-shader-iv shader :compile-status (static-vector-pointer compiled))
        (error-when (zerop (aref compiled 0))
                    'opengl-shader-error
                    :action "compiling"
                    :message "unknown error")))
    shader))

(defun make-spir-v-shader* (spir-v shader-type name)
  (static-vectors:with-static-vector (binary (length spir-v)
                                             :element-type 'uint32)
    (replace binary spir-v)
    (make-spir-v-shader binary shader-type name)))

(define-function (make-shader-program :check-type t
                                      :default-type symbol)
    (name
     &key
     (options nil (maybe list))
     vertex
     tess-control
     tess-evaluation
     geometry
     fragment)
  "
Returns a shader-program, automatically converting symbols to the
keywords that the program object expects.
"
  (make-instance 'shader-program
                 :name (symbol-to-keyword name)
                 :shader-pipeline (make-instance 'shader-pipeline
                                                 :vertex-shader (symbol-to-keyword vertex)
                                                 :tessellation-control-shader (and tess-control
                                                                                   (symbol-to-keyword tess-control))
                                                 :tessellation-evaluation-shader (and tess-evaluation
                                                                                      (symbol-to-keyword tess-evaluation))
                                                 :geometry-shader (and geometry (symbol-to-keyword geometry))
                                                 :fragment-shader (symbol-to-keyword fragment))
                 :texture-names (getf options :texture-names (vector :default-texture))))

(defmethod shaders ((shader-program shader-program))
  (list (vertex-shader shader-program) (fragment-shader shader-program)))

;;;; Shaders class

(defclass shaders ()
  ((%shaders
    :initarg :shaders
    :accessor shaders
    :documentation "All of an application's shaders.")
   (%shader-programs
    :initarg :shader-programs
    :accessor shader-programs)))

(define-function check-fragment-shader-input
    ((vertex-outputs shader-output-variable-list)
     (fragment-inputs shader-input-variable-list))
  (let ((vertex-variables (make-hash-table))
        (fragment-variables (make-hash-table)))
    (do-shader-output-variable-list (out vertex-outputs)
      (let ((name (shader-output-variable-name out)))
        (unless (eql name :gl-position)
          (setf (gethash name vertex-variables)
                (shader-output-variable-type out)))))
    (do-shader-input-variable-list (in fragment-inputs)
      (setf (gethash (shader-input-variable-name in) fragment-variables)
            (shader-input-variable-type in)))
    (do-hash-table (name type vertex-variables)
      (multiple-value-bind (value value-exists)
          (gethash name fragment-variables)
        (error-unless value-exists
                      'missing-object-error
                      :printable-type-name "fragment shader input"
                      :object-name name)
        ;; TODO: replace with custom-defined condition
        (error-unless (eql type value)
                      "Fragment shader type ~A does not match expected type ~A~%"
                      value
                      type)
        (remhash name fragment-variables)))
    (do-hash-table (name type fragment-variables)
      (declare (ignore type))
      (error 'missing-object-error
             :printable-type-name "vertex shader output"
             :object-name name)))
  nil)

(defun insert-into-shader-hash-table (shader-hash-table shader)
  (check-type shader shader)
  (insert-unique-hash-table-value shader-hash-table
                                  (name shader)
                                  shader
                                  "Shader names must be unique, but ~A is not!"))

(define-function (find-gl-position :inline t) (vertex-shader vertex-outputs)
  (let ((gl-position nil))
    (do-shader-output-variable-list (item vertex-outputs)
      (when (eql :gl-position (shader-output-variable-name item))
        (setf gl-position item)))
    ;; TODO: replace with custom-defined conditions
    (error-unless gl-position
                  "The vertex shader ~A needs a gl-position in its output!"
                  (name vertex-shader))
    (error-unless (eql (shader-output-variable-type gl-position) :vec4)
                  "gl-position has to be of the type :vec4")
    gl-position))

(define-function process-fragment-inputs ((fragment-inputs shader-input-variable-list)
                                          (program-inputs shader-input-variable-list))
  (let ((program-inputs-last (shader-input-variable-last program-inputs))
        (fragment-inputs* nil)
        (current-cons nil))
    (do-shader-input-variable-list (item fragment-inputs fragment-inputs*)
      (if (typep (shader-input-variable-storage-qualifier item)
                 'shader-uniform-storage-qualifier)
          (setf program-inputs-last (setf (shader-input-variable-cdr program-inputs-last)
                                          (shader-input-variable-cons item nil)))
          (let ((cons (shader-input-variable-cons item nil)))
            (if fragment-inputs*
                (setf (shader-input-variable-cdr current-cons) cons)
                (setf fragment-inputs* cons))
            (setf current-cons cons))))))

(define-function insert-into-shader-program-hash-table
    ((shader-programs hash-table)
     (vertex-shader (maybe shader))
     (fragment-shader (maybe shader))
     (shader-program shader-program))
  (error-unless vertex-shader
                'missing-object-error
                :printable-type-name "vertex shader"
                :object-name (name shader-program)
                :details (format nil
                                 "This is required by the program ~A"
                                 (vertex-shader shader-program)))
  (error-unless fragment-shader
                'missing-object-error
                :printable-type-name "fragment shader"
                :object-name (name shader-program)
                :details (format nil
                                 "This is required by the program ~A"
                                 (vertex-shader shader-program)))
  (let* ((program-inputs (inputs vertex-shader))
         (vertex-outputs (outputs vertex-shader))
         (fragment-inputs (inputs fragment-shader))
         (program-outputs (shader-output-variable-cons (find-gl-position vertex-shader vertex-outputs)
                                                       (outputs fragment-shader))))
    (declare (shader-input-variable-list program-inputs fragment-inputs)
             (shader-output-variable-list vertex-outputs program-outputs))
    (check-fragment-shader-input vertex-outputs
                                 (process-fragment-inputs fragment-inputs program-inputs))
    (psetf (inputs shader-program) program-inputs
           (outputs shader-program) program-outputs))
  (insert-unique-hash-table-value shader-programs
                                  (name shader-program)
                                  shader-program
                                  "Shader program names must be unique, but ~A is not!")
  nil)

;;; First, this function makes a shader hash table, with the shader
;;; names as the keys. Then, it makes a shader program hash table,
;;; using the shaders stored in the shader hash table. Both hash
;;; tables are stored in the shader-data object.
;;;
;;; This is a helper function for the shader-data constructor.
(defun %make-shader-data (shader-data)
  (with-accessors* (shader-programs shaders)
      shader-data
    (check-type shaders (and list (not null)))
    (check-type shader-programs (and list (not null)))
    (let ((shaders* (make-hash-table))
          (shader-programs* (make-hash-table)))
      (dolist (shader shaders)
        (insert-into-shader-hash-table shaders* shader))
      (dolist (shader-program shader-programs)
        (check-type shader-program shader-program)
        (dolist (shader-name (shaders shader-program))
          (check-shader-existence shaders* shader-name))
        (insert-into-shader-program-hash-table shader-programs*
                                               (gethash (vertex-shader shader-program) shaders*)
                                               (gethash (fragment-shader shader-program) shaders*)
                                               shader-program))
      (psetf shaders shaders*
             shader-programs shader-programs*))))

(defmethod initialize-instance :after ((shaders shaders) &rest init-args)
  (declare (ignore init-args))
  (%make-shader-data shaders))

(defun check-shaders (shaders)
  (with-accessors* (shader-programs shaders)
      shaders
    (check-type shaders hash-table)
    (check-type shader-programs hash-table)
    (error-when (zerop (hash-table-count shaders))
                'empty-container-error
                :container shaders
                :details "You need shaders.")
    (error-when (zerop (hash-table-count shader-programs))
                'empty-container-error
                :container shader-programs
                :details "You need shader programs.")
    (do-hash-table (shader-name shader shaders)
      (check-type shader-name keyword)
      (check-type shader shader)
      ;; TODO: replace with custom-defined condition
      (error-unless (eql (name shader) shader-name)
                    "I thought the shader name was ~A, but it is ~A"
                    shader-name
                    (name shader)))
    (do-hash-table (program-name shader-program shader-programs)
      (check-type program-name keyword)
      (check-type shader-program shader-program)
      ;; TODO: replace with custom-defined condition
      (error-unless (eql (name shader-program) program-name)
                    "I thought the program name was ~A, but it is ~A"
                    program-name
                    (name shader-program))
      (dolist (shader-name (shaders shader-program))
        (check-shader-existence shaders shader-name))))
  t)

;;;; Shader and shader program definitions

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun function-version (name)
    (if (fboundp name)
        (multiple-value-bind (data version)
            (funcall (fdefinition name))
          (declare (ignore data))
          (if (and version (integerp version))
              (incf version)
              1))
        0)))

(defmacro zrvl:define-shader ((name shader-stage) inputs outputs &body body)
  "
`define-shader' describes shaders in s-expressions that are then
turned into SPIR-V. The ZRVL shader globals are represented by lists
of in and out variables. Other output variables can also be set in the
list of output variables. They will then be placed inside the SPIR-V
main function when the SPIR-V is generated.

The main function itself is defined in the body of `define-shader' and
anything there is run before the out variables are created. The end
result is something loosely resembling the `do' macro, including the
possibility for shaders to not have a body. You should be able to tell
if it is in the body by the indentation level in a properly-configured
editor.
"
  `(defun ,name ()
     (values ,(let ((shader (make-instance 'shader
                                           :name (symbol-to-keyword name)
                                           :stage shader-stage
                                           :data (let ((*shader-name* name))
                                                   (shader-body* shader-stage inputs outputs body))
                                           :inputs (shader-inputs inputs)
                                           :outputs (shader-outputs outputs))))
                (handler-case shader
                  (style-warning (condition)
                    (format *error-output* "~%; Caught style-warning in ZRVL shader ~A:~%" name)
                    (format *error-output* ";   ~A~%" condition)
                    shader)))
             ,(function-version name))))

(defmacro zrvl:define-shader-data (shader-data-name shader-programs &body shaders)
  "
Defines a function with the name SHADER-DATA-NAME that uses the
defined SHADER-PROGRAMS with a body consisting of function names that
return shader objects to create a data structure representing shaders
and shader programs.
"
  (let ((shaders* (mapcar #'list shaders))
        (shader-programs* (mapcar (destructuring-lambda (shader-program-name options &rest shader-list)
                                    (apply #'make-shader-program
                                           shader-program-name
                                           :options options
                                           shader-list))
                                  shader-programs)))
    `(defun ,shader-data-name ()
       (make-instance 'shaders
                      :shaders (list ,@shaders*)
                      :shader-programs (list ,@shader-programs*)))))

;;;; OpenGL

(define-function load-shaders ((shaders hash-table))
  "Compiles the high-level shader objects"
  (let ((compiled-shaders (make-hash-table)))
    (do-hash-table (k shader shaders compiled-shaders)
      (declare (ignore k))
      (let ((name (name shader)))
        (setf (gethash name compiled-shaders)
              (make-spir-v-shader* (shader-binary shader)
                                   (shader-stage shader)
                                   name))))))

(define-function link-gl-program (compiled-shaders &optional (debug nil boolean))
  "Creates a GL program from a list of shaders, with optional error checking"
  (let ((program (create-program)))
    (unwind-protect
         (progn
           (map nil (curry #'attach-shader program) compiled-shaders)
           (link-program program)
           (error-when (and debug (not (get-program program :link-status)))
                       'opengl-shader-error
                       :action "linking"
                       :mesage (get-shader-info-log program)))
      (map nil (curry #'detach-shader program) compiled-shaders))
    program))

(define-function make-gl-shader-programs ((shader-programs hash-table)
                                          (compiled-shaders hash-table)
                                          (debug boolean))
  "Processes the high-level shader program objects using `link-gl-program'"
  (let ((programs (make-hash-table)))
    (do-hash-table (k shader-program shader-programs programs)
      (declare (ignore k))
      (setf (gethash (name shader-program) programs)
            (link-gl-program (mapcar (rcurry #'gethash compiled-shaders)
                                     (shaders shader-program))
                             debug)))))
