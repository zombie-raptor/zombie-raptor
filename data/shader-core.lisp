(defpackage #:zombie-raptor/data/shader-core
  (:documentation "The core representation of the shader, needed by other shader packages.")
  (:use #:cl
        #:zr-utils)
  (:import-from #:cffi
                #:translate-camelcase-name)
  (:export #:do-shader-input-variable-list
           #:do-shader-output-variable-list
           #:find-shader-variable-location
           #:glsl-name
           #:shader-input-storage-qualifier
           #:shader-input-variable-cdr
           #:shader-input-variable-cons
           #:shader-input-variable-last
           #:shader-input-variable-list
           #:shader-input-variable-location
           #:shader-input-variable-name
           #:shader-input-variable-storage-qualifier
           #:shader-input-variable-type
           #:shader-inputs
           #:shader-output-variable-cdr
           #:shader-output-variable-cons
           #:shader-output-variable-list
           #:shader-output-variable-name
           #:shader-output-variable-type
           #:shader-outputs
           #:shader-stage
           #:shader-to-glsl-string
           #:shader-type
           #:shader-uniform-storage-qualifier
           #:swizzle?))

(in-package #:zombie-raptor/data/shader-core)

(deftype shader-type ()
  "All supported GLSL types"
  'keyword)

(deftype shader-input-storage-qualifier ()
  "The storage qualifiers for input storage."
  `(and keyword (member :in :uniform :uniform-block)))

(deftype shader-uniform-storage-qualifier ()
  "The storage qualifiers for uniform storage."
  `(and keyword (member :uniform :uniform-block)))

(deftype shader-stage ()
  "All shader stage names"
  `(and keyword (member :compute-shader :vertex-shader
                        :tess-control-shader :tess-evaluation-shader
                        :geometry-shader :fragment-shader)))

(deftype swizzle-xyzw ()
  "Valid parts of an XYZW swizzle function name"
  `(member #\X #\Y #\Z #\W))

(deftype swizzle-rgba ()
  "Valid parts of an RGBA swizzle function name"
  `(member #\R #\G #\B #\A))

(deftype swizzle-stpq ()
  "Valid parts of an STPQ swizzle function name"
  `(member #\S #\T #\P #\Q))

(defstruct shader-input-variable
  "A GLSL shader input variable, assumed immutable."
  (name              nil :type keyword)
  (type              nil :type shader-type)
  (storage-qualifier nil :type shader-input-storage-qualifier)
  (location          -1  :type (signed-byte 16))
  (block-name        nil :type (maybe keyword)))

(define-typed-list shader-input-variable)

(define-simple-slot-saving-for-class shader-input-variable)

(defstruct shader-output-variable
  "A GLSL shader output variable, assumed immutable."
  (name     nil :type keyword)
  (type     nil :type shader-type)
  (location  -1 :type (signed-byte 16)))

(define-typed-list shader-output-variable)

(define-function (shader-input-variable :inline t) (name type storage-qualifier &key block-name location)
  (make-shader-input-variable :name (symbol-to-keyword name)
                              :type type
                              :storage-qualifier storage-qualifier
                              :location (if location location -1)
                              :block-name (if block-name
                                              (symbol-to-keyword block-name)
                                              block-name)))

(define-simple-slot-saving-for-class shader-output-variable)

(define-function (shader-output-variable :inline t) (name type &key location)
  (make-shader-output-variable :name (symbol-to-keyword name)
                               :type type
                               :location (if location location -1)))

(define-function find-shader-variable-location ((variable-name symbol)
                                                (inputs shader-input-variable-list))
  (do-shader-input-variable-list (input inputs -1)
    (when (eql (shader-input-variable-name input)
               variable-name)
      (return-from find-shader-variable-location
        (shader-input-variable-location input)))))

(define-function shader-inputs ((shader-variables list))
  (let* ((result (shader-input-variable-cons (shader-input-variable :temporary :bool :in) nil))
         (active-cons result))
    (do-destructuring-bind ((storage-qualifier &rest variable-info)
                            shader-variables
                            (shader-input-variable-cdr result))
      (if (eql storage-qualifier :uniform-block)
          (destructuring-bind (block-name &rest block-variables)
              variable-info
            (do-destructuring-bind ((name type &key interpolation) block-variables)
              (declare (ignore interpolation))
              (setf active-cons
                    (setf (shader-input-variable-cdr active-cons)
                          (shader-input-variable-cons (shader-input-variable name
                                                                             (symbol-to-keyword type)
                                                                             storage-qualifier
                                                                             :block-name block-name)
                                                      nil)))))
          (destructuring-bind (name type &key location &allow-other-keys)
              variable-info
            (setf active-cons
                  (setf (shader-input-variable-cdr active-cons)
                        (shader-input-variable-cons (shader-input-variable name
                                                                           (symbol-to-keyword type)
                                                                           storage-qualifier
                                                                           :location location)
                                                    nil))))))))

(defun shader-outputs (shader-output-variables)
  (list-to-shader-output-variable-list shader-output-variables
                                       (destructuring-lambda (storage-qualifier name type &key location &allow-other-keys)
                                         (declare (ignore storage-qualifier))
                                         (shader-output-variable name (symbol-to-keyword type) :location location))))

(define-function swizzle? ((symbol symbol))
  (let ((string (symbol-name symbol)))
    (and (<= 1 (length string) 4)
         (or (every (lambda (c) (typep c 'swizzle-xyzw)) string)
             (every (lambda (c) (typep c 'swizzle-rgba)) string)
             (every (lambda (c) (typep c 'swizzle-stpq)) string)))))

(define-function glsl-name ((symbol symbol) &optional upper-case-start?)
  "Converts a Lisp symbol to a GLSL identifier."
  (let ((camelcase-string (translate-camelcase-name symbol :special-words '("ID" "1D" "2D" "3D" "MS"))))
    (cond ((and (> (length camelcase-string) 2)
                (string= camelcase-string "gl" :end1 2)
                (upper-case-p (elt camelcase-string 2)))
           (concatenate 'string "gl_" (subseq camelcase-string 2)))
          (upper-case-start?
           (setf (char camelcase-string 0)
                 (char-upcase (char camelcase-string 0)))
           camelcase-string)
          (t
           camelcase-string))))
