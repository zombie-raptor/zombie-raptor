(cl:defpackage #:zombie-raptor/data/terrain
  (:documentation "Turns a simple terrain description into 3D geometry.")
  (:use #:cl
        #:zombie-raptor/data/model
        #:zombie-raptor/data/texture
        #:zr-utils)
  (:import-from #:static-vectors
                #:make-static-vector)
  (:export #:+chunk-size+
           #:generate-terrain-map
           #:make-terrain
           #:terrain-chunk
           #:terrain-map-texture
           #:terrain-model))

(cl:in-package #:zombie-raptor/data/terrain)

;;; Constants

;;; How large each item that goes to the GPU has. 3 means (x, y, z)
;;; while 4 means (x, y, z, w)
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defconstant +vertex+ 3)
  (defconstant +normal+ 3)
  (defconstant +weight+ 3)
  (defconstant +texcoord+ 2)
  (defconstant +num-textures+ 3)
  (defconstant +vertex-size+ (+ +vertex+ +normal+ +weight+ (* +num-textures+ +texcoord+))))

;;; Geometric shape of one item in grid
(defconstant +tri+ 3)

;;; Geometric shape of the grid itself
(defconstant +tiles-per-row+ 128)
(defconstant +tiles-per-column+ 128)
(defconstant +chunk-size+ (* +tiles-per-row+ +tiles-per-column+))

(defconstant +triangles-per-shape+ 2)
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defconstant +total-elements+ (* +tri+ +triangles-per-shape+ (1- +tiles-per-column+) (1- +tiles-per-row+))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defconstant +vertex-count+ (* +tiles-per-column+ +tiles-per-row+))
  (defconstant +vertex-array-size+ (* +vertex-size+ +vertex-count+)))

;;; Types

(deftype terrain-chunk ()
  `(simple-array uint8 (,+chunk-size+)))

(defstruct (terrain (:constructor %make-terrain))
  (model nil :type model)
  (map-texture nil :type texture))

;;; Helper functions

(define-function (%vertex-array-index :inline t) (vertex-number)
  (* +vertex-size+ vertex-number))

(define-function (%texel-coords :inline t) (x y)
  (values x y))

;;; Arrays used in %make-terrain

(deftype vertex-array ()
  `(simple-array single-float (,+vertex-array-size+)))

(deftype vertex-number ()
  `(mod ,+vertex-count+))

(define-function (make-vertex-array :inline t) ()
  (make-static-vector +vertex-array-size+
                      :element-type 'single-float
                      :initial-element 0f0))

(deftype element-array ()
  `(simple-array uint16 (,+total-elements+)))

(define-function (make-element-array :inline t) ()
  (make-static-vector +total-elements+
                      :element-type 'uint16
                      :initial-element 0))

(deftype element-number ()
  `(mod ,+total-elements+))

(deftype column ()
  `(integer 0 ,+tiles-per-column+))

(deftype row ()
  `(integer 0 ,+tiles-per-row+))

;;; Generate the terrain

(define-function %make-grid-elements ((element-array element-array))
  (loop :for k :of-type fixnum :from 0 :by (* +tri+ +triangles-per-shape+)
        :for row0-elt :of-type (mod #.(1+ +total-elements+)) := 0
          :then (let ((inc (1+ row0-elt)))
                  (if (= (1- +tiles-per-row+) (mod inc +tiles-per-row+))
                      (1+ inc)
                      inc))
        :for row1-elt :of-type (mod #.(1+ +total-elements+)) := (+ row0-elt +tiles-per-row+)
        :repeat (* (- +tiles-per-column+ 1) (- +tiles-per-row+ 1))
        :do
           (with-array-accessors ((triangle-1 element-array :range 3)
                                  (triangle-2 element-array :range 3 :offset 3))
               k
             (psetf triangle-1 (values (+ row1-elt 0) (+ row1-elt 1) (+ row0-elt 1))
                    triangle-2 (values (+ row0-elt 0) (+ row1-elt 0) (+ row0-elt 1))))
        :finally (return element-array)))

(define-function %make-grid-vertices ((vertex-array vertex-array))
  (declare (optimize (speed 3)))
  (loop :with width := 0.5f0
        :for i :of-type fixnum :from 0
        :for col :of-type column := 0 :then (mod (1+ col) +tiles-per-row+)
        :for row :of-type row := 0 :then (if (zerop (mod i +tiles-per-row+)) (1+ row) row)
        :for x* := (* 2f0 width (/ (float* col) (float* +tiles-per-row+)))
        :for y* := (* 2f0 width (/ (float* row) (float* +tiles-per-column+)))
        :until (>= i (* +tiles-per-row+ +tiles-per-column+))
        :do
           (with-array-accessors ((vertex vertex-array
                                          :range 3
                                          := (values (- (* col width) (* 0.5f0 width +tiles-per-row+))
                                                     0f0
                                                     (- (* row width) (* 0.5f0 width +tiles-per-column+))))
                                  (normal vertex-array
                                          :range 3
                                          :offset 3
                                          := (values 0f0 1f0 0f0))
                                  (weight vertex-array
                                          :range 3
                                          :offset 6
                                          := (values 1f0 0f0 0f0))
                                  (texcoord-1 vertex-array
                                              :range 2
                                              :offset 9
                                              := (%texel-coords x* y*))
                                  (texcoord-2 vertex-array
                                              :range 2
                                              :offset 11
                                              := (%texel-coords x* y*))
                                  (texcoord-3 vertex-array
                                              :range 2
                                              :offset 13
                                              := (%texel-coords x* y*)))
               (%vertex-array-index i))
        :finally (return vertex-array)))

(define-function %make-texture-data ((terrain-codes terrain-chunk))
  (let ((texture-data (make-texture-data (length terrain-codes) :initial-element 0)))
    (declare (type (texture-data #.+chunk-size+) texture-data))
    (replace texture-data terrain-codes)
    texture-data))

(define-function make-terrain-map ((terrain-codes terrain-chunk) (name keyword))
  (make-instance 'texture
                 :name name
                 :height (isqrt +chunk-size+)
                 :width (isqrt +chunk-size+)
                 :depth 1
                 :texel-size 1
                 :dimension 2
                 :linear-filtering? nil
                 :texture-wrap :clamp-to-edge
                 :data (%make-texture-data terrain-codes)))

(define-function make-terrain-model (name
                                     (vertex-array vertex-array)
                                     (element-array element-array)
                                     program
                                     textures)
  (make-instance 'model
                 :name name
                 :vertex-array vertex-array
                 :element-array element-array
                 :attribute-names #(:normal :weight :texcoord-1 :texcoord-2 :texcoord-3)
                 :attribute-sizes `#(,+normal+ ,+weight+ ,+texcoord+ ,+texcoord+ ,+texcoord+)
                 :program program
                 :textures textures))

(define-function make-terrain ((terrain-codes terrain-chunk)
                               &key
                               (terrain-map-name :terrain-map keyword)
                               (model-name :terrain keyword)
                               program
                               texture)
  (%make-terrain :model (make-terrain-model model-name
                                            (%make-grid-vertices (make-vertex-array))
                                            (%make-grid-elements (make-element-array))
                                            program
                                            (vector texture terrain-map-name))
                 :map-texture (make-terrain-map terrain-codes terrain-map-name)))
