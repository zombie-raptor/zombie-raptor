(cl:defpackage #:zombie-raptor/data/game-data
  (:documentation "Reads and writes game data files that aren't handled elsewhere.")
  (:use #:cl
        #:zombie-raptor/data/data-path
        #:zr-utils)
  (:import-from #:uiop/filesystem
                #:file-exists-p)
  (:import-from #:uiop/pathname
                #:make-pathname*
                #:merge-pathnames*)
  (:export #:create-key-bindings-in-directory
           #:extension-table
           #:find-key-bindings
           #:font-path
           #:import-sxp-data
           #:read-sxp
           #:write-configuration-sxp
           #:write-sxp))

(cl:in-package #:zombie-raptor/data/game-data)

;;;; Defines .SXP file format

(defun read-sxp (data-path &key (read-eval nil))
  "
Reads an s-expression (sxp) file at data-path using the Common Lisp
reader. Every s-expression in the file is placed into one
list. Everything read is literal (as if quoted), with no evaluation
unless read-eval is enabled.
"
  (let ((*read-eval* read-eval)
        (eof (gensym "EOF")))
    (with-open-file (file-stream data-path :direction :input)
      (with-input-from-string (start "(")
        (with-input-from-string (end ")")
          (with-open-stream (stream (make-concatenated-stream start file-stream end))
            (let ((result (read stream)))
              ;; Handles unmatched close parenthesis and possibly other errors
              (error-unless (eq eof (read stream nil eof))
                            'reader-error
                            :stream file-stream)
              result)))))))

(defun write-sxp (data-path sxp &key (if-exists :error) (read-case :invert))
  "
Writes an s-expression (sxp) file at the ensured path data-path. sxp
must be a list, and it should be structured so that `read-sxp' can
read it. if-exists supports a keyword from the long list of if-exists
keywords that `open' uses.

read-case is a readtable case keyword, which is :invert by default so
that the capital letters are turned into the more readable lower case
forms. They will then be converted to upper case again when `read-sxp'
is run. If you need this operation to work exactly like `read-sxp',
use :upcase instead, but it will be less readable by humans."
  (let ((*readtable* (copy-readtable nil)))
    (setf (readtable-case *readtable*) read-case)
    (with-open-file (file-stream (ensure-directories-exist data-path)
                                 :direction :output
                                 :if-exists if-exists)
      (dolist (expression sxp)
        (write expression :stream file-stream)
        (terpri file-stream)))
    sxp))

(define-function (%write-formatted-list :inline t) (expression stream)
  (format stream "(~{~S ~S~^~% ~})~%~%" expression))

(define-function (%write-header :inline t) (stream)
  (let ((prefix ";;;; "))
    (write-string prefix stream)
    (write-line "This file was automatically generated! Only edit if you know what" stream)
    (write-string prefix stream)
    (write-line "you're doing!" stream)
    (terpri stream)))

(defun write-configuration-sxp (data-path sxp-type sxp &key (if-exists :error) (read-case :invert) (header t))
  "
This is like `write-sxp', but it assumes that lists are plists. This
function has special handling of strings, characters, and plists. This
is used to produce human-readable plist configuration files that have
two items per line (key and value) and that might contain
comments (represented as strings)."
  (let ((*readtable* (copy-readtable nil)))
    (setf (readtable-case *readtable*) read-case)
    (with-open-file (file-stream (ensure-directories-exist data-path)
                                 :direction :output
                                 :if-exists if-exists)
      (when header
        (%write-header file-stream))
      (%write-formatted-list (list :s-expression sxp-type) file-stream)
      (dolist (expression sxp)
        (typecase expression
          (string (write-string expression file-stream))
          (list (%write-formatted-list expression file-stream))
          (character (write-char expression file-stream))
          (t (write expression :stream file-stream)))))))

(defun create-key-bindings-in-directory (key-bindings-plists app-name &optional org-name)
  (let ((key-bindings-path (merge-pathnames* (local-config-path app-name org-name)
                                             (make-pathname* :name "key-bindings"
                                                             :type "sxp"))))
    (unless (file-exists-p key-bindings-path)
      (write-configuration-sxp key-bindings-path :key-bindings key-bindings-plists))))

(defun import-sxp-data (path)
  "
Imports structured data from an sxp file. The first line must
be (:s-expression name) where name is the type of s-expression, to
dispatch on. Optionally, there can be more arguments past name. If
there are, they are passed in as additional arguments and must be
handled there."
  (let ((data (read-sxp path)))
    (cons (pathname-name path)
          (destructuring-bind (header &rest items)
              data
            (destructuring-bind (file-type file-subtype &key &allow-other-keys)
                header
              (when (eql file-type :s-expression)
                (case file-subtype
                  (:key-bindings (destructuring-bind (item-type item-name &rest item-contents)
                                     (car items)
                                   (declare (ignore item-name))
                                   (when (eql item-type :key-bindings)
                                     item-contents))))))))))

;;;; Loads data

(defun find-key-bindings (app-name &optional org-name)
  (let ((key-bindings-path (merge-pathnames* (local-config-path app-name org-name)
                                             (make-pathname* :name "key-bindings"
                                                             :type "sxp"))))
    (cdr (import-sxp-data key-bindings-path))))

(define-function (font-path :check-type t) ((name string)
                                            (subdirectory string)
                                            &optional
                                            (subsubdirectory nil (or string null)))
  (merge-pathnames* (if subsubdirectory
                        (main-data-directory "zombie-raptor" "fonts" subdirectory subsubdirectory)
                        (main-data-directory "zombie-raptor" "fonts" subdirectory))
                    (make-pathname* :name name :type "ttf")))
