(defpackage #:zombie-raptor/data/texture
  (:documentation "Handles textures via OpenGL.")
  (:use #:cl
        #:zombie-raptor/core/conditions
        #:zombie-raptor/data/model
        #:zr-utils)
  (:import-from #:%gl)
  (:import-from #:cffi
                #:foreign-enum-value)
  (:import-from #:cl-opengl
                #:bind-texture
                #:gen-textures
                #:generate-mipmap
                #:tex-parameter)
  (:import-from #:pngload)
  (:import-from #:static-vectors
                #:free-static-vector
                #:make-static-vector
                #:static-vector-pointer)
  (:import-from #:zombie-raptor/core/constants
                #:+max-texture-depth+
                #:+max-texture-dimension+
                #:+max-texture-size+
                #:+min-texture-dimension+)
  (:export #:all-textures
           #:check-textures
           #:do-texture-list
           #:load-textures
           #:loaded-texture
           #:loaded-texture-id
           #:loaded-texture-type
           #:make-texture-data
           #:make-textures
           #:read-file-into-texture
           #:texture
           #:texture-data
           #:texture-length
           #:texture-name
           #:texture-type-to-enum
           #:textures
           #:with-texture-data-make-texture))

(in-package #:zombie-raptor/data/texture)

(defmacro gl-enum-ecase (keyform &body cases)
  "
Caches the OpenGL enum lookup at compile time and makes sure that the
provided keyform is part of the very tiny subset of expected OpenGL
enums.
"
  `(ecase ,keyform
     ,@(mapcar (lambda (name)
                 (check-type name keyword)
                 `(,name ,(foreign-enum-value '%gl:enum name)))
        cases)))

(defparameter *default-texture-size* 128
  "
The default size of a texture whose size isn't specified. If it is not
a power of two integer in the range specified by the type
texture-size, then a texture whose size isn't specified will be an
error.
")

(deftype texture-data (length)
  `(simple-array (unsigned-byte 8) (,length)))

(deftype texture-size ()
  "
A valid texture size has two sides, each consisting of a power of two
in this range. If it is 3D, the third dimension is its depth, which is
handled separately because it is usually smaller.
"
  `(integer 1 ,+max-texture-size+))

(deftype texture-depth ()
  "
A 2D texture has a depth of 0 or, equivalently, 1. Otherwise, it has a
nonzero depth in this range.
"
  `(integer 1 ,+max-texture-depth+))

(deftype texture-dimension ()
  "
A valid texture has one of these dimensions.
"
  `(integer ,+min-texture-dimension+ ,+max-texture-dimension+))

(deftype texel ()
  "The different types of OpenGL texels supported by the engine."
  `(member :rgba :rgb :rg :red))

(deftype texel-size ()
  "A texel type has to have one of these sizes."
  `(integer 1 4))

(deftype texture-wrap-behavior ()
  "The different ways a texture can wrap in OpenGL"
  `(member :repeat :clamp-to-edge :mirrored-repeat))

(deftype simple-texture-type ()
  `(member :texture-1D :texture-2D :texture-3D :texture-2D-multisample
           :texture-cube-map :texture-rectangle :texture-buffer))

(deftype texture-array-type ()
  `(member :texture-1D-array :texture-2D-array :texture-2D-multisample-array
           :texture-cube-map-array))

(deftype texture-type ()
  `(or simple-texture-type texture-array-type))

(define-function (texel-to-enum :inline t) ((texel texel))
  (declare (optimize (speed 3)))
  (gl-enum-ecase texel
    :red :rg :rgb :rgba))

(define-function (texture-type-to-enum :inline t) ((texture-type texture-type))
  (declare (optimize (speed 3)))
  (gl-enum-ecase texture-type
    :texture-1D :texture-2D :texture-3D :texture-2D-multisample
    :texture-cube-map :texture-rectangle :texture-buffer
    :texture-1D-array :texture-2D-array :texture-2D-multisample-array
    :texture-cube-map-array))

(define-function (power-of-two? :inline t) ((n integer))
  (zerop (logand n (1- n))))

;;;; Texture class

(defclass texture ()
  ((%name
    :accessor name
    :initarg :name
    :checked-type keyword
    :initform (error 'required-input-error
                     :printable-class-name "texture"
                     :missing-input-name "name")
    :documentation "The name of the texture.")
   (%width
    :reader width
    :initarg :width
    :checked-type texture-size
    :initform *default-texture-size*
    :documentation "The width of the texture-data.")
   (%height
    :reader height
    :initarg :height
    :checked-type texture-size
    :initform *default-texture-size*
    :documentation "The height of the texture-data, if > 1 dimension.")
   (%depth
    :reader depth
    :initarg :depth
    :checked-type texture-depth
    :initform 1
    :documentation "The depth of the texture data, if > 2 dimensions.")
   (%dimension
    :reader dimension
    :initarg :dimension
    :checked-type texture-dimension
    :initform 2
    :documentation "The dimension of the texture. If an array, it is actually one dimension larger.")
   (%texture-array?
    :reader texture-array?
    :initarg :texture-array?
    :checked-type boolean
    :initform nil
    :documentation "Whether or not the texture is an array.")
   (%texture-rectangle?
    :reader texture-rectangle?
    :initarg :texture-rectangle?
    :checked-type boolean
    :initform nil
    :documentation "Whether or not the texture is a texture rectangle. It must be 2D.")
   (%texture-buffer?
    :reader texture-buffer?
    :initarg :texture-buffer?
    :checked-type boolean
    :initform nil
    :documentation "Whether or not the texture is a texture buffer. It must be 1D.")
   (%texel-size
    :reader texel-size
    :initarg :texel-size
    :checked-type texel-size
    :initform 4
    :documentation "The size of the texel in the texture-data.")
   (%texel-type
    :reader texel-type
    :checked-type texel
    :documentation "The type of the texel in the texture-data.")
   (%linear-filtering?
    :reader linear-filtering?
    :initarg :linear-filtering?
    :checked-type boolean
    :initform t
    :documentation "Whether or not linear filtering is applied.")
   (%multisample
    :reader multisample
    :initarg :multisample
    :checked-type (integer 0 16)
    :initform 0
    :documentation "If > 0, then it is multisampled this many times.")
   (%texture-wrap
    :reader texture-wrap
    :initarg :texture-wrap
    :checked-type texture-wrap-behavior
    :initform :repeat
    :documentation "The wrapping behavior of the texture at the edges.")
   (%data
    :accessor data
    :initarg :data
    ;; Note: NIL means that the texture-data was freed.
    :checked-type (maybe (texture-data *))
    :initform (error 'required-input-error
                     :printable-class-name "texture"
                     :missing-input-name "texture data array")
    :documentation "The actual texture-data array."))
  (:metaclass checked-types))

(define-typed-list texture)

(define-function check-texture ((texture texture))
  "Makes sure a texture object represents a valid texture."
  (with-accessors* (name width height depth data dimension texel-size texture-array? texture-rectangle? texture-buffer? multisample)
      texture
    (let ((internal-dimension (if texture-array? (1+ dimension) dimension)))
      ;; TODO: Name this error
      (error-when (> internal-dimension 3)
                  "Texture ~A with internal dimension ~D cannot be a texture array"
                  name
                  dimension)
      ;; TODO: Name this error
      (error-when (and (not (zerop multisample))
                       (or (not (= dimension 2))
                           texture-rectangle?))
                  "Texture ~A is an invalid multisample texture."
                  name)
      (cond (texture-buffer?
             ;; TODO: Name this error
             (error-unless (and (= dimension 1)
                                (not (or texture-array?
                                         texture-rectangle?)))
                           "Texture ~A is an invalid texture-buffer."
                           name))
            (texture-rectangle?
             (error-unless (and (= dimension 2) (not texture-array?))
                           'invalid-dimension-for-texture-error
                           :texture-name name
                           :dimension :dimension
                           :texture-problem "2 because it is a texture rectangle"
                           :size dimension))
            (t
             (error-unless (power-of-two? width)
                           'invalid-dimension-for-texture-error
                           :texture-name name
                           :dimension :width
                           :texture-problem "a power of 2"
                           :size width)
             (error-unless (power-of-two? height)
                           'invalid-dimension-for-texture-error
                           :texture-name name
                           :dimension :height
                           :texture-problem "a power of 2"
                           :size height)))
      (error-when (and (< internal-dimension 3) (not (= depth 1)))
                  'invalid-dimension-for-texture-error
                  :texture-name name
                  :dimension :depth
                  :texture-problem 1
                  :size depth)
      (error-when (and (= internal-dimension 1) (not (= height 1)))
                  'invalid-dimension-for-texture-error
                  :texture-name name
                  :dimension :height
                  :texture-problem 1
                  :size height)
      (error-when (null data)
                  'missing-object-error
                  :printable-type-name "texture-data"
                  :object-name "data"
                  :details "It cannot be initialized to NIL.")
      (error-unless (= (length data) (* width height depth texel-size))
                    'invalid-texture-data-error
                    :texture-name name
                    :texture-length (length data)
                    :expected-length (* width height depth texel-size)))))

(defmethod initialize-instance :after ((texture texture) &rest init-args)
  (declare (ignore init-args))
  (setf (slot-value texture '%texel-type)
        (case= (texel-size texture)
          (4 :rgba)
          (3 :rgb)
          (2 :rg)
          (1 :red)))
  (check-texture texture))

(defmethod print-object ((texture texture) stream)
  (print-unreadable-object (texture stream :type t)
    (format stream
            "~A ~D ~D~@[ ~D~]"
            (name texture)
            (width texture)
            (height texture)
            (when (= (dimension texture) 3)
              (depth texture))))
  texture)

(defmethod cleanup ((texture texture))
  (with-accessors* (data)
      texture
    (when data
      (free-static-vector data))
    (setf data nil)))

(defstruct (textures (:constructor %make-textures)
                     (:conc-name nil))
  (all-textures nil :type (maybe texture-list)))

(define-function make-textures ((list list))
  (%make-textures :all-textures (list-to-texture-list list #'identity)))

(defmethod cleanup ((textures textures))
  (with-accessors* (all-textures) textures
    (do-texture-list (texture all-textures)
      (cleanup texture))
    (setf all-textures nil)))

(define-function (make-texture-data :inline t) (length &key (initial-element 0))
  "Creates a texture that is a valid array in both Common Lisp and C."
  (make-static-vector length
                      :element-type '(unsigned-byte 8)
                      :initial-element initial-element))

(defmacro with-texture-data-make-texture ((texture-data-binding texture-name)
                                          (dimension texel-size width
                                           &key (depth 1) (initial-element 0) (linear-filtering? t) (wrap :repeat) (array? nil))
                                          &body body)
  "
Creates a texture object given the necessary texture data, using
`make-texture-data' to create an array that both Common Lisp and
OpenGL's C API will recognize as an array, which avoids slow copying.

Note: This array will have to be freed by calling `cleanup' on the
texture object.
"
  (let ((texture-data-size (* texel-size (expt width 2) (if (zerop depth) 1 depth))))
    `(let ((,texture-data-binding (make-texture-data ,texture-data-size
                                                     :initial-element ,initial-element)))
       (declare ((texture-data ,texture-data-size) ,texture-data-binding))
       ,@body
       (make-instance 'texture
                      :name ,texture-name
                      :height ,width
                      :width ,width
                      :depth ,depth
                      :texture-array? ,array?
                      :texel-size ,texel-size
                      :dimension ,dimension
                      :linear-filtering? ,linear-filtering?
                      :texture-wrap ,wrap
                      :data ,texture-data-binding))))

(defun check-textures (textures)
  "
Checks a list of textures to make sure that all of its contents are
valid textures.
"
  (check-type textures list)
  (dolist (texture textures)
    (check-type texture texture)))

;;;; Load texture from a file

;;; TODO: also have a read-files-into-texture-array

(defun read-file-into-texture (name
                               path
                               &key
                                 (linear-filtering? t)
                                 (wrap :repeat))
  (let* ((png (pngload:load-file path
                                 :decode t
                                 :flip-y t
                                 :flatten t
                                 :static-vector t))
         (height (pngload:height png))
         (width (pngload:width png))
         (length (length (pngload:data png))))
    (make-instance 'texture
                   :name name
                   :height height
                   :width width
                   ;; Reconstruct what the texel size must be because
                   ;; it is not exposed by the library.
                   :texel-size (floor length (* width height))
                   :dimension 2
                   :linear-filtering? linear-filtering?
                   :texture-wrap wrap
                   :data (pngload:data png))))

;;;; OpenGL texture creation

;; TODO: buffer textures are sufficiently different that they can
;; probably go in their own function
(define-function make-gl-texture ((name keyword)
                                  (width texture-size)
                                  (height texture-size)
                                  (depth texture-depth)
                                  (max-anisotropy (unsigned-byte 32))
                                  (index (unsigned-byte 32))
                                  (texture (texture-data *))
                                  (texel-type texel)
                                  (linear-filtering? boolean)
                                  (wrap texture-wrap-behavior)
                                  (texture-type texture-type)
                                  (samples (integer 0 16))
                                  (buffer? boolean)
                                  (debug boolean))
  "Initializes an OpenGL texture given the necessary data."
  (let ((texel-type (texel-to-enum texel-type))
        (element-type :unsigned-byte)
        (pointer (static-vector-pointer texture))
        (dimension (ecase texture-type
                     ((:texture-1D :texture-buffer)
                      1)
                     ((:texture-2D :texture-rectangle :texture-1D-array :texture-cube-map :texture-2D-multisample)
                      2)
                     ((:texture-3D :texture-2D-array :texture-cube-map-array :texture-2D-multisample-array)
                      3)))
        (mipmap? (not (member texture-type '(:texture-rectangle :texture-buffer
                                             :texture-2D-multisample :texture-2D-multisample-array))))
        (multisample? (member texture-type '(:texture-2D-multisample :texture-2D-multisample-array))))
    (when (and debug (every #'zerop texture))
      (warn "The texture ~A is empty." name))
    (multiple-value-bind (mag-filter min-filter)
        (if linear-filtering?
            (values :linear
                    (if mipmap? :linear-mipmap-linear :linear))
            (values :nearest
                    (if mipmap? :nearest-mipmap-nearest :nearest)))
      (bind-texture texture-type index)
      (unless buffer?
        (tex-parameter texture-type :texture-wrap-s wrap)
        (tex-parameter texture-type :texture-wrap-t wrap)
        (tex-parameter texture-type :texture-max-anisotropy-ext max-anisotropy)
        (tex-parameter texture-type :texture-min-filter min-filter)
        (tex-parameter texture-type :texture-mag-filter mag-filter))
      (cond (buffer?
             ;; TODO: %gl:tex-buffer-range also works
             ;;
             ;; TODO: fixme: texel-type is almost certainly wrong
             ;; here, but this doesn't need to be fixed until full
             ;; support for buffer textures is added to the engine
             (%gl:tex-buffer texture-type texel-type pointer))
            (multisample?
             (ecase= dimension
               (2 (%gl:tex-image-2D-multisample texture-type samples texel-type width height pointer))
               (3 (%gl:tex-image-3D-multisample texture-type samples texel-type width height depth pointer))))
            (t
             (ecase= dimension
               (1 (%gl:tex-image-1D texture-type 0 texel-type width 0 texel-type element-type pointer))
               (2 (%gl:tex-image-2D texture-type 0 texel-type width height 0 texel-type element-type pointer))
               (3 (%gl:tex-image-3D texture-type 0 texel-type width height depth 0 texel-type element-type pointer)))))
      (when mipmap?
        (generate-mipmap texture-type))
      (bind-texture texture-type 0)
      index)))

(defstruct loaded-texture
  (id             0 :type uint32)
  (type :texture-2D :type keyword))

(define-function load-texture ((textures hash-table)
                               (max-anisotropy (unsigned-byte 32))
                               (texture-count fixnum)
                               (texture texture)
                               (debug boolean))
  "Loads a texture into the textures hash table."
  ;; TODO: :texture-cube-map :texture-cube-map-array
  (let* ((texture-type (cond ((texture-buffer? texture) :texture-buffer)
                             ((texture-rectangle? texture) :texture-rectangle)
                             ((not (zerop (multisample texture)))
                              (case= (dimension texture)
                                (2 (if (texture-array? texture)
                                       :texture-2D-multisample-array
                                       :texture-2D-multisample))
                                (t (error 'texture-dimension-error :dimension (dimension texture)))))
                             (t
                              (case= (dimension texture)
                                (1 (if (texture-array? texture) :texture-1D-array :texture-1D))
                                (2 (if (texture-array? texture) :texture-2D-array :texture-2D))
                                (3 :texture-3D)
                                (t (error 'texture-dimension-error :dimension (dimension texture)))))))
         (texture-id (make-gl-texture (name texture)
                                      (width texture)
                                      (height texture)
                                      (depth texture)
                                      max-anisotropy
                                      texture-count
                                      (data texture)
                                      (texel-type texture)
                                      (linear-filtering? texture)
                                      (texture-wrap texture)
                                      texture-type
                                      (multisample texture)
                                      (texture-buffer? texture)
                                      debug)))
    (setf (gethash (name texture) textures)
          (make-loaded-texture :id texture-id
                               :type texture-type))))

(define-function load-textures ((textures textures)
                                (max-anisotropy (unsigned-byte 32))
                                (debug boolean))
  "
Loads a list of textures into a new hash table and returns that hash
table.
"
  (gen-textures (texture-length (all-textures textures)))
  (loop :for texture-id :of-type (unsigned-byte 32) :from 1
        :with texture-hash-table := (make-hash-table)
        :for texture* := (all-textures textures) :then (and texture* (texture-cdr texture*))
        :for texture := (and texture* (texture-car texture*))
        :while texture*
        :do (load-texture texture-hash-table
                          max-anisotropy
                          texture-id
                          texture
                          debug)
        :finally (return texture-hash-table)))
