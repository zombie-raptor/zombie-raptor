(defpackage #:zombie-raptor/data/model
  (:documentation "Generates 3D models.")
  (:use #:cl
        #:zombie-raptor/core/conditions
        #:zombie-raptor/math/matrix
        #:zombie-raptor/math/quaternion
        #:zombie-raptor/math/vector
        #:zombie-raptor/zrvl/zrvl
        #:zr-utils)
  (:import-from #:alexandria
                #:doplist)
  (:import-from #:static-vectors
                #:free-static-vector
                #:make-static-vector)
  (:export #:all-models
           #:attribute-names
           #:attribute-sizes
           #:attribute2
           #:attribute3
           #:begin
           #:count*
           #:cuboid
           #:do-model-list
           #:element-array
           #:element-place-counter
           #:element-vertex-counter
           #:end
           #:make-cube
           #:make-cuboid
           #:make-equilateral-triangle
           #:make-hexagon
           #:make-models
           #:make-rectangle
           #:make-square
           #:model
           #:model-list
           #:model-textures
           #:models
           #:procedural-model
           #:program
           #:set-elements
           #:stride
           #:vertex-array
           #:vertex-size
           #:vertex3))

(in-package #:zombie-raptor/data/model)

(defconstant +vertex-size+ 3)
(defconstant +normal-size+ 3)
(defconstant +color-size+ 3)
(defconstant +texcoord-size+ 2)

(defgeneric count* (object))

;;;; Calculate normals

(define-function (triangle-normal :inline t :default-type vec3) (result-vector vec1 vec2 vec3)
  (with-zrvl
      ((vec1 zrvl:vec3)
       (vec2 zrvl:vec3)
       (vec3 zrvl:vec3))
      ((result-vector zrvl:vec3))
    (zrvl-int:setf result-vector (zrvl:normalize (zrvl:cross (zrvl:- vec2 vec1)
                                                             (zrvl:- vec3 vec1)))))
  result-vector)

(define-function (vertex-normal :inline t) ((triangle-normals vector))
  (let ((sum-of-normals (reduce (lambda (combined-vec new-vec)
                                  (declare (vec3 combined-vec new-vec))
                                  (setf (array-of-3 combined-vec)
                                        (multiple-value-call #'vec+/mv
                                          (array-of-3 combined-vec)
                                          (array-of-3 new-vec))))
                                triangle-normals
                                :initial-value (vec3 0f0 0f0 0f0))))
    (map-into sum-of-normals
              (lambda (x)
                (declare (single-float x))
                (/ x (length triangle-normals)))
              sum-of-normals)))

;;;; 3D models

(defclass model ()
  ((%name
    :accessor name
    :initarg :name
    :documentation "The name of the 3D model.")
   (%vertex-array
    :accessor vertex-array
    :initarg :vertex-array)
   (%element-array
    :accessor element-array
    :initarg :element-array)
   (%program
    :accessor program
    :initarg :program)
   (%textures
    :accessor model-textures
    :initarg :textures)
   (%vertex-size
    :initform +vertex-size+
    :reader vertex-size
    :initarg :vertex-size)
   (%attribute-sizes
    :reader attribute-sizes
    :initarg :attribute-sizes
    :initform (make-array 3 :initial-contents (list +normal-size+
                                                    +color-size+
                                                    +texcoord-size+)))
   (%attribute-names
    :reader attribute-names
    :initarg :attribute-names
    :initform (make-array 3 :initial-contents (list :normal
                                                    :color
                                                    :texcoord)))
   (%stride
    :accessor stride)))

(define-typed-list model)

(defmethod initialize-instance :after ((model model) &rest init-args)
  (declare (ignore init-args))
  (with-accessors* (attribute-sizes vertex-size)
      model
    (setf (stride model) (+ vertex-size (reduce #'+ attribute-sizes)))))

(defmethod cleanup ((model model))
  (with-accessors* (element-array vertex-array)
      model
    (when vertex-array
      (free-static-vector vertex-array))
    (when element-array
      (free-static-vector element-array))
    (psetf vertex-array nil
           element-array nil)))

(defstruct (models (:constructor %make-models)
                   (:conc-name nil))
  (all-models nil :type (maybe model-list)))

(define-function make-models ((list list))
  (%make-models :all-models (list-to-model-list list #'identity)))

(defmethod cleanup ((models models))
  (with-accessors* (all-models) models
    (do-model-list (model all-models)
      (cleanup model))
    (setf all-models nil)))

(defmethod count* ((models models))
  (with-accessors* (all-models) models
    (model-length all-models)))

;;;; Generate models

(defclass procedural-model (model)
  ((%attributes
    :accessor attributes)
   (%vertex-count
    :accessor vertex-count)
   (%element-count
    :accessor element-count)
   (%vertex-counter
    :initform 0
    :accessor vertex-counter)
   (%element-vertex-counter
    :initform 0
    :accessor element-vertex-counter)
   (%element-place-counter
    :initform 0
    :accessor element-place-counter)))

(defun attribute3 (state attribute a b c &optional d)
  (declare (ignore d))
  (with-accessors* (attributes)
      state
    (replace (getf attributes attribute) (vector a b c))))

(defun attribute2 (state attribute a b)
  (with-accessors* (attributes)
      state
    (replace (getf attributes attribute) (vector a b))))

(define-function (%set-vertex-attributes :inline t) (state)
  (with-accessors* ((array vertex-array)
                    attributes
                    (counter vertex-counter)
                    stride)
      state
    (let ((i (+ 3 counter)))
      (doplist (attribute-name attribute-array attributes)
        (dotimes (j (length attribute-array))
          (setf (aref array i) (elt attribute-array j))
          (incf i)))
      (incf counter stride))))

(defun vertex3 (state x y z &optional w)
  (declare (ignore w))
  (with-accessors* ((array vertex-array)
                    attributes
                    (counter vertex-counter))
      state
    (setf (aref array (+ 0 counter)) x
          (aref array (+ 1 counter)) y
          (aref array (+ 2 counter)) z)
    (%set-vertex-attributes state)))

(defmacro vertex3-with-transformation (state matrix x y z)
  `(multiple-value-call #'vertex3 ,state (matrix*vector/mv ,matrix ,x ,y ,z)))

(defmacro attribute3-with-transformation (state name matrix x y z)
  `(multiple-value-call #'attribute3 ,state ,name (matrix*vector/mv ,matrix ,x ,y ,z)))

;;; Define a BEGIN for "immediate mode" generation
(defun begin (state &key vertices elements)
  (with-accessors* (attribute-names
                    (attribute-plist attributes)
                    attribute-sizes
                    element-array
                    element-count
                    stride
                    vertex-array
                    vertex-count)
      state
    (let ((type 'single-float))
      (setf vertex-count vertices
            element-count elements
            vertex-array (make-static-vector (* stride vertices) :element-type type)
            element-array (make-static-vector elements :element-type '(unsigned-byte 16))
            attribute-plist (make-list (* 2 (length attribute-sizes))))
      (dotimes (i (length attribute-sizes))
        (setf (elt attribute-plist (* 2 i)) (elt attribute-names i)
              (elt attribute-plist (1+ (* 2 i))) (make-array (elt attribute-sizes i) :element-type type))))))

(define-function (%elements-list-max :inline t) ((elements list))
  (reduce (lambda (e1 e2)
            (declare (fixnum e1 e2))
            (max e1 e2))
          elements
          :initial-value 0))

(defun set-elements (state &rest elements)
  (with-accessors* (element-array element-place-counter element-vertex-counter)
      state
    (declare ((simple-array (unsigned-byte 16) (*)) element-array))
    (map nil
         (lambda (element)
           ;; needed to silence an sbcl warning on (elt elements i)
           (check-type element fixnum))
         elements)
    (dotimes (i (length elements))
      (setf (aref element-array (+ element-place-counter i)) (+ element-vertex-counter (elt elements i))))
    (incf element-place-counter (length elements))
    (incf element-vertex-counter (1+ (%elements-list-max elements)))))

(defun end (state)
  "Checks for errors in the state construction and if there are none,
the state is returned"
  (with-accessors* (vertex-count vertex-counter stride)
      state
    (error-unless (= vertex-count (/ vertex-counter stride))
                  'model-vertex-count-error
                  :vertices-expected vertex-count
                  :vertices-given (/ vertex-counter stride)))
  state)

;;;; Basic shapes
;;;;
;;;; These shapes are basic building blocks on top of the immediate
;;;; mode for everything else. Any 3D shape is built from multiple 2D
;;;; faces.

(defun triangle-face (state
                      transformation-matrix
                      rotation-matrix
                      x1 y1 x2 y2 x3 y3
                      s1 t1 s2 t2 s3 t3)
  "Creates a triangle face, then rotates it, and then translates it to
produce an arbitrary triangle in 3D space as part of a larger mesh."
  (set-elements state 0 1 2)
  (attribute3-with-transformation state :normal rotation-matrix 0f0 0f0 1f0)
  (attribute2 state :texcoord s1 t1)
  (vertex3-with-transformation state transformation-matrix x1 y1 0f0)
  (attribute2 state :texcoord s2 t2)
  (vertex3-with-transformation state transformation-matrix x2 y2 0f0)
  (attribute2 state :texcoord s3 t3)
  (vertex3-with-transformation state transformation-matrix x3 y3 0f0))

(defun quadrilateral-face (state
                           transformation-matrix
                           rotation-matrix
                           x1 y1 x2 y2 x3 y3 x4 y4
                           s1 t1 s2 t2 s3 t3 s4 t4)
  "Creates a quadrilateral face, then rotates it, and then translates
it to produce an arbitrary quad face in 3D space as part of a larger
mesh."
  (set-elements state 0 1 2 2 3 0)
  (attribute3-with-transformation state :normal rotation-matrix 0f0 0f0 1f0)
  (attribute2 state :texcoord s1 t1)
  (vertex3-with-transformation state transformation-matrix x1 y1 0f0)
  (attribute2 state :texcoord s2 t2)
  (vertex3-with-transformation state transformation-matrix x2 y2 0f0)
  (attribute2 state :texcoord s3 t3)
  (vertex3-with-transformation state transformation-matrix x3 y3 0f0)
  (attribute2 state :texcoord s4 t4)
  (vertex3-with-transformation state transformation-matrix x4 y4 0f0))

;;;; Derived shapes
;;;;
;;;; These shapes are common, easier to use shapes that are derived
;;;; from the basic faces so that managing points doesn't become a
;;;; mess.

(defun equilateral-triangle-face (state &key
                                          (scale 1f0)
                                          rotation-quaternion
                                          (x-translate 0f0)
                                          (y-translate 0f0)
                                          (z-translate 0f0)
                                          tilep)
  "Scales, then rotates, then translates an equilateral triangle in 3D
space as part of a larger mesh."
  (let ((m1 (translate x-translate y-translate z-translate))
        (m2 (multiple-value-call #'quaternion-rotation-matrix
              (if rotation-quaternion
                  (array-of-4 rotation-quaternion)
                  (values 0f0 0f0 0f0 1f0)))))
    (declare (matrix m1 m2)
             (dynamic-extent m1 m2))
    (when rotation-quaternion
      (matrix* m1 m1 m2))
    (let* ((x-min (* scale -0.5f0))
           (x-max (- x-min))
           (x-half 0f0)
           (y-min (* scale -0.5f0))
           (y-max (- y-min))
           (s1 0f0)
           (s1.5 0.5f0)
           (s2 1f0)
           (t1 0f0)
           (t2 1f0))
      (when tilep
        (setf s1 (* s1 scale)
              s1.5 (* s1.5 scale)
              s2 (* s2 scale)
              t1 (* t1 scale)
              t2 (* t2 scale)))
      (triangle-face state
                     m1
                     m2
                     x-min y-min
                     x-max y-min
                     x-half y-max
                     s1 t1
                     s2 t1
                     s1.5 t2))))

(defun rectangle-face (state &key
                               (x-scale 1f0)
                               (y-scale 1f0)
                               rotation-quaternion
                               (x-translate 0f0)
                               (y-translate 0f0)
                               (z-translate 0f0)
                               tilep)
  "Scales, then rotates, then translates a square to produce an
arbitrary rectangle in 3D space as part of a larger mesh."
  (let ((m1 (translate x-translate y-translate z-translate))
        (m2 (multiple-value-call #'quaternion-rotation-matrix
              (if rotation-quaternion
                  (array-of-4 rotation-quaternion)
                  (values 0f0 0f0 0f0 1f0)))))
    (declare (matrix m1 m2)
             (dynamic-extent m1 m2))
    (when rotation-quaternion
      (matrix* m1 m1 m2))
    (let* ((x-min (* x-scale -0.5f0))
           (x-max (- x-min))
           (y-min (* y-scale -0.5f0))
           (y-max (- y-min))
           (s1 0f0)
           (s2 1f0)
           (t1 0f0)
           (t2 1f0))
      (when tilep
        (setf s1 (* s1 x-scale)
              s2 (* s2 x-scale)
              t1 (* t1 y-scale)
              t2 (* t2 y-scale)))
      (quadrilateral-face state
                          m1
                          m2
                          x-min y-min
                          x-max y-min
                          x-max y-max
                          x-min y-max
                          s1 t1
                          s2 t1
                          s2 t2
                          s1 t2))))

(defun regular-hexagon-face (state &key
                                     (x-scale 1f0)
                                     (y-scale 1f0))
  (let* ((r (* y-scale (/ (sqrt 3f0) 2f0)))
         (x1 (* x-scale (- 0.5f0)))
         (x2 (* x-scale (- 0.25f0)))
         (x3 (- x2))
         (x4 (- x1))
         (y1 (* 0.5f0 r))
         (y2 (* y-scale 0f0))
         (y3 (- y1)))
    (set-elements state 0 1 2 2 3 4 4 5 0 0 2 4)
    (attribute3 state :normal 0f0 0f0 1f0)
    (attribute2 state :texcoord (+ x1 0.5f0) (+ (/ y2 r) 0.5f0))
    (vertex3 state x1 y2 0f0)
    (attribute2 state :texcoord (+ x2 0.5f0) (+ (/ y3 r) 0.5f0))
    (vertex3 state x2 y3 0f0)
    (attribute2 state :texcoord (+ x3 0.5f0) (+ (/ y3 r) 0.5f0))
    (vertex3 state x3 y3 0f0)
    (attribute2 state :texcoord (+ x4 0.5f0) (+ (/ y2 r) 0.5f0))
    (vertex3 state x4 y2 0f0)
    (attribute2 state :texcoord (+ x3 0.5f0) (+ (/ y1 r) 0.5f0))
    (vertex3 state x3 y1 0f0)
    (attribute2 state :texcoord (+ x2 0.5f0) (+ (/ y1 r) 0.5f0))
    (vertex3 state x2 y1 0f0)))

;;;; 3D shapes, from the above faces

(defun cuboid (state x-size y-size z-size &key
                                            (x-offset 0f0)
                                            (y-offset 0f0)
                                            (z-offset 0f0)
                                            (tilep t)
                                            (rotation-quaternion (quaternion 0f0 0f0 0f0 1f0))
                                            side-list)
  (check-type side-list list)
  ;; Rotate the local offsets
  (multiple-value-bind (x-sides-offset-x x-sides-offset-y x-sides-offset-z)
      (let ((x-size* (float* x-size)))
        (with-zrvl/mv*
            ((rotation-quaternion zrvl:vec4)
             (x-size* zrvl:f32))
          (zrvl:rotate (zrvl:quat<-vec4 rotation-quaternion)
                       (zrvl:vec3 (zrvl:* 0.5f0 x-size*) 0f0 0f0))))
    (multiple-value-bind (y-sides-offset-x y-sides-offset-y y-sides-offset-z)
        (let ((y-size* (float* y-size)))
          (with-zrvl/mv*
              ((rotation-quaternion zrvl:vec4)
               (y-size* zrvl:f32))
            (zrvl:rotate (zrvl:quat<-vec4 rotation-quaternion)
                         (zrvl:vec3 0f0 (zrvl:* 0.5f0 y-size*) 0f0))))
      (multiple-value-bind (z-sides-offset-x z-sides-offset-y z-sides-offset-z)
          (let ((z-size* (float* z-size)))
            (with-zrvl/mv*
                ((rotation-quaternion zrvl:vec4)
                 (z-size* zrvl:f32))
              (zrvl:rotate (zrvl:quat<-vec4 rotation-quaternion)
                           (zrvl:vec3 0f0 0f0 (zrvl:* 0.5f0 z-size*)))))
        (when (or (null side-list) (find :y- side-list))
          (rectangle-face state
                          :x-scale x-size
                          :y-scale z-size
                          :rotation-quaternion (quaternion* rotation-quaternion
                                                            (rotate-pitch (* 0.5f0 (float* pi))))
                          :x-translate (- x-offset y-sides-offset-x)
                          :y-translate (- y-offset y-sides-offset-y)
                          :z-translate (- z-offset y-sides-offset-z)
                          :tilep tilep))
        (when (or (null side-list) (find :y+ side-list))
          (rectangle-face state
                          :x-scale x-size
                          :y-scale z-size
                          :rotation-quaternion (quaternion* rotation-quaternion
                                                            (rotate-pitch (* -0.5f0 (float* pi))))
                          :x-translate (+ x-offset y-sides-offset-x)
                          :y-translate (+ y-offset y-sides-offset-y)
                          :z-translate (+ z-offset y-sides-offset-z)
                          :tilep tilep))
        (when (or (null side-list) (find :z- side-list))
          (rectangle-face state
                          :x-scale x-size
                          :y-scale y-size
                          :rotation-quaternion (quaternion* rotation-quaternion
                                                            (rotate-yaw (float* pi)))
                          :x-translate (- x-offset z-sides-offset-x)
                          :y-translate (- y-offset z-sides-offset-y)
                          :z-translate (- z-offset z-sides-offset-z)
                          :tilep tilep))
        (when (or (null side-list) (find :z+ side-list))
          (rectangle-face state
                          :x-scale x-size
                          :y-scale y-size
                          :rotation-quaternion rotation-quaternion
                          :x-translate (+ x-offset z-sides-offset-x)
                          :y-translate (+ y-offset z-sides-offset-y)
                          :z-translate (+ z-offset z-sides-offset-z)
                          :tilep tilep))
        (when (or (null side-list) (find :x- side-list))
          (rectangle-face state
                          :x-scale z-size
                          :y-scale y-size
                          :rotation-quaternion (quaternion* rotation-quaternion
                                                            (rotate-yaw (* -0.5f0 (float* pi))))
                          :x-translate (- x-offset x-sides-offset-x)
                          :y-translate (- y-offset x-sides-offset-y)
                          :z-translate (- z-offset x-sides-offset-z)
                          :tilep tilep))
        (when (or (null side-list) (find :x+ side-list))
          (rectangle-face state
                          :x-scale z-size
                          :y-scale y-size
                          :rotation-quaternion (quaternion* rotation-quaternion
                                                            (rotate-yaw (* 0.5f0 (float* pi))))
                          :x-translate (+ x-offset x-sides-offset-x)
                          :y-translate (+ y-offset x-sides-offset-y)
                          :z-translate (+ z-offset x-sides-offset-z)
                          :tilep tilep)))))
  state)

;;;; Make 2D shapes

(defun make-equilateral-triangle (&key
                                    (name (error 'required-input-error))
                                    (color-r 0f0)
                                    (color-g 0f0)
                                    (color-b 0f0)
                                    (scale 1f0)
                                    rotation-angle
                                    (tilep t)
                                    program
                                    texture)
  (check-type program keyword)
  (check-type texture keyword)
  (let ((state (make-instance 'procedural-model
                              :name name
                              :attribute-names #(:normal :color :texcoord)
                              :attribute-sizes #(3 3 2)
                              :program program
                              :textures (vector texture))))
    (begin state
           :vertices 3
           :elements 3)
    (attribute3 state :color color-r color-g color-b)
    (equilateral-triangle-face state
                               :scale scale
                               :rotation-quaternion (if rotation-angle
                                                        (rotate-roll rotation-angle)
                                                        nil)
                               :tilep tilep)
    (end state)))

(defun make-rectangle (&key
                         (name (error 'required-input-error))
                         (color-r 0f0)
                         (color-g 0f0)
                         (color-b 0f0)
                         (x-scale 1f0)
                         (y-scale 1f0)
                         rotation-angle
                         (tilep t)
                         program
                         texture)
  (check-type program keyword)
  (check-type texture keyword)
  (let ((state (make-instance 'procedural-model
                              :name name
                              :attribute-names #(:normal :color :texcoord)
                              :attribute-sizes #(3 3 2)
                              :program program
                              :textures (vector texture))))
    (begin state
           :vertices 4
           :elements 6)
    (attribute3 state :color color-r color-g color-b)
    (rectangle-face state
                    :x-scale x-scale
                    :y-scale y-scale
                    :rotation-quaternion (if rotation-angle
                                             (rotate-roll rotation-angle)
                                             nil)
                    :tilep tilep)
    (end state)))

(defun make-square (&key
                      (name (error 'required-input-error))
                      (color-r 0f0)
                      (color-g 0f0)
                      (color-b 0f0)
                      (scale 1f0)
                      rotation-angle
                      (tilep t)
                      program
                      texture)
  (check-type program keyword)
  (check-type texture keyword)
  (make-rectangle :name name
                  :color-r color-r
                  :color-g color-g
                  :color-b color-b
                  :x-scale scale
                  :y-scale scale
                  :rotation-angle rotation-angle
                  :tilep tilep
                  :program program
                  :texture texture))

(defun make-hexagon (&key
                       (name (error 'required-input-error))
                       (color-r 0f0)
                       (color-g 0f0)
                       (color-b 0f0)
                       (scale 1f0)
                       program
                       texture)
  (check-type program keyword)
  (check-type texture keyword)
  (let ((state (make-instance 'procedural-model
                              :name name
                              :attribute-names #(:normal :color :texcoord)
                              :attribute-sizes #(3 3 2)
                              :program program
                              :textures (vector texture))))
    (begin state
           :vertices 6
           :elements 12)
    (attribute3 state :color color-r color-g color-b)
    (regular-hexagon-face state
                          :x-scale scale
                          :y-scale scale)
    (end state)))

;;;; Make 3D shapes

(defun make-cuboid (x-size
                    y-size
                    z-size
                    &key
                      (name (error 'required-input-error))
                      program
                      texture)
  (check-type program keyword)
  (check-type texture keyword)
  (let ((faces 6)
        (state (make-instance 'procedural-model
                              :name name
                              :attribute-names #(:normal :color :texcoord)
                              :attribute-sizes #(3 3 2)
                              :program program
                              :textures (vector texture))))
    (begin state
           :vertices (* 4 faces)
           :elements (* 6 faces))
    (attribute3 state :color 1f0 1f0 1f0)
    (cuboid state x-size y-size z-size)
    (end state)))

(defun make-cube (size
                  &key
                    (name (error 'required-input-error))
                    program
                    texture)
  (check-type program keyword)
  (check-type texture keyword)
  (make-cuboid size size size :name name :program program :texture texture))
