# Changelog

This changelog uses the style from [Keep a
Changelog](https://keepachangelog.com/en/1.0.0/).

This project does **NOT** use semantic versioning. Instead of having a
`MAJOR.MINOR.PATCH` version scheme, it uses an
`EPOCH.MAJOR.MINOR.PATCH` version scheme. This follows the standard
practice of game engines, which is to have the most significant
version number update extremely infrequently. The epoch version
represents a generation of the game engine. It is potentially
extremely incompatible with prior generations.

However, you can think of each epoch of Zombie Raptor as its own,
distinct game engine that *does* follow [semantic versioning
2.0.0](https://semver.org/).

That is, a hypothetical "Zombie Raptor 2.1.0.0" can be thought of as
version "1.0.0" of the "Zombie Raptor 2" game engine. This is simpler
than actually creating separate repositories for each major epoch, or
mapping a marketing number to an internal version number, etc.

Zombie Raptor provides both a `+version-string+` constant that
includes the epoch and a `+semantic-version-string+` that does not.
This changelog includes the epoch.

## [Unreleased]

Represents *many* miscellaneous additions as well quite a few
milestones. Unlike some projects, the milestones represent features
rather than versions themselves because there are a considerable
number of features required by the game engine and many of them depend
at least partially on each other. A simple 0.0.1.0 will be released
when enough milestones are completed.

The current rewrite of the engine dates to April 2016, with some of
the engine's code dating all the way back to late 2013. Thus, it is
impossible to completely describe everything that has been added since
the initial release. This game engine project has also led to other
projects, including the zr-utils utility library and three entire
languages: the SXP data format, the ZRVL shading language, and the
Airship Scheme implementation of Scheme in Common Lisp.

The milestones included in this release are:

- [Textures 1]
- [Map Format 1]
- [Terrain 1]
- [ZRVL and Shaders 1]

### Added
 - A math system, mostly aimed at supporting real-time vector math.
   - Supports common matrix, vector, and quaternion operations.
   - Contains a variety of common matrices, such as a perspective
     projection matrix.
   - Implements "Boolean sets", which use bits of an integer
     (preferably a fixnum) to represent the presence or absence of an
     item in a set in an efficient way for the ECS.
 - A simple physics engine.
   - Updates the motion of entities based either on input (from
     controls) or on physics simulations.
   - Implements the Runge—Kutta method (RK4) to approximate ODEs in
     the physics engine.
 - A high-level abstraction of the computer's directory layout.
   - Finds configuration, cache, save, and data files.
 - A simple draft of a "SXP" configuration language, using literal
   s-expressions.
 - A shader language, called ZRVL, built on top of SPIR-V.
   - Directly compiles s-expressions down to SPIR-V binaries.
   - Supports vertex and fragment (pixel) shaders.
 - A texture object.
   - Supports most of the common OpenGL texture functionality, such as
     mipmaps and anisotropy.
   - Supports all types of OpenGL textures.
   - Uses `pngload` to directly load a PNG file into a texture object.
 - A model object.
   - Contains vertex and element arrays.
 - A simple terrain data structure.
   - Generates some textures and models for the engine.
 - A simple map format that is capable enough to be used in 3D games
   of a mid-90s style. Features include:
   - Collision detection with the world object.
   - AI pathfinding.
 - A simple OpenGL renderer.
   - Passes model (vertex and element), texture, and shader
     information to the OpenGL driver.
   - Tells the GPU to draw the stored geometry and textures.
 - A basic entity component system.
   - Stores simple information, such as location, velocity, rotation,
     scale, and so on.
   - Calls the relevant updates (physics and rendering) on the data.
   - Exposes some functionality to the scripting function, which can
     be passed into the game loop.
 - A basic game client.
   - Abstracts over SDL2, which in turn abstracts over the host OS.
   - Keeps track of settings.
   - Keeps track of time.
   - Handles controls.
   - Runs the game loop.
     - Designed to be non-consing in 64-bit SBCL (other
       implementations don't expose enough information and/or don't do
       enough optimizations) unless the *user* conses.
     - Handles the input (which is copied from SDL2).
     - Calls the script function and the physics functions.
     - Calls the render functions.
 - A simple parser and renderer for fonts in the TTF format.
 - Example programs.
   - Demonstrates a first person 3D world with interior geometry, an
     overhead 3D world with terrain, and the 2D HUD.
   - Shows how to use the scripting, controls, and shaders.
   - Generates the required terrain, textures, and vertices
     procedurally so that space isn't wasted on these demonstration
     programs because git is not a good format for storing binary
     data.
 - Unit tests for many of the easy-to-test functions.

[Unreleased]: https://gitlab.com/zombie-raptor/zombie-raptor/-/compare/055bb2ae...master
[ZRVL and Shaders 1]: https://gitlab.com/zombie-raptor/zombie-raptor/-/milestones/1
[Textures 1]: https://gitlab.com/zombie-raptor/zombie-raptor/-/milestones/2
[Terrain 1]: https://gitlab.com/zombie-raptor/zombie-raptor/-/milestones/3
[Map Format 1]: https://gitlab.com/zombie-raptor/zombie-raptor/-/milestones/8
