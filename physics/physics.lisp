(defpackage #:zombie-raptor/physics/physics
  (:documentation "The core of the engine's physics.")
  (:use #:cl
        #:zombie-raptor/core/constants
        #:zombie-raptor/core/types
        #:zombie-raptor/entity/entity
        #:zr-utils)
  (:import-from #:alexandria
                #:with-gensyms)
  (:import-from #:zombie-raptor/zrvl
                #:with-zrvl)
  (:export #:fall!
           #:update-absolute-input-movement!
           #:update-input-movement!
           #:update-location-and-velocity!))

(in-package #:zombie-raptor/physics/physics)

(defpackage #:zombie-raptor/physics/physics.script
  (:documentation "ZRVL scripting of the physics.")
  (:use #:zrvl)
  (:export #:with-vec2-rk4
           #:with-vec2-rk4*)
  (:nicknames #:zr-physics-script))

(deftype entity-vec3s ()
  `(vecs 3 ,+max-entities+))

(define-function (%three-zerop :inline t) (a b c)
  (and (zerop a) (zerop b) (zerop c)))

(in-package #:zombie-raptor/physics/physics.script)

(cl:defmacro with-vec2-rk4 ((v v1 v2) (time h) cl:&body body)
  (alexandria:with-gensyms (k1- k2- k3- k4- h/2-)
    `(let* ((,v (vec2 ,v1 ,v2))
            (,h/2- (* 0.5f0 ,h))
            (,k1- (let ((,time ,time)
                        (,v ,v))
                    ,@body))
            (,k2- (let ((,time (+ ,time ,h/2-))
                        (,v (+ ,v (* ,h/2- ,k1-))))
                    ,@body))
            (,k3- (let ((,time (+ ,time ,h/2-))
                        (,v (+ ,v (* ,h/2- ,k2-))))
                    ,@body))
            (,k4- (let ((,time (+ ,time ,h))
                        (,v (+ ,v (* ,h ,k3-))))
                    ,@body)))
       (+ ,v
          (* (/ ,h 6.0f0)
             (+ ,k1- (* 2f0 (+ ,k2- ,k3-)) ,k4-))))))

(cl:defmacro with-vec2-rk4* ((v v1 v2) (h) cl:&body body)
  (alexandria:with-gensyms (k1- k2- k3- k4- h/2-)
    `(let* ((,v (vec2 ,v1 ,v2))
            (,h/2- (* 0.5f0 ,h))
            (,k1- (let ((,v ,v))
                    ,@body))
            (,k2- (let ((,v (+ ,v (* ,h/2- ,k1-))))
                    ,@body))
            (,k3- (let ((,v (+ ,v (* ,h/2- ,k2-))))
                    ,@body))
            (,k4- (let ((,v (+ ,v (* ,h ,k3-))))
                    ,@body)))
       (+ ,v
          (* (/ ,h 6.0f0)
             (+ ,k1- (* 2f0 (+ ,k2- ,k3-)) ,k4-))))))

(in-package #:zombie-raptor/physics/physics)

(define-function %update-location-and-velocity! ((entity-id entity-id)
                                                 (location entity-vec3s)
                                                 (velocity entity-vec3s)
                                                 (acceleration entity-vec3s)
                                                 (time-step single-float))
  (declare (optimize (speed 3) (space 0) (safety 1) (debug 0) (compilation-speed 0)))
  (let ((location* location)
        (velocity* velocity))
    (with-zrvl
        ((location zrvl:vec3 :row entity-id)
         (velocity zrvl:vec3 :row entity-id)
         (acceleration zrvl:vec3 :row entity-id)
         (time-step zrvl:f32))
        ((location* zrvl:vec3 :row entity-id)
         (velocity* zrvl:vec3 :row entity-id))
      (zrvl:let ((v1 (zr-physics-script:with-vec2-rk4*
                         (y (zrvl:aref location 0)
                            (zrvl:aref velocity 0))
                         (time-step)
                       (zrvl:vec2 (zrvl:aref y 1)
                                  (zrvl:aref acceleration 0))))
                 (v2 (zr-physics-script:with-vec2-rk4*
                         (y (zrvl:aref location 1)
                            (zrvl:aref velocity 1))
                         (time-step)
                       (zrvl:vec2 (zrvl:aref y 1)
                                  (zrvl:aref acceleration 1))))
                 (v3 (zr-physics-script:with-vec2-rk4*
                         (y (zrvl:aref location 2)
                            (zrvl:aref velocity 2))
                         (time-step)
                       (zrvl:vec2 (zrvl:aref y 1)
                                  (zrvl:aref acceleration 2)))))
        (zrvl-int:setf location* (zrvl:vec3 (zrvl:aref v1 0)
                                            (zrvl:aref v2 0)
                                            (zrvl:aref v3 0)))
        (zrvl-int:setf velocity* (zrvl:vec3 (zrvl:aref v1 1)
                                            (zrvl:aref v2 1)
                                            (zrvl:aref v3 1))))))
  nil)

(define-function update-location-and-velocity! ((ecs entity-component-system)
                                                (entity-id entity-id)
                                                (time single-float)
                                                (time-step single-float))
  (declare (optimize (speed 3) (space 0) (safety 1) (debug 0) (compilation-speed 0)))
  (declare (ignore time))
  (with-selection ecs
      (id :id entity-id :changed? t)
      ((velocity (velocity* velocity :row-of 3))
       (physics (input input-direction :row-of 3)
                (input.x input-direction :elt 0)
                (input.y input-direction :elt 1)
                (input.z input-direction :elt 2)
                (acceleration* acceleration :row-of 3))
       (location (location.x location :elt 0)
                 (location.y location :elt 1)
                 (location.z location :elt 2)))
    (unless (and (multiple-value-call #'%three-zerop velocity*)
                 (multiple-value-call #'%three-zerop input)
                 (multiple-value-call #'%three-zerop acceleration*))
      (incf location.x input.x)
      (incf location.y input.y)
      (incf location.z input.z)
      (setf input (values 0f0 0f0 0f0))
      (%update-location-and-velocity! id location velocity acceleration time-step)))
  nil)

(define-function update-absolute-input-movement! ((ecs entity-component-system) (entity-id entity-id))
  (declare (optimize (speed 3) (space 0) (safety 1) (debug 0) (compilation-speed 0)))
  "Moves an entity based on the absolute input direction."
  (with-selection ecs
      (id :id entity-id :changed? t)
      ((physics (input-direction* input-direction :row-of 3)
                input-direction
                input-velocity))
    (unless (multiple-value-call #'%three-zerop input-direction*)
      (let ((%input-direction* %input-direction))
        (with-zrvl
            ((%input-direction zrvl:vec3 :row entity-id)
             (%input-velocity zrvl:f32 :offset entity-id))
            ((%input-direction* zrvl:vec3 :row entity-id))
          (zrvl-int:setf %input-direction* (zrvl:* %input-velocity
                                                   (zrvl:normalize %input-direction))))))))

(define-function update-input-movement! ((ecs entity-component-system) (entity-id entity-id))
  (declare (optimize (speed 3) (space 0) (safety 1) (debug 0) (compilation-speed 0)))
  "Moves an entity based on the input in the direction it is facing."
  (with-selection ecs
      (id :id entity-id :changed? t)
      ((rotation rotation)
       (physics (input-direction* input-direction :row-of 3)
                input-direction
                input-velocity))
    (unless (multiple-value-call #'%three-zerop input-direction*)
      (let ((%input-direction* %input-direction))
        (with-zrvl
            ((%input-direction zrvl:vec3 :row entity-id)
             (%input-velocity zrvl:f32 :offset entity-id)
             (%rotation zrvl:vec4 :row entity-id))
            ((%input-direction* zrvl:vec3 :row entity-id))
          (zrvl-int:setf %input-direction* (zrvl:rotate (zrvl:quat<-vec4 %rotation)
                                                        (zrvl:* %input-velocity
                                                                (zrvl:normalize %input-direction)))))))))

(define-function (apply-gravity-force! :inline t) (acceleration entity-id gravity)
  (with-array-accessors ((acceleration.y acceleration :elt 1))
      entity-id
    (setf acceleration.y (- gravity))))

(define-function (remove-gravity-force! :inline t) (acceleration entity-id gravity)
  (declare (ignore gravity))
  (with-array-accessors ((acceleration.y acceleration :elt 1))
      entity-id
    (setf acceleration.y 0f0)))

(define-function fall! ((ecs entity-component-system) (entity-id entity-id) (gravity single-float))
  (declare (optimize (speed 3) (space 0) (safety 1) (debug 0) (compilation-speed 0)))
  (let ((entity-height-offset 1.75f0)
        (ground-level 0f0)
        (epsilon 0.01f0))
    (with-selection ecs
        (id :id entity-id)
        ((physics (gravity? gravity?)
                  acceleration)
         (location (location.y location :elt 1))
         (velocity (velocity.y velocity :elt 1)))
      (if (= 1 gravity?)
          (when (< location.y (+ ground-level entity-height-offset epsilon))
            (remove-gravity-force! %acceleration id gravity)
            (psetf velocity.y 0f0
                   location.y (+ ground-level entity-height-offset)
                   gravity? 0))
          (when (> location.y (+ ground-level entity-height-offset epsilon))
            (apply-gravity-force! %acceleration id gravity)
            (setf gravity? 1)))))
  nil)
