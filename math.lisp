(uiop:define-package #:zombie-raptor/math
  (:nicknames #:zr/math)
  (:use #:cl
        #:zr-utils)
  (:use-reexport #:zombie-raptor/math/boolean-set
                 #:zombie-raptor/math/matrix
                 #:zombie-raptor/math/quaternion
                 #:zombie-raptor/math/vector))
