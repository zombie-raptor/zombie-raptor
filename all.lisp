;;;; This file is used to reexport all of the individual packages into
;;;; a more convenient zombie-raptor package because of the way
;;;; package-inferred-system works.

(uiop:define-package #:zombie-raptor/all
  (:nicknames #:zombie-raptor #:zr)
  (:use #:cl
        #:zr-utils)
  (:use-reexport #:zombie-raptor/audio
                 #:zombie-raptor/core
                 #:zombie-raptor/data
                 #:zombie-raptor/entity
                 #:zombie-raptor/examples
                 #:zombie-raptor/math
                 #:zombie-raptor/physics
                 #:zombie-raptor/render
                 #:zombie-raptor/text
                 #:zombie-raptor/zrvl))
