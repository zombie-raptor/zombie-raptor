(defpackage #:zombie-raptor/tests/zrvl/zrvl
  (:use #:cl
        #:zombie-raptor/math/vector
        #:zombie-raptor/zrvl/core-types
        #:zombie-raptor/zrvl/zrvl
        #:zombie-raptor/tests/all
        #:zr-utils)
  (:import-from #:alexandria
                #:rcurry)
  (:export #:zombie-raptor/zrvl/zrvl))

(in-package #:zombie-raptor/tests/zrvl/zrvl)

(def-suite zombie-raptor/zrvl/zrvl
    :in zombie-raptor/tests)

(in-suite zombie-raptor/zrvl/zrvl)

;; TODO: test other returned types; test input/output (in a different test set?)
(test basic-with-zrvl-usage
  ;; Scalar result
  (is (= 1f0 (with-zrvl () () 1f0)))
  (is (= 1f0 (with-zrvl* () 1f0)))
  (is (= 1f0 (with-zrvl/mv () () 1f0)))
  (is (= 1f0 (with-zrvl/mv* () 1f0)))
  ;; vec2 result
  (is (equalp (vec2 1f0 2f0)
              (with-zrvl () ()
                (zrvl:vec2 1f0 2f0))))
  (is (equalp (vec2 1f0 2f0)
              (with-zrvl* ()
                (zrvl:vec2 1f0 2f0))))
  (multiple-value-bind (a b)
      (with-zrvl/mv () ()
        (zrvl:vec2 1f0 2f0))
    (is (and (= 1f0 a)
             (= 2f0 b))))
  (multiple-value-bind (a b)
      (with-zrvl/mv* ()
        (zrvl:vec2 1f0 2f0))
    (is (and (= 1f0 a)
             (= 2f0 b))))
  ;; vec3 result
  (is (equalp (vec3 1f0 2f0 3f0)
              (with-zrvl () ()
                (zrvl:vec3 1f0 2f0 3f0))))
  (is (equalp (vec3 1f0 2f0 3f0)
              (with-zrvl* ()
                (zrvl:vec3 1f0 2f0 3f0))))
  (multiple-value-bind (a b c)
      (with-zrvl/mv () ()
        (zrvl:vec3 1f0 2f0 3f0))
    (is (and (= 1f0 a)
             (= 2f0 b)
             (= 3f0 c))))
  (multiple-value-bind (a b c)
      (with-zrvl/mv* ()
        (zrvl:vec3 1f0 2f0 3f0))
    (is (and (= 1f0 a)
             (= 2f0 b)
             (= 3f0 c))))
  ;; vec4 result
  (is (equalp (vec4 1f0 2f0 3f0 4f0)
              (with-zrvl () ()
                (zrvl:vec4 1f0 2f0 3f0 4f0))))
  (is (equalp (vec4 1f0 2f0 3f0 4f0)
              (with-zrvl* ()
                (zrvl:vec4 1f0 2f0 3f0 4f0))))
  (multiple-value-bind (a b c d)
      (with-zrvl/mv () ()
        (zrvl:vec4 1f0 2f0 3f0 4f0))
    (is (and (= 1f0 a)
             (= 2f0 b)
             (= 3f0 c)
             (= 4f0 d))))
  (multiple-value-bind (a b c d)
      (with-zrvl/mv* ()
        (zrvl:vec4 1f0 2f0 3f0 4f0))
    (is (and (= 1f0 a)
             (= 2f0 b)
             (= 3f0 c)
             (= 4f0 d))))
  ;; quat result
  (is (equalp (vec4 1f0 2f0 3f0 4f0)
              (with-zrvl () ()
                (zrvl:quat 1f0 2f0 3f0 4f0))))
  (is (equalp (vec4 1f0 2f0 3f0 4f0)
              (with-zrvl* ()
                (zrvl:quat 1f0 2f0 3f0 4f0))))
  (multiple-value-bind (a b c d)
      (with-zrvl/mv () ()
        (zrvl:quat 1f0 2f0 3f0 4f0))
    (is (and (= 1f0 a)
             (= 2f0 b)
             (= 3f0 c)
             (= 4f0 d))))
  (multiple-value-bind (a b c d)
      (with-zrvl/mv* ()
        (zrvl:quat 1f0 2f0 3f0 4f0))
    (is (and (= 1f0 a)
             (= 2f0 b)
             (= 3f0 c)
             (= 4f0 d)))))

;;; TODO: go through the rest of arithmetic, possibly in multiple sets
;;; of tests
(test basic-zrvl-functions
  (is (= 1f0
         (with-zrvl* ()
           (zrvl:+ 2f0 -1f0))))
  (is (= 12f0
         (with-zrvl* ()
           (zrvl:+ 1f0 4f0 7f0))))
  (is (= 12f0
         (with-zrvl* ()
           (zrvl:+ (zrvl:+ 1f0 4f0) 7f0))))
  (is (equalp (vec4 1f0 2f0 3f0 4f0)
              (with-zrvl* ()
                (zrvl:+ (zrvl:vec4 0f0 1f0 -1f0  5f0)
                        (zrvl:vec4 1f0 1f0  4f0 -1f0)))))
  (is (equalp (vec4 5f0 -1f0 5f0 -4f0)
              (with-zrvl* ()
                (zrvl:+ (zrvl:vec4 0f0  1f0 -1f0  5f0)
                        (zrvl:vec4 1f0  1f0  4f0 -1f0)
                        (zrvl:vec4 4f0 -3f0  2f0 -8f0)))))
  (is (equalp (vec4 5f0 -1f0 5f0 -4f0)
              (with-zrvl* ()
                (zrvl:+ (zrvl:+ (zrvl:vec4 0f0 1f0 -1f0  5f0)
                                (zrvl:vec4 1f0 1f0  4f0 -1f0))
                        (zrvl:vec4 4f0 -3f0 2f0 -8f0))))))

(define-function (close-to-zero-p :inline t)
    ((x single-float)
     &optional (epsilon 1e-7 single-float))
  (< (abs x) epsilon))

(test quaternions
  ;; Use an alternate definition of quaternion multiplication via the
  ;; scalar and vector components (easily expressible in ZRVL) and
  ;; compare it to the manually-written element-by-element definition
  ;; of the quaternion multiplication that is built into ZRVL. If the
  ;; difference is close to zero, then they are equivalent.
  (is (every #'close-to-zero-p
             (with-zrvl* ()
               (zrvl:let* ((q1 (zrvl:normalize (zrvl:quat 1f0 2f0 3f0 4f0)))
                           (q2 (zrvl:normalize (zrvl:quat 4f0 3f0 2f0 1f0)))
                           (v1 (zrvl:vec3<-quat q1))
                           (v2 (zrvl:vec3<-quat q2))
                           (s1 (zrvl:swizzle q1 :w))
                           (s2 (zrvl:swizzle q2 :w)))
                 (zrvl:- (zrvl:quat<-vec4 (zrvl:vec4 (zrvl:+ (zrvl:cross v1 v2)
                                                             (zrvl:* s1 v2)
                                                             (zrvl:* s2 v1))
                                                     (zrvl:- (zrvl:* s1 s2)
                                                             (zrvl:dot v1 v2))))
                         (zrvl:* q1 q2))))))
  (is (every #'close-to-zero-p
             (with-zrvl* ()
               (zrvl:let* ((q1 (zrvl:normalize (zrvl:quat 0.5f0 -0.5f0 0.5f0 0.5f0)))
                           (q2 (zrvl:normalize (zrvl:quat<-euler (zrvl:vec3 #.(float* (* 1/3 pi))
                                                                            #.(float* (* 4/3 pi))
                                                                            #.(float* (* 1/6 pi))))))
                           (v1 (zrvl:vec3<-quat q1))
                           (v2 (zrvl:vec3<-quat q2))
                           (s1 (zrvl:swizzle q1 :w))
                           (s2 (zrvl:swizzle q2 :w)))
                 (zrvl:- (zrvl:quat<-vec4 (zrvl:vec4 (zrvl:+ (zrvl:cross v1 v2)
                                                             (zrvl:* s1 v2)
                                                             (zrvl:* s2 v1))
                                                     (zrvl:- (zrvl:* s1 s2)
                                                             (zrvl:dot v1 v2))))
                         (zrvl:* q1 q2))))))
  ;; Compare axis rotations with yaw/pitch/roll with euler angle rotations
  (is (every #'close-to-zero-p
             (with-zrvl* ()
               (zrvl:- (zrvl:rotation (zrvl:vec3 1f0 0f0 0f0) #.(float* (* 1/3 pi)))
                       (zrvl:quat<-pitch #.(float* (* 1/3 pi)))))))
  (is (every #'close-to-zero-p
             (with-zrvl* ()
               (zrvl:- (zrvl:quat<-euler (zrvl:vec3 0f0 #.(float* (* 1/3 pi)) 0f0))
                       (zrvl:quat<-pitch #.(float* (* 1/3 pi)))))))
  (is (every #'close-to-zero-p
             (with-zrvl* ()
               (zrvl:- (zrvl:rotation (zrvl:vec3 0f0 1f0 0f0) #.(float* (* 1/3 pi)))
                       (zrvl:quat<-yaw #.(float* (* 1/3 pi)))))))
  (is (every #'close-to-zero-p
             (with-zrvl* ()
               (zrvl:- (zrvl:quat<-euler (zrvl:vec3 #.(float* (* 4/3 pi)) 0f0 0f0))
                       (zrvl:quat<-yaw #.(float* (* 4/3 pi)))))))
  (is (every #'close-to-zero-p
             (with-zrvl* ()
               (zrvl:- (zrvl:rotation (zrvl:vec3 0f0 0f0 1f0) #.(float* (* 1/2 pi)))
                       (zrvl:quat<-roll #.(float* (* 1/2 pi)))))))
  (is (every #'close-to-zero-p
             (with-zrvl* ()
               (zrvl:- (zrvl:quat<-euler (zrvl:vec3 0f0 0f0 #.(float* (* 1/2 pi))))
                       (zrvl:quat<-roll #.(float* (* 1/2 pi)))))))
  ;; Rotate points around simple axes
  (is (every (rcurry #'close-to-zero-p 5e-7)
             (with-zrvl* ()
               (zrvl:- (zrvl:rotate (zrvl:quat<-yaw #.(float* pi))
                                    (zrvl:vec 1f0 2f0 3f0))
                       (zrvl:vec3 -1f0 2f0 -3f0)))))
  (is (every (rcurry #'close-to-zero-p 5e-7)
             (with-zrvl* ()
               (zrvl:- (zrvl:rotate (zrvl:quat<-pitch #.(float* pi))
                                    (zrvl:vec 1f0 2f0 3f0))
                       (zrvl:vec3 1f0 -2f0 -3f0)))))
  (is (every (rcurry #'close-to-zero-p 5e-7)
             (with-zrvl* ()
               (zrvl:- (zrvl:rotate (zrvl:quat<-roll #.(float* pi))
                                    (zrvl:vec 1f0 2f0 3f0))
                       (zrvl:vec3 -1f0 -2f0 3f0)))))
  ;; Test slerp by comparing it to an alternate definition
  (is (every (rcurry #'close-to-zero-p 5e-7)
             (with-zrvl* ()
               (zrvl:let* ((q0 (zrvl:quat<-yaw #.(float* (* 4/3 pi))))
                           (q1 (zrvl:quat<-pitch #.(float* (* 1/2 pi))))
                           (q (zrvl:* (zrvl:conjugate q0) q1))
                           (x 0.42f0)
                           (phi (zrvl:acos (zrvl:/ (zrvl:swizzle q :w)
                                                   (zrvl:magnitude q)))))
                 ;; q0 * q^x is an alternate definition for slerp. All
                 ;; of this complexity (including phi) is because EXPT
                 ;; is not defined for quat types right now.
                 (zrvl:- (zrvl:* q0 (zrvl:quat<-vec4 (zrvl:* (zrvl:expt (zrvl:magnitude q) x)
                                                             (zrvl:vec4 (zrvl:* (zrvl:normalize (zrvl:vec3<-quat q))
                                                                                (zrvl:sin (zrvl:* x phi)))
                                                                        (zrvl:cos (zrvl:* x phi))))))
                         (zrvl:slerp q0 q1 x)))))))
