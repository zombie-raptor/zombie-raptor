(defpackage #:zombie-raptor/tests/zrvl/core-types
  (:use #:cl
        #:zombie-raptor/zrvl/core-types
        #:zombie-raptor/zrvl/instructions
        #:zombie-raptor/tests/all
        #:zr-utils)
  (:export #:zombie-raptor/zrvl/core-types))

(in-package #:zombie-raptor/tests/zrvl/core-types)

(def-suite zombie-raptor/zrvl/core-types
    :in zombie-raptor/tests)

(in-suite zombie-raptor/zrvl/core-types)

;;; Helper function to test printable representations.
(defun linear-algebra-type-to-string (type)
  (with-output-to-string (stream)
    (write-linear-algebra-type type stream)))

(test primitive-linear-algebra-types
  "Do the decoded, printable linear algebra types match the constructors?"
  (is (string-equal "(%%i 8)"
                    (linear-algebra-type-to-string (%%i 8))))
  (is (string= "(%%i 8)"
               (let ((*print-case* :downcase))
                 (linear-algebra-type-to-string (%%i 8)))))
  (is (string= "(%%I 8)"
               (let ((*print-case* :upcase))
                 (linear-algebra-type-to-string (%%i 8)))))
  (is (string-equal "(%%i 16)"
                    (linear-algebra-type-to-string (%%i 16))))
  (is (string-equal "(%%i 32)"
                    (linear-algebra-type-to-string (%%i 32))))
  (is (string-equal "(%%i 64)"
                    (linear-algebra-type-to-string (%%i 64))))
  (is (string-equal "(%%u 8)"
                    (linear-algebra-type-to-string (%%u 8))))
  (is (string= "(%%u 8)"
               (let ((*print-case* :downcase))
                 (linear-algebra-type-to-string (%%u 8)))))
  (is (string= "(%%U 8)"
               (let ((*print-case* :upcase))
                 (linear-algebra-type-to-string (%%u 8)))))
  (is (string-equal "(%%u 32)"
                    (linear-algebra-type-to-string (%%u 32))))
  (is (string-equal "(%%f 16)"
                    (linear-algebra-type-to-string (%%f 16))))
  (is (string-equal "(%%f 32)"
                    (linear-algebra-type-to-string (%%f 32))))
  (is (string-equal "(%%f 64)"
                    (linear-algebra-type-to-string (%%f 64))))
  (is (string= "(%%f 32)"
               (let ((*print-case* :downcase))
                 (linear-algebra-type-to-string (%%f 32)))))
  (is (string= "(%%F 32)"
               (let ((*print-case* :upcase))
                 (linear-algebra-type-to-string (%%f 32)))))
  (is (string-equal "(%%boolean)"
                    (linear-algebra-type-to-string (%%boolean))))
  (is (string= "(%%boolean)"
               (let ((*print-case* :downcase))
                 (linear-algebra-type-to-string (%%boolean)))))
  (is (string= "(%%BOOLEAN)"
               (let ((*print-case* :upcase))
                 (linear-algebra-type-to-string (%%boolean)))))
  (is (string-equal "(%%void)"
                    (linear-algebra-type-to-string (%%void))))
  (is (string= "(%%void)"
               (let ((*print-case* :downcase))
                 (linear-algebra-type-to-string (%%void)))))
  (is (string= "(%%VOID)"
               (let ((*print-case* :upcase))
                 (linear-algebra-type-to-string (%%void)))))
  (is (string-equal "(%%vec (%%u 16) 1)"
                    (linear-algebra-type-to-string (%%vec (%%u 16) 1))))
  (is (string-equal "(%%vec (%%i 8) 2)"
                    (linear-algebra-type-to-string (%%vec (%%i 8) 2))))
  (is (string-equal "(%%vec (%%boolean) 3)"
                    (linear-algebra-type-to-string (%%vec (%%boolean) 3))))
  (is (string= "(%%vec (%%boolean) 3)"
               (let ((*print-case* :downcase))
                 (linear-algebra-type-to-string (%%vec (%%boolean) 3)))))
  (is (string= "(%%VEC (%%BOOLEAN) 3)"
               (let ((*print-case* :upcase))
                 (linear-algebra-type-to-string (%%vec (%%boolean) 3)))))
  (is (string-equal "(%%vec (%%f 32) 4)"
                    (linear-algebra-type-to-string (%%vec (%%f 32) 4))))
  (is (string-equal "(%%matrix (%%u 16) 3 2)"
                    (linear-algebra-type-to-string (%%matrix (%%u 16) 3 2))))
  (is (string= "(%%matrix (%%u 16) 3 2)"
               (let ((*print-case* :downcase))
                 (linear-algebra-type-to-string (%%matrix (%%u 16) 3 2)))))
  (is (string= "(%%MATRIX (%%U 16) 3 2)"
               (let ((*print-case* :upcase))
                 (linear-algebra-type-to-string (%%matrix (%%u 16) 3 2)))))
  (is (string-equal "(%%matrix (%%i 8) 2 4)"
                    (linear-algebra-type-to-string (%%matrix (%%i 8) 2 4))))
  (is (string-equal "(%%matrix (%%boolean) 4 3)"
                    (linear-algebra-type-to-string (%%matrix (%%boolean) 4 3))))
  (is (string-equal "(%%matrix (%%f 32) 4 4)"
                    (linear-algebra-type-to-string (%%matrix (%%f 32) 4 4)))))

;;; TODO: test derivative-linear-algebra-types

(test enumerate-types
  "Are types correctly assigned IDs?"
  (is
   (equalp (with-spir-v-id (2)
             (reverse (enumerate-types (%%ftype (%%void)
                                                (%%f 32)
                                                (%%vec (%%f 32) 3)
                                                (%%matrix (%%f 64) 3 2)
                                                (%%array 4 (%%f 32)))
                                       (make-hash-table :test 'equalp)
                                       (list)
                                       #'new-spir-v-id)))
           `((2  (:void))
             (3  (:float 32))
             (4  (:vector 3 3))
             (5  (:float 64))
             (6  (:vector 5 3))
             (7  (:matrix 6 2))
             (8  (:int 32 0))
             (9  (:length 8 4))
             (10 (:array 3 9))
             (11 (:function 2 3 4 7 10)))))
  (is
   (equalp (with-spir-v-id (2)
             (let* ((h (make-hash-table :test 'equalp))
                    (l (list))
                    (l* (enumerate-types (%%struct (%%vec (%%f 32) 3)
                                                   (%%f 32))
                                         h
                                         l
                                         #'new-spir-v-id))
                    (l** (enumerate-types (%%matrix (%%f 32) 3 4)
                                          h
                                          l*
                                          #'new-spir-v-id)))
               (reverse l**)))
           '((2 (:float 32))
             (3 (:vector 2 3))
             (4 (:struct 3 2))
             (5 (:matrix 3 4))))))
