(defpackage #:zombie-raptor/tests/zrvl/a-normal
  (:use #:cl
        #:zombie-raptor/zrvl/a-normal
        #:zombie-raptor/zrvl/core-types
        #:zombie-raptor/tests/all
        #:zr-utils)
  (:import-from #:zombie-raptor/zrvl/a-normal
                #:new-ir-id)
  (:export #:zombie-raptor/zrvl/a-normal))

(in-package #:zombie-raptor/tests/zrvl/a-normal)

(def-suite zombie-raptor/zrvl/a-normal
    :in zombie-raptor/tests)

(in-suite zombie-raptor/zrvl/a-normal)

;;; TODO: Remove the NIL bindings and test gensyms in each one.
;;;
;;; TODO: should non-NIL symbols consistently be quoted? probably
(test printable-bindings
  (let ((binding (new-ir-id)))
    (is (string-equal (format nil "(%%binding (%%i 32) ~S (%%constant 42) nil)" binding)
                      (format nil "~A" (%%binding (%%i 32) binding (%%constant 42))))))
  (let ((ref (new-ir-id)))
    (is (string-equal (format nil "(%%binding nil 0 (%%literal ~S) nil)" ref)
                      (format nil "~A" (%%binding nil 0 (%%literal ref))))))
  (let ((reference (new-ir-id)))
    (is (string-equal (format nil "(%%binding nil 42 (%%variable 'x ~S) nil)" reference)
                      (format nil "~A" (%%binding nil 42 (%%variable 'x reference))))))
  (let ((item (new-ir-id)))
    (is (string-equal (format nil "(%%binding nil 0 (%%variable-binding 'foo ~S) nil)" item)
                      (format nil "~A" (%%binding nil 0 (%%variable-binding 'foo item))))))
  (is (string-equal (format nil "(%%binding nil 0 (%%parameter 'foo ~S) nil)" :function)
                    (format nil "~A" (%%binding nil 0 (%%parameter 'foo :function)))))
  (is (string-equal "(%%binding nil 0 (%%constant-composite (%%args)) nil)"
                    (format nil "~A" (%%binding nil 0 (%%constant-composite (%%args))))))
  (let ((a (new-ir-id))
        (b (new-ir-id))
        (c (new-ir-id)))
    (is (string-equal (format nil "(%%binding nil 0 (%%constant-composite (%%args ~S)) nil)" a)
                      (format nil "~A" (%%binding nil 0 (%%constant-composite (%%args a))))))
    (is (string-equal (format nil "(%%binding nil 0 (%%constant-composite (%%args ~S ~S ~S)) nil)" a b c)
                      (format nil "~A" (%%binding nil 0 (%%constant-composite (%%args a b c)))))))
  (is (string-equal "(%%binding nil 42 (%%funcall '+ (%%args) #*) nil)"
                    (format nil "~A" (%%binding nil 42 (%%funcall '+ (%%args) #*)))))
  (is (string-equal "(%%binding nil 0 (%%funcall '+ (%%args 1 2 3) #*000) nil)"
                    (format nil "~A" (%%binding nil 0 (%%funcall '+ (%%args 1 2 3) #*000))))))

(test a-normal-bindings
  (let ((zombie-raptor/zrvl/a-normal::*variadic* (hash '+ (vector 'zero '%numeric-identity '%+)
                                                       '* (vector 'one '%numeric-identity '%*)
                                                       '- (vector nil '%negate '%-)
                                                       '/ (vector nil '%reciprocal '%/)))
        (zombie-raptor/zrvl/a-normal::*generics* (hash '%+ (hash #.(%%f 32) (hash #.(%%f 32) 'f32+)
                                                                 #.(%%i 32) (hash #.(%%i 32) 'i32+))))
        (zombie-raptor/zrvl/a-normal::*signatures* (hash 'zero (%%ftype (%%i 32))
                                                         'one (%%ftype (%%i 32))
                                                         '%negate (%%ftype (%%i 32) (%%i 32))
                                                         '%reciprocal (%%ftype (%%i 32) (%%i 32))
                                                         '%* (%%ftype (%%i 32) (%%i 32) (%%i 32))
                                                         '%- (%%ftype (%%i 32) (%%i 32) (%%i 32))
                                                         '%/ (%%ftype (%%i 32) (%%i 32) (%%i 32))
                                                         'f32+ (%%ftype (%%f 32) (%%f 32) (%%f 32))
                                                         'i32+ (%%ftype (%%i 32) (%%i 32) (%%i 32)))))
    (is (destructuring-bind (constants-binding funcall-bindings)
            (intermediate-representation '((+)))
          (and (eq constants-binding nil)
               (%%binding-equal* (%%binding (%%i 32) 0 (%%funcall 'zero (%%args) #*))
                                 (car funcall-bindings))
               (= 1 (length funcall-bindings)))))
    (is (destructuring-bind (constants-binding funcall-bindings)
            (intermediate-representation '((- 1)))
          (and (%%binding-equal* (%%binding (%%i 32) 0 (%%constant 1))
                                 (car constants-binding))
               (%%binding-equal* (%%binding (%%i 32) 0 (%%funcall '%negate (%%args 0) #*0))
                                 (car funcall-bindings))
               (= 1 (length constants-binding))
               (= 1 (length funcall-bindings)))))
    (let ((ir (intermediate-representation '((+ (- 2 (* 3 4)) (/ 5 6))))))
      (is (and (= 2 (length ir))
               (= 5 (length (car ir)))
               (= 4 (length (cadr ir)))
               (every #'%%binding-equal*
                      (car ir)
                      (list (%%binding (%%i 32) 0 (%%constant 2))
                            (%%binding (%%i 32) 0 (%%constant 3))
                            (%%binding (%%i 32) 0 (%%constant 4))
                            (%%binding (%%i 32) 0 (%%constant 5))
                            (%%binding (%%i 32) 0 (%%constant 6))))
               (every #'%%binding-equal*
                      (cadr ir)
                      (list (%%binding (%%i 32) 0 (%%funcall '%* (%%args 0 0) #*00))
                            (%%binding (%%i 32) 0 (%%funcall '%- (%%args 0 0) #*00))
                            (%%binding (%%i 32) 0 (%%funcall '%/ (%%args 0 0) #*00))
                            (%%binding (%%i 32) 0 (%%funcall 'i32+ (%%args 0 0) #*00)))))))
    (let ((ir (intermediate-representation '((+ 1f0 2f0 3f0 4f0 5f0 6f0 7f0)))))
      (is (and (= 2 (length ir))
               (= 7 (length (car ir)))
               (= 6 (length (cadr ir)))
               (every #'%%binding-equal*
                      (car ir)
                      (list (%%binding (%%f 32) 0 (%%constant 1f0))
                            (%%binding (%%f 32) 0 (%%constant 2f0))
                            (%%binding (%%f 32) 0 (%%constant 3f0))
                            (%%binding (%%f 32) 0 (%%constant 4f0))
                            (%%binding (%%f 32) 0 (%%constant 5f0))
                            (%%binding (%%f 32) 0 (%%constant 6f0))
                            (%%binding (%%f 32) 0 (%%constant 7f0))))
               (every #'%%binding-equal*
                      (cadr ir)
                      (list (%%binding (%%f 32) 0 (%%funcall 'f32+ (%%args 0 0) #*00))
                            (%%binding (%%f 32) 0 (%%funcall 'f32+ (%%args 0 0) #*00))
                            (%%binding (%%f 32) 0 (%%funcall 'f32+ (%%args 0 0) #*00))
                            (%%binding (%%f 32) 0 (%%funcall 'f32+ (%%args 0 0) #*00))
                            (%%binding (%%f 32) 0 (%%funcall 'f32+ (%%args 0 0) #*00))
                            (%%binding (%%f 32) 0 (%%funcall 'f32+ (%%args 0 0) #*00)))))))))
