(cl:defpackage #:zombie-raptor/tests/all
  (:use #:cl #:zr-utils)
  (:import-from #:fiveam
                #:def-suite
                #:for-all
                #:in-suite
                #:is
                #:test)
  (:export #:+most-positive-single-float-in-vec*+
           #:def-suite
           #:for-all
           #:gen-buffer
           #:gen-float
           #:gen-matrix
           #:gen-vec
           #:gen-vec2
           #:gen-vec3
           #:in-suite
           #:is
           #:random-integer-in-range
           #:test
           #:zombie-raptor/tests))

(cl:in-package #:zombie-raptor/tests/all)

(def-suite zombie-raptor/tests)

(defconstant +most-positive-single-float-in-vec*+
  (sqrt (min most-positive-single-float (- most-negative-single-float)))
  "
This is the most positive bound for the individual elements in a vec*
operation, above which overflowing can happen.")

(define-function (random-integer-in-range :inline t) (min max)
  (let ((range (1+ (- max min)))
        (offset min))
    (+ (random range) offset)))

;;; Note: This is probably not going to generate values very close to
;;; zero, even though those might be important in testing.
(define-function (gen-float :inline t) (&key (bounded? t))
  "
Generates a SINGLE-FLOAT with a random sign that is zero, one, a
number between 0 or 1, or an arbitrary number that is probably very
large. If bounded? is requested, then it will use a large number that
is small enough to avoid most overflows."
  (lambda ()
    (* (let ((sign (random 2)))
         (ecase sign (0 -1f0) (1 1f0)))
       (let ((range (random 4)))
         (ecase range
           (0 0f0)
           (1 1f0)
           (2 (random 1f0))
           (3 (if bounded?
                  +most-positive-single-float-in-vec*+
                  (min most-positive-single-float (- most-negative-single-float)))))))))

(define-function (%gen-vec :inline t) (length &key (bounded? t))
  (lambda ()
    (let ((vec (make-array length :element-type 'single-float)))
      (map-into vec (gen-float :bounded? bounded?)))))

(define-function (gen-vec2 :inline t) (&key (bounded? t))
  (%gen-vec 2 :bounded? bounded?))

(define-function (gen-vec3 :inline t) (&key (bounded? t))
  (%gen-vec 3 :bounded? bounded?))

(define-function (gen-vec :inline t) (&key (bounded? t) (min 3) (max 4))
  (%gen-vec (if (= min max)
               max
               (random-integer-in-range min max))
           :bounded? bounded?))

(define-function (gen-matrix :inline t) (&key bounded?)
  (%gen-vec 16 :bounded? bounded?))
