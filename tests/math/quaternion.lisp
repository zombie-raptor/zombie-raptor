(cl:defpackage #:zombie-raptor/tests/math/quaternion
  (:use #:cl
        #:zombie-raptor/math/quaternion
        #:zombie-raptor/tests/all)
  (:export #:zombie-raptor/math/quaternion))

(cl:in-package #:zombie-raptor/tests/math/quaternion)

(def-suite zombie-raptor/math/quaternion
    :in zombie-raptor/tests)

(in-suite zombie-raptor/math/quaternion)

;;; make-quaternion quaternion
;;; scalar*quaternion scalar*quaternion-into! scalar*quaternion/mv
;;; invert-quaternion invert-quaternion-into! invert-quaternion/mv
;;; quaternion+ quaternion+-into! quaternion+/mv
;;; quaternion- quaternion--into! quaternion-/mv
;;; quaternion-dot quaternion-dot/mv
;;; quaternion* quaternion*-into! quaternion*/mv
;;; quaternion->rotation-matrix
;;; quaternion-rotation-of-coordinates quaternion-rotation-of-vector quaternion-rotation-of-vector-into! quaternion-rotation/mv
;;; rotation-quaternion rotation/mv
;;; rotate-pitch rotate-pitch/mv rotate-roll rotate-roll/mv rotate-yaw rotate-yaw/mv
