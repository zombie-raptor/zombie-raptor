(cl:defpackage #:zombie-raptor/tests/math/matrix
  (:use #:cl
        #:zombie-raptor/math/matrix
        #:zombie-raptor/tests/all
        #:zr-utils/array)
  (:import-from #:static-vectors
                #:free-static-vector)
  (:export #:zombie-raptor/math/matrix))

(cl:in-package #:zombie-raptor/tests/math/matrix)

(def-suite zombie-raptor/math/matrix
    :in zombie-raptor/tests)

(in-suite zombie-raptor/math/matrix)

;;; (setf matrix) matref (setf matref)

(test matrix-type
  "Is the matrix type defined properly?"
  (for-all ((m (gen-matrix)))
    (is (typep m 'matrix))))

(test matrix-is-of-matrix-type
  "
Is the object created by the matrix constructor of the matrix type?"
  (let ((m (matrix  0f0  1f0  2f0  3f0
                    4f0  5f0  6f0  7f0
                    8f0  9f0 10f0 11f0
                    12f0 13f0 14f0 15f0)))
    (is (typep m 'matrix))))

(test print-matrix-simple
  "
Does print-matrix correctly print a simple matrix with the expected
string output?"
  (let* ((m (identity-matrix))
         (output-string (with-output-to-string (stream)
                          (print-matrix m stream))))

    (is (string= output-string #.(concatenate 'string
                                              (format nil "[   1.000000    0.000000    0.000000    0.000000 ]~%")
                                              (format nil "[   0.000000    1.000000    0.000000    0.000000 ]~%")
                                              (format nil "[   0.000000    0.000000    1.000000    0.000000 ]~%")
                                              (format nil "[   0.000000    0.000000    0.000000    1.000000 ]~%"))))))

;; ;;; Should print matrix handle close to zero? Test all of print matrix here.
;; (test print-matrix-fancy
;;   "
;; Does print-matrix correctly print a matrix full of edge cases with the
;; expected string output?"
;;   (let* ((m (matrix 1f0 0f0 -5.4f0 1f-6
;;                     3f20 -2f-8 1.50f0 2.2f-7
;;                     (coerce (/ 18) 'single-float) (coerce (/ 32) 'single-float) (coerce (/ 3) 'single-float) 1.11111111f0
;;                     0.00001f0 -5f0 -3f13 9.9999999f0))
;;          (output-string (with-output-to-string (stream)
;;                           (print-matrix m stream))))

;;     (is (string= output-string #.(concatenate 'string
;;                                               (format nil "[   1.000000    0.000000   -5.000000    0.000001 ]~%")
;;                                               (format nil "[ 3.0000e+20    1.000000    1.500000    2.200000 ]~%")
;;                                               (format nil "[   0.055556    0.031250    0.333333    1.111111 ]~%")
;;                                               (format nil "[   0.000010 -3.0000e+13   10.000000    1.000000 ]~%"))))))


(test zero-matrix
  "Is the zero matrix correctly defined?"
  (let ((m (zero-matrix)))
    (is (every #'zerop m))))

(test zero-matrix=
  "Does matrix= works on zero matrices?"
  (is (matrix= (zero-matrix) (zero-matrix)))
  (is (matrix= (zero-matrix)
               (matrix 0f0 0f0 0f0 0f0
                       0f0 0f0 0f0 0f0
                       0f0 0f0 0f0 0f0
                       0f0 0f0 0f0 0f0))))

(test zero-matrix*vector
  "
Will the zero matrix multiplied with a vector always produce a zero
vector? This version hardcodes the vector."
  (let ((m (zero-matrix)))
    (is (every #'zerop (multiple-value-list (matrix*vector/mv m 1f0 2f0 3f0))))))

(test zero-matrix*vector/random
  "
Will the zero matrix multiplied with a random vector always produce a
zero vector? This fancier assumes #'array-of-3 and #'array-of-4 work
correctly and that the vector types are defined correctly.
"
  (for-all ((v (gen-vec :bounded? nil)))
    (let ((m (zero-matrix)))
      (if (= 4 (length v))
          (is (every #'zerop (multiple-value-list (multiple-value-call #'matrix*vector/mv m (array-of-4 v)))))
          (is (every #'zerop (multiple-value-list (multiple-value-call #'matrix*vector/mv m (array-of-3 v)))))))))

(test identity-matrix
  "
Is the identity matrix correctly defined? That is, the diagonals must
be 1 and everything else must be 0."
  (is (matrix= (identity-matrix)
               (matrix 1f0 0f0 0f0 0f0
                       0f0 1f0 0f0 0f0
                       0f0 0f0 1f0 0f0
                       0f0 0f0 0f0 1f0))))

(test zero-matrix-is-not-identity-matrix
  "
Does matrix= return NIL when comparing a zero-matrix to the
identity-matrix?"
  (is (not (matrix= (zero-matrix) (identity-matrix)))))

(test matrix=/random
  "Is a random matrix equal to itself? Is a copy?"
  (for-all ((m (gen-matrix)))
    (is (matrix= m m))
    (is (matrix= m (copy-seq m)))))

(test not-matrix=/random
  "
Is a random matrix that differs by one element not equal to itself?"
  (for-all ((k (lambda () (random-integer-in-range 0 15)))
            (m1 (gen-matrix)))
    (let ((m2 (copy-seq m1)))
      (setf (aref m1 k) 42f0
            (aref m2 k) 0f0)
      (is (not (matrix= m1 m2))))))

(test c-matrix-is-matrix
  "
Is a \"C matrix\", created by static-vectors, identical to a native CL
matrix, except for the need to manually manage its memory?"
  (let ((m1 (matrix 1f0 2f0 3f0 4f0
                    2f0 4f0 5f0 6f0
                    3f0 4f0 5f0 6f0
                    5f0 6f0 6f0 7f0))
        (m2 (c-matrix 1f0 2f0 3f0 4f0
                      2f0 4f0 5f0 6f0
                      3f0 4f0 5f0 6f0
                      5f0 6f0 6f0 7f0)))
    (unwind-protect
         (progn (is (matrix= m1 m2))
                (is (typep m2 'matrix)))
      (free-static-vector m2))))

(test scale-matrix
  "
Is the scale matrix properly defined? The diagonals should be the
scale and everything else should be 0."
  (for-all ((v (gen-vec3 :bounded? nil)))
    (let ((m (scale (aref v 0) (aref v 1) (aref v 2))))
      (is (matrix= m
                   (matrix (aref v 0) 0f0        0f0        0f0
                           0f0        (aref v 1) 0f0        0f0
                           0f0        0f0        (aref v 2) 0f0
                           0f0        0f0        0f0        1f0))))))

(test scacle-matrix*vector-of-ones
  "Does the scale matrix correctly scale a vector of ones?"
  (let ((m (scale 3f0 -5f0 10f3)))
    (multiple-value-bind (x y z w)
        (multiple-value-call #'matrix*vector/mv m 1f0 1f0 1f0)
      (is (= x 3f0))
      (is (= y -5f0))
      (is (= z 10f3))
      (is (= w 1f0)))))

(test scale-matrix*vector
  "Does the scale matrix correctly scale a random vector?"
  (for-all ((v1 (gen-vec3))
            (v2 (gen-vec3)))
    (let ((m (scale (aref v2 0) (aref v2 1) (aref v2 2))))
      (multiple-value-bind (x y z w)
          (multiple-value-call #'matrix*vector/mv m (array-of-3 v1))
        (is (= x (* (aref v1 0) (aref v2 0))))
        (is (= y (* (aref v1 1) (aref v2 1))))
        (is (= z (* (aref v1 2) (aref v2 2))))
        (is (= w 1f0))))))

(test translation-matrix
  "
Is the translation matrix properly defined? Everything should be 0
except the diagonals, which are 1, and the rightmost column, which is
the translation."
  (for-all ((v (gen-vec3 :bounded? nil)))
    (let ((m (translate (aref v 0) (aref v 1) (aref v 2))))
      (is (matrix= m
                   (matrix 1f0 0f0 0f0 (aref v 0)
                           0f0 1f0 0f0 (aref v 1)
                           0f0 0f0 1f0 (aref v 2)
                           0f0 0f0 0f0 1f0))))))

(test translation-matrix*zero-vector
  "Does the translation matrix correctly translate the zero vector?"
  (let ((m (translate 1f0 2f0 -3f0)))
    (multiple-value-bind (x y z w)
        (multiple-value-call #'matrix*vector/mv m 0f0 0f0 0f0)
      (is (= x 1f0))
      (is (= y 2f0))
      (is (= z -3f0))
      (is (= w 1f0)))))

(test translation-matrix*vector
  "Does the translation matrix correctly translate a random vector?"
  (for-all ((v1 (gen-vec3))
            (v2 (gen-vec3)))
    (let ((m (translate (aref v2 0) (aref v2 1) (aref v2 2))))
      (multiple-value-bind (x y z w)
          (if (= 4 (length v1))
              (multiple-value-call #'matrix*vector/mv m (array-of-4 v1))
              (multiple-value-call #'matrix*vector/mv m (array-of-3 v1)))
        (is (= x (+ (aref v1 0) (aref v2 0))))
        (is (= y (+ (aref v1 1) (aref v2 1))))
        (is (= z (+ (aref v1 2) (aref v2 2))))
        (is (= w (if (= 4 (length v1)) (aref v1 0) 1f0)))))))

;;; matrix*
;;; inverse-matrix
;;; transpose-matrix
;;; quaternion-rotation-matrix
;;; perspective-matrix finite-frustum infinite-frustum ortho
;;; make-projection-matrix make-ortho
;;; camera-lerp
;;; camera-matrix camera-2D-matrix model-matrix* normal-matrix model-matrix

;;; maybe test: make-gl-matrix-storage define-matrix-constructor-pair foreign-matrices
