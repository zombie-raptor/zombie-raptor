(defpackage #:zombie-raptor/tests/math/vector
  (:use #:cl
        #:zombie-raptor/math/vector
        #:zombie-raptor/tests/all)
  (:export #:zombie-raptor/math/vector))

(in-package #:zombie-raptor/tests/math/vector)

(def-suite zombie-raptor/math/vector
    :in zombie-raptor/tests)

(in-suite zombie-raptor/math/vector)

(test vector-constructors
  "Are the vector constructors and the vector type properly defined?"
  (is (equalp (vec 1f0 2f0 3f0)
              (make-array 3
                          :element-type 'single-float
                          :initial-contents '(1f0 2f0 3f0))))
  (is (equalp (make-vec3) (vec3 0f0 0f0 0f0)))
  (is (equalp (make-vec3 :initial-element -42f0) (vec3 -42f0 -42f0 -42f0)))
  (is (equalp (vec -10f0 20f-3 -30f3) (vec3 -10f0 20f-3 -30f3)))
  (is (equalp (vec2 1f0 2f0)
              (make-array 2
                          :element-type 'single-float
                          :initial-contents '(1f0 2f0))))
  (is (equalp (make-vec2) (vec2 0f0 0f0)))
  (is (equalp (make-vec2 :initial-element -42f0) (vec2 -42f0 -42f0)))
  (is (equalp (vec4 1f0 2f0 3f0 4f0)
              (make-array 4
                          :element-type 'single-float
                          :initial-contents '(1f0 2f0 3f0 4f0))))
  (is (equalp (make-vec4) (vec4 0f0 0f0 0f0 0f0)))
  (is (equalp (make-vec4 :initial-element -42f0) (vec4 -42f0 -42f0 -42f0 -42f0)))
  (is (typep (make-array 2 :element-type 'single-float :initial-element 2f0) 'vec2))
  (is (typep (make-array 3 :element-type 'single-float :initial-element 3f0) 'vec3))
  (is (typep (make-array 4 :element-type 'single-float :initial-element 4f0) 'vec4))
  (is (typep (make-vec2) 'vec2))
  (is (typep (make-vec3) 'vec3))
  (is (typep (make-vec4) 'vec4))
  (is (typep (vec2 1f0 2f0) 'vec2))
  (is (not (typep (make-vec3) 'vec2)))
  (is (typep (vec3 2f0 3f0 -10f0) 'vec3))
  (is (typep (vec3 -87f3 33f-3 1f0) '(and vec (vec 3) vec3)))
  (is (typep (vec4 3f0 4f0 5f0 9f0) 'vec4)))

(test vec=
  "Are the variants of vec= properly defined?"
  (let* ((v2.1 (vec2 1f0 2f0))
         (v2.2 (map 'vec2 #'/ v2.1 (vec2 10f0 20f0)))
         (v3.1 (vec3 3f0 4f0 5f0))
         (v3.2 (map 'vec3 #'/ v3.1 (vec3 10f0 20f0 40f0)))
         (v4.1 (vec4 5f0 6f0 7f0 -1f0))
         (v4.2 (map 'vec4 #'/ v4.1 (vec4 10f0 20f0 40f0 80f0))))
    (is (vec2= v2.1 v2.1))
    (is (not (vec2= v2.1 v2.2)))
    (is (vec3= v3.1 v3.1))
    (is (not (vec3= v3.1 v3.2)))
    (is (vec4= v4.1 v4.1))
    (is (not (vec4= v4.1 v4.2)))))

(test scalar*vec/mv
  "Are the variants of scalar*vec properly defined?"
  (for-all ((a (gen-float))
            (b (gen-float))
            (c (gen-float))
            (d (gen-float)))
    (multiple-value-bind (x y z)
        (scalar*vec/mv a b c d)
      (is (= x (* a b)))
      (is (= y (* a c)))
      (is (= z (* a d))))))

(test vec+/mv
  "Are the variants of vec+ properly defined?"
  (for-all ((a (gen-float))
            (b (gen-float))
            (c (gen-float))
            (d (gen-float))
            (e (gen-float))
            (f (gen-float)))
    (multiple-value-bind (x y z)
        (vec+/mv a b c d e f)
      (is (= x (+ a d)))
      (is (= y (+ b e)))
      (is (= z (+ c f))))))

(test vec-/mv
  "Are the variants of vec- properly defined?"
  (for-all ((a (gen-float))
            (b (gen-float))
            (c (gen-float))
            (d (gen-float))
            (e (gen-float))
            (f (gen-float)))
    (multiple-value-bind (x y z)
        (vec-/mv a b c d e f)
      (is (= x (- a d)))
      (is (= y (- b e)))
      (is (= z (- c f))))))

;;; vec-dot vec-dot/mv
;;; vec-cross vec-cross-into! vec-cross/mv
;;; vec-lerp vec-lerp/mv
;;; normalize normalize-into! normalize/mv
