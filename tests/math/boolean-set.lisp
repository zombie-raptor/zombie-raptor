(cl:defpackage #:zombie-raptor/tests/math/boolean-set
  (:use #:cl
        #:zombie-raptor/math/boolean-set
        #:zombie-raptor/tests/all)
  (:export #:zombie-raptor/math/boolean-set))

(cl:in-package #:zombie-raptor/tests/math/boolean-set)

(def-suite zombie-raptor/math/boolean-set
    :in zombie-raptor/tests)

(in-suite zombie-raptor/math/boolean-set)

;;; boolean-set
;;; boolean-set-difference
;;; boolean-set-index
;;; boolean-set-intersection
;;; boolean-set-member-p
;;; boolean-set-subset-p
;;; boolean-set-symmetric-difference
;;; boolean-set-union
;;; index-to-boolean-set
;;; print-boolean-set
