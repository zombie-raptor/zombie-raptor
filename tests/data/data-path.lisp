(cl:defpackage #:zombie-raptor/tests/data/data-path
  (:use #:cl
        #:zombie-raptor/data/data-path
        #:zombie-raptor/tests/all)
  (:import-from #:uiop)
  ;; Test the private functions, too.
  (:import-from #:zombie-raptor/data/data-path
                #:absolute-path
                #:relative-path)
  (:export #:zombie-raptor/data/data-path))

(cl:in-package #:zombie-raptor/tests/data/data-path)

(def-suite zombie-raptor/data/data-path
    :in zombie-raptor/tests)

(in-suite zombie-raptor/data/data-path)

(test absolute-and-relative-pathnames
  "Are the absolute and relative pathname helper functions correct?"
  (is (equal (absolute-path "foo" "bar" "baz")
             #P"/foo/bar/baz/"))
  (is (equal (absolute-path "foo")
             #P"/foo/"))
  (is (equal (relative-path "foo" "bar" "baz")
             #P"foo/bar/baz/")
      (equal (relative-path "foo")
             #P"foo/")))

;;; Note: These tests are near trivial, but anything more
;;; sophisticated would have to reinvent the logic of
;;; `uiop:xdg-data-home' and would be more of a test of that function
;;; than these functions.
;;
;; This tests both the one directory style (app-name only) and the two
;; directory style (org-name and app-name).
(test xdg-pathnames
  "Do the XDG-derived pathname functions behave properly?"
  ;; The data path, which is probably ~/.local/share/
  (is (equal (local-data-path "bar" "foo")
             (uiop:xdg-data-home #P"foo/" #P"bar/")))
  (is (equal (local-data-path "bar")
             (uiop:xdg-data-home #P"bar/")))
  ;; The config path, which is probably ~/.config/
  (is (equal (local-config-path "bar" "foo")
             (uiop:xdg-config-home #P"foo/" #P"bar/")))
  (is (equal (local-config-path "bar")
             (uiop:xdg-config-home #P"bar/")))
  ;; The cache path, which is probably ~/.cache/
  (is (equal (local-cache-path "bar" "foo")
             (uiop:xdg-cache-home #P"foo/" #P"bar/")))
  (is (equal (local-cache-path "bar")
             (uiop:xdg-cache-home  #P"bar/"))))

;;; Note: Again, this is testing the additional functionality, not the
;;; library functionality.
;;;
;;; This tests for the directory that contains this file, which should
;;; definitely exist. It won't work if this project isn't recognized
;;; by ASDF for whatever reason, though.
(test subdirectories-of-zr
  "Does main-data-directory correctly add subdirectories to the end?"
  ;; First, test the version with each directory as its own string.
  (is (equal (main-data-directory "zombie-raptor" "tests" "data")
             (uiop:subpathname (asdf:system-source-directory "zombie-raptor")
                               #P"tests/data/")))
  ;; Next, make sure the variant that takes a relative path is
  ;; equivalent to the regular version.
  (is (equal (main-data-directory* "zombie-raptor" #P"tests/data/")
             (main-data-directory "zombie-raptor" "tests" "data")))
  ;; That variant should also accept a string.
  (is (equal (main-data-directory* "zombie-raptor" "tests/data/")
             (main-data-directory "zombie-raptor" "tests" "data")))
  ;; Finally, make sure that keywords can be used instead of strings
  ;; for the system name.
  (is (equal (main-data-directory :zombie-raptor "tests" "data")
             (main-data-directory "zombie-raptor" "tests" "data"))))
