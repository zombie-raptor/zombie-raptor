(cl:defpackage #:zombie-raptor/tests/data/game-data
  (:use #:cl
        #:zombie-raptor/data/game-data
        #:zombie-raptor/tests/all)
  (:export #:zombie-raptor/data/game-data))

(cl:in-package #:zombie-raptor/tests/data/game-data)

(def-suite zombie-raptor/data/game-data
    :in zombie-raptor/tests)

(in-suite zombie-raptor/data/game-data)
