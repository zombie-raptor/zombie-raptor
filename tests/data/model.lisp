(cl:defpackage #:zombie-raptor/tests/data/model
  (:use #:cl
        #:zombie-raptor/data/model
        #:zombie-raptor/tests/all)
  (:export #:zombie-raptor/data/model))

(cl:in-package #:zombie-raptor/tests/data/model)

(def-suite zombie-raptor/data/model
    :in zombie-raptor/tests)

(in-suite zombie-raptor/data/model)
