(cl:defpackage #:zombie-raptor/tests/text/font
  (:use #:cl
        #:zombie-raptor/text/font
        #:zombie-raptor/tests/all)
  (:export #:zombie-raptor/text/font))

(cl:in-package #:zombie-raptor/tests/text/font)

(def-suite zombie-raptor/text/font
    :in zombie-raptor/tests)

(in-suite zombie-raptor/text/font)
