(cl:defpackage #:zombie-raptor/tests/core/settings
  (:use #:cl
        #:zombie-raptor/core/settings
        #:zombie-raptor/tests/all)
  (:export #:zombie-raptor/core/settings))

(cl:in-package #:zombie-raptor/tests/core/settings)

(def-suite zombie-raptor/core/settings
    :in zombie-raptor/tests)

(in-suite zombie-raptor/core/settings)

(test deep-copy-of-settings
  "Does changing a sequence stored in the original settings affect its copy?"
  ;; First the setup and the bindings.
  ;;
  ;; Note: The literal strings are copied so that modifying them is
  ;; not undefined behavior.
  (let* ((a (copy-seq "a"))
         (b (copy-seq "b"))
         (c (copy-seq "c"))
         (s1 (make-settings :title a :app-name b :org-name c))
         (s2 (copy-settings s1)))
    (with-settings ((title-1 title)
                    (app-name-1 app-name)
                    (org-name-1 org-name))
        s1
      (with-settings ((title-2 title)
                      (app-name-2 app-name)
                      (org-name-2 org-name))
          s2
        ;; Test identity.
        (is (not (eq title-1 title-2)))
        (is (not (eq app-name-1 app-name-2)))
        (is (not (eq org-name-1 org-name-2)))
        ;; Change the originals and then test string equality.
        (psetf (aref title-1 0) #\x
               (aref app-name-1 0) #\y
               (aref org-name-1 0) #\z)
        (is (string/= title-1 title-2))
        (is (string/= app-name-1 app-name-2))
        (is (string/= org-name-1 org-name-2))))))
