(cl:defpackage #:zombie-raptor/tests/entity/entity
  (:use #:cl
        #:zombie-raptor/entity/entity
        #:zombie-raptor/tests/all)
  (:export #:zombie-raptor/entity/entity))

(cl:in-package #:zombie-raptor/tests/entity/entity)

(def-suite zombie-raptor/entity/entity
    :in zombie-raptor/tests)

(in-suite zombie-raptor/entity/entity)
