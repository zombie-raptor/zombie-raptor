(cl:defpackage #:zombie-raptor/tests/entity/physics
  (:use #:cl
        #:zombie-raptor/entity/physics
        #:zombie-raptor/tests/all)
  (:export #:zombie-raptor/entity/physics))

(cl:in-package #:zombie-raptor/tests/entity/physics)

(def-suite zombie-raptor/entity/physics
    :in zombie-raptor/tests)

(in-suite zombie-raptor/entity/physics)
