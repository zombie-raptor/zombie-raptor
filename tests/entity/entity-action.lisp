(cl:defpackage #:zombie-raptor/tests/entity/entity-action
  (:use #:cl
        #:zombie-raptor/entity/entity-action
        #:zombie-raptor/tests/all)
  (:export #:zombie-raptor/entity/entity-action))

(cl:in-package #:zombie-raptor/tests/entity/entity-action)

(def-suite zombie-raptor/entity/entity-action
    :in zombie-raptor/tests)

(in-suite zombie-raptor/entity/entity-action)
