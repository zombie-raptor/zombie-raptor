(uiop:define-package #:zombie-raptor/render
  (:nicknames #:zr/render)
  (:use #:cl
        #:zr-utils)
  (:use-reexport #:zombie-raptor/render/gl-data
                 #:zombie-raptor/render/gl))
