(defpackage #:zombie-raptor/render/gl
  (:documentation "The OpenGL renderer.")
  (:use #:cl
        #:zombie-raptor/core/conditions
        #:zombie-raptor/core/constants
        #:zombie-raptor/data/model
        #:zombie-raptor/data/shader
        #:zombie-raptor/data/texture
        #:zr-utils)
  (:import-from #:cl-opengl)
  (:import-from #:%gl)
  (:import-from #:alexandria
                #:with-gensyms)
  (:import-from #:cffi
                #:foreign-enum-value
                #:foreign-pointer
                #:foreign-type-size)
  (:import-from #:static-vectors
                #:static-vector-pointer
                #:with-static-vectors)
  (:export #:delete-vaos
           #:draw
           #:draw-2D
           #:geometry-id
           #:load-render-data
           #:make-render-data
           #:max-texture-max-anisotropy-ext
           #:setup-gl
           #:with-gl-rendering))

(in-package #:zombie-raptor/render/gl)

(defconstant +float-size+ (foreign-type-size :float))
(defconstant +uint16-size+ (foreign-type-size :uint16))

(defconstant +gl-triangles+ (foreign-enum-value '%gl:enum :triangles))
(defconstant +gl-unsigned-short+ (foreign-enum-value '%gl:enum :unsigned-short))

;;; Note: the eval-when is needed for CCL
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defconstant +max-attributes+ 10)
  (defconstant +max-matrices+ 4)
  (defconstant +max-textures+ 16))

(deftype program-id ()
  'uint32)

(deftype geometry-id ()
  `(mod ,+max-geometry+))

(define-function (uniform-matrix :inline t) (matrix-location matrix)
  (%gl:uniform-matrix-4fv matrix-location 1 nil matrix))

(defmacro define-gl-values (name &body gl-value-list)
  (with-gensyms (gl-values int32-array int32-array-pointer)
    `(progn
       (defstruct (,name (:conc-name nil)
                         (:constructor ,(symbol-to-%make-symbol name)))
         ,@(mapcar (lambda (gl-value)
                     `(,gl-value -1 :type int32))
            gl-value-list))
       (define-function (,(symbol-to-make-symbol name) :inline t) ()
         (with-static-vectors ((,int32-array 1 :element-type 'int32))
           (declare ((simple-array int32 (1)) ,int32-array))
           (let ((,gl-values (,(symbol-to-%make-symbol name)))
                 (,int32-array-pointer (static-vector-pointer ,int32-array)))
             ,@(mapcar (lambda (gl-value)
                         `(progn (%gl:get-integer-v ,(symbol-to-keyword gl-value) ,int32-array-pointer)
                                 (setf (,gl-value ,gl-values) (aref ,int32-array 0))))
                       gl-value-list)
             ,gl-values))))))

(define-gl-values gl-values
  max-texture-max-anisotropy-ext)

(defun setup-gl (&key reverse-cull? wireframe?)
  "Sets up GL. An SDL window and a GL context need to be created
before you run this, and this must be run in the main SDL thread."
  (if wireframe?
      (gl:enable :depth-test)
      (gl:enable :depth-test :cull-face))
  (gl:polygon-mode :front-and-back (if wireframe? :line :fill))
  (unless wireframe?
    (gl:cull-face (if reverse-cull? :front :back)))
  (gl:hint :line-smooth-hint :nicest)
  (gl:hint :polygon-smooth-hint :nicest)
  (make-gl-values))

;;; Note: this could be rewritten
(define-struct-of-arrays render-data #.+max-geometry+
  (vao                uint32)
  (size               uint32)
  (texture-type-enums uint32 :size #.+max-textures+)
  (texture-ids        fixnum :size #.+max-textures+ :initial-element 1)
  (texture-locations  int8 :size #.+max-textures+ :initial-element -1)
  (program            program-id)
  (matrix-locations   int16 :size #.+max-matrices+))

(defmethod print-object ((render-data render-data) stream)
  (print-unreadable-object (render-data stream :type t :identity t))
  render-data)

(define-accessor-macro with-render-data-accessors #:render-data-)

(defun make-vao (array-buffer element-array-buffer vertex-data elements vertex-size attribute-count attribute-sizes model-stride)
  (let ((vao-id (gl:gen-vertex-array)))
    (declare (type uint32 vao-id))
    (gl:bind-vertex-array vao-id)
    (gl:bind-buffer :array-buffer array-buffer)
    (%gl:buffer-data :array-buffer
                     (* (length vertex-data) +float-size+)
                     (static-vector-pointer vertex-data)
                     :static-draw)
    (gl:bind-buffer :element-array-buffer element-array-buffer)
    (%gl:buffer-data :element-array-buffer
                     (* (length elements) +uint16-size+)
                     (static-vector-pointer elements)
                     :static-draw)
    (loop :for i :from 0 :below (1+ attribute-count)
          :for memory-offset :of-type uint32 := 0
            :then (+ memory-offset (* vertex-attribute-size +float-size+))
          :for vertex-attribute-size := vertex-size :then (aref attribute-sizes (- i 1))
          :with stride-size-in-memory := (* model-stride +float-size+)
          :do
             (gl:enable-vertex-attrib-array i)
             (gl:vertex-attrib-pointer i
                                       vertex-attribute-size
                                       :float
                                       nil
                                       stride-size-in-memory
                                       memory-offset))
    (gl:bind-vertex-array 0)
    (gl:bind-buffer :array-buffer 0)
    (gl:bind-buffer :element-array-buffer 0)
    vao-id))

(define-function delete-vaos ((render-data render-data))
  (with-render-data-accessors (vao) render-data
    (gl:delete-vertex-arrays (remove-if-not #'plusp (remove-duplicates vao)))))

(define-function texture-location ((texture-name keyword)
                                   (program shader-program))
  (let ((location (find-location-in-program texture-name program)))
    (error-when (= location -1)
                'missing-object-error
                :printable-type-name "texture"
                :object-name texture-name
                :details (format nil "This is required by the program ~A." (name program)))
    location))

(define-function (find-texture :return loaded-texture) ((texture-name keyword)
                                                        (model-name keyword)
                                                        (textures hash-table))
  (error-unless (hash-table-value-present? texture-name textures)
                'missing-object-error
                :printable-type-name "texture"
                :name texture-name
                :details (format nil "It is required by the model ~A" model-name))
  (let ((texture (gethash texture-name textures)))
    (check-type texture loaded-texture)
    texture))

(define-function load-texture-locations ((render-data render-data)
                                         (id geometry-id)
                                         (model-textures vector)
                                         (texture-names vector)
                                         (textures hash-table)
                                         (program shader-program)
                                         (model-name keyword))
  ;; TODO: name this error
  (error-when (> (length texture-names) +max-textures+)
              "~D textures is more than the limit of ~D"
              (length texture-names)
              +max-textures+)
  (with-render-data-accessors (texture-type-enums
                               texture-ids
                               texture-locations)
      render-data
    (loop :for i :below +max-textures+
          :for texture-name :across texture-names
          :for model-texture :across model-textures
          :do (with-array-accessors ((texture-type-enums.i texture-type-enums :elt i)
                                     (texture-ids.i texture-ids :elt i)
                                     (texture-locations.i texture-locations :elt i))
                  id
                (multiple-value-bind (texture-id texture-type-enum)
                    (let ((texture (find-texture model-texture model-name textures)))
                      (values (loaded-texture-id texture)
                              (texture-type-to-enum (loaded-texture-type texture))))
                  (psetf texture-type-enums.i texture-type-enum
                         texture-ids.i texture-id
                         texture-locations.i (texture-location texture-name
                                                               program)))))))

(define-function load-matrix-locations ((render-data render-data)
                                        (id geometry-id)
                                        (program shader-program)
                                        (matrix-names vector))
  (with-render-data-accessors (matrix-locations) render-data
    (loop :for i :below (length matrix-names)
          :for matrix-name :across matrix-names
          :do (with-array-accessors ((matrix-location matrix-locations :elt i)) id
                (let ((location (find-location-in-program matrix-name program)))
                  (setf matrix-location location))))))

(defun load-render-data (render-data
                         id
                         model
                         textures
                         buffers
                         shader-programs
                         compiled-programs
                         debug)
  "
Stores the necessary OpenGL data into render-data, to be used later in
`draw' and/or `draw-2D'
"
  (let* ((vertex-size       (vertex-size model))
         (attribute-sizes   (attribute-sizes model))
         (attribute-count   (length attribute-sizes))
         (model-stride      (stride model))
         (vertex-array      (vertex-array model))
         (element-array     (element-array model))
         (model-program     (program model))
         (program-id        (gethash model-program compiled-programs))
         (program           (gethash model-program shader-programs)))
    (when (and debug (every #'zerop vertex-array))
      (warn "The model ~A has an empty vertex array." (name model)))
    (when (and debug (every #'zerop element-array))
      (warn "The model ~A has an empty element array." (name model)))
    (with-render-data-accessors (vao size (program-id* program)) render-data
      (with-array-accessors ((vao vao) (size size) (program-id* program-id*)) id
        (destructuring-bind (array-buffer element-array-buffer) buffers
          (psetf vao (make-vao array-buffer
                               element-array-buffer
                               vertex-array
                               element-array
                               vertex-size
                               attribute-count
                               attribute-sizes
                               model-stride)
                 size (length element-array)
                 program-id* program-id))
        (load-texture-locations render-data
                                id
                                (model-textures model)
                                (texture-names program)
                                textures
                                program
                                (name model))
        (load-matrix-locations render-data
                               id
                               program
                               (matrix-names program)))))
  render-data)

(define-function (use-gl-program :inline t) ((render-data render-data) (id geometry-id))
  "
Looks up the appropriate program ID for the given geometry ID and
tells OpenGL to use it.
"
  (with-render-data-accessors (program) render-data
    (with-array-accessors ((program-id program)) id
        (gl:use-program program-id))))

(defmacro uniform-matrices (matrices render-data id)
  "
Converts a list of 4x4 single-float uniform matrices into the function
calls that OpenGL expects inside of a draw call. This is done at a
macro to add no runtime cost because it only needs to be done once.
"
  (multiple-value-bind (bindings uniforms)
      (loop :for i :from 0
            :for matrix :in matrices
            :for gensym := (gensym (symbol-name '#:matrix-location-))
            :collect `(,gensym matrix-locations :elt ,i) :into bindings
            :collect `(uniform-matrix ,gensym ,matrix) :into uniforms
            :finally (return (values bindings uniforms)))
    `(with-render-data-accessors (matrix-locations)
         ,render-data
       (with-array-accessors ,bindings ,id
         ,@uniforms))))

(define-function (%invalid-texture? :inline t) ((render-data render-data)
                                                (id geometry-id)
                                                (index (mod #.+max-textures+)))
  (with-render-data-accessors (texture-locations) render-data
    (with-array-accessors ((texture-location texture-locations :elt index)) id
      (minusp texture-location))))

(define-function %draw-texture ((render-data render-data) (id geometry-id))
  (declare (optimize (speed 3)))
  (loop :for i :below +max-textures+
        :until (%invalid-texture? render-data id i)
        :do (with-render-data-accessors (texture-type-enums texture-ids texture-locations)
                render-data
              (with-array-accessors ((texture-type-enum texture-type-enums :elt i)
                                     (texture-id texture-ids :elt i)
                                     (texture-location texture-locations :elt i))
                  id
                (gl:active-texture i)
                (gl:bind-texture texture-type-enum texture-id)
                (gl:uniformi texture-location i)))))

(define-function (%draw-vao :inline t) ((render-data render-data) (id geometry-id))
  (with-render-data-accessors (vao size) render-data
    (with-array-accessors ((vao vao) (size size)) id
      (gl:bind-vertex-array vao)
      (%gl:draw-elements +gl-triangles+ size +gl-unsigned-short+ 0))))

(defmacro define-draw (name (render-data geometry-id &rest uniform-matrices) &body documentation-and-body)
  "
Defines a draw function that uses the appropriate program, gets the
uniform matrices in the given order, and then calls %draw.

Note: The matrix order matters and should match the location order in
the shader program.
"
  `(define-function ,name ((,render-data render-data)
                           (,geometry-id geometry-id)
                           ,@uniform-matrices)
     (declare (optimize (speed 3)))
     ,@documentation-and-body
     (use-gl-program ,render-data ,geometry-id)
     (uniform-matrices ,uniform-matrices ,render-data ,geometry-id)
     (%draw-texture ,render-data ,geometry-id)
     (%draw-vao ,render-data ,geometry-id)))

(define-draw draw-2D (render-data id projection-matrix view-matrix model-matrix)
  "Draws 2D entities using the given data.")

(define-draw draw (render-data id projection-matrix view-matrix model-matrix normal-matrix)
  "Draws 3D entities using the given data.")

(defmacro with-gl-rendering ((red green blue alpha &key swap-window) &body body)
  "Handles the per-frame commands that surround the draw calls."
  `(progn
     (%gl:clear-color ,red ,green ,blue ,alpha)
     (gl:clear :color-buffer :depth-buffer)
     ,@body
     (%gl:flush)
     ,swap-window))
