(cl:defpackage #:zombie-raptor/render/gl-data
  (:documentation "Handles data in formats that OpenGL expects.")
  (:use #:cl
        #:zombie-raptor/core/constants
        #:zombie-raptor/core/settings
        #:zombie-raptor/data/shader
        #:zombie-raptor/data/texture
        #:zombie-raptor/data/model
        #:zombie-raptor/math/matrix
        #:zombie-raptor/render/gl
        #:zr-utils)
  (:import-from #:alexandria
                #:maphash-values)
  (:import-from #:cl-opengl
                #:delete-buffers
                #:delete-shader
                #:delete-program
                #:gen-buffers
                #:use-program)
  (:export #:foreign-matrices
           #:gl-data
           #:make-gl-data
           #:max-mesh-id
           #:mesh-keys
           #:meshes
           #:texture))

(cl:in-package #:zombie-raptor/render/gl-data)

;;;; This file is a very high-level abstraction over render/gl

(defclass gl-data ()
  ((%meshes
    :accessor meshes
    :initform (make-render-data)
    :documentation "The render data used in `draw', designed to be
    passed to the C OpenGL API.")
   (%mesh-keys
    :accessor mesh-keys
    :documentation "Matches keyword names with internal ID numbers for
    meshes, starting at 0.")
   (%models
    :accessor models
    :initarg :models
    :documentation "Stores the models that were used to generate the
    meshes.")
   (%texture-objects
    :accessor texture-objects
    :documentation "Stores the texture objects that are turned into
    the OpenGL textures.")
   (%textures
    :accessor textures
    :documentation "Matches keyword names with an array consisting of
    the internal OpenGL ID numbers for textures, starting at 1, and
    the dimension of the texture.")
   (%compiled-shaders
    :accessor compiled-shaders
    :documentation "Matches keyword names with internal OpenGL ID
    numbers for shaders, starting at 1. The same index numbers appear
    to be used for shader programs, too.")
   (%programs
    :accessor programs
    :documentation "Matches keyword names with internal OpenGL ID
    numbers for shader programs, starting at 1. The same index numbers
    appear to be used for shaders, too.")
   ;; note: this assumes that every ID between 1 and this is valid
   (%max-mesh-id
    :accessor max-mesh-id
    :documentation "The maximum OpenGL mesh ID for OpenGL meshes.")
   (%foreign-matrices
    :accessor foreign-matrices
    :documentation "C matrices for use in the OpenGL API.")
   (%buffers
    :accessor buffers)))

(defmethod initialize-instance :after ((gl-data gl-data)
                                       &key
                                         settings
                                         shaders
                                         textures
                                         update-window-dimensions
                                         verify)
  (with-settings (debug)
      settings
    (when verify
      (check-shaders shaders))
    (let ((gl-values (setup-gl)))
      (funcall update-window-dimensions settings)
      (with-accessors* (buffers
                        compiled-shaders
                        max-mesh-id
                        mesh-keys
                        meshes
                        models
                        programs
                        (texture-hash-table textures)
                        texture-objects
                        foreign-matrices)
          gl-data
        (with-accessors* (shader-programs shaders)
            shaders
          (let ((model-count (count* models)))
            (setf buffers (gen-buffers (* 2 model-count))
                  max-mesh-id model-count
                  texture-objects textures
                  compiled-shaders (load-shaders shaders)
                  programs (make-gl-shader-programs shader-programs compiled-shaders debug)
                  texture-hash-table (load-textures textures (max-texture-max-anisotropy-ext gl-values) debug)
                  mesh-keys (load-models models model-count meshes texture-hash-table buffers shader-programs programs debug)
                  foreign-matrices (make-foreign-matrices settings))))))))

(defun load-models (models model-count meshes textures buffers shader-programs compiled-programs debug)
  ;; TODO: replace with custom-defined condition
  (error-when (> model-count +max-geometry+)
              "Too many models! The maximum is ~D"
              +max-geometry+)
  (let ((mesh-keys (make-hash-table))
        (model-counter 0))
    (declare (geometry-id model-counter))
    (do-model-list (model (all-models models) mesh-keys)
      (load-render-data meshes
                        model-counter
                        model
                        textures
                        (subseq buffers (* model-counter 2) (* 2 (1+ model-counter)))
                        shader-programs
                        compiled-programs
                        debug)
      (setf (gethash (name model) mesh-keys) model-counter)
      (incf-mod model-counter +max-geometry+))))

(define-function (make-gl-data :inline t) (&key shaders textures models settings update-window-dimensions verify)
  (make-instance 'gl-data
                 :shaders shaders
                 :textures textures
                 :models models
                 :settings settings
                 :update-window-dimensions update-window-dimensions
                 :verify verify))

(defmethod cleanup ((gl-data gl-data))
  (with-accessors* (buffers
                    compiled-shaders
                    foreign-matrices
                    meshes
                    models
                    programs
                    texture-objects)
      gl-data
    (use-program 0)
    (cleanup models)
    (cleanup foreign-matrices)
    (cleanup texture-objects)
    (maphash-values #'delete-shader compiled-shaders)
    (maphash-values #'delete-program programs)
    (delete-buffers buffers)
    (delete-vaos meshes)
    (setf compiled-shaders nil
          foreign-matrices nil
          programs nil
          buffers nil)))
