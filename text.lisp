(uiop:define-package #:zombie-raptor/text
  (:nicknames #:zr/text)
  (:use #:cl
        #:zr-utils)
  (:use-reexport #:zombie-raptor/text/font
                 #:zombie-raptor/text/ttf))
