(uiop:define-package #:zombie-raptor/core
  (:nicknames #:zr/core)
  (:use #:cl
        #:zr-utils)
  (:use-reexport #:zombie-raptor/core/client
                 #:zombie-raptor/core/conditions
                 #:zombie-raptor/core/constants
                 #:zombie-raptor/core/input
                 #:zombie-raptor/core/sdl2
                 #:zombie-raptor/core/settings
                 #:zombie-raptor/core/times
                 #:zombie-raptor/core/types
                 #:zombie-raptor/core/window))
