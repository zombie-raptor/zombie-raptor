(uiop:define-package #:zombie-raptor/tests
  (:use #:cl
        #:zr-utils)
  (:use-reexport #:zombie-raptor/tests/all
                 #:zombie-raptor/tests/core/settings
                 #:zombie-raptor/tests/data/data-path
                 #:zombie-raptor/tests/data/game-data
                 #:zombie-raptor/tests/data/model
                 ;; #:zombie-raptor/tests/data/shader
                 ;; #:zombie-raptor/tests/data/terrain
                 ;; #:zombie-raptor/tests/data/texture
                 #:zombie-raptor/tests/entity/entity
                 #:zombie-raptor/tests/entity/entity-action
                 #:zombie-raptor/tests/entity/physics
                 #:zombie-raptor/tests/math/boolean-set
                 #:zombie-raptor/tests/math/matrix
                 #:zombie-raptor/tests/math/quaternion
                 #:zombie-raptor/tests/math/vector
                 #:zombie-raptor/tests/text/font
                 #:zombie-raptor/tests/zrvl/a-normal
                 #:zombie-raptor/tests/zrvl/core-types
                 #:zombie-raptor/tests/zrvl/zrvl))
