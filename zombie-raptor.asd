;;; Requires an ASDF version with package-inferred-system
(cl:unless (asdf:version-satisfies (asdf:asdf-version) "3.1.2")
  (cl:error "Zombie Raptor requires ASDF 3.1.2 or later."))

(asdf:defsystem #:zombie-raptor
  :description "A game engine written in Common Lisp"
  :version "0.0.0.0"
  :author "Michael Babich"
  :maintainer "Michael Babich"
  :license "MIT"
  :homepage "https://zombieraptor.com/"
  :bug-tracker "https://gitlab.com/zombie-raptor/zombie-raptor/issues"
  :source-control (:git "https://gitlab.com/zombie-raptor/zombie-raptor.git")
  :class :package-inferred-system
  :defsystem-depends-on (:asdf-package-system)
  :depends-on (:alexandria
               :bordeaux-threads
               :cffi
               :cl-autowrap
               :cl-opengl
               :cl-plus-c
               :float-features
               :pngload
               :sdl2
               :static-vectors
               :trivial-with-current-source-form
               :uiop
               :zpb-ttf
               :zr-utils
               :zombie-raptor/all)
  :in-order-to ((asdf:test-op (asdf:test-op "zombie-raptor/tests"))))

(asdf:defsystem #:zombie-raptor/tests
  :description "The tests for Zombie Raptor"
  :author "Michael Babich"
  :maintainer "Michael Babich"
  :license "MIT"
  :class :package-inferred-system
  :defsystem-depends-on (:asdf-package-system)
  :depends-on (:fiveam
               :zr-utils)
  :perform (asdf:test-op (o s) (uiop:symbol-call :fiveam
                                                 :run!
                                                 (cl:intern (cl:symbol-name '#:zombie-raptor/tests)
                                                            '#:zombie-raptor/tests))))

(cl:dolist (system-packages '((:cffi        (:cffi-sys))
                              (:cl-autowrap (:autowrap))
                              (:cl-opengl   (:gl :%gl))
                              (:fiveam      (:it.bese.fiveam))
                              (:sdl2        (:sdl2-ffi :sdl2-ffi.accessors :sdl2-ffi.functions))
                              (:cl-plus-c   (:plus-c))
                              (:zombie-raptor/data/generate-glsl (:zr-glsl))
                              (:zombie-raptor/zrvl/core-types (:zrvlir))
                              (:zombie-raptor/zrvl/spir-v-instructions (:zrspv))
                              (:zombie-raptor/zrvl/zrvl (:zrvl))))
  (cl:destructuring-bind (system packages)
      system-packages
    (asdf:register-system-packages system packages)))
