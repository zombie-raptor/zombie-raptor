Contributing to Zombie Raptor
=============================

Commit messages
---------------

Zombie Raptor uses a fairly conventional git style for its commit
messages:

- The first line of a commit message must be no longer than 70
  characters. If there is a body in the commit message, the first line
  must be followed by a blank line and then the remaining text should
  not have lines longer than 72 characters.

- Paragraphs in the body must be separated by blank lines and must not
  begin with any indentation.

- Sentences may be separated by one space instead of two if you really
  want to conform to one uniform commit style.

- The first line must not end in punctuation.

- The first line should use the imperative mood.

- Bug fixes must begin with the word "Fix". If they fix something in
  the issue tracker, they must refer to the issue number in [a way
  that GitLab recognizes](https://docs.gitlab.com/ee/user/project/issues/automatic_issue_closing.html).

  - For example, a commit message could start with "Fix #42".

- If the commit implements a feature request in the issue tracker, the
  commit message must begin with "Implement".

  - For example, a commit message could start with "Implement #1".

Consing (Heap Allocations)
--------------------------

Avoid overuse of the garbage collected heap in large and long-running
loops, especially the game loop.

- Everything in the game loop must not cons in (64-bit) SBCL. The game
  loop should not cons in other supported implementations, either, but
  those are much harder to profile for consing.

- In loops that matter, prefer using pre-allocated mutable data
  structures and non-boxed multiple return values. Only use (declare
  (dynamic-extent foo)) to stack allocate when the alternatives are
  considerably messier. Currently, stack allocation is useful for some
  matrix code.

- Closures cons unless they are stack allocated through declare
  dynamic-extent. Pre-allocating the closures before the loop begins
  is another possibility.

- Working with double-float or 64-bit integers larger than the fixnum
  size will cons unless you make sure they're always constant or in
  specialized arrays or in typed structure slots. When in doubt,
  disassemble and see if the function has allocations in it. In SBCL,
  such allocations are usually commented.

  - **Note:** This is the main reason why only 64-bit implementations
    are supported. Workarounds for smaller integer sizes and
    single-floats would make the engine much harder to write and read,
    and arrays that hold 64-bit integers would be T arrays.

- This only applies to the engine itself. Users of the game engine
  should be free to cons in the game loop if they want.

- Consing can be detected in SBCL through multiple ways. Some of these
  ways are:
  - `disassemble` (or `sb-disassem:disassemble-code-component`)
  - `room`
  - `sb-profile`
  - `sb-sprof`

Style
-----

- In general, this project follows the
  [Google Common Lisp Style Guide](https://google.github.io/styleguide/lispguide.xml).

- **Documentation**. Everything that is exported should have a
  docstring. Everything else should be commented if it is
  non-obvious. Functions that are large enough to need comments might
  be better off as multiple smaller functions that are obvious.

  - Docstrings for functions and methods should mention the inputs,
    the outputs, and the side effects.

  - Multi-line docstrings should have a leading newline so the
    indentation lines up properly.

  - Multi-line docstrings should also have a trailing newline unless
    the closing " is immediately followed by a right parentheses.

  - **Note:** This rule is currently violated in many parts of the
    engine because the API is heavily under construction and a lot of
    that documentation will quickly become stale and misleading.

- **Indentation**. As the style guide says, use the indentation that a
  properly configured emacs + SLIME uses. The only exception is if
  auto-indentation is broken for a macro or reader macro.

- **Lambdas**. lambdas are written without the `#'`

  - Both `#'(lambda (x) (+ 2 x))` and `(lambda (x) (+ 2 x))` are valid
    ways to write a lambda in modern, supported implementations. The
    second way is both more concise and more readable so it should
    consistently be used everywhere.

  - Additionally, the `#'` style will not work everywhere, such as in
    `:report` in `define-condition`.

- **Line length**. As in the Google style guide, 100 characters is the
  line length limit.

  - **Note:** The line length limit is a soft limit. The readability
    and clarity of the code should not be sacrificed to meet line
    length limits.

- **List vs. arrays**. This is a game engine. Take memory layout into
  account instead of just the O(n) random access. Where possible, use
  arrays that have types that optimized CL implementations can
  upgrade.

  - Generally, this means use arrays that hold bit, byte, or other
    numerical types where possible. The main exception is the integer
    type, which could be a fixnum or a bignum. Try to use a specific,
    finite integer size that is small enough.

  - Use bits instead of booleans in these arrays because booleans
    cannot be upgraded in SBCL.

  - You can see if your CL implementation supports the upgrade by
    running `(upgraded-array-element-type 'foo)`, where `'foo` is what
    you would put in `:element-type` in `make-array`. If it returns
    `T`, nothing is happening when you're specifying the element type.

  - This isn't as important in areas that do not need to be as fast,
    but it is very important if it's run every iteration of the game
    loop.

- **CLOS**. In general, this engine tries to use composition over
  inheritance. CLOS is generally used outside of the game loop and
  structs with types and specialized arrays are preferred for things
  within the game loop.

  - **Note:** Perhaps some MOP magic can reduce the overhead of CLOS
    so it can be used in more places. Make sure to benchmark it if you
    try it.

- **Functional?** The style varies depending on where in the engine it
  is. Most of the code is written in a "mostly functional" style,
  avoiding `setf`, mutation, etc., where possible, but eventually
  using those pure functions to modify mutable data structures.

  - Multiple return values of non-consing types (like single-float and
    fixnums) can be used to write pure functions where consing would
    otherwise happen.

  - Macros can also be used to provide declarative interfaces to code
    that is not declarative.

- **Tests**. Unit tests go in directories under the top-level tests
  directory. Their directory layout matches the top-level directory
  layout. A test of `foo/bar.lisp` would be placed in
  `tests/foo/bar.lisp`. Not every file needs tests because not
  everything is possible to unit test, such as rendering.

  - **Note:** The tests are currently very incomplete because the
    engine is currently very incomplete.

- **Side effects**. In areas of the code that are mostly functional in
  style, functions with side effects use the Scheme convention of
  ending the function name with an exclamation mark. For instance,
  `foo!` instead of `foo`.

- **foo-p vs. foo?**. This library uses the Scheme style `foo?` over
  the traditional Lisp `foop` or `foo-p` style unless the function is
  a variant of a built-in Lisp function that uses the p convention. In
  other libraries, use whichever convention the library uses. For new
  libraries, prefer the unambiguous `foo-p` over `foop` if the
  traditional non-Scheme style is used.

- **TODO comments**. TODO comments are for things too small for an
  issue. They are generally for things that should be prioritized
  sooner rather than indefinitely postponed. If you're reading this
  before contributing, then you probably shouldn't be adding any TODO
  comments to the main branch.

### Packages and Systems

There are three acceptable styles for handling *packages* in Common
Lisp. The first style, which works best for small projects, is to have
one `package.lisp` for the entire project that contains the
`defpackage`. The second style, which is fairly common for large
projects, is to have a `package.lisp` for each directory. The third
style, is to put the `defpackage` at the top of every file, which
results in something not unlike Python or Java. Zombie Raptor uses
this third style, which is semi-automated in ASDF by its
package-inferred-system. Other styles are equally valid, and projects
related to the core project might use a different style.

ASDF *systems* are usually a separate thing from Common Lisp's built
in packages and do not necessarily have to correspond to
packages. When using package-inferred-system, however, there is a
system for every file as well as a package for every file. This means
that not everything is necessarily loaded. In practice, this means
that Zombie Raptor is actually a collection of independent
directory-level modules because every directory `foo` has a
corresponding `foo.lisp` file and `zombie-raptor/foo` package and
`zombie-raptor/foo` system. That being said, it is still possible to
have additional packages from other `defpackage`s that do not
correspond to a file or an ASDF system.

Finally, the third "module"-like unit that modern Common Lisp offers
is Quicklisp *projects*. This usually corresponds to a top-level
directory containing one or more `.asd` files, and usually corresponds
to a git repository. This is not covered in this section, but is
mentioned here for completeness. Installing this project is briefly
described in [INSTALL.md](INSTALL.md).

- In `defpackage` in normal source files, **do not `:use`** more than
  one external package other than `CL` because it quickly becomes
  problematic to use many packages.

  - In practice, this exception is reserved here for `zr-utils`, which
    is designed to be `:use`d here without issues. It is treated as an
    external dependency for this rule because large portions of it are
    eventually going to be spun off into a separate project. That
    being said, package-local-nicknames could be used instead, with
    `z` for the utils.

  - Another exception is made for other files within the same project,
    and especially within the same directory.

    - It is safe to `:use` most other `zombie-raptor/foo` and
      `zombie-raptor/foo/bar` packages because their exported symbols
      also are exported by the `zombie-raptor/all` package. This
      exception only applies to packages that are `:use-reexport`ed
      (see below).

    - For packages that are in some sense "private" (i.e. not part of
      the final API of the `zombie-raptor/all` package), they should
      only be `:use`d if they are in the same directory because they
      are only meant for use within that directory by
      convention. These are thus an exception to the exception to the
      rule.

  - The phrase "normal source files" here refers to files that are not
    unit tests and are not files describing the public interface of a
    directory for package-inferred-system (see below for more on the
    latter).

- This library uses the **package-inferred-system** from ASDF 3. Each
  file is its own package, with its own imports and exports at the top
  of the file rather than in a separate package.lisp file.

  - See
    [this blog post](http://davazp.net/2014/11/26/modern-library-with-asdf-and-package-inferred-system.html)
    and
    [this section of the ASDF documentation](https://common-lisp.net/project/asdf/asdf/The-package_002dinferred_002dsystem-extension.html).

  - Except for the tests, each directory `foo` must have a matching
    `foo.lisp` in the level above. This allows users of
    package-inferred-system to load directories as systems since they
    are really loading the system associated with the file with the
    matching name.

    - These files should use `:use-reexport` in `uiop:define-package`
      to reexport everything from the files in the directory. This
      defines a public interface for the directory, while still
      allowing convenient `in-package` use in the REPL.

    - If the file is only there to help another file, it shouldn't be
      reexported. An example of a "private" file like that is
      `typed-bindings.lisp` in `zr-utils`. These files are useful to
      avoid hundreds of lines of code wrapped in an `eval-when` when
      defining functions used by elaborate macros. These should still
      be `:use`d, though.

  - The top-level `all.lisp` reexports from the directory files, not
    from each individual file. This keeps the top-level
    `zombie-raptor` package clean.

  - Unless there is a good reason not to, all packages that have a
    `:use-reexport` should also have a `(:use #:cl)` in the package
    definition. This allows `in-package` at the REPL to work properly,
    but it wasn't done in the blog example that was linked to earlier.

  - Even though it isn't necessary, the `defsystem` in
    `zombie-raptor.asd` must include the external dependencies in its
    `:depends-on` because that is where Lispers typically look for the
    a list of dependencies.

Graphics
--------

- **OpenGL version**. The OpenGL API will be raised from 3.3 only when
  features from those later versions are needed.

- **GLSL vs SPIR-V**. SPIR-V will be the preferred way to do shaders
  once OpenGL supports SPIR-V via `ARB_gl_spirv` (required in OpenGL
  4.6). This extension needs to be widely supported in the major
  drivers first. The use of GLSL shaders is temporary, but necessary
  for now.

- **Vulkan**. It's more important to get a working game engine first
  and cl-vulkan isn't ready yet. Adding a Vulkan renderer is a planned
  long term goal.
