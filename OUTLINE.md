Outline
=======

| Directory    | Description                                                      |
|--------------|------------------------------------------------------------------|
| **audio**    | handles music and sound                                          |
| **core**     | ties everything else together into a high level game engine      |
| **data**     | handles all configuration, graphics, and game data               |
| **entity**   | the core of the entity component system (ECS)                    |
| **examples** | some tiny demonstrations of the engine's capabilities            |
| **fonts**    | all of the font files used by the game engine                    |
| **math**     | general mathematics used by the rest of the engine               |
| **physics**  | 3D game physics                                                  |
| **render**   | 2D and 3D rendering                                              |
| **text**     | handles fonts and text                                           |
| **tests**    | the test files                                                   |
