(defpackage #:zombie-raptor/entity/meta-entity
  (:documentation "Defines things to simplify defining ECSes.")
  (:use #:cl
        #:zombie-raptor/core/types
        #:zombie-raptor/math/boolean-set
        #:zr-utils)
  (:export #:changed-bits
           #:define-entity-component-system
           #:entity
           #:max-index
           #:removed-entity))

(in-package #:zombie-raptor/entity/meta-entity)

(defmacro define-components ((component-set component-index component-set-union) &body components)
  (let ((component-length (length components)))
    (check-type component-length boolean-set-index))
  (loop :for i :from 0
        :for component :in components
        :for component-keyword := (symbol-to-keyword component)
        :collect `(,component-keyword ,(index-to-boolean-set i))
          :into component-sets
        :collect `(,component-keyword ,i)
          :into component-indices
        :finally
           (return `(progn
                      (define-function (,component-set :inline t) ((component keyword))
                        (ecase component
                          ,@component-sets))
                      (define-function (,component-index :inline t) ((component keyword))
                        (ecase component
                          ,@component-indices))
                      (defun ,component-set-union (components)
                        (reduce (lambda* ((boolean-set boolean-set)
                                     (component keyword))
                                  (boolean-set-union boolean-set (,component-set component)))
                                components
                                :initial-value +empty-boolean-set+))))))

(defmacro define-entity-component-system (name (max-entities-binding max-entities)
                                          (component-set component-index component-set-union)
                                          &body components)
  ;; An entity component system of size max-entities (which could be a
  ;; constant or a numeric literal) consists of one or more component
  ;; structs.
  (let* ((max-entities (if (and (symbolp max-entities) (constantp max-entities))
                           (symbol-value max-entities)
                           max-entities))
         (component-struct-definitions (mapcar (destructuring-lambda (component-name &rest component-fields)
                                                 `(define-simple-struct-with-accessor-macro ,component-name
                                                    ,@(subst max-entities max-entities-binding component-fields)))
                                               components))
         (component-names (mapcar #'first components)))
    `(progn
       ;; Track the removed entities in a queue so their IDs can be
       ;; recycled later on.
       (define-struct-queue removed-entity ((id (mod ,max-entities)) ,max-entities))
       ;; Define all of the component structs.
       ,@component-struct-definitions
       ;; Define the ECS itself.
       (define-simple-struct (,name (:conc-name nil))
         (max-index (mod ,max-entities))
         (changed-bits (simple-bit-vector ,max-entities))
         (entity (entity-set-array ,max-entities))
         removed-entity
         ,@component-names)
       ;; Define the component-related functions.
       (define-components (,component-set ,component-index ,component-set-union)
         ,@component-names)
       ;; Use a short printable representation because the number of
       ;; entities is probably large so printing all of the arrays
       ;; contained by all of the structs at the REPL is unwise.
       (defmethod print-object ((ecs ,name) stream)
         (print-unreadable-object (ecs stream :type t :identity t))
         ecs))))
