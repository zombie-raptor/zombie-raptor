(defpackage #:zombie-raptor/entity/render
  (:documentation "The rendering system of the ECS.")
  (:use #:cl
        #:zombie-raptor/core/constants
        #:zombie-raptor/core/types
        #:zombie-raptor/entity/entity
        #:zombie-raptor/math/matrix
        #:zombie-raptor/render/gl
        #:zr-utils)
  (:export #:draw-2D-hud
           #:render-2D-hud
           #:draw-entities
           #:render-entities))

(in-package #:zombie-raptor/entity/render)

(define-function render-2D-hud ((ecs entity-component-system)
                                (foreign-matrices foreign-matrices)
                                (time single-float)
                                (render-ids (gl-ids #.+max-entities+))
                                (render-bits (simple-bit-vector #.+max-entities+))
                                (changed-bits (simple-bit-vector #.+max-entities+)))
  (with-foreign-matrices (hud-cache)
      foreign-matrices
    (with-selection ecs
        (entity-id :when (= 1 renderable?)
                   :let-changed? changed?
                   :changed? :clear)
        ((location location old-location)
         (rotation rotation old-rotation)
         (geometry (renderable? renderable?)
                   (mesh-id mesh-id))
         (scale scale))
      (setf (aref render-bits entity-id) 1)
      (when changed?
        (psetf (aref render-ids entity-id) mesh-id
               (aref changed-bits entity-id) 1)
        (model-matrix* hud-cache
                       entity-id
                       %old-location
                       %location
                       %old-rotation
                       %rotation
                       %scale
                       time))))
  nil)

(define-function draw-2D-hud ((render-ids (gl-ids #.+max-entities+))
                              (render-bits (simple-bit-vector #.+max-entities+))
                              meshes
                              (foreign-matrices foreign-matrices))
  (with-foreign-matrices (hud-matrix-pointer hud-view-matrix-pointer hud-cache-pointers)
      foreign-matrices
    (loop :for entity-id :below +max-entities+
          :for render-bit :across render-bits
          :for mesh-id :across render-ids
          :when (= 1 render-bit)
            :do (draw-2D meshes
                         mesh-id
                         hud-matrix-pointer
                         hud-view-matrix-pointer
                         (aref hud-cache-pointers entity-id))))
  (fill render-bits 0)
  nil)

(define-function render-entities ((ecs entity-component-system)
                                  (foreign-matrices foreign-matrices)
                                  (time single-float)
                                  (camera-id entity-id)
                                  (render-ids (gl-ids #.+max-entities+))
                                  (render-bits (simple-bit-vector #.+max-entities+))
                                  (changed-bits (simple-bit-vector #.+max-entities+)))
  (with-foreign-matrices (camera-cache
                          model-cache
                          normal-cache)
      foreign-matrices
    (with-selection ecs
        (entity-id :id camera-id
                   :let-changed? changed?
                   :when changed?)
        ((location location old-location)
         (rotation rotation old-rotation))
      ;; Either there's a camera independent of the entity's
      ;; orientation or not. No matter what, some sort of ID needs to
      ;; be available.
      (let ((camera-entity-id (or (with-selection ecs
                                      (entity-id :id camera-id)
                                      ((look (camera-entity camera-entity)))
                                    camera-entity)
                                  entity-id)))
        (camera-matrix camera-cache
                       entity-id
                       camera-entity-id
                       %old-location
                       %location
                       %old-rotation
                       %rotation
                       time)))
    (with-selection ecs
        (entity-id :when (= 1 renderable?)
                   :let-changed? changed?
                   :changed? :clear)
        ((location location old-location)
         (rotation rotation old-rotation)
         (geometry (renderable? renderable?)
                   (mesh-id mesh-id))
         (scale scale))
      (setf (aref render-bits entity-id) 1)
      (when changed?
        (psetf (aref render-ids entity-id) mesh-id
               (aref changed-bits entity-id) 1)
        (model-matrix model-cache
                      normal-cache
                      entity-id
                      %old-location
                      %location
                      %old-rotation
                      %rotation
                      %scale
                      time))))
  nil)

(define-function draw-entities ((render-ids (gl-ids #.+max-entities+))
                                (render-bits (simple-bit-vector #.+max-entities+))
                                meshes
                                (camera-id entity-id)
                                (foreign-matrices foreign-matrices))
  (with-foreign-matrices ((perspective-matrix-pointer projection-matrix-pointer)
                          camera-cache-pointers
                          model-cache-pointers
                          normal-cache-pointers)
      foreign-matrices
    (loop :for entity-id :below +max-entities+
          :for render-bit :across render-bits
          :for mesh-id :across render-ids
          :when (= 1 render-bit)
            :do (draw meshes
                      mesh-id
                      perspective-matrix-pointer
                      (aref camera-cache-pointers camera-id)
                      (aref model-cache-pointers entity-id)
                      (aref normal-cache-pointers entity-id))))
  (fill render-bits 0)
  nil)
