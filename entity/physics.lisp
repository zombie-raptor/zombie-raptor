(defpackage #:zombie-raptor/entity/physics
  (:documentation "The physics system of the ECS.")
  (:use #:cl
        #:zombie-raptor/core/types
        #:zombie-raptor/entity/entity
        #:zombie-raptor/physics/physics
        #:zr-utils)
  (:export #:physics))

(in-package #:zombie-raptor/entity/physics)

(define-function physics ((ecs entity-component-system)
                          (time single-float)
                          (time-step single-float))
  "The physics system of the ECS."
  (with-selection ecs
      (entity-id)
      ((location)
       (velocity)
       (rotation)
       (physics (input? input?)
                (allow-falling? allow-falling?)))
    (when (= 1 input?)
      (update-input-movement! ecs entity-id)
      (setf input? 0))
    (let ((gravity 9.80665f0))
      (when (= 1 allow-falling?)
        (fall! ecs entity-id gravity)))
    (update-location-and-velocity! ecs entity-id time time-step)))
