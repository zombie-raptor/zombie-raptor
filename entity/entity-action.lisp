(defpackage #:zombie-raptor/entity/entity-action
  (:documentation "Does some basic actions with entities.")
  (:use #:cl
        #:zombie-raptor/core/types
        #:zombie-raptor/entity/entity
        #:zombie-raptor/math/quaternion
        #:zombie-raptor/math/vector
        #:zombie-raptor/zrvl/zrvl
        #:zr-utils)
  (:export #:boid-zone
           #:entity-crouch
           #:entity-jump
           #:flock-boids
           #:make-basic-entity
           #:make-fps-camera-entity
           #:make-location-entity
           #:make-map-node
           #:make-map-node-connection
           #:make-overhead-camera-entity
           #:make-world-data
           #:make-world-entity
           #:make-zone-entity
           #:map-node
           #:map-node-adjacency
           #:map-node-adjacent-nodes
           #:map-node-connection
           #:move-and-rotate-entity
           #:move-entity
           #:rotate-entity
           #:rotate-entity-camera-pitch
           #:update-boid-goal
           #:with-world-data
           #:world-data))

(in-package #:zombie-raptor/entity/entity-action)

;;;; Helper function
(define-function (clamp :inline t) (min x max)
  (max min (min x max)))

;;;; World data structures for entity navigation

(defstruct world-data
  (walls nil :type (simple-array single-float (* 4)))
  (floors nil :type (simple-array single-float (* 4)))
  (map-nodes nil :type simple-vector)
  (zone-IDs nil :type (simple-array entity-id (*))))

(define-accessor-macro with-world-data #:world-data-)

(defstruct map-node
  (id 0 :type entity-id)
  (adjacency nil :type (simple-bit-vector *))
  (adjacent-nodes nil :type list))

(defstruct map-node-connection
  (id 0 :type entity-id)
  (connected-node nil :type map-node)
  (points nil :type list))

(defmethod print-object ((object map-node-connection) stream)
  (print-unreadable-object (object stream :type t)
    (format stream "~A" (map-node-connection-id object)))
  object)

;;;; Make certain kinds of entities

(define-function make-location-entity
    ((ecs entity-component-system)
     &key
     (location (vec3 0f0 0f0 0f0) vec3))
  (let ((entity-id (add-entity ecs))
        (location* location))
    (with-selection ecs
        (id :id entity-id
            :add (location)
            :changed? t
            :return entity-id)
        ((location (_ old-location :row-of 3 := (array-of-3 location*))
                   (_ location :row-of 3 := (array-of-3 location*)))))))

(define-function make-basic-entity
    ((ecs entity-component-system)
     (mesh-keys hash-table)
     (mesh keyword)
     &key
     (location (vec3 0f0 0f0 0f0) vec3)
     (rotation (vec3 0f0 0f0 0f0) vec3)
     (scale (vec3 1f0 1f0 1f0) vec3)
     (falling? t boolean)
     (movement-speed 0.025f0 single-float))
  "Creates an entity with the a mesh, a location, a velocity, an
acceleration, a rotation, and a scale in the entity-component-system
ECS. Returns its ID."
  (let ((entity-id (add-entity ecs))
        (location* location)
        (scale* scale)
        (rotation* rotation))
    (with-selection ecs
        (id :id entity-id
            :add (geometry location velocity scale physics rotation)
            :changed? t
            :return entity-id)
        ((geometry (_ mesh-id := (gethash mesh mesh-keys 0))
                   (_ renderable? := 1))
         (location (_ old-location :row-of 3 := (array-of-3 location*))
                   (_ location :row-of 3 := (array-of-3 location*)))
         (velocity (_ velocity :row-of 3 := (values 0f0 0f0 0f0)))
         (scale (_ scale :row-of 3 := (array-of-3 scale*)))
         (physics (_ input-velocity := movement-speed)
                  (_ input-direction :row-of 3 := (values 0f0 0f0 0f0))
                  (_ acceleration :row-of 3 := (values 0f0 0f0 0f0))
                  (_ input? := 0)
                  (_ allow-falling? := (if falling? 1 0))
                  (_ gravity? := 0))
         (rotation (_ rotation :row-of 4 := (with-zrvl/mv* ((rotation* zrvl:vec3))
                                              (zrvl:quat<-euler rotation*)))
                   (_ old-rotation :row-of 4 := (with-zrvl/mv* ((rotation* zrvl:vec3))
                                                  (zrvl:quat<-euler rotation*))))))))

(define-function make-world-entity
    ((ecs entity-component-system)
     (mesh-keys hash-table)
     (mesh keyword)
     &key
     (location (vec3 0f0 0f0 0f0) vec3)
     (rotation (vec3 0f0 0f0 0f0) vec3)
     (scale (vec3 1f0 1f0 1f0) vec3))
  (let ((entity-id (add-entity ecs))
        (location* location)
        (scale* scale)
        (rotation* rotation))
    (with-selection ecs
        (id :id entity-id
            :add (geometry location scale rotation)
            :changed? t
            :return entity-id)
        ((geometry (_ mesh-id := (gethash mesh mesh-keys 0))
                   (_ renderable? := 1))
         (location (_ old-location :row-of 3 := (array-of-3 location*))
                   (_ location :row-of 3 := (array-of-3 location*)))
         (scale (_ scale :row-of 3 := (array-of-3 scale*)))
         (rotation (_ rotation :row-of 4 := (with-zrvl/mv* ((rotation* zrvl:vec3))
                                              (zrvl:quat<-euler rotation*)))
                   (_ old-rotation :row-of 4 := (with-zrvl/mv* ((rotation* zrvl:vec3))
                                                  (zrvl:quat<-euler rotation*))))))))

(define-function make-fps-camera-entity
    ((ecs entity-component-system)
     &key
     (location (vec3 0f0 0f0 0f0) vec3)
     (rotation (vec3 0f0 0f0 0f0) vec3)
     (movement-speed 0.06f0 single-float))
  "Creates a first person camera in the entity-component-system ECS
with a location and a rotation. Returns its ID."
  (let ((entity-id (add-entity ecs))
        (camera-id (add-entity ecs))
        (location* location)
        (rotation* rotation))
    (with-selection ecs
        (id :id camera-id
            :add (rotation)
            :changed? t)
        ((rotation (_ rotation :row-of 4 := (with-zrvl/mv* ((rotation* zrvl:vec3))
                                              (zrvl:quat<-pitch (zrvl:aref rotation* 1))))
                   (_ old-rotation :row-of 4 := (with-zrvl/mv* ((rotation* zrvl:vec3))
                                                  (zrvl:quat<-pitch (zrvl:aref rotation* 1)))))))
    (with-selection ecs
        (id :id entity-id
            :add (geometry location velocity scale physics look rotation)
            :changed? t
            :return entity-id)
        ((geometry (_ mesh-id := 0)
                   (_ renderable? := 0))
         (location (_ old-location :row-of 3 := (array-of-3 location*))
                   (_ location :row-of 3 := (array-of-3 location*)))
         (velocity (_ velocity :row-of 3 := (values 0f0 0f0 0f0)))
         (scale (_ scale :row-of 3 := (values 1f0 1f0 1f0)))
         (physics (_ input-velocity := movement-speed)
                  (_ input-direction :row-of 3 := (values 0f0 0f0 0f0))
                  (_ acceleration :row-of 3 := (values 0f0 0f0 0f0))
                  (_ input? := 0)
                  (_ allow-falling? := 1)
                  (_ gravity? := 0))
         (look (_ camera-entity := camera-id))
         (rotation (_ rotation :row-of 4 := (with-zrvl/mv* ((rotation* zrvl:vec3))
                                              (zrvl:quat<-euler (zrvl:vec3 (zrvl:aref rotation* 0)
                                                                           0f0
                                                                           (zrvl:aref rotation* 2)))))
                   (_ old-rotation :row-of 4 := (with-zrvl/mv* ((rotation* zrvl:vec3))
                                                  (zrvl:quat<-euler (zrvl:vec3 (zrvl:aref rotation* 0)
                                                                               0f0
                                                                               (zrvl:aref rotation* 2))))))))))

(define-function make-overhead-camera-entity
    ((ecs entity-component-system)
     &key
     (location (vec3 0f0 0f0 0f0) vec3)
     (rotation (vec3 0f0 0f0 0f0) vec3))
  "Creates an overhead, third person camera in the
  entity-component-system ECS with a location and a rotation. Returns
  its ID."
  (let ((entity-id (add-entity ecs))
        (camera-id (add-entity ecs))
        (location* location)
        (rotation* rotation))
    (with-selection ecs
        (id :id camera-id
            :add (rotation)
            :changed? t)
        ((rotation (_ rotation :row-of 4 := (with-zrvl/mv* ((rotation* zrvl:vec3))
                                              (zrvl:quat<-pitch (zrvl:aref rotation* 1))))
                   (_ old-rotation :row-of 4 := (with-zrvl/mv* ((rotation* zrvl:vec3))
                                                  (zrvl:quat<-pitch (zrvl:aref rotation* 1)))))))
    (with-selection ecs
        (id :id entity-id
            :add (geometry location velocity scale physics look rotation)
            :changed? t
            :return entity-id)
        ((geometry (_ mesh-id := 0)
                   (_ renderable? := 0))
         (location (_ old-location :row-of 3 := (array-of-3 location*))
                   (_ location :row-of 3 := (array-of-3 location*)))
         (velocity (_ velocity :row-of 3 := (values 0f0 0f0 0f0)))
         (scale (_ scale :row-of 3 := (values 1f0 1f0 1f0)))
         (physics (_ input-velocity := 0.55f0)
                  (_ input-direction :row-of 3 := (values 0f0 0f0 0f0))
                  (_ acceleration :row-of 3 := (values 0f0 0f0 0f0))
                  (_ input? := 0)
                  (_ allow-falling? := 0)
                  (_ gravity? := 0))
         (look (_ camera-entity := camera-id))
         (rotation (_ rotation :row-of 4 := (with-zrvl/mv* ((rotation* zrvl:vec3))
                                              (zrvl:quat<-euler (zrvl:vec3 (zrvl:aref rotation* 0)
                                                                           0f0
                                                                           (zrvl:aref rotation* 2)))))
                   (_ old-rotation :row-of 4 := (with-zrvl/mv* ((rotation* zrvl:vec3))
                                                  (zrvl:quat<-euler (zrvl:vec3 (zrvl:aref rotation* 0)
                                                                               0f0
                                                                               (zrvl:aref rotation* 2))))))))))

(define-function (make-zone-entity :default-type (maybe vec3))
    ((ecs entity-component-system)
     &key corner-a corner-b)
  (let ((entity-id (add-entity ecs))
        (a (or corner-a (vec3 0f0 0f0 0f0)))
        (b (or corner-b (vec3 0f0 0f0 0f0))))
    (with-selection ecs
        (id :id entity-id
            :add (bounding-box)
            :changed? t
            :return entity-id)
        ((bounding-box (_ corner-a :row-of 3 := (values (min (aref a 0) (aref b 0))
                                                        (min (aref a 1) (aref b 1))
                                                        (min (aref a 2) (aref b 2))))
                       (_ corner-b :row-of 3 := (values (max (aref a 0) (aref b 0))
                                                        (max (aref a 1) (aref b 1))
                                                        (max (aref a 2) (aref b 2)))))))))

;;;; Actions on entities

(defun move-entity (ecs entity-id direction &optional (magnitude 1f0))
  (with-selection ecs
      (id :id entity-id)
      ((physics (input? input?)
                (input-direction.x input-direction :elt 0)
                (input-direction.y input-direction :elt 1)
                (input-direction.z input-direction :elt 2)))
    (setf input? 1)
    (ecase direction
      (:x- (decf input-direction.x magnitude))
      (:x+ (incf input-direction.x magnitude))
      (:y- (decf input-direction.y magnitude))
      (:y+ (incf input-direction.y magnitude))
      (:z- (decf input-direction.z magnitude))
      (:z+ (incf input-direction.z magnitude)))))

;;; You can only jump if not currently affected by gravity... for now.
(define-function (can-jump? :inline t :return boolean) ((gravity? boolean))
  (not gravity?))

;;; This function is for an entity that can actually jump, as opposed
;;; to one that uses the "jump" action to simply move upward.
(define-function %entity-jump ((ecs entity-component-system)
                               (entity-id entity-id))
  (with-selection ecs
      (id :id entity-id)
      ((physics (gravity? gravity?)
                (jump-strength jump-strength))
       (velocity (velocity.y velocity :elt 1)))
    ;; A requested jump only happens if the conditions for jumping are
    ;; met. Otherwise, nothing happens.
    (when (can-jump? (= 1 gravity?))
      (setf velocity.y jump-strength))))

(define-function entity-crouch ((ecs entity-component-system)
                                (entity-id entity-id))
  (with-selection ecs
      (id :id entity-id)
      ((physics (allow-falling? allow-falling?)))
    (if (zerop allow-falling?)
        (move-entity ecs entity-id :y-)
        ;; TODO: actual crouch not yet implemented
        nil)))

;;; An entity either jumps if it can fall due to gravity, or it simply
;;; moves up on the y-axis when a jump is requested.
(define-function entity-jump ((ecs entity-component-system)
                              (entity-id entity-id))
  (with-selection ecs
      (id :id entity-id)
      ((physics (allow-falling? allow-falling?)))
    (if (= 1 allow-falling?)
        (%entity-jump ecs entity-id)
        (move-entity ecs entity-id :y+))))

(define-function rotate-entity ((ecs entity-component-system)
                                (entity-id entity-id)
                                (rotation-axis keyword)
                                (angle single-float))
  (declare (optimize (speed 3)))
  (with-selection ecs
      (id :id entity-id :changed? t)
      ((rotation (rotation rotation :row-of 4)))
    (setf rotation (multiple-value-call #'quaternion*/mv
                     rotation
                     (ecase rotation-axis
                       (:yaw (rotate-yaw/mv angle))
                       (:pitch (rotate-pitch/mv angle))
                       (:roll (rotate-roll/mv angle)))))))

(define-function rotate-entity-camera-pitch ((ecs entity-component-system)
                                             (entity-id entity-id)
                                             (angle single-float))
  (declare (optimize (speed 3)))
  (with-selection ecs
      (id :id entity-id :changed? t)
      ((look (camera-entity camera-entity)))
    (with-selection ecs
        (id :id camera-entity :changed? t)
        ((rotation rotation))
      ;; TODO: When ZRVL adds WHEN, this can be combined in one
      (multiple-value-bind (x y z w)
          (with-zrvl/mv*
              ((%rotation zrvl:vec4 :row camera-entity)
               (angle zrvl:f32))
            (zrvl:* (zrvl:quat<-vec4 %rotation)
                    (zrvl:quat<-pitch angle)))
        (when (<= (abs x) (float* (* 0.5d0 (sqrt 2d0))))
          (with-zrvl
              ((x zrvl:f32)
               (y zrvl:f32)
               (z zrvl:f32)
               (w zrvl:f32))
              ((%rotation zrvl:vec4 :row camera-entity))
            (zrvl-int:setf %rotation (zrvl:vec4<-quat (zrvl:quat x y z w)))))))))

;;; Elaborate move-rotate motion and planar (for now) boid flocking
;;; for basic AI-controlled entities

(define-function (distance :inline t) (v1-x v1-z v2-x v2-z)
  (with-zrvl/mv* ((v1-x zrvl:f32)
                  (v1-z zrvl:f32)
                  (v2-x zrvl:f32)
                  (v2-z zrvl:f32))
    (zrvl:distance (zrvl:vec2 v1-x v1-z)
                   (zrvl:vec2 v2-x v2-z))))

(define-function (find-distance :inline t)
    ((ecs entity-component-system)
     (id entity-id)
     (x single-float)
     (z single-float))
  (with-selection ecs
      (i :id id)
      ((location (location.x location :elt 0)
                 (location.z location :elt 2)))
    (distance location.x location.z x z)))

(define-function rotate-toward-angle ((ecs entity-component-system)
                                      (id entity-id)
                                      (x single-float)
                                      (z single-float))
  (let ((rotate-at (make-quaternion)))
    (declare (dynamic-extent rotate-at))
    (with-selection ecs
        (i :id id :changed? t)
        ((location location)
         (rotation (rotation rotation :row-of 4)
                   rotation))
      (setf (array-of-4 rotate-at)
            (with-zrvl/mv*
                ((%location zrvl:vec3 :row id)
                 (x zrvl:f32)
                 (z zrvl:f32))
              (zrvl:let* ((start (zrvl:swizzle %location :xz))
                          (target (zrvl:vec2 x z))
                          (point (zrvl:- start target))
                          (rotate-at (zrvl:quat<-yaw (zrvl-int:atan2 (zrvl:aref point 0)
                                                                     (zrvl:aref point 1)))))
                rotate-at)))
      (let* ((angle-distance (let ((x (with-zrvl/mv*
                                          ((rotate-at zrvl:vec4)
                                           (%rotation zrvl:vec4 :row id))
                                        (zrvl:abs (zrvl:dot rotate-at %rotation)))))
                               (if (> x 1f0) 0f0 (* 2f0 (acos x)))))
             ;; Don't interpolate the rotation too quickly if the
             ;; distance is large.
             (step (cond ((> angle-distance (float* pi))
                          0.02f0)
                         ((> angle-distance (float* (/ pi 2d0)))
                          0.05f0)
                         (t 0.1f0))))
        (setf rotation (if (< angle-distance (float* (/ pi 64d0)))
                           ;; A very close distance means don't bother
                           ;; interpolating.
                           (with-zrvl/mv*
                               ((rotate-at zrvl:vec4))
                             rotate-at)
                           ;; A large distance means SLERP, but it
                           ;; must be normalized or else sometimes it
                           ;; gets unstable.
                           (multiple-value-call #'normalize/mv4
                             (multiple-value-call #'quaternion-slerp*/mv
                               step
                               rotation
                               (with-zrvl/mv*
                                   ((rotate-at zrvl:vec4))
                                 rotate-at)))))
        ;; Only move forward if the angle is close to the goal.
        (<= angle-distance (float* (/ pi 6d0)))))))

(define-function move-step-in-distance ((ecs entity-component-system)
                                        (id entity-id)
                                        (distance single-float)
                                        (x single-float)
                                        (z single-float)
                                        (zone-IDs (maybe (simple-array entity-id (*))))
                                        (update-zone? boolean))
  (with-selection ecs
      (i :id id :changed? t)
      ((physics (input? input?)
                (input-velocity input-velocity)
                (input-direction.z input-direction :elt 2))
       (location (location location :row-of 3)
                 (location.x location :elt 0)
                 (location.y location :elt 1)
                 (location.z location :elt 2)
                 location)
       (rotation rotation))
    ;; Predict the movement
    (multiple-value-bind (location-x* location-z*)
        (let ((velocity input-velocity))
          (with-zrvl/mv*
              ((%rotation zrvl:vec4 :row id)
               (%location zrvl:vec3 :row id)
               (velocity zrvl:f32))
            (zrvl:swizzle (zrvl:- %location
                                  (zrvl:rotate (zrvl:quat<-vec4 %rotation)
                                               (zrvl:vec3 0f0 0f0 velocity)))
                          :xz)))
      (progn
        ;; Instead of moving, return early if the move would cause a
        ;; collision with another object, unless they are already
        ;; colliding and this would move it further away.
        (with-selection ecs
            (j)
            ((location (other-location.x location :elt 0)
                       (other-location.z location :elt 2))
             ;; Note: the world entities have no physics
             (physics))
          (when (and (/= id j)
                     (< (distance other-location.x other-location.z
                                  location-x* location-z*)
                        (distance other-location.x other-location.z
                                  location.x location.z)
                        (+ (* (sqrt 2f0) 0.65f0) input-velocity)))
            (return-from move-step-in-distance nil)))
        ;; Also return early if the move would cause leaving the current
        ;; navigation zone for a boid.
        (when (and zone-IDs (not update-zone?))
          (with-selection ecs
              (j :id id)
              ((boid (in-zone? in-zone?)
                     (zone-id zone)
                     (move-zone? move-zone?)
                     (waypoint.x waypoint :elt 0)
                     (waypoint.z waypoint :elt 2)))
            (when (= 1 in-zone?)
              (let ((goal-distance (distance location.x location.z
                                             waypoint.x waypoint.z)))
                (when (or (zerop move-zone?)
                          (> goal-distance 0.7f0))
                  (multiple-value-bind (x-min x-max z-min z-max)
                      (boid-bounds-for-zone ecs (aref zone-ids zone-id))
                    #+(or)
                    (format t "(~A) ~A ~A ~A | ~A ~A ~A~%" zone-id location.x location-x* x-min location.z location-z* z-max)
                    (unless (and (<= (+ x-min 0.5f0) location-x* (- x-max 0.5f0))
                                 (<= (+ z-min 0.5f0) location-z* (- z-max 0.5f0)))
                      (return-from move-step-in-distance nil))))))))))
    (cond ((or (< distance input-velocity) update-zone?)
           (setf location (values x location.y z))
           t)
          (t
           (setf input? 1)
           (decf input-direction.z)
           nil))))

;;; Rotate toward a goal and then step toward the goal.
(define-function move-and-rotate-entity ((ecs entity-component-system)
                                         (id entity-id)
                                         (x single-float)
                                         (z single-float)
                                         &optional
                                         (zone-IDs nil (maybe (simple-array entity-id (*))))
                                         (update-zone? nil boolean))
  (let ((distance (find-distance ecs id x z)))
    (and (> (abs distance) 0.00001f0)
         (rotate-toward-angle ecs id x z)
         (move-step-in-distance ecs id distance x z zone-IDs update-zone?)
         t)))

;;; Return the x, z of the stored boid goal.
(define-function boid-goal ((ecs entity-component-system)
                            (goal entity-id))
  (with-selection ecs
      (j :id goal)
      ((location (goal.x location :elt 0)
                 (goal.z location :elt 2)))
    (values goal.x goal.z)))

;;; Find the "center of mass" of nearby boids. Cohesion operates at a
;;; further distance than separation so there are actually two of
;;; these.
(define-function find-boids ((ecs entity-component-system)
                             (boid entity-id)
                             (boid-zone (maybe entity-id))
                             (x single-float)
                             (z single-float))
  (let ((flock-x 0f0)
        (flock-z 0f0)
        (flock-count 0)
        (close-x 0f0)
        (close-z 0f0)
        (close-count 0))
    (with-selection ecs
        (id)
        ((location (other.x location :elt 0)
                   (other.z location :elt 2))
         (boid (in-zone? in-zone?)
               (other.zone zone)))
      (if (and (/= id boid)
               zone
               (= 1 in-zone?)
               (= boid-zone other.zone))
        (let ((distance (distance x z other.x other.z)))
          (when (< distance 10f0)
            (incf flock-x other.x)
            (incf flock-z other.z)
            (incf flock-count 1f0))
          (when (< distance 2.5f0)
            (incf close-x other.x)
            (incf close-z other.z)
            (incf close-count 1f0)))))
    (when (> flock-count 1f0)
      (psetf flock-x (/ flock-x flock-count)
             flock-z (/ flock-z flock-count)))
    (when (> close-count 1f0)
      (psetf close-x (/ close-x close-count)
             close-z (/ close-z close-count)))
    (values flock-x flock-z flock-count close-x close-z close-count)))

;;; Choose goal, choose center of mass and goal, or choose avoidance
;;; and goal.
;;;
;;; Being driven by a goal gives an effect similar to the alignment of
;;; traditional boids.
(define-function (choose-boid-waypoint
                  :return (single-float single-float))
    ((ecs entity-component-system)
     (boid entity-id)
     (goal-x single-float)
     (goal-z single-float)
     (goal-distance single-float))
  (multiple-value-bind (result-x result-z)
      (with-selection ecs
          (i :id boid)
          ((location (x location :elt 0)
                     (z location :elt 2))
           (boid (zone-id zone)
                 (move-zone? move-zone?)))
        (multiple-value-bind (flock-x flock-z flock-count close-x close-z close-count)
            (find-boids ecs boid zone-id x z)
          (cond ((or (and (zerop flock-count)
                          (zerop close-count))
                     ;; Chasing goal
                     (and (zerop move-zone?)
                          (< goal-distance 3.5f0))
                     ;; Moving waypoint zones
                     (and (= 1 move-zone?)
                          (< goal-distance 2f0)))
                 (values goal-x goal-z))
                ;; Separation
                ((plusp close-count)
                 (with-zrvl/mv*
                     ((goal-x zrvl:f32)
                      (goal-z zrvl:f32)
                      (x zrvl:f32)
                      (z zrvl:f32)
                      (close-x zrvl:f32)
                      (close-z zrvl:f32))
                   (zrvl:let* ((goal (zrvl:vec2 goal-x goal-z))
                               (start (zrvl:vec2 x z))
                               (target (zrvl:vec2 close-x close-z))
                               (point (zrvl:- start target))
                               (rotate-at (zrvl:quat<-yaw (zrvl-int:atan2 (zrvl:aref point 0)
                                                                          (zrvl:aref point 1))))
                               (move-step (zrvl:vec3 0f0 0f0 (zrvl:expt (zrvl:/ 3f0 (zrvl:distance start target)) 2f0))))
                     (zrvl:* 0.5f0 (zrvl:+ goal start (zrvl:swizzle (zrvl:rotate rotate-at move-step) :xz))))))
                ;; Cohesion
                ((plusp flock-count)
                 (values (* 0.5f0 (+ goal-x flock-x))
                         (* 0.5f0 (+ goal-z flock-z))))
                ;; Individual; should be the reached from the first case already
                (t
                 (values goal-x goal-z)))))
    (if result-x
        (values result-x result-z)
        (values 0f0 0f0))))

;;; Check directly in front for entities in the way.
(define-function find-boid-collidables ((ecs entity-component-system)
                                        (boid entity-id)
                                        (x single-float)
                                        (z single-float)
                                        (waypoint-x single-float)
                                        (waypoint-z single-float))
  (let ((avoid-x 0f0)
        (avoid-z 0f0)
        (avoid-count 0f0))
    (multiple-value-bind (x z)
        (with-zrvl/mv*
            ((x zrvl:f32)
             (z zrvl:f32)
             (waypoint-x zrvl:f32)
             (waypoint-z zrvl:f32))
          (zrvl:let* ((start (zrvl:vec2 x z))
                      (target (zrvl:vec2 waypoint-x waypoint-z))
                      (point (zrvl:- start target))
                      (rotate-at (zrvl:quat<-yaw (zrvl-int:atan2 (zrvl:aref point 0)
                                                                 (zrvl:aref point 1)))))
            (zrvl:+ start (zrvl:swizzle (zrvl:rotate rotate-at (zrvl:vec3 0f0 0f0 -1f0)) :xz))))
      (with-selection ecs
          (id)
          ((location (other.x location :elt 0)
                     (other.z location :elt 2))
           (physics))
        (unless (= id boid)
          (let ((distance (distance x z other.x other.z)))
            (when (< distance 1.1f0)
              (incf avoid-x other.x)
              (incf avoid-z other.z)
              (incf avoid-count 1f0))))))
    (values avoid-x avoid-z avoid-count)))

(define-function (rotate-away :inline t
                              :default-type single-float)
    (x z avoid-x avoid-z x-dir z-dir)
  (with-zrvl/mv*
      ((x zrvl:f32)
       (z zrvl:f32)
       (avoid-x zrvl:f32)
       (avoid-z zrvl:f32)
       (x-dir zrvl:f32)
       (z-dir zrvl:f32))
    (zrvl:let* ((start (zrvl:vec2 x z))
                (target (zrvl:vec2 avoid-x avoid-z))
                (vec (zrvl:vec3 x-dir 0f0 z-dir))
                (point (zrvl:- start target))
                (rotate-at (zrvl:quat<-yaw (zrvl-int:atan2 (zrvl:aref point 0)
                                                           (zrvl:aref point 1)))))
      (zrvl:swizzle (zrvl:rotate rotate-at vec) :xz))))

;;; Collision avoidance: if far from the goal, change the waypoint to
;;; something else.
(define-function (avoid-boid-collision
                  :return (single-float single-float))
    ((ecs entity-component-system)
     (boid entity-id)
     (waypoint-x single-float)
     (waypoint-z single-float))
  (multiple-value-bind (result-x result-z)
      (with-selection ecs
          (i :id boid)
          ((location (x location :elt 0)
                     (z location :elt 2)))
        (multiple-value-bind (avoid-x avoid-z avoid-count)
            (find-boid-collidables ecs boid x z waypoint-x waypoint-z)
          ;; Try a completely new direction that is almost
          ;; perpendicular if there is something in the way.
          (if (plusp avoid-count)
              (multiple-value-bind (x1 z1) (rotate-away x z avoid-x avoid-z 3f0 1f0)
                (multiple-value-bind (x2 z2) (rotate-away x z avoid-x avoid-z -3f0 1f0)
                  (let* ((d1 (distance x z (+ x x1) (+ z z1)))
                         (d2 (distance x z (+ x x2) (+ z z2)))
                         (distance (- d1 d2)))
                    (if (zerop (cond ((> distance 0.05f0) 1)
                                     ((< distance -0.05f0) 0)
                                     (t (random 2))))
                        (values (+ x x1) (+ z z1))
                        (values (+ x x2) (+ z z2))))))
              (values waypoint-x waypoint-z))))
    (if result-x
        (values result-x result-z)
        (values 0f0 0f0))))

;;; Stay in the zones for local waypoints. Pathfinding is used to go
;;; between zones.
(define-function boid-zone ((zones (simple-array single-float (* 4)))
                            (x single-float)
                            (z single-float))
  (loop :for k :from 0 :below (array-dimension zones 0)
        :for x-min := (aref zones k 0)
        :for x-max := (aref zones k 1)
        :for z-min := (aref zones k 2)
        :for z-max := (aref zones k 3)
        :when (and (<= x-min x) (< x x-max)
                   (<= z-min z) (< z z-max))
          :do (return (values t k))
        :finally (return (values nil -1))))

(define-function (boid-bounds-for-zone
                  :return (single-float single-float single-float single-float))
    ((ecs entity-component-system)
     (id entity-id))
  (multiple-value-bind (x1 x2 z1 z2)
      (with-selection ecs
          (i :id id)
          ((bounding-box (a.x corner-a :elt 0)
                         (b.x corner-b :elt 0)
                         (a.z corner-a :elt 2)
                         (b.z corner-b :elt 2)))
        (values a.x b.x a.z b.z))
    (if x1
        (values x1 x2 z1 z2)
        (error "Navigation zone not found."))))

;;; Find which zone to go to next if the goal is not in the current
;;; zone.
(define-function find-zone-path-to-goal ((ecs entity-component-system)
                                         (world-data world-data)
                                         (zone-id entity-id)
                                         (goal-x single-float)
                                         (goal-z single-float))
  (with-world-data (map-nodes zone-ids) world-data
    (let* ((node (aref map-nodes zone-id))
           (other-node-id (loop :for node* :across (map-node-adjacency node)
                                :for adjacency? := (= 1 node*)
                                :for i :from 0
                                ;; TODO: only one level of adjacent node
                                ;; for now instead of full pathfinding
                                :when (and adjacency?
                                           (multiple-value-bind (x-min* x-max* z-min* z-max*)
                                               (boid-bounds-for-zone ecs (aref zone-ids i))
                                             (and (< x-min* goal-x x-max*)
                                                  (< z-min* goal-z z-max*))))
                                  :do (return i)
                                :finally (return nil)))
           (other-node (and other-node-id
                            (find other-node-id
                                  (map-node-adjacent-nodes node)
                                  :key #'map-node-connection-id))))
      (if other-node
          (values other-node-id other-node)
          (values nil nil)))))

;;; Adjust the boid waypoint based on the goal.
(define-function (adjust-boid-goal
                  :return (single-float single-float))
    ((ecs entity-component-system)
     (boid entity-id)
     (goal-x single-float)
     (goal-z single-float)
     (x-min single-float)
     (x-max single-float)
     (z-min single-float)
     (z-max single-float))
  (multiple-value-bind (result-x result-z)
      (with-selection ecs
          (i :id boid)
          ((location (boid.x location :elt 0)
                     (boid.z location :elt 2))
           (boid (move-zone? move-zone?)))
        (let ((goal-distance (distance boid.x boid.z goal-x goal-z)))
          (multiple-value-bind (x z)
              (choose-boid-waypoint ecs
                                    boid
                                    goal-x
                                    goal-z
                                    goal-distance)
            (multiple-value-bind (x z)
                (if (or (and (zerop move-zone?) (< goal-distance 2f0))
                        (< goal-distance 0.5f0))
                    (values x z)
                    (avoid-boid-collision ecs boid x z))
              (declare (type single-float x z))
              (let ((border (if (and (= 1 move-zone?) (< goal-distance 0.7f0))
                                0f0
                                0.7f0)))
                (values (clamp (+ x-min border) x (- x-max border))
                        (clamp (+ z-min border) z (- z-max border))))))))
    (if result-x
        (values result-x result-z)
        (values 0f0 0f0))))

(define-function update-boid-zone? ((ecs entity-component-system)
                                    (boid entity-id))
  (with-selection ecs
      (i :id boid)
      ((location (boid.x location :elt 0)
                 (boid.z location :elt 2))
       (boid (waypoint.x waypoint :elt 0)
             (waypoint.z waypoint :elt 2)
             (in-zone? in-zone?)
             (move-zone? move-zone?)))
    (and (= 1 in-zone?)
         (= 1 move-zone?)
         (< (distance boid.x boid.z waypoint.x waypoint.z) 0.001f0))))

(define-function update-boid-zone ((ecs entity-component-system)
                                   (boid entity-id))
  (with-selection ecs
      (i :id boid)
      ((location (boid.x location :elt 0)
                 (boid.z location :elt 2))
       (boid (waypoint.x waypoint :elt 0)
             (waypoint.z waypoint :elt 2)
             (in-zone? in-zone?)
             (zone-id zone)
             (move-zone? move-zone?)
             (next-zone next-zone)))
    (when (and (= 1 in-zone?)
               (= 1 move-zone?)
               ;; Check again that it hasn't moved further away
               (< (distance boid.x boid.z waypoint.x waypoint.z) 0.001f0))
      (psetf zone-id next-zone
             move-zone? 0))))

;;; Check to see if the boid goal is outside of its zone
(define-function (outside-goal-zone? :inline t)
    (goal-x goal-z x-min x-max z-min z-max)
  (or (not (< x-min goal-x x-max))
      (not (< z-min goal-z z-max))))

;;; Update the flocking goal of the boid
(define-function update-boid-flocking ((ecs entity-component-system)
                                       (boid entity-id)
                                       (world-data world-data))
  (with-selection ecs
      (i :id boid)
      ((location (boid.x location :elt 0)
                 (boid.z location :elt 2))
       (boid (goal goal)
             (waypoint.x waypoint :elt 0)
             (waypoint.z waypoint :elt 2)
             (in-zone? in-zone?)
             (zone-id zone)
             (move-zone? move-zone?)
             (next-zone next-zone)))
    (when (= 1 in-zone?)
      (multiple-value-bind (x-min x-max z-min z-max)
          (with-world-data (zone-ids) world-data
            (boid-bounds-for-zone ecs (aref zone-ids zone-id)))
        (multiple-value-bind (goal-x goal-z) (boid-goal ecs goal)
          (let ((adjust-goal? (if (outside-goal-zone? goal-x goal-z x-min x-max z-min z-max)
                                  (multiple-value-bind (other-node-id other-node)
                                      (find-zone-path-to-goal ecs world-data zone-id goal-x goal-z)
                                    (when other-node
                                      (psetf (values goal-x goal-z)
                                             (array-of-2 (elt (map-node-connection-points other-node) 0))
                                             move-zone? 1
                                             next-zone other-node-id))
                                    (and other-node-id t))
                                  (progn
                                    (setf move-zone? 0)
                                    t))))
            (setf (values waypoint.x waypoint.z)
                  (if adjust-goal?
                      (adjust-boid-goal ecs boid goal-x goal-z x-min x-max z-min z-max)
                      (values boid.x boid.z)))))))))

;; Moves and rotates boid towards its waypoint
(define-function move-and-rotate-boid ((ecs entity-component-system)
                                       (boid entity-id)
                                       (world-data world-data)
                                       (update-boid-zone? boolean))
  (with-selection ecs
      (i :id boid)
      ((location)
       (boid (waypoint.x waypoint :elt 0)
             (waypoint.z waypoint :elt 2)))
    (with-world-data (zone-ids) world-data
      (move-and-rotate-entity ecs boid waypoint.x waypoint.z zone-ids update-boid-zone?))))

;;; Only update the target periodically after a certain number of
;;; ticks have passed. Add the entity-id of the boid so they probably
;;; don't all update at the same time.
(define-function (boid-update-tick? :inline t) ((offset entity-id)
                                                (tick tick)
                                                (update-interval tick))
  (zerop (mod (+ tick offset) update-interval)))

;;; Flocks one boid to the other boids by determining a new waypoint
;;; and then moving to it. Only motion is done every tick.
(define-function flock-boid ((ecs entity-component-system)
                             (boid entity-id)
                             (tick tick)
                             (world-data world-data)
                             &key
                             (update-interval 20 tick))
  (let ((update-boid-zone? (update-boid-zone? ecs boid)))
    (when (and (not update-boid-zone?)
               (boid-update-tick? boid tick update-interval))
      (update-boid-flocking ecs boid world-data))
    (move-and-rotate-boid ecs boid world-data update-boid-zone?)
    (when update-boid-zone?
      (update-boid-zone ecs boid))))

;;; Flocks all boids to each other on the update interval
(define-function flock-boids ((ecs entity-component-system)
                              (tick tick)
                              (world-data world-data)
                              &key (update-interval 20 tick))
  (with-selection ecs
      (boid-id)
      ((boid))
    (flock-boid ecs boid-id tick world-data :update-interval update-interval)))

;;; Update the goal location to the current target entity's location.
;;; This isn't done every tick, which gives the boids a delayed
;;; reaction and saves on computation.
(define-function update-boid-goal ((ecs entity-component-system)
                                   (target-id entity-id)
                                   (goal entity-id))
  (with-selection ecs
      (i :id target-id)
      ((location (target-location location :row-of 3)))
    (with-selection ecs
        (j :id goal)
        ((location (goal-location location :row-of 3)))
      (setf goal-location target-location))))
