(defpackage #:zombie-raptor/entity/entity
  (:documentation "Defines the core of the engine's Entity Component System (ECS).")
  (:use #:cl
        #:zombie-raptor/core/constants
        #:zombie-raptor/core/types
        #:zombie-raptor/entity/meta-entity
        #:zombie-raptor/math/boolean-set
        #:zr-utils)
  (:import-from #:alexandria
                #:with-gensyms)
  (:export #:add-entity
           #:entity-component-system
           #:make-entity-component-system
           #:remove-entity
           #:store-old-states
           #:with-selection))

(in-package #:zombie-raptor/entity/entity)

;;;; Entity component system

(define-accessor-macro with-ecs-accessors)

;;; Note: This eval-when is required by CCL.
(eval-when (:compile-toplevel :load-toplevel :execute)
  ;; This is the actual entity component system for now. The way this
  ;; is defined is not the final way to do it.
  (define-entity-component-system entity-component-system
      ;; Entities. An entity object doesn't actually exist at the
      ;; moment, but it wouldn't be hard to create an entity class
      ;; that just looks up data from the ECS. What the entity really
      ;; consists of is just a "boolean set" containing bits that
      ;; indicate the presence or absence of a component.
      (count +max-entities+)
      (component-boolean-set component-index component-boolean-set-union)
    ;; Component "tables", each consisting of one or more arrays. At
    ;; the moment, these are all of the possible components, but
    ;; parallel components with their own boolean sets should be added
    ;; in the future. The main complication would be when a system
    ;; requires a certain mix from this built-in ECS and from a
    ;; user-defined ECS.
    (location
     (old-location (vecs 3 count))
     (location (vecs 3 count)))
    (velocity
     (velocity (vecs 3 count)))
    (rotation
     (old-rotation (vecs 4 count))
     (rotation (vecs 4 count)))
    (geometry
     ;; Most geometry data is in a separate "table", where the data is
     ;; referred to by this mesh-id. This effectively serves as a
     ;; "foreign key" into that "table".
     (mesh-id (gl-ids count))
     (renderable? (simple-bit-vector count)))
    (scale
     (scale (vecs 3 count)))
    ;; Most of the actual state is related to the physics system,
    ;; which includes the input-based movement
    (physics
     (input-velocity (single-floats count) :initial-element 0.04f0)
     (input-direction (vecs 3 count))
     (input? (simple-bit-vector count))
     (acceleration (vecs 3 count))
     (allow-falling? (simple-bit-vector count) :initial-element 1)
     (gravity? (simple-bit-vector count))
     (jump-strength (single-floats count) :initial-element 4.75f0))
    (bounding-box
     (corner-a (vecs 3 count))
     (corner-b (vecs 3 count)))
    (look
     (camera-entity (entity-ids count)))
    ;; For entities that move like "boids", i.e. flocks, schools,
    ;; herds, swarms...
    (boid
     (waypoint (vecs 3 count))
     (goal (entity-ids count))
     (in-zone? (simple-bit-vector count))
     (zone (entity-ids count))
     (move-zone? (simple-bit-vector count))
     (next-zone (entity-ids count)))))

(define-simple-struct render-system
  (location location)
  (velocity velocity)
  (rotation rotation)
  (geometry geometry)
  (scale    scale)
  (look     look))

(define-accessor-macro with-render-system-accessors #.(symbol-name '#:render-system-))

(defmethod print-object ((system render-system) stream)
  (print-unreadable-object (system stream :type t :identity t))
  system)

;;;; Select from the entity component system

(eval-when (:compile-toplevel :load-toplevel :execute)
  ;; Generates the body of the macro with-selection, which
  ;; conditionally executes the code on components of the ECS.
  (defun %body (body %changed? changed? when let-changed?)
    (let* ((body `(,@(list-if (and changed? (not (eql changed? :clear)))
                              `(setf ,%changed? 1))
                   ,@body
                   ,@(list-if (eql changed? :clear) `(setf ,%changed? 0))))
           (body (if when `((when ,when ,@body)) body))
           (body (if let-changed?
                     `((let ((,let-changed? (= 1 ,%changed?)))
                         ,@body))
                     body)))
      body))
  ;; Generates the outer accessors of the macro with-selection
  (defun %outer-accessors (accessors)
    (loop :with unique-arrays := (make-hash-table)
          :for accessor :in accessors
          :for array-name := (if (listp accessor)
                                 (nth 1 accessor)
                                 accessor)
          :unless (gethash array-name unique-arrays)
            :do (setf (gethash array-name unique-arrays) t)
            :and :collect array-name))
  ;; Generates the bindings for the ECS selection
  (defun %selection-bindings (tables-and-accessors)
    (loop :for (table . accessors) :in tables-and-accessors
          :for name := (gensym (symbol-name table))
          :collect `(,name ,table)
            :into bindings
          :collect `(,(surround-symbol (symbol-name '#:with-)
                                       table
                                       (symbol-name '#:-accessors)
                                       '#:zombie-raptor/entity/entity)
                     ,(%outer-accessors accessors)
                     ,name)
            :into outer-accessors
          :collect name
            :into table-locations
          :finally
             (return (values bindings outer-accessors table-locations))))
  ;; Generates the code that conditionally adds or removes components
  ;; in with-selection if requested
  (defun %add-and-remove-components (add remove)
    (let ((add* (mapcar #'symbol-to-keyword (if (listp add) add (list add))))
          (remove* (mapcar #'symbol-to-keyword (if (listp remove) remove (list remove)))))
      (dolist (component remove*)
        ;; TODO: replace with custom-defined condition
        (error-when (member component add*)
                    "A component cannot be added and removed at the same time."))
      (values add* remove*)))
  ;; Generates the test to see if an entity has certain components
  ;; (i.e. the component bits are 1, not 0)
  (defun %component-bits (entity-bits tables-and-accessors add remove)
    (let ((component-keywords (mapcar (lambda (item)
                                        (symbol-to-keyword (first item)))
                                      tables-and-accessors)))
      (let* ((add-component-bits (component-boolean-set-union add))
             (remove-component-bits (component-boolean-set-union remove))
             (component-bits (boolean-set-difference (boolean-set-union (component-boolean-set-union component-keywords)
                                                                        remove-component-bits)
                                                     add-component-bits))
             (implements-components? `(boolean-set-subset-p ,component-bits ,entity-bits))
             (add-components (if add
                                 `((setf ,entity-bits
                                         (boolean-set-union ,entity-bits ,add-component-bits)))
                                 nil))
             (remove-components (if remove
                                    `((setf ,entity-bits
                                            (boolean-set-difference ,entity-bits ,remove-component-bits)))
                                    nil)))
        `(,implements-components? ,@remove-components ,@add-components)))))

(defmacro with-selection (ecs (index-name &key id add remove changed? let-changed? when return)
                          tables-and-accessors
                          &body body)
  "
Selects one or more entities from the entity component system that
implement the given API. If an id is given, then either the entity
implements the API and the code is run or the entity does not and the
code is not run. If an id is not given, then this macro iterates over
all matches.

Note that entities themselves do not actually exist as objects so the
only way entities can be manipulated is indirectly, through the
provided accessors of one or multiple values.
"
  (multiple-value-bind (add remove)
      (%add-and-remove-components add remove)
    (multiple-value-bind (bindings outer-accessors table-locations)
        (%selection-bindings tables-and-accessors)
      (with-gensyms (entity-set-array changed-bits %changed? entity-bits)
        (let* ((ecs-accessors `((,entity-set-array entity)
                                ,@(if (or changed? let-changed?)
                                      `((,changed-bits changed-bits))
                                      '())
                                ,@bindings))
               (array-accessors `(,@(list-if (or changed? let-changed?)
                                             `(,%changed? ,changed-bits))
                                  ,@(mapcan #'rest tables-and-accessors)))
               (entity-binding (if id
                                   `(let ((,index-name ,id)))
                                   `(dotimes (,index-name (max-index ,ecs))))))
          `(with-ecs-accessors ,ecs-accessors ,ecs
             (declare (ignorable ,@table-locations))
             (with-flat-accessors ,outer-accessors
               (,@entity-binding
                (with-array-accessors ((,entity-bits ,entity-set-array)) ,index-name
                  (when ,@(%component-bits entity-bits tables-and-accessors add remove)
                    (with-array-accessors ,array-accessors ,index-name
                      ,@(%body body %changed? changed? when let-changed?))))))
             ,@(list-if return return)))))))

;;;; Misc. ECS functions

(define-function store-old-states ((ecs entity-component-system))
  "Stores the old ECS states for interpolation."
  ;; Note: Do the updates separately because an entity may not
  ;; implement all components.
  (with-selection ecs
      (entity-id)
      ((location (location location     :row-of 3)
                 (_        old-location :row-of 3 := location))))
  (with-selection ecs
      (entity-id)
      ((rotation (rotation      rotation          :row-of 4)
                 (_             old-rotation      :row-of 4 := rotation)))))

;;;; Entity

(define-function add-entity ((ecs entity-component-system))
  "Adds a new entity to the ECS."
  (with-ecs-accessors (removed-entity max-index) ecs
    (if (removed-entity-empty? removed-entity)
        (prog1 max-index (incf max-index))
        (removed-entity-dequeue removed-entity))))

(define-function remove-entity ((ecs entity-component-system)
                                (entity-id entity-id))
  "Removes all components from an entity."
  (with-ecs-accessors (entity removed-entity) ecs
    (with-array-accessors ((entity-bits entity)) entity-id
      (setf entity-bits +empty-boolean-set+))
    (removed-entity-enqueue removed-entity entity-id)))
