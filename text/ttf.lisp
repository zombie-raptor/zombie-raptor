(cl:defpackage #:zombie-raptor/text/ttf
  (:documentation "Read and parse a standard TTF file.")
  (:import-from #:alexandria
                #:make-gensym-list)
  (:use #:cl
        #:zr-utils)
  (:export #:read-ttf))

(cl:in-package #:zombie-raptor/text/ttf)

(defparameter *debug* nil)

;;; Note: Beyond 4, loop unrolling via macro probably isn't worth it.
;;;
;;; Note: This doesn't do the signed shift at the end, if it's signed.
(defmacro define-read-bytes (name (stream) size)
  "
Defines an inline function for reading multiple octets into one
unsigned integer.
"
  `(define-function (,name :inline t) (,stream)
     (logior ,@(loop :for i :from 0 :below size
                     :collect `(ash (the octet (read-byte ,stream)) (* 8 ,(- size (1+ i))))))))

(defmacro define-read-byte-array (name (stream length) type type-reader)
  "
Defines an inline function for creating an array of some integer size
and reading an octet stream to fill its contents.
"
  ;; TODO: replace with custom-defined condition
  (error-when (and (listp type) (not (listp type-reader)))
              "Either both type and type-reader must be lists or neither can be.")
  (let* ((array-names (make-gensym-list (if (listp type) (length type) 1) "A"))
         (make-arrays (loop :for item :in (if (listp type)
                                              type
                                              (list type))
                            :for array-name :in array-names
                            :append `(:with ,array-name := (make-array ,length
                                                                       :element-type ',item
                                                                       :initial-element 0))))
         (read-bytes (loop :for item :in (if (listp type-reader)
                                             type-reader
                                             (list type-reader))
                           :for array-name :in array-names
                           :append `((aref ,array-name i) (,item ,stream)))))
    `(define-function (,name :inline t) (,stream ,length)
       (loop ,@make-arrays
             :for i :from 0 :below ,length
             :do (setf ,@read-bytes)
             :finally (return ,@(if (listp type)
                                    `((values ,@array-names))
                                    array-names))))))

;;; Convenience functions for reading unsigned integers.
(define-read-bytes read-uint16 (stream) 2)
(define-read-bytes read-uint32 (stream) 4)

;;; Convenience functions for reading unsigned integer arrays.
(define-read-byte-array read-uint16-array (stream length) uint16 read-uint16)
(define-read-byte-array read-uint32-array (stream length) uint32 read-uint32)

;;; Reads CMAP metadata.
(define-read-byte-array read-cmap-metadata (stream length)
  (uint16 uint16 uint32)
  (read-uint16 read-uint16 read-uint32))

;;; Types specific to TTF that are not already given a name elsewhere.
(deftype f-word () `int16)
(deftype u-f-word () `uint16)

;;; TTF data structures

(define-simple-struct (ttf-offset-subtable (:conc-name nil))
  (scalar-type    uint32)
  (num-tables     uint16)
  (search-range   uint16)
  (entry-selector uint16)
  (range-shift    uint16))

(define-function (table-directory-tag :inline t) (tables table)
  (aref tables table 0))

(define-function (table-directory-checksum :inline t) (tables table)
  (aref tables table 1))

(define-function (table-directory-offset :inline t) (tables table)
  (aref tables table 2))

(define-function (table-directory-length :inline t) (tables table)
  (aref tables table 3))

(defstruct cmap-format-4
  (format              4 :type uint16 :read-only t)
  (length              0 :type uint16)
  (language            0 :type uint16)
  (seg-count*2         0 :type uint16)
  (search-range        0 :type uint16)
  (entry-selector      0 :type uint16)
  (range-shift         0 :type uint16)
  (end-code          nil :type (simple-array uint16 (*)))
  (reserved-pad        0 :type uint16 :read-only t)
  (start-code        nil :type (simple-array uint16 (*)))
  (is-delta          nil :type (simple-array uint16 (*)))
  (id-range-offset   nil :type (simple-array uint16 (*)))
  (glyph-index-array nil :type (simple-array uint16 (*))))

(defstruct cmap
  (version                 0 :type uint16)
  (number-subtables        0 :type uint16)
  (platform-ids          nil :type (simple-array uint16 (*)))
  (platform-specific-ids nil :type (simple-array uint16 (*)))
  (offsets               nil :type (simple-array uint32 (*)))
  (subtables             nil))

(defstruct simple-glyph
  (end-pts-of-contours nil :type (simple-array uint16 (*)))
  (instruction-length  nil :type uint16)
  (instructions        nil :type (simple-array uint8 (*)))
  (flags               nil :type (simple-array uint8 (*)))
  (x-coordinates       nil :type (or (simple-array uint8 (*))
                                     (simple-array int16 (*))))
  (y-coordinates       nil :type (or (simple-array uint8 (*))
                                     (simple-array int16 (*)))))

(defstruct compound-glyph
  (flags                   0 :type uint16)
  (glyph-index             0 :type uint16)
  (argument-1              0 :type (or int16 uint16 int8 uint8))
  (argument-2              0 :type (or int16 uint16 int8 uint8))
  (transformation-option nil))

(defstruct glyf
  (number-of-contours   0 :type int16)
  (x-min                0 :type f-word)
  (y-min                0 :type f-word)
  (x-max                0 :type f-word)
  (y-max                0 :type f-word)
  (simple-or-complex  nil :type (or simple-glyph compound-glyph)))

;;; Warns if the value read in from the TTF value is not the expected
;;; value when derived from some other read value. This means that the
;;; parser could have failed or that the data might be corrupted. This
;;; is a warning. It attempts to continue anyway, especially since
;;; many of these fields are not actually used.
;;;
;;; Note: A simple way to test this is to replace = with /= or
;;; `unless' with `when'. Then everything should warn, providing
;;; numbers that are identical in the warning.
(defun expected-value-warning (name actual-value expected-value &optional number)
  (unless (= actual-value expected-value)
    (warn "In ~A~@[ #~D~]: expected ~D, got ~D"
          name
          number
          expected-value
          actual-value)))

(defmacro expected-value-warnings (&body warning)
  "Combines consecutive `expected-value-warning's into one form."
  `(progn ,@(mapcar (lambda (item)
                      `(expected-value-warning ,@item))
                    warning)))

;;; Read a TTF cmap subtable of type format 4.
(define-function (read-cmap-format-4 :return cmap-format-4) ((file stream) (i uint16))
  (let* ((length (read-uint16 file))
         (language (read-uint16 file))
         (seg-count*2 (read-uint16 file))
         (seg-count (floor seg-count*2 2))
         (search-range (read-uint16 file))
         (entry-selector (read-uint16 file))
         (range-shift (read-uint16 file))
         (end-code (read-uint16-array file seg-count))
         (reserved-pad (read-uint16 file))
         (start-code (read-uint16-array file seg-count))
         (is-delta (read-uint16-array file seg-count))
         (id-range-offset (read-uint16-array file seg-count))
         (remaining-length (- length (* 2 8) (* 2 4 seg-count)))
         (glyph-index-array (read-uint16-array file (floor remaining-length 2))))
    ;; Validate before creating the struct
    (expected-value-warnings
      ("search-range in TTF cmap subtable"
       search-range (* 2 (expt 2 (floor (log seg-count 2)))) i)
      ("entry-selector in TTF cmap subtable"
       entry-selector (floor (log (floor search-range 2) 2)) i)
      ("range-shift in TTF cmap subtable"
       range-shift (- seg-count*2 search-range) i)
      ("the last end-code value in cmap subtable"
       (aref end-code (- (length end-code) 1)) #xffff i)
      ("reserved-pad in TTF cmap subtable"
       reserved-pad 0 i))
    ;; Create the struct
    (make-cmap-format-4 :length length
                        :language language
                        :seg-count*2 seg-count*2
                        :search-range search-range
                        :entry-selector entry-selector
                        :range-shift range-shift
                        :end-code end-code
                        :start-code start-code
                        :is-delta is-delta
                        :id-range-offset id-range-offset
                        :glyph-index-array glyph-index-array)))

;;; Read the TTF cmap table
(define-function (read-cmap :return cmap) ((file stream) (checksum uint32) (offset uint32) (length uint32))
  ;; TODO: A temporary printing of the global "cmap" data
  (when *debug*
    (format t "cmap ~D ~D ~D~%" checksum offset length))
  ;; TODO: replace with custom-defined condition
  (error-unless (input-stream-p file)
                "~A needs an input stream!"
                'read-cmap)
  (file-position file offset)
  (let ((version (read-uint16 file))
        (number-subtables (read-uint16 file)))
    (multiple-value-bind (platform-ids platform-specific-ids offsets)
        (read-cmap-metadata file number-subtables)
      ;; TODO: validate the above before jumping around in the file
      ;; multiple times
      (let ((subtables (loop :with cmap-subtables := (make-array number-subtables :initial-element nil)
                             :for offset* :of-type uint32 :across offsets
                             :for i :from 0 :below number-subtables
                             :for format := (progn (file-position file (+ offset offset*))
                                                   (read-uint16 file))
                             :with ignored-subtables := 0
                             :do
                                ;; TODO: support format 12
                                (case= format
                                  (4 (setf (aref cmap-subtables i) (read-cmap-format-4 file i)))
                                  (t (warn "The cmap format ~D in subtable ~D is unsupported for now. Ignoring subtable." format i)
                                     (incf ignored-subtables)))
                             :finally
                                ;; TODO: replace with custom-defined condition
                                (error-when (>= ignored-subtables number-subtables)
                                            "Every cmap subtable (~D of ~D) was ignored in this TTF."
                                            ignored-subtables
                                            number-subtables)
                                (return (if (= 1 number-subtables)
                                            (aref cmap-subtables 0)
                                            cmap-subtables)))))
        (make-cmap :version version
                   :number-subtables number-subtables
                   :platform-ids platform-ids
                   :platform-specific-ids platform-specific-ids
                   :offsets offsets
                   :subtables subtables)))))

;;; Converts a literal string to its uint32 representation used in the
;;; TTF format, assuming ASCII (or Unicode) char-code
(defmacro literal-string-to-uint32 (literal-string)
  (check-type literal-string (string 4))
  (loop :for char :across literal-string
        :for char-code :of-type octet := (char-code char)
        :for i :from 0
        :sum (ash char-code (* 8 (- 3 i))) :of-type uint32))

(defun read-ttf (path)
  "
Parses the TTF file format as described in the official documentation
of the TTF file format.

The parse is done as close to the TTF documentation as possible so
that reading (and eventually writing) TTFs is as straightforward as
possible for people who are following along with the official
documentation. This means that the lowest level of the TTF support
parses things into structs with the same names as the structs in the
TTF documentation. The only difference is that the camelCase names are
converted to the hyphenated style, even if the choice of names
themselves still might not be the most idiomatic.

This is not unlike using a library wrapped with the CFFI in a
low-level way, except that this TTF reader implementation is done
entirely in Common Lisp. Without having this lowest layer, the user
would be stuck using yet another poorly documented library with
unknown abstractions and would have to waste time trying to unravel
these abstractions. With this layer, the user can use existing TTF
knowledge and resources to try to figure out how this works.

For the official Apple TTF documentation, visit the following URL:

    https://developer.apple.com/fonts/TrueType-Reference-Manual/

Not everything there is relevant to parsing a TTF file and not
everything in a TTF file will be parsed. For instance, supporting
PostScript for printing is outside of the scope of a game engine
project and so the \"post\" table will never be supported even though
it is a required table.
"
  ;; Parse the start of the TTF file and find the tables that are
  ;; currently in use.
  ;;
  ;; Note: Use this order as the basic minimum:
  ;;
  ;; start, name, head, maxp, hhead, hmtx, cmap, loca, glyf
  (with-open-file (file path :direction :input :element-type 'octet)
    (let ((offset-subtable (make-ttf-offset-subtable :scalar-type (read-uint32 file)
                                                     :num-tables (read-uint16 file)
                                                     :search-range (read-uint16 file)
                                                     :entry-selector (read-uint16 file)
                                                     :range-shift (read-uint16 file))))
      ;; One of these two numbers is required for an ordinary TTF
      ;; file according to the specification.
      ;;
      ;; TODO: replace with custom-defined condition
      (error-when (/= (scalar-type offset-subtable) #x74727565 #x00010000)
                  "Unsupported TTF format. Expected #x~8,'0X or #x~8,'0X, but got #x~8,'0X"
                  #x74727565
                  #x00010000
                  (scalar-type offset-subtable))
      ;; Check the integrity of the entries in the offset subtable
      ;; based on the values they should be from the specification.
      (let ((log-2 (floor (log (num-tables offset-subtable) 2d0))))
        (expected-value-warning "search-range in the TTF offset subtable"
                                (search-range offset-subtable)
                                (* 16 (expt 2 log-2)))
        (expected-value-warning "entry-selector in the TTF offset subtable"
                                (entry-selector offset-subtable)
                                log-2)
        (expected-value-warning "range-shift in the TTF offset subtable"
                                (range-shift offset-subtable)
                                (- (* (num-tables offset-subtable) 16) (search-range offset-subtable))))
      (let* ((table-directory (loop :for table :from 0 :below (num-tables offset-subtable)
                                    :with table-directory := (make-array (list (num-tables offset-subtable) 4)
                                                                         :initial-element 0
                                                                         :element-type 'uint32)
                                    :do (with-array-accessors ((table-directory table-directory :row-of 4)) table
                                          (let ((tag (read-uint32 file))
                                                (checksum (read-uint32 file))
                                                (offset (read-uint32 file))
                                                (length (read-uint32 file)))
                                            (setf table-directory (values tag checksum offset length))))
                                    :finally (return table-directory)))
             (cmap (multiple-value-call #'read-cmap
                     file
                     ;; Find "cmap"
                     (loop :for i :from 0 :below (num-tables offset-subtable)
                           :with target-tag :of-type uint32 := (literal-string-to-uint32 "cmap")
                           :for tag :of-type uint32 := (table-directory-tag table-directory i)
                           :when (= tag target-tag)
                             :do (return (values (table-directory-checksum table-directory i)
                                                 (table-directory-offset table-directory i)
                                                 (table-directory-length table-directory i)))
                                 ;; TODO: replace with custom-defined condition
                           :finally (error "Table ~S not found!" "cmap")))))
        (when *debug*
          (format t "~S~%" cmap))
        ;; TODO: the next step is to find the "loca" table
        ;; Find "glyf" (i.e. glyph)
        (multiple-value-bind (checksum offset length)
            (loop :for i :from 0 :below (num-tables offset-subtable)
                  :with target-tag :of-type uint32 := (literal-string-to-uint32 "glyf")
                  :for tag :of-type uint32 := (table-directory-tag table-directory i)
                  :when (= tag target-tag)
                    :do (return (values (table-directory-checksum table-directory i)
                                        (table-directory-offset table-directory i)
                                        (table-directory-length table-directory i)))
                        ;; TODO: replace with custom-defined condition
                  :finally (error "Table ~S not found!" "glyf"))
          (when *debug*
            (format t "glyf ~D ~D ~D~%" checksum offset length))))))
  ;; TODO: temporarily return nothing
  nil)
