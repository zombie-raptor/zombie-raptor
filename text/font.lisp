(defpackage #:zombie-raptor/text/font
  (:documentation "Load a supported font into a format that the engine recognizes.")
  (:use #:cl
        #:zombie-raptor/data/game-data
        #:zombie-raptor/math/vector
        #:zombie-raptor/text/ttf
        #:zr-utils)
  (:import-from #:alexandria
                #:lerp)
  (:import-from #:zpb-ttf
                #:contour
                #:contour-count
                #:find-glyph
                #:on-curve-p
                #:with-font-loader
                #:x
                #:y)
  (:export #:generate-glyph-coordinates))

(in-package #:zombie-raptor/text/font)

(define-function (copy-into-point! :inline t :default-type double-float :return dvec2)
    ((point dvec2) x y scale x-offset y-offset)
  (setf (array-of-2 point)
        (values (+ x-offset (* x scale))
                (+ y-offset (* y scale))))
  point)

(define-function (%quadratic-discriminant :inline t :default-type double-float) (a b c)
  (- (expt b 2) (* 4 a c)))

(define-function (%quadratic+ :inline t :default-type double-float) (a b discriminant)
  (/ (+ (- b) (sqrt discriminant)) (* 2 a)))

(define-function (%quadratic- :inline t :default-type double-float) (a b discriminant)
  (/ (- (- b) (sqrt discriminant)) (* 2 a)))

(define-function (%quadratic-simple :inline t :default-type double-float) (a b)
  (/ (- b) (* 2 a)))

(define-function (%bezier-point :inline t :default-type double-float) (a b c t*)
  (+ (* a (expt t* 2)) (* b t*) c))

(define-function (%bezier-coord-array :inline t :default-type double-float)
    (point p0 p1 p2 (coord-array dvec2) (solution-array dvec2))
  (let ((a (+ p0 (* -2 p1) p2)))
    (if (zerop a)
        0
        (let* ((b (+ (* -2 p0) (* 2 p1)))
               (c (- p0 point))
               (delta (%quadratic-discriminant a b c)))
          (cond ((zerop delta)
                 (let* ((solution (%quadratic-simple a b)))
                   (setf (aref coord-array 0) (%bezier-point a b p0 solution)
                         (aref solution-array 0) solution))
                 1)
                ((plusp delta)
                 (let* ((solution+ (%quadratic+ a b delta))
                        (solution- (%quadratic- a b delta)))
                   (let ((solution-min (min solution- solution+))
                         (solution-max (max solution+ solution-)))
                     (setf (aref coord-array 0) (%bezier-point a b p0 solution-min)
                           (aref coord-array 1) (%bezier-point a b p0 solution-max)
                           (aref solution-array 0) solution-min
                           (aref solution-array 1) solution-max)))
                 2)
                (t 0))))))

(define-function (distance :inline t :default-type double-float) (p0.x p1.x p0.y p1.y)
  (sqrt (+ (expt (- p0.x p1.x) 2)
           (expt (- p0.y p1.y) 2))))

(defstruct (glyph-bezier-curves (:conc-name nil))
  (linear-bezier-curves    nil :type (simple-array double-float (* 2 2)))
  (quadratic-bezier-curves nil :type (simple-array double-float (* 3 2))))

(defun generate-glyphs ()
  (let* ((characters "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-/*œæ!.,?")
         (glyphs (make-array (length characters)))
         (path (font-path "NotoSans-Regular" "noto-fonts" "unhinted")))
    (read-ttf path)
    (with-font-loader (font-loader path)
      (loop :for i :from 0 :below (length characters)
            :for character :across characters
            :for glyph := (find-glyph character font-loader)
            :with line-count := 0
            :with quadratic-count := 0
            :do
               (loop :for j :below (contour-count glyph)
                     :for contour := (contour glyph j)
                     :do
                        (loop :for i :from 0 :below (length contour)
                              :for first := (aref contour i)
                              :for second := (aref contour (mod (1+ i) (length contour)))
                              :for line? := (and (on-curve-p first) (on-curve-p second))
                              :for quadratic? := (not (on-curve-p second))
                              :do
                                 (cond (line? (incf line-count))
                                       (quadratic? (incf quadratic-count)))))
               (let* ((lines (make-array (list line-count 2 2) :element-type 'double-float))
                      (quadratics (make-array (list quadratic-count 3 2) :element-type 'double-float))
                      (glyph-curves (make-glyph-bezier-curves :linear-bezier-curves lines
                                                              :quadratic-bezier-curves quadratics)))
                 (setf (aref glyphs i) glyph-curves)
                 (loop :for j :below (contour-count glyph)
                       :for contour := (contour glyph j)
                       :with l := 0
                       :with q := 0
                       :do
                          (loop :for i :from 0 :below (length contour)
                                :for first := (aref contour i)
                                :for second := (aref contour (mod (1+ i) (length contour)))
                                :for third := (aref contour (mod (+ 2 i) (length contour)))
                                :for line? := (and (on-curve-p first) (on-curve-p second))
                                :for quadratic? := (not (on-curve-p second))
                                :do
                                   (cond (line?
                                          (setf (aref lines l 0 0) (double-float* (x first))
                                                (aref lines l 0 1) (double-float* (y first))
                                                (aref lines l 1 0) (double-float* (x second))
                                                (aref lines l 1 1) (double-float* (y second)))
                                          (incf l))
                                         (quadratic?
                                          (let ((p2.x (double-float* (x second)))
                                                (p2.y (double-float* (y second))))
                                            (multiple-value-bind (p1.x p1.y)
                                                (if (on-curve-p first)
                                                    (values (double-float* (x first)) (double-float* (y first)))
                                                    (values (/ (+ p2.x (double-float* (x first))) 2d0)
                                                            (/ (+ p2.y (double-float* (y first))) 2d0)))
                                              (multiple-value-bind (p3.x p3.y)
                                                  (if (on-curve-p third)
                                                      (values (double-float* (x third)) (double-float* (y third)))
                                                      (values (/ (+ p2.x (double-float* (x third))) 2d0)
                                                              (/ (+ p2.y (double-float* (y third))) 2d0)))
                                                (setf (aref quadratics q 0 0) p1.x
                                                      (aref quadratics q 0 1) p1.y
                                                      (aref quadratics q 1 0) p2.x
                                                      (aref quadratics q 1 1) p2.y
                                                      (aref quadratics q 2 0) p3.x
                                                      (aref quadratics q 2 1) p3.y)
                                                (incf q)))))))))
            :finally (return glyphs)))))

(defconstant +epsilon+ 0.001d0)

(define-function (in-range? :inline t) (start point end)
  (<= (- start +epsilon+) point (+ end +epsilon+)))

(define-function (find-quadratic-bezier-x! :default-type dvec2 :return ((mod 3)))
    (x-coords (point.y double-float) p0 p1 p2 (directions (simple-bit-vector 2)))
  (let* ((p0.x (aref p0 0))
         (p0.y (aref p0 1))
         (p1.x (aref p1 0))
         (p1.y (aref p1 1))
         (p2.x (aref p2 0))
         (p2.y (aref p2 1))
         (y-coords (make-array 2 :element-type 'double-float))
         (solution-array (make-array 2 :element-type 'double-float))
         (solutions (%bezier-coord-array point.y p0.y p1.y p2.y y-coords solution-array)))
    (declare (dynamic-extent y-coords solution-array))
    (let* ((valid-0 (if (and (> solutions 0)
                             (in-range? point.y (aref y-coords 0) point.y)
                             (in-range? 0d0 (aref solution-array 0) 1d0))
                        t
                        nil))
           (valid-1 (if (and (> solutions 1)
                             (in-range? point.y (aref y-coords 1) point.y)
                             (in-range? 0d0 (aref solution-array 1) 1d0))
                        t
                        nil))
           (solutions (cond ((and valid-0 valid-1) 2)
                            (valid-0 1)
                            (valid-1 (setf (aref solution-array 0) (aref solution-array 1))
                                     1)
                            (t 0))))
      (when (<= 1 solutions 2)
        (let ((a.x (+ p0.x (* -2 p1.x) p2.x))
              (b.x (+ (* -2 p0.x) (* 2 p1.x)))
              (c.x p0.x)
              (a.y (+ p0.y (* -2 p1.y) p2.y))
              (b.y (+ (* -2 p0.y) (* 2 p1.y)))
              (c.y p0.y))
          (dotimes (i solutions)
            (let ((x-solution (%bezier-point a.x b.x c.x (aref solution-array i)))
                  (point.y* (%bezier-point a.y b.y c.y (+ (aref solution-array i) 0.01d0))))
              (setf (aref x-coords i) x-solution
                    (aref directions i) (if (> (- point.y point.y*) 0d0) 1 0))))))
      solutions)))

(define-function (find-linear-bezier-x! :default-type dvec2 :return ((mod 3)))
    (x-coords (point.y double-float) p0 p1 (directions (simple-bit-vector 2)))
  (let ((p0.x (aref p0 0))
        (p0.y (aref p0 1))
        (p1.x (aref p1 0))
        (p1.y (aref p1 1)))
    (if (zerop (- p1.y p0.y))
        (if (= point.y (fround p0.y))
            (let* ((min (fround (min p0.x p1.x)))
                   (max (fround (max p0.x p1.x))))
              (setf (aref x-coords 0) min
                    (aref x-coords 1) max
                    (aref directions 0) 0
                    (aref directions 1) 1)
              2)
            0)
        (let ((t* (/ (- point.y p0.y)
                     (- p1.y p0.y))))
          (if (<= 0d0 t* 1d0)
              (let ((x-solution (lerp t* p0.x p1.x))
                    (point.y* (lerp (+ t* 0.01d0) p0.y p1.y)))
                (progn (setf (aref x-coords 0) x-solution
                             (aref directions 0) (if (> (- point.y point.y*) 0d0) 1 0))
                       1))
              0)))))

(defun generate-glyph-coordinates ()
  (let* ((size 1024)
         ;; "ZR" stands for "Zombie Raptor"
         (string "ZR")
         (glyphs (generate-glyphs)))
    (flet ((index (x y) (flat-index 1 size x y)))
      (let ((x-coords (dvec2 0d0 0d0))
            (directions (make-array 2 :element-type 'bit :initial-element 0))
            (p0 (dvec2 0d0 0d0))
            (p1 (dvec2 0d0 0d0))
            (p2 (dvec2 0d0 0d0)))
        (declare (dynamic-extent x-coords directions p0 p1 p2)
                 (dvec2 p0 p1 p2))
        (loop :for char :across string
              :for i :below (length string)
              :for glyph := (aref glyphs (position char "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-/*œæ!.,?"))
              :for linear := (linear-bezier-curves glyph)
              :for quadratic := (quadratic-bezier-curves glyph)
              :with scale := 0.4d0
              :for x-offset := (+ 10d0 (* scale i 1100d0))
              :with y-offset := 20d0
              :with overlay := (make-array (expt size 2)
                                           :element-type 'bit
                                           :initial-element 0)
              ;; Iterate over the rows and find all of the
              ;; intersections of the Bezier curves with each
              ;; row. Store these intersections, sort them, and remove
              ;; the duplicates. Store a second set of intersections
              ;; for when a curve is heading upward between p0 and
              ;; p1. Then draw, from the intersections with upward
              ;; curves, filled lines between those intersections and
              ;; the next encountered intersection. (The situation of
              ;; "right" being inside is simplified to upward because
              ;; this is scanline-based.)
              ;;
              ;; The idea of this elaborate scanline fill algorithm
              ;; instead of a more naive one is to handle the edge
              ;; cases that look like this:
              ;;
              ;;     ↑*****↕*****↓
              ;;
              ;; The simpler algorithms will fail there where the
              ;; middle point is part of both an upward and downward
              ;; facing curve. This, however, will start the fill
              ;; twice, once at each upward facing curve, where the
              ;; first scanline fill will terminate at the middle and
              ;; the second will terminate at the end.
              :do
                 (loop :for row :below size
                       :for y := (+ (double-float* row) 0.5d0)
                       :with intersections := (make-array (* 2 (+ (array-dimension linear 0)
                                                                  (array-dimension quadratic 0)))
                                                          :element-type 'double-float
                                                          :initial-element 0d0)
                       :with upward-intersections := (make-array (* 2 (+ (array-dimension linear 0)
                                                                         (array-dimension quadratic 0)))
                                                                 :element-type 'double-float
                                                                 :initial-element 0d0)
                       :for intersection-count := 0
                       :for upward-intersection-count := 0
                       :do
                          ;; Bezier line intersections. Store all
                          ;; intersections and all up-facing
                          ;; intersections.
                          (loop :for j :below (array-dimension linear 0)
                                :for p0* :of-type dvec2 := (copy-into-point! p0 (aref linear j 0 0) (aref linear j 0 1) scale x-offset y-offset)
                                :for p1* :of-type dvec2 := (copy-into-point! p1 (aref linear j 1 0) (aref linear j 1 1) scale x-offset y-offset)
                                :for solutions :of-type (mod 3) := (find-linear-bezier-x! x-coords y p0* p1* directions)
                                :do (loop :for solution :of-type double-float :across x-coords
                                          :for direction :of-type bit :across directions
                                          :for i :of-type (mod 3) :from 0 :below solutions
                                          :when (<= 0 solution (1- size))
                                            :do
                                               (setf (aref intersections intersection-count) solution)
                                               (incf intersection-count)
                                               (when (zerop direction)
                                                 (setf (aref upward-intersections upward-intersection-count) solution)
                                                 (incf upward-intersection-count))))
                          ;; Quadratic Bezier curve
                          ;; intersections. Store all intersections
                          ;; and all up-facing intersections.
                          (loop :for j :below (array-dimension quadratic 0)
                                :for p0* :of-type dvec2 := (copy-into-point! p0 (aref quadratic j 0 0) (aref quadratic j 0 1) scale x-offset y-offset)
                                :for p1* :of-type dvec2 := (copy-into-point! p1 (aref quadratic j 1 0) (aref quadratic j 1 1) scale x-offset y-offset)
                                :for p2* :of-type dvec2 := (copy-into-point! p2 (aref quadratic j 2 0) (aref quadratic j 2 1) scale x-offset y-offset)
                                :for solutions :of-type (mod 3) := (find-quadratic-bezier-x! x-coords y p0* p1* p2* directions)
                                :do (loop :for solution :of-type double-float :across x-coords
                                          :for direction :of-type bit :across directions
                                          :for i :of-type (mod 3) :from 0 :below solutions
                                          :when (<= 0 solution (1- size))
                                            :do
                                               (setf (aref intersections intersection-count) solution)
                                               (incf intersection-count)
                                               (when (zerop direction)
                                                 (setf (aref upward-intersections upward-intersection-count) solution)
                                                 (incf upward-intersection-count))))
                          ;; Fill row by row using the intersection
                          ;; information determined from the
                          ;; above. The fill starts from the
                          ;; upward-facing intersections.
                          (loop :with intersections := (delete-duplicates (sort (subseq intersections 0 intersection-count) #'<)
                                                                          :test (lambda* ((x double-float) (y double-float))
                                                                                  (< (abs (- x y)) 1d-15)))
                                :with upward-intersections := (delete-duplicates (sort (subseq upward-intersections 0 upward-intersection-count) #'<)
                                                                                 :test (lambda* ((x double-float) (y double-float))
                                                                                         (< (abs (- x y)) 1d-15)))
                                :for start-intersection :of-type double-float :across upward-intersections
                                :do (loop :with stop-intersection := (round (+ 0.5d0 (or (find-if (lambda* ((x double-float))
                                                                                                    (> (- x 1d-24) start-intersection))
                                                                                                  intersections)
                                                                                         (1- size))))
                                          :for column :from (round (+ 0.5d0 start-intersection)) :below stop-intersection
                                          :do (setf (aref overlay (index column row)) 1))))
              :finally (return overlay))))))
