(defpackage #:zombie-raptor/zrvl/scalar
  (:documentation "The scalar Common Lisp backend for ZRVL.")
  (:use #:cl
        #:zombie-raptor/math/vector
        #:zombie-raptor/zrvl/core-types
        #:zombie-raptor/zrvl/spir-v-enums
        #:zr-utils)
  (:export #:*processed-bindings*
           #:lisp-abs
           #:lisp-clamp
           #:lisp-composite-construct
           #:lisp-constant
           #:lisp-constant-composite
           #:lisp-cref
           #:lisp-cross
           #:lisp-degrees
           #:lisp-distance
           #:lisp-dot
           #:lisp-f*
           #:lisp-f+
           #:lisp-f-
           #:lisp-f/
           #:lisp-init-var
           #:lisp-magnitude
           #:lisp-matmul
           #:lisp-negate
           #:lisp-normalize
           #:lisp-radians
           #:lisp-setf
           #:lisp-simple-unary
           #:lisp-simple-binary
           #:lisp-var
           #:lisp-vector*scalar
           #:lisp-vector-shuffle
           #:make-processed-binding
           #:processed-binding-id
           #:processed-binding-type))

(in-package #:zombie-raptor/zrvl/scalar)

(defvar *processed-bindings* (make-hash-table))

(defstruct processed-binding
  (id nil :type symbol)
  (type (%%void) :type (or primitive-type-encoding derivative-type-encoding)))

(defun check-simple-type-match (type-1 type-2)
  (unless (%%type= type-1 type-2)
    ;; TODO: When derivative-types have a printable representation, a
    ;; printable representation can be used here for the types
    (error "Types do not match.")))

(defmacro with-processed-binding ((id-binding type-binding) instance-id &body body)
  `(with-accessors ((,id-binding processed-binding-id)
                    (,type-binding processed-binding-type))
       (gethash ,instance-id *processed-bindings*)
     ,@body))

(defun lisp-init-var (result-id result-type storage-class metadata)
  (unless (or (= storage-class (storage-class-enum :input))
              (= storage-class (storage-class-enum :output)))
    (error "Variable needs to be input or output."))
  ;; A name is required.
  ;;
  ;; An offset or row is optional. If an offset or row is provided,
  ;; then the scalar value or vec is assumed to be part of a larger 1D
  ;; (offset-only) or 2D (row provided) array. If neither is provided,
  ;; then the type must match exactly. Note that an offset of 0 and an
  ;; offset of NIL thus behave differently unless a row is provided.
  ;; No matter what, all of the values are translated into multiple
  ;; scalar values.
  (destructuring-bind (&key name offset row) metadata
    (let* ((case (%%typecase result-type
                   ((%%f 32) :float)
                   ((%%vec (%%f 32) 2) :vec2)
                   ((%%vec (%%f 32) 3) :vec3)
                   ((%%vec (%%f 32) 4) :vec4)))
           ;; Hint the type to the implementation and check if
           ;; necessary.
           (cl-type (case case
                      (:float (cond (row '(simple-array single-float (* *)))
                                    (offset '(simple-array single-float (*)))
                                    (t 'single-float)))
                      (:vec2 (cond (row '(simple-array single-float (* *)))
                                   (offset '(simple-array single-float (*)))
                                   (t 'vec2)))
                      (:vec3 (cond (row '(simple-array single-float (* *)))
                                   (offset '(simple-array single-float (*)))
                                   (t 'vec3)))
                      (:vec4 (cond (row '(simple-array single-float (* *)))
                                   (offset '(simple-array single-float (*)))
                                   (t 'vec4)))
                      (t (error "Unsupported type."))))
           (variable-init `(the ,cl-type ,name))
           ;; A scalar is either directly a scalar value or is an AREF
           ;; of a 1D or 2D array. A vec either is an array of
           ;; matching length and is decomposed into multiple values
           ;; from that or it is a contiguous section of a 1D (offset)
           ;; or 2D (row and optionally offset) array and is
           ;; decomposed into multiple values from one of those.
           (variable-init (case case
                            (:float (if (or row offset)
                                        `(aref
                                          ,variable-init
                                          ,@(if row (list row) nil)
                                          ,(if offset  offset 0))
                                        variable-init))
                            (:vec2 `(,(if row 'array-row-of-2 'array-of-2)
                                     ,variable-init
                                     ,@(if row (list row) nil)
                                     ,@(if offset (list offset) nil)))
                            (:vec3 `(,(if row 'array-row-of-3 'array-of-3)
                                     ,variable-init
                                     ,@(if row (list row) nil)
                                     ,@(if offset (list offset) nil)))
                            (:vec4 `(,(if row 'array-row-of-4 'array-of-4)
                                     ,variable-init
                                     ,@(if row (list row) nil)
                                     ,@(if offset (list offset) nil)))
                            (t variable-init))))
      (values `(,result-id ,variable-init)
              (if (= storage-class (storage-class-enum :input))
                  (case case
                    ((:vec2 :vec3 :vec4) :values)
                    (t :object))
                  :alias)
              (if (= storage-class (storage-class-enum :input))
                  (case case
                    (:vec2 2)
                    (:vec3 3)
                    (:vec4 4)
                    (t nil))
                  nil)))))

(defun lisp-var (result-id result-type var)
  (with-processed-binding (var.id var.type) var
    (check-simple-type-match result-type var.type)
    (values `(,result-id ,var.id) :alias)))

(defun lisp-setf (var object)
  (with-processed-binding (var.id var.type) var
    (with-processed-binding (object.id object.type) object
      (check-simple-type-match var.type object.type)
      (values `(,(gensym (symbol-name '#:void))
                (progn
                  (setf ,var.id ,object.id)
                  nil))
              :void))))

;;; TODO: extend
(defun lisp-cref (result-id result-type composite &rest index-literals)
  (declare (ignore result-type))
  (let ((index (car index-literals)))
    (with-processed-binding (composite.id composite.type) composite
      (let ((simple-type (%%typecase composite.type
                           ((%%vec (%%f 32) 2) :vec2)
                           ((%%vec (%%f 32) 3) :vec3)
                           ((%%vec (%%f 32) 4) :vec4)
                           ((%%matrix (%%f 32) 2 2) :mat2x2)
                           ((%%matrix (%%f 32) 2 3) :mat2x3)
                           ((%%matrix (%%f 32) 2 4) :mat2x4)
                           ((%%matrix (%%f 32) 3 2) :mat3x2)
                           ((%%matrix (%%f 32) 3 3) :mat3x3)
                           ((%%matrix (%%f 32) 3 4) :mat3x4)
                           ((%%matrix (%%f 32) 4 2) :mat4x2)
                           ((%%matrix (%%f 32) 4 3) :mat4x3)
                           ((%%matrix (%%f 32) 4 4) :mat4x4))))
        (unless simple-type
          (error "Type currently not supported."))
        (cond ((member simple-type '(:vec2 :vec3 :vec4))
               (cond ((< index 0)
                      (error "Invalid index."))
                     ((>= index (ecase simple-type
                                  (:vec2 2)
                                  (:vec3 3)
                                  (:vec4 4)))
                      (error "Invalid index.")))
               (values `(,result-id (nth-value ,index ,composite.id))
                       :object))
              ((member simple-type '(:mat2x2 :mat2x3 :mat2x4
                                     :mat3x2 :mat3x3 :mat3x4
                                     :mat4x2 :mat4x3 :mat4x4))
               (cond ((< index 0)
                      (error "Invalid index."))
                     ((>= index (ecase simple-type
                                  ((:mat2x2 :mat3x2 :mat4x2) 2)
                                  ((:mat2x3 :mat3x3 :mat4x3) 3)
                                  ((:mat2x4 :mat3x4 :mat4x4) 4)))
                      (error "Invalid index.")))
               (values `(,result-id
                         (,(ecase simple-type
                             ((:mat2x2 :mat2x3 :mat2x4) 'array-row-of-2)
                             ((:mat3x2 :mat3x3 :mat3x4) 'array-row-of-3)
                             ((:mat4x2 :mat4x3 :mat4x4) 'array-row-of-4))
                          ,composite.id
                          ,index))
                       :values
                       (ecase simple-type
                             ((:mat2x2 :mat2x3 :mat2x4) 2)
                             ((:mat3x2 :mat3x3 :mat3x4) 3)
                             ((:mat4x2 :mat4x3 :mat4x4) 4))))
              (t
               (error "Invalid type.")))))))

(defun lisp-constant (result-id result-type value)
  ;; TODO: handle more types
  (values `(,result-id ,(if (%%type= result-type (%%f 32))
                            (float-features:bits-single-float value)
                            value))
          :object))

(defun lisp-composite-construct (result-id result-type &rest constituents)
  (unless (or (%%typep result-type '%%matrix)
              (%%typep result-type '%%vec))
    (error "Not yet implemented."))
  (cond ((%%typep result-type '%%vec)
         (values `(,result-id (values ,@(mapcar (lambda (x)
                                                  (processed-binding-id (gethash x *processed-bindings*)))
                                                constituents)))
                 :values
                 (length constituents)))
        ((%%typep result-type '%%matrix)
         (let ((cols (%%matrix-cols result-type))
               (rows (%%matrix-rows result-type)))
           ;; TODO: get the matrix element-type from the matrix type
           (values `(,result-id (let ((m (make-array '(,cols ,rows) :element-type 'single-float)))
                                  (psetf ,@(loop :for col :in constituents
                                                 :for i :from 0 :below 4
                                                 :append `((,(ecase= rows
                                                               (2 'array-row-of-2)
                                                               (3 'array-row-of-3)
                                                               (4 'array-row-of-4))
                                                            m
                                                            ,i)
                                                           ,(processed-binding-id (gethash col *processed-bindings*)))))
                                  m))
                   :object)))))

(defun lisp-constant-composite (result-id result-type &rest constituents)
  (apply #'lisp-composite-construct result-id result-type constituents))

(defun lisp-simple-unary (result-id result-type function x)
  (declare (ignore result-type))
  (with-processed-binding (x.id x.type) x
    (%%typecase x.type
      ((%%f 32)
       (values `(,result-id (,function ,x.id))
               :object))
      ((%%vec (%%f 32) 2)
       (values `(,result-id (values (,function (nth-value 0 ,x.id))
                                    (,function (nth-value 1 ,x.id))))
               :values
               2))
      ((%%vec (%%f 32) 3)
       (values `(,result-id (values (,function (nth-value 0 ,x.id))
                                    (,function (nth-value 1 ,x.id))
                                    (,function (nth-value 2 ,x.id))))
               :values
               3))
      ((%%vec (%%f 32) 4)
       (values `(,result-id (values (,function (nth-value 0 ,x.id))
                                    (,function (nth-value 1 ,x.id))
                                    (,function (nth-value 2 ,x.id))
                                    (,function (nth-value 3 ,x.id))))
               :values
               4)))))

(defun lisp-simple-binary (result-id result-type function x y)
  (declare (ignore result-type))
  (with-processed-binding (x.id x.type) x
    (with-processed-binding (y.id y.type) y
      (check-simple-type-match x.type y.type)
      (%%typecase x.type
        ((%%f 32)
         (values `(,result-id (,function ,x.id ,y.id))
                 :object))
        ((%%vec (%%f 32) 2)
         (values `(,result-id (values (,function (nth-value 0 ,x.id) (nth-value 0 ,y.id))
                                      (,function (nth-value 1 ,x.id) (nth-value 1 ,y.id))))
                 :values
                 2))
        ((%%vec (%%f 32) 3)
         (values `(,result-id (values (,function (nth-value 0 ,x.id) (nth-value 0 ,y.id))
                                      (,function (nth-value 1 ,x.id) (nth-value 1 ,y.id))
                                      (,function (nth-value 2 ,x.id) (nth-value 2 ,y.id))))
                 :values
                 3))
        ((%%vec (%%f 32) 4)
         (values `(,result-id (values (,function (nth-value 0 ,x.id) (nth-value 0 ,y.id))
                                      (,function (nth-value 1 ,x.id) (nth-value 1 ,y.id))
                                      (,function (nth-value 2 ,x.id) (nth-value 2 ,y.id))
                                      (,function (nth-value 3 ,x.id) (nth-value 3 ,y.id))))
                 :values
                 4))))))

(defun lisp-abs (result-id result-type x)
  (lisp-simple-unary result-id result-type 'abs x))

(defun lisp-f+ (result-id result-type a b)
  (lisp-simple-binary result-id result-type '+ a b))

(defun lisp-f- (result-id result-type a b)
  (lisp-simple-binary result-id result-type '- a b))

(defun lisp-f* (result-id result-type a b)
  (lisp-simple-binary result-id result-type '* a b))

(defun lisp-f/ (result-id result-type a b)
  (lisp-simple-binary result-id result-type '/ a b))

(defun lisp-negate (result-id result-type x)
  (lisp-simple-unary result-id result-type '- x))

(define-function (%vec2-dot :inline t)
    (u1 u2 v1 v2)
  (+ (* u1 v1)
     (* u2 v2)))

(define-function (%vec3-dot :inline t)
    (u1 u2 u3 v1 v2 v3)
  (+ (* u1 v1)
     (* u2 v2)
     (* u3 v3)))

(define-function (%vec4-dot :inline t)
    (u1 u2 u3 u4 v1 v2 v3 v4)
  (+ (* u1 v1)
     (* u2 v2)
     (* u3 v3)
     (* u4 v4)))

(defun lisp-dot (result-id result-type a b)
  (declare (ignore result-type))
  (with-processed-binding (a.id a.type) a
    (with-processed-binding (b.id b.type) b
      (check-simple-type-match a.type b.type)
      (%%typecase a.type
        ((%%vec (%%f 32) 2)
         (values `(,result-id (multiple-value-call #'%vec2-dot ,a.id ,b.id))
                 :object))
        ((%%vec (%%f 32) 3)
         (values `(,result-id (multiple-value-call #'%vec3-dot ,a.id ,b.id))
                 :object))
        ((%%vec (%%f 32) 4)
         (values `(,result-id (multiple-value-call #'%vec4-dot ,a.id ,b.id))
                 :object))))))

(define-function (transposed-array-row-of-2 :inline t) (array row &optional (offset 0))
  (values (aref array (+ offset 0) row)
          (aref array (+ offset 1) row)))

(define-function (transposed-array-row-of-3 :inline t) (array row &optional (offset 0))
  (values (aref array (+ offset 0) row)
          (aref array (+ offset 1) row)
          (aref array (+ offset 2) row)))

(define-function (transposed-array-row-of-4 :inline t) (array row &optional (offset 0))
  (values (aref array (+ offset 0) row)
          (aref array (+ offset 1) row)
          (aref array (+ offset 2) row)
          (aref array (+ offset 3) row)))

(defun lisp-matmul (result-id result-type a b)
  (let ((cols (%%matrix-cols result-type))
        (rows (%%matrix-rows result-type)))
    (with-processed-binding (a.id a.type) a
      (with-processed-binding (b.id b.type) b
        (let* ((a-cols (%%matrix-cols a.type))
               (a-rows (%%matrix-rows a.type))
               (b-cols (%%matrix-cols b.type))
               (b-rows (%%matrix-rows b.type))
               (dot (ecase= a-cols
                      (2 #'%vec2-dot)
                      (3 #'%vec3-dot)
                      (4 #'%vec4-dot)))
               (a-row-ref (ecase= a-cols
                            (2 'transposed-array-row-of-2)
                            (3 'transposed-array-row-of-3)
                            (4 'transposed-array-row-of-4)))
               (b-col-ref (ecase= b-rows
                            (2 'array-row-of-2)
                            (3 'array-row-of-3)
                            (4 'array-row-of-4))))
          (values `(,result-id
                    (let ((m (make-array '(,cols ,rows) :element-type 'single-float)))
                      (psetf ,@(loop :for row :from 0 :below a-rows
                                     :append (loop :for col :from 0 :below b-cols
                                                   :append `((aref m ,col ,row)
                                                             (multiple-value-call ,dot
                                                               (,a-row-ref ,a.id ,row)
                                                               (,b-col-ref ,b.id ,col))))))
                      m))
                  :object))))))

(define-function (vec2*scalar :inline t)
    (v1 v2 a)
  (values (* v1 a)
          (* v2 a)))

(define-function (vec3*scalar :inline t)
    (v1 v2 v3 a)
  (values (* v1 a)
          (* v2 a)
          (* v3 a)))

(define-function (vec4*scalar :inline t)
    (v1 v2 v3 v4 a)
  (values (* v1 a)
          (* v2 a)
          (* v3 a)
          (* v4 a)))

(defun lisp-vector*scalar (result-id result-type vector scalar)
  (declare (ignore result-type))
  (with-processed-binding (vector.id vector.type) vector
    (with-processed-binding (scalar.id scalar.type) scalar
      (unless (%%type= (%%vec-scalar vector.type) scalar.type)
        (error "Types do not match."))
      (%%typecase vector.type
        ((%%vec (%%f 32) 2)
         (values `(,result-id (multiple-value-call #'vec2*scalar ,vector.id ,scalar.id))
                 :values
                 2))
        ((%%vec (%%f 32) 3)
         (values `(,result-id (multiple-value-call #'vec3*scalar ,vector.id ,scalar.id))
                 :values
                 3))
        ((%%vec (%%f 32) 4)
         (values `(,result-id (multiple-value-call #'vec4*scalar ,vector.id ,scalar.id))
                 :values
                 4))))))

;;; TODO: matrix*scalar

(defun lisp-vector-shuffle (result-id result-type v1 v2 &rest constituents)
  (let ((result-size (%%vec-size result-type)))
    (unless (= (length constituents) result-size)
      (error "Invalid operation."))
    (unless (<= 2 result-size 4)
      (error "Invalid operation."))
    (with-processed-binding (v1.id v1.type) v1
      (with-processed-binding (v2.id v2.type) v2
        (let ((v1-size (%%vec-size v1.type))
              (v2-size (%%vec-size v2.type)))
          (when (or (find-if #'minusp constituents)
                    (>= (reduce #'max constituents)
                        (+ v1-size v2-size)))
            (error "Invalid vector index."))
          (unless (= (%%vec-scalar result-type)
                     (%%vec-scalar v1.type)
                     (%%vec-scalar v2.type))
            (error "Vector type mismatch."))
          (values `(,result-id (values ,@ (loop :for index :in constituents
                                                :for v2? := (>= index v1-size)
                                                :for actual-index := (or (and v2?
                                                                              (- index v1-size))
                                                                         index)
                                                :collect `(nth-value ,actual-index
                                                                     ,(if v2? v2.id v1.id)))))
                  :values
                  result-size))))))

(defun lisp-normalize (result-id result-type x)
  (declare (ignore result-type))
  (with-processed-binding (x.id x.type) x
    (%%typecase x.type
      ((%%f 32)
       ;; Note: Divides by the result of signum so 0 is either an
       ;; error or positive infinity because this is normalize. For
       ;; nonzero, only signum matters, i.e. + or - 1.
       (values `(,result-id (/ (signum ,x.id)))
               :object))
      ((%%vec (%%f 32) 2)
       (values `(,result-id (let ((length (sqrt (+ (expt (nth-value 0 ,x.id) 2)
                                                   (expt (nth-value 1 ,x.id) 2)))))
                              (values (/ (nth-value 0 ,x.id) length)
                                      (/ (nth-value 1 ,x.id) length))))
               :values
               2))
      ((%%vec (%%f 32) 3)
       (values `(,result-id (let ((length (sqrt (+ (expt (nth-value 0 ,x.id) 2)
                                                   (expt (nth-value 1 ,x.id) 2)
                                                   (expt (nth-value 2 ,x.id) 2)))))
                              (values (/ (nth-value 0 ,x.id) length)
                                      (/ (nth-value 1 ,x.id) length)
                                      (/ (nth-value 2 ,x.id) length))))
               :values
               3))
      ((%%vec (%%f 32) 4)
       (values `(,result-id (let ((length (sqrt (+ (expt (nth-value 0 ,x.id) 2)
                                                   (expt (nth-value 1 ,x.id) 2)
                                                   (expt (nth-value 2 ,x.id) 2)
                                                   (expt (nth-value 3 ,x.id) 2)))))
                              (values (/ (nth-value 0 ,x.id) length)
                                      (/ (nth-value 1 ,x.id) length)
                                      (/ (nth-value 2 ,x.id) length)
                                      (/ (nth-value 3 ,x.id) length))))
               :values
               4)))))

(defun lisp-magnitude (result-id result-type x)
  (declare (ignore result-type))
  (with-processed-binding (x.id x.type) x
    (%%typecase x.type
      ((%%f 32)
       (values `(,result-id (abs ,x.id))
               :object))
      ((%%vec (%%f 32) 2)
       (values `(,result-id (sqrt (+ (expt (nth-value 0 ,x.id) 2)
                                     (expt (nth-value 1 ,x.id) 2))))
               :object))
      ((%%vec (%%f 32) 3)
       (values `(,result-id (sqrt (+ (expt (nth-value 0 ,x.id) 2)
                                     (expt (nth-value 1 ,x.id) 2)
                                     (expt (nth-value 2 ,x.id) 2))))
               :object))
      ((%%vec (%%f 32) 4)
       (values `(,result-id (sqrt (+ (expt (nth-value 0 ,x.id) 2)
                                     (expt (nth-value 1 ,x.id) 2)
                                     (expt (nth-value 2 ,x.id) 2)
                                     (expt (nth-value 3 ,x.id) 2))))
               :object)))))

(defun lisp-distance (result-id result-type p1 p2)
  (declare (ignore result-type))
  (with-processed-binding (p1.id p1.type) p1
    (with-processed-binding (p2.id p2.type) p2
      (%%typecase p1.type
        ((%%f 32)
         (values `(,result-id (abs (- ,p1.id ,p2.id)))
                 :object))
        ((%%vec (%%f 32) 2)
         (values `(,result-id (sqrt (+ (expt (- (nth-value 0 ,p1.id)
                                                (nth-value 0 ,p2.id))
                                             2)
                                       (expt (- (nth-value 1 ,p1.id)
                                                (nth-value 1 ,p2.id))
                                             2))))
                 :object))
        ((%%vec (%%f 32) 3)
         (values `(,result-id (sqrt (+ (expt (- (nth-value 0 ,p1.id)
                                                (nth-value 0 ,p2.id))
                                             2)
                                       (expt (- (nth-value 1 ,p1.id)
                                                (nth-value 1 ,p2.id))
                                             2)
                                       (expt (- (nth-value 2 ,p1.id)
                                                (nth-value 2 ,p2.id))
                                             2))))
                 :object))
        ((%%vec (%%f 32) 4)
         (values `(,result-id (sqrt (+ (expt (- (nth-value 0 ,p1.id)
                                                (nth-value 0 ,p2.id))
                                             2)
                                       (expt (- (nth-value 1 ,p1.id)
                                                (nth-value 1 ,p2.id))
                                             2)
                                       (expt (- (nth-value 2 ,p1.id)
                                                (nth-value 2 ,p2.id))
                                             2)
                                       (expt (- (nth-value 3 ,p1.id)
                                                (nth-value 3 ,p2.id))
                                             2))))
                 :object))))))

(defun lisp-cross (result-id result-type x y)
  (with-processed-binding (x.id x.type) x
    (with-processed-binding (y.id y.type) y
      ;; TODO: support other floating point types as well
      (unless (%%type= x.type (%%vec (%%f 32) 3))
        (error "Invalid type for cross product."))
      (check-simple-type-match x.type y.type)
      (check-simple-type-match x.type result-type)
      (values `(,result-id (values (- (* (nth-value 1 ,x.id) (nth-value 2 ,y.id))
                                      (* (nth-value 2 ,x.id) (nth-value 1 ,y.id)))
                                   (- (* (nth-value 2 ,x.id) (nth-value 0 ,y.id))
                                      (* (nth-value 0 ,x.id) (nth-value 2 ,y.id)))
                                   (- (* (nth-value 0 ,x.id) (nth-value 1 ,y.id))
                                      (* (nth-value 1 ,x.id) (nth-value 0 ,y.id)))))
              :values
              3))))

(defun lisp-clamp (result-id result-type x min max)
  (with-processed-binding (x.id x.type) x
    (with-processed-binding (min.id min.type) min
      (with-processed-binding (max.id max.type) max
        (check-simple-type-match x.type min.type)
        (check-simple-type-match x.type max.type)
        (check-simple-type-match x.type result-type)
        (%%typecase x.type
          ((%%f 32)
           (values `(,result-id (max ,min.id (min ,x.id ,max.id)))
                   :object))
          ((%%vec (%%f 32) 2)
           (values `(,result-id (values (max (nth-value 0 ,min.id)
                                             (min (nth-value 0 ,x.id)
                                                  (nth-value 0 ,max.id)))
                                        (max (nth-value 1 ,min.id)
                                             (min (nth-value 1 ,x.id)
                                                  (nth-value 1 ,max.id)))))
                   :values
                   2))
          ((%%vec (%%f 32) 3)
           (values `(,result-id (values (max (nth-value 0 ,min.id)
                                             (min (nth-value 0 ,x.id)
                                                  (nth-value 0 ,max.id)))
                                        (max (nth-value 1 ,min.id)
                                             (min (nth-value 1 ,x.id)
                                                  (nth-value 1 ,max.id)))
                                        (max (nth-value 2 ,min.id)
                                             (min (nth-value 2 ,x.id)
                                                  (nth-value 2 ,max.id)))))
                   :values
                   3))
          ((%%vec (%%f 32) 4)
           (values `(,result-id (values (max (nth-value 0 ,min.id)
                                             (min (nth-value 0 ,x.id)
                                                  (nth-value 0 ,max.id)))
                                        (max (nth-value 1 ,min.id)
                                             (min (nth-value 1 ,x.id)
                                                  (nth-value 1 ,max.id)))
                                        (max (nth-value 2 ,min.id)
                                             (min (nth-value 2 ,x.id)
                                                  (nth-value 2 ,max.id)))
                                        (max (nth-value 3 ,min.id)
                                             (min (nth-value 3 ,x.id)
                                                  (nth-value 3 ,max.id)))))
                   :values
                   4)))))))

(defun lisp-degrees (result-id result-type radians)
  (with-processed-binding (radians.id radians.type) radians
    (check-simple-type-match result-type radians.type)
    (%%typecase radians.type
        ((%%f 32)
         (values `(,result-id (* ,radians.id (float* (/ 180 pi))))
                 :object))
        ((%%vec (%%f 32) 2)
         (values `(,result-id (values (* (nth-value 0 ,radians.id) (float* (/ 180 pi)))
                                      (* (nth-value 1 ,radians.id) (float* (/ 180 pi)))))
                 :values
                 2))
        ((%%vec (%%f 32) 3)
         (values `(,result-id (values (* (nth-value 0 ,radians.id) (float* (/ 180 pi)))
                                      (* (nth-value 1 ,radians.id) (float* (/ 180 pi)))
                                      (* (nth-value 2 ,radians.id) (float* (/ 180 pi)))))
                 :values
                 3))
        ((%%vec (%%f 32) 4)
         (values `(,result-id (values (* (nth-value 0 ,radians.id) (float* (/ 180 pi)))
                                      (* (nth-value 1 ,radians.id) (float* (/ 180 pi)))
                                      (* (nth-value 2 ,radians.id) (float* (/ 180 pi)))
                                      (* (nth-value 3 ,radians.id) (float* (/ 180 pi)))))
                 :values
                 4)))))

(defun lisp-radians (result-id result-type degrees)
  (with-processed-binding (degrees.id degrees.type) degrees
    (check-simple-type-match result-type degrees.type)
    (%%typecase degrees.type
      ((%%f 32)
       (values `(,result-id (* ,degrees.id (float* (/ pi 180))))
               :object))
      ((%%vec (%%f 32) 2)
       (values `(,result-id (values (* (nth-value 0 ,degrees.id) (float* (/ pi 180)))
                                    (* (nth-value 1 ,degrees.id) (float* (/ pi 180)))))
               :values
               2))
      ((%%vec (%%f 32) 3)
       (values `(,result-id (values (* (nth-value 0 ,degrees.id) (float* (/ pi 180)))
                                    (* (nth-value 1 ,degrees.id) (float* (/ pi 180)))
                                    (* (nth-value 2 ,degrees.id) (float* (/ pi 180)))))
               :values
               3))
      ((%%vec (%%f 32) 4)
       (values `(,result-id (values (* (nth-value 0 ,degrees.id) (float* (/ pi 180)))
                                    (* (nth-value 1 ,degrees.id) (float* (/ pi 180)))
                                    (* (nth-value 2 ,degrees.id) (float* (/ pi 180)))
                                    (* (nth-value 3 ,degrees.id) (float* (/ pi 180)))))
               :values
               4)))))
