(defpackage #:zombie-raptor/zrvl/zrvl
  (:documentation "The core of the language ZRVL.")
  (:import-from #:trivial-with-current-source-form
                #:with-current-source-form)
  (:use #:cl
        #:zombie-raptor/data/shader-core
        #:zombie-raptor/zrvl/a-normal
        #:zombie-raptor/zrvl/core-types
        #:zombie-raptor/zrvl/instructions
        #:zombie-raptor/zrvl/spir-v-enums
        #:zombie-raptor/zrvl/spir-v-instructions
        #:zr-utils)
  (:export #:define-zrvl-struct
           #:with-zrvl
           #:with-zrvl*
           #:with-zrvl/mv
           #:with-zrvl/mv*))

(in-package #:zombie-raptor/zrvl/zrvl)

(defpackage #:zrvl
  (:documentation "The Zombie Raptor Vector Language (ZRVL), is designed to run on
either GPU shading/compute or on the CPU.")
  (:use #:zombie-raptor/zrvl/instructions)
  (:import-from #:cl
                #:defpackage
                #:function
                #:in-package
                #:nil
                #:progn
                #:quote)
  (:import-from #:zrvlir
                #:false
                #:true)
  (:import-from #:zombie-raptor/zrvl/a-normal
                #:%declare-parameter
                #:%initialize-variable
                #:%literal
                #:%scope
                #:%setf)
  (:export #:defpackage
           #:function
           #:in-package
           #:nil
           #:progn
           #:quote
           ;; Unique (not rexported) symbols
           ;; Types and constructors/conversions (non-matrix)
           #:bvec
           #:bvec2
           #:bvec3
           #:bvec4
           #:dvec
           #:dvec2
           #:dvec3
           #:dvec4
           #:ivec
           #:ivec2
           #:ivec3
           #:ivec4
           #:quat
           #:quat<-vec3
           #:quat<-vec4
           #:uvec
           #:uvec2
           #:uvec3
           #:uvec4
           #:vec
           #:vec2
           #:vec2<-
           #:vec2<-3
           #:vec2<-4
           #:vec3
           #:vec3<-
           #:vec3<-2
           #:vec3<-4
           #:vec3<-quat
           #:vec4
           #:vec4<-
           #:vec4<-2
           #:vec4<-2s
           #:vec4<-3
           #:vec4<-quat
           ;; Types and constructors (matrix)
           #:dmat2
           #:dmat2x2
           #:dmat2x3
           #:dmat2x4
           #:dmat3
           #:dmat3x2
           #:dmat3x3
           #:dmat3x4
           #:dmat4
           #:dmat4x2
           #:dmat4x3
           #:dmat4x4
           #:mat2
           #:mat2x2
           #:mat2x3
           #:mat2x4
           #:mat3
           #:mat3x2
           #:mat3x3
           #:mat3x4
           #:mat4
           #:mat4x2
           #:mat4x3
           #:mat4x4
           #:dmat2<-
           #:dmat2x2<-
           #:dmat2x3<-
           #:dmat2x4<-
           #:dmat3<-
           #:dmat3x2<-
           #:dmat3x3<-
           #:dmat3x4<-
           #:dmat4<-
           #:dmat4x2<-
           #:dmat4x3<-
           #:dmat4x4<-
           #:mat2<-
           #:mat2x2<-
           #:mat2x3<-
           #:mat2x4<-
           #:mat3<-
           #:mat3x2<-
           #:mat3x3<-
           #:mat3x4<-
           #:mat4<-
           #:mat4x2<-
           #:mat4x3<-
           #:mat4x4<-
           ;; Core language
           #:aref
           #:cond
           #:define
           #:define-shader
           #:define-shader-data
           #:do-for
           #:if
           #:let
           #:let*
           #:swizzle
           #:unless
           #:when
           ;; Basic numbers
           #:*
           #:*+
           #:+
           #:-
           #:/
           #:/=
           #:<
           #:<=
           #:=
           #:>
           #:>=
           #:1+
           #:exp
           #:expt
           #:float-infinity-p
           #:float-nan-p
           #:inverse-sqrt
           #:isqrt
           #:log
           #:mod
           #:sqrt
           ;; Complex and quaternions
           #:conjugate
           ;; Only quaternions
           #:quat<-euler
           #:quat<-pitch
           #:quat<-roll
           #:quat<-yaw
           #:rotate
           #:rotation
           ;; Element-wise basic numbers
           #:.*
           #:./
           #:.exp
           #:.expt
           #:.float-infinity-p
           #:.float-nan-p
           #:.log
           ;; Boolean logic
           #:.and
           #:.not
           #:.or
           #:.xor
           #:and
           #:false
           #:not
           #:or
           #:true
           #:xor
           ;; Sign
           #:abs
           #:minusp
           #:plusp
           #:signum
           #:zerop
           ;; Trigonometry
           #:acos
           #:acosh
           #:asin
           #:asinh
           #:atan
           #:atanh
           #:cos
           #:cosh
           #:degrees
           #:radians
           #:sin
           #:sinh
           #:tan
           #:tanh
           ;; Linear algebra
           #:cross
           #:det
           #:diagonal
           #:distance
           #:dot
           #:eig
           #:eigvals
           #:face-forward
           #:inverse
           #:magnitude
           #:normalize
           #:outer
           #:reflect
           #:refract
           #:trace
           #:transpose
           #:wedge
           ;; Bits
           #:ash
           #:bits-double-float
           #:bits-short-float
           #:bits-single-float
           #:double-float-bits
           #:short-float-bits
           #:single-float-bits
           ;; Rounding, clamping, etc.
           #:ceiling
           #:clamp
           #:fceiling
           #:ffloor
           #:floor
           #:fround
           #:ftruncate
           #:max
           #:min
           #:n-max
           #:n-min
           #:round
           #:sat
           #:step
           #:truncate
           ;; Sequence functions
           #:count
           #:fill
           #:length
           #:map
           #:reduce
           #:subseq
           ;; Useful numerical algorithms/formulas
           #:euler
           #:lerp
           #:mix
           #:rk4
           #:si-euler
           #:slerp
           #:slerp*
           #:smooth-step
           ;; Shader-specific
           #:texture
           ;; Trivial functions
           #:identity
           #:one
           #:zero
           ;; Type names not already a symbol
           #:f16
           #:f32
           #:f64
           #:half
           #:half-float
           #:short-float
           #:single
           #:single-float
           #:double
           #:double-float
           #:bool
           #:boolean
           #:uint8
           #:uint16
           #:uint32
           #:uint64
           #:int8
           #:int16
           #:int32
           #:int64
           #:u8
           #:u16
           #:u32
           #:u64
           #:i8
           #:i16
           #:i32
           #:i64
           #:f32.2
           #:f32.3
           #:f32.4
           #:f64.2
           #:f64.3
           #:f64.4
           #:f32.2x2
           #:f32.2x3
           #:f32.2x4
           #:f32.3x2
           #:f32.3x3
           #:f32.3x4
           #:f32.4x2
           #:f32.4x3
           #:f32.4x4
           #:f64.2x2
           #:f64.2x3
           #:f64.2x4
           #:f64.3x2
           #:f64.3x3
           #:f64.3x4
           #:f64.4x2
           #:f64.4x3
           #:f64.4x4
           #:sampler-2D
           #:sampler-2D-array
           #:void
           ;; Restricted to special shader contexts
           #:gl-per-vertex
           #:gl-position))

(defpackage #:zrvl-int
  (:use #:zombie-raptor/zrvl/instructions)
  (:import-from #:cl
                #:defpackage
                #:function
                #:in-package
                #:nil
                #:progn
                #:quote)
  (:import-from #:zombie-raptor/zrvl/a-normal
                #:%declare-parameter
                #:%initialize-variable
                #:%literal
                #:%scope
                #:%setf)
  (:export #:+
           #:*
           #:-
           #:/
           #:.*
           #:./
           #:=
           #:/=
           #:<
           #:>
           #:<=
           #:>=
           #:aref
           #:aref-0D
           #:atan2
           #:cross
           #:define-alias
           #:define-binary-elementwise-matrix-primitives
           #:define-unary-elementwise-matrix-primitives
           #:define-generic
           #:define-primitive
           #:define-primitives
           #:define-simple-binary-predicates
           #:define-simple-binary-primitives
           #:define-simple-ternary-predicates
           #:define-simple-ternary-primitives
           #:define-simple-unary-predicates
           #:define-simple-unary-primitives
           #:expt2
           #:ln
           #:log2
           #:max
           #:min
           #:n-max
           #:n-min
           #:negate
           #:reciprocal
           #:select
           #:seq-or-mat-reciprocal
           #:swizzle2
           #:swizzle3
           #:swizzle4
           #:vec-identity
           #:vec2
           #:vec2-identity
           #:vec2<-scalar
           #:vec2<-vec
           #:vec3
           #:vec3-identity
           #:vec3<-2
           #:vec3<-2*
           #:vec3<-2-expand
           #:vec3<-scalar
           #:vec3<-vec
           #:vec4
           #:vec4-identity
           #:vec4<-2
           #:vec4<-2*
           #:vec4<-2**
           #:vec4<-2-expand
           #:vec4<-3
           #:vec4<-3*
           #:vec4<-3-expand
           #:vec4<-ab
           #:vec4<-scalar
           #:access-chain
           #:setf
           #:with-defines
           #:matrix*scalar
           #:scalar*matrix
           #:vector*scalar
           #:scalar*vector
           #:matrix*vector
           #:vector*matrix
           #:matrix*matrix
           #:with-function
           #:function-begin
           #:function-end
           #:label
           #:return))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun generate-symbol-list (prefixes symbols)
    (loop :for prefix :in prefixes
          :append (loop :for symbol :in symbols
                        :collect (make-symbol (concatenate 'string
                                                           (symbol-name prefix)
                                                           (symbol-name symbol))))))
  (defun generate-symbol-list* (prefixes-and-symbols)
    (loop :for sublist :in prefixes-and-symbols
          :append (destructuring-bind (prefixes symbols) sublist
                    (generate-symbol-list prefixes symbols)))))

(defmacro define-monomorphic-package (name docstring &body prefixes-and-symbols)
  `(defpackage ,name
     (:documentation ,docstring)
     (:use)
     (:import-from #:cl
                   #:defpackage
                   #:function
                   #:in-package
                   #:nil
                   #:progn
                   #:quote)
     (:import-from #:zrvlir
                   #:false
                   #:true)
     (:export #:defpackage
              #:function
              #:in-package
              #:nil
              #:progn
              #:quote
              ,@(generate-symbol-list* prefixes-and-symbols))))

(define-monomorphic-package #:zrvl-mono
    "Monomorphic low-level functions."
  ;; Scalar or element-wise
  ((#:f32
    #:f32.2
    #:f32.3
    #:f32.4)
   (#:=
    #:/=
    #:<
    #:>
    #:<=
    #:>=
    #:+
    #:-
    #:-1+
    #:-negate
    #:-recip
    #:-abs
    #:-clamp
    #:-max
    #:-min
    #:-sat))
  ;; Matrix element-wise (temporary; todo: combine)
  ((#:f32.2x2
    #:f32.2x3
    #:f32.2x4
    #:f32.3x2
    #:f32.3x3
    #:f32.3x4
    #:f32.4x2
    #:f32.4x3
    #:f32.4x4)
   (#:+
    #:-
    #:-1+
    #:-negate))
  ;; Scalar-only
  ((#:f32)
   (#:*
    #:/
    #:-sqrt))
  ;; Vector-only
  ((#:f32.2
    #:f32.3
    #:f32.4
    #:quat)
   (#:-dot
    #:-vector*scalar
    #:-scalar*vector
    #:-swizzle2
    #:-swizzle3
    #:-swizzle4))
  ;; Element-wise
  ((#:f32.2
    #:f32.3
    #:f32.4
    #:f32.2x2
    #:f32.2x3
    #:f32.2x4
    #:f32.3x2
    #:f32.3x3
    #:f32.3x4
    #:f32.4x2
    #:f32.4x3
    #:f32.4x4)
   (#:.*
    #:./))
  ;; Only 16-bit and 32-bit floats in GLSL-flavored SPIR-V? Why? Any
  ;; workarounds? Maybe CPU-only 64-bit variation?
  ((#:f32
    #:f32.2
    #:f32.3
    #:f32.4)
   (#:-degrees
    #:-radians
    #:-sin
    #:-cos
    #:-tan
    #:-asin
    #:-acos
    #:-atan
    #:-atan2
    #:-sinh
    #:-cosh
    #:-tanh
    #:-asinh
    #:-acosh
    #:-atanh
    #:-ln
    #:-log2
    #:-exp
    #:-expt
    #:-expt2))
  ;; Only floating-point
  ((#:f32
    #:f32.2
    #:f32.3
    #:f32.4)
   (#:*+
    #:-distance
    #:-magnitude
    #:-normalize
    #:-reflect
    #:-round
    #:-fround
    #:-truncate
    #:-ftruncate
    #:-ceiling
    #:-fceiling
    #:-floor
    #:-ffloor
    #:-lerp))
  ;; Only floating-point vecs of length 3
  ((#:f32.3)
   (#:-cross))
  ;; Only matrices
  ((#:f32.2x2
    #:f32.2x3
    #:f32.2x4
    #:f32.3x2
    #:f32.3x3
    #:f32.3x4
    #:f32.4x2
    #:f32.4x3
    #:f32.4x4)
   (#:-matrix*scalar
    #:-scalar*matrix
    #:-transpose))
  ;; Only square matrices
  ((#:f32
    #:f32.2x2
    #:f32.3x3
    #:f32.4x4
    ;; #:f64
    ;; #:f64.2x2
    ;; #:f64.3x3
    ;; #:f64.4x4
    )
   (#:-determinant
    #:-inverse))
  ;; Matrix*vector
  ((#:f32.2x2*2
    #:f32.2x3*3
    #:f32.2x4*4
    #:f32.3x2*2
    #:f32.3x3*3
    #:f32.3x4*4
    #:f32.4x2*2
    #:f32.4x3*3
    #:f32.4x4*4)
   (#:-matrix*vector))
  ;; Vector*matrix
  ((#:f32.2*2x2
    #:f32.2*2x3
    #:f32.2*2x4
    #:f32.3*3x2
    #:f32.3*3x3
    #:f32.3*3x4
    #:f32.4*4x2
    #:f32.4*4x3
    #:f32.4*4x4)
   (#:-vector*matrix))
  ;; Matrix*matrix
  ((#:f32.2x2*2x2
    #:f32.2x2*2x3
    #:f32.2x2*2x4
    #:f32.2x3*3x2
    #:f32.2x3*3x3
    #:f32.2x3*3x4
    #:f32.2x4*4x2
    #:f32.2x4*4x3
    #:f32.2x4*4x4
    #:f32.3x2*2x2
    #:f32.3x2*2x3
    #:f32.3x2*2x4
    #:f32.3x3*3x2
    #:f32.3x3*3x3
    #:f32.3x3*3x4
    #:f32.3x4*4x2
    #:f32.3x4*4x3
    #:f32.3x4*4x4
    #:f32.4x2*2x2
    #:f32.4x2*2x3
    #:f32.4x2*2x4
    #:f32.4x3*3x2
    #:f32.4x3*3x3
    #:f32.4x3*3x4
    #:f32.4x4*4x2
    #:f32.4x4*4x3
    #:f32.4x4*4x4)
   (#:-matrix*matrix))
  ;; Matrices from diagonals
  ((#:f32.2x2<-f32
    #:f32.2x3<-f32
    #:f32.2x4<-f32
    #:f32.3x2<-f32
    #:f32.3x3<-f32
    #:f32.3x4<-f32
    #:f32.4x2<-f32
    #:f32.4x3<-f32
    #:f32.4x4<-f32
    #:f32.2x2<-f32.2
    #:f32.2x3<-f32.2
    #:f32.2x4<-f32.2
    #:f32.3x2<-f32.2
    #:f32.3x3<-f32.3
    #:f32.3x4<-f32.3
    #:f32.4x2<-f32.2
    #:f32.4x3<-f32.3
    #:f32.4x4<-f32.4)
   (#:-diag))
  ;; Matrix conversion
  ((#:f32.2x2<-2x2
    #:f32.2x2<-2x3
    #:f32.2x2<-2x4
    #:f32.2x2<-3x2
    #:f32.2x2<-3x3
    #:f32.2x2<-3x4
    #:f32.2x2<-4x2
    #:f32.2x2<-4x3
    #:f32.2x2<-4x4
    #:f32.2x3<-2x2
    #:f32.2x3<-2x3
    #:f32.2x3<-2x4
    #:f32.2x3<-3x2
    #:f32.2x3<-3x3
    #:f32.2x3<-3x4
    #:f32.2x3<-4x2
    #:f32.2x3<-4x3
    #:f32.2x3<-4x4
    #:f32.2x4<-2x2
    #:f32.2x4<-2x3
    #:f32.2x4<-2x4
    #:f32.2x4<-3x2
    #:f32.2x4<-3x3
    #:f32.2x4<-3x4
    #:f32.2x4<-4x2
    #:f32.2x4<-4x3
    #:f32.2x4<-4x4
    #:f32.3x2<-2x2
    #:f32.3x2<-2x3
    #:f32.3x2<-2x4
    #:f32.3x2<-3x2
    #:f32.3x2<-3x3
    #:f32.3x2<-3x4
    #:f32.3x2<-4x2
    #:f32.3x2<-4x3
    #:f32.3x2<-4x4
    #:f32.3x3<-2x2
    #:f32.3x3<-2x3
    #:f32.3x3<-2x4
    #:f32.3x3<-3x2
    #:f32.3x3<-3x3
    #:f32.3x3<-3x4
    #:f32.3x3<-4x2
    #:f32.3x3<-4x3
    #:f32.3x3<-4x4
    #:f32.3x4<-2x2
    #:f32.3x4<-2x3
    #:f32.3x4<-2x4
    #:f32.3x4<-3x2
    #:f32.3x4<-3x3
    #:f32.3x4<-3x4
    #:f32.3x4<-4x2
    #:f32.3x4<-4x3
    #:f32.3x4<-4x4
    #:f32.4x2<-2x2
    #:f32.4x2<-2x3
    #:f32.4x2<-2x4
    #:f32.4x2<-3x2
    #:f32.4x2<-3x3
    #:f32.4x2<-3x4
    #:f32.4x2<-4x2
    #:f32.4x2<-4x3
    #:f32.4x2<-4x4
    #:f32.4x3<-2x2
    #:f32.4x3<-2x3
    #:f32.4x3<-2x4
    #:f32.4x3<-3x2
    #:f32.4x3<-3x3
    #:f32.4x3<-3x4
    #:f32.4x3<-4x2
    #:f32.4x3<-4x3
    #:f32.4x3<-4x4
    #:f32.4x4<-2x2
    #:f32.4x4<-2x3
    #:f32.4x4<-2x4
    #:f32.4x4<-3x2
    #:f32.4x4<-3x3
    #:f32.4x4<-3x4
    #:f32.4x4<-4x2
    #:f32.4x4<-4x3
    #:f32.4x4<-4x4)
   (#:-conv))
  ;; Only composite
  ((#:f32.2
    #:f32.3
    #:f32.4
    #:f32.2x2
    #:f32.2x3
    #:f32.2x4
    #:f32.3x2
    #:f32.3x3
    #:f32.3x4
    #:f32.4x2
    #:f32.4x3
    #:f32.4x4
    #:quat)
   (#:-ref))
  ;; Quaternions
  ((#:quat)
   (#:*
    #:+
    #:-
    #:-1+
    #:-conjugate
    #:-magnitude
    #:-negate
    #:-normalize))
  ;; Textures
  ((#:sampler-2D
    #:sampler-2D-array)
   (#:-texture)))

;;; Type definitions

(progn
  (define-zrvl-type zrvl:f16 () (%%f 16))
  (define-zrvl-type zrvl:f32 () (%%f 32))
  (define-zrvl-type zrvl:f64 () (%%f 64))
  (define-zrvl-type zrvl:half () (%%f 16))
  (define-zrvl-type zrvl:half-float () (%%f 16))
  (define-zrvl-type zrvl:short-float () (%%f 16))
  (define-zrvl-type zrvl:single () (%%f 32))
  (define-zrvl-type zrvl:single-float () (%%f 32))
  (define-zrvl-type zrvl:double () (%%f 64))
  (define-zrvl-type zrvl:double-float () (%%f 64))
  (define-zrvl-type zrvl:bool () (%%boolean))
  (define-zrvl-type zrvl:boolean () (%%boolean))
  (define-zrvl-type zrvl:uint8 () (%%u 8))
  (define-zrvl-type zrvl:uint16 () (%%u 16))
  (define-zrvl-type zrvl:uint32 () (%%u 32))
  (define-zrvl-type zrvl:uint64 () (%%u 64))
  (define-zrvl-type zrvl:int8 () (%%i 8))
  (define-zrvl-type zrvl:int16 () (%%i 16))
  (define-zrvl-type zrvl:int32 () (%%i 32))
  (define-zrvl-type zrvl:int64 () (%%i 64))
  (define-zrvl-type zrvl:u8 () (%%u 8))
  (define-zrvl-type zrvl:u16 () (%%u 16))
  (define-zrvl-type zrvl:u32 () (%%u 32))
  (define-zrvl-type zrvl:u64 () (%%u 64))
  (define-zrvl-type zrvl:i8 () (%%i 8))
  (define-zrvl-type zrvl:i16 () (%%i 16))
  (define-zrvl-type zrvl:i32 () (%%i 32))
  (define-zrvl-type zrvl:i64 () (%%i 64))
  (define-zrvl-type zrvl:vec2 () (%%vec (%%f 32) 2))
  (define-zrvl-type zrvl:vec3 () (%%vec (%%f 32) 3))
  (define-zrvl-type zrvl:vec4 () (%%vec (%%f 32) 4))
  (define-zrvl-type zrvl:f32.2 () (%%vec (%%f 32) 2))
  (define-zrvl-type zrvl:f32.3 () (%%vec (%%f 32) 3))
  (define-zrvl-type zrvl:f32.4 () (%%vec (%%f 32) 4))
  (define-zrvl-type zrvl:dvec2 () (%%vec (%%f 64) 2))
  (define-zrvl-type zrvl:dvec3 () (%%vec (%%f 64) 3))
  (define-zrvl-type zrvl:dvec4 () (%%vec (%%f 64) 4))
  (define-zrvl-type zrvl:f64.2 () (%%vec (%%f 64) 2))
  (define-zrvl-type zrvl:f64.3 () (%%vec (%%f 64) 3))
  (define-zrvl-type zrvl:f64.4 () (%%vec (%%f 64) 4))
  (define-zrvl-type zrvl:ivec2 () (%%vec (%%i 32) 2))
  (define-zrvl-type zrvl:ivec3 () (%%vec (%%i 32) 3))
  (define-zrvl-type zrvl:ivec4 () (%%vec (%%i 32) 4))
  (define-zrvl-type zrvl:uvec2 () (%%vec (%%u 32) 2))
  (define-zrvl-type zrvl:uvec3 () (%%vec (%%u 32) 3))
  (define-zrvl-type zrvl:uvec4 () (%%vec (%%u 32) 4))
  (define-zrvl-type zrvl:bvec2 () (%%vec (%%boolean) 2))
  (define-zrvl-type zrvl:bvec3 () (%%vec (%%boolean) 3))
  (define-zrvl-type zrvl:bvec4 () (%%vec (%%boolean) 4))
  (define-zrvl-type zrvl:mat2 () (%%matrix (%%f 32) 2 2))
  (define-zrvl-type zrvl:mat3 () (%%matrix (%%f 32) 3 3))
  (define-zrvl-type zrvl:mat4 () (%%matrix (%%f 32) 4 4))
  (define-zrvl-type zrvl:mat2x2 () (%%matrix (%%f 32) 2 2))
  (define-zrvl-type zrvl:mat2x3 () (%%matrix (%%f 32) 2 3))
  (define-zrvl-type zrvl:mat2x4 () (%%matrix (%%f 32) 2 4))
  (define-zrvl-type zrvl:mat3x2 () (%%matrix (%%f 32) 3 2))
  (define-zrvl-type zrvl:mat3x3 () (%%matrix (%%f 32) 3 3))
  (define-zrvl-type zrvl:mat3x4 () (%%matrix (%%f 32) 3 4))
  (define-zrvl-type zrvl:mat4x2 () (%%matrix (%%f 32) 4 2))
  (define-zrvl-type zrvl:mat4x3 () (%%matrix (%%f 32) 4 3))
  (define-zrvl-type zrvl:mat4x4 () (%%matrix (%%f 32) 4 4))
  (define-zrvl-type zrvl:f32.2x2 () (%%matrix (%%f 32) 2 2))
  (define-zrvl-type zrvl:f32.2x3 () (%%matrix (%%f 32) 2 3))
  (define-zrvl-type zrvl:f32.2x4 () (%%matrix (%%f 32) 2 4))
  (define-zrvl-type zrvl:f32.3x2 () (%%matrix (%%f 32) 3 2))
  (define-zrvl-type zrvl:f32.3x3 () (%%matrix (%%f 32) 3 3))
  (define-zrvl-type zrvl:f32.3x4 () (%%matrix (%%f 32) 3 4))
  (define-zrvl-type zrvl:f32.4x2 () (%%matrix (%%f 32) 4 2))
  (define-zrvl-type zrvl:f32.4x3 () (%%matrix (%%f 32) 4 3))
  (define-zrvl-type zrvl:f32.4x4 () (%%matrix (%%f 32) 4 4))
  (define-zrvl-type zrvl:dmat2 () (%%matrix (%%f 64) 2 2))
  (define-zrvl-type zrvl:dmat3 () (%%matrix (%%f 64) 3 3))
  (define-zrvl-type zrvl:dmat4 () (%%matrix (%%f 64) 4 4))
  (define-zrvl-type zrvl:dmat2x2 () (%%matrix (%%f 64) 2 2))
  (define-zrvl-type zrvl:dmat2x3 () (%%matrix (%%f 64) 2 3))
  (define-zrvl-type zrvl:dmat2x4 () (%%matrix (%%f 64) 2 4))
  (define-zrvl-type zrvl:dmat3x2 () (%%matrix (%%f 64) 3 2))
  (define-zrvl-type zrvl:dmat3x3 () (%%matrix (%%f 64) 3 3))
  (define-zrvl-type zrvl:dmat3x4 () (%%matrix (%%f 64) 3 4))
  (define-zrvl-type zrvl:dmat4x2 () (%%matrix (%%f 64) 4 2))
  (define-zrvl-type zrvl:dmat4x3 () (%%matrix (%%f 64) 4 3))
  (define-zrvl-type zrvl:dmat4x4 () (%%matrix (%%f 64) 4 4))
  (define-zrvl-type zrvl:f64.2x2 () (%%matrix (%%f 64) 2 2))
  (define-zrvl-type zrvl:f64.2x3 () (%%matrix (%%f 64) 2 3))
  (define-zrvl-type zrvl:f64.2x4 () (%%matrix (%%f 64) 2 4))
  (define-zrvl-type zrvl:f64.3x2 () (%%matrix (%%f 64) 3 2))
  (define-zrvl-type zrvl:f64.3x3 () (%%matrix (%%f 64) 3 3))
  (define-zrvl-type zrvl:f64.3x4 () (%%matrix (%%f 64) 3 4))
  (define-zrvl-type zrvl:f64.4x2 () (%%matrix (%%f 64) 4 2))
  (define-zrvl-type zrvl:f64.4x3 () (%%matrix (%%f 64) 4 3))
  (define-zrvl-type zrvl:f64.4x4 () (%%matrix (%%f 64) 4 4))
  (define-zrvl-type zrvl:void () (%%void)))

(define-zrvl-type zrvl:sampler-2D ()
  (%%spir-v-sampled-image (%%spir-v-image (%%f 32) 1 0 0 0 1 0)))

(define-zrvl-type zrvl:sampler-2D-array ()
  (%%spir-v-sampled-image (%%spir-v-image (%%f 32) 1 0 1 0 1 0)))

;; TODO: don't special case quaternions... add a new macro called
;; define-disjoint-zrvl-type that tracks with a hidden counter every
;; time a type has a disjoint type made from it, starting with 1
(define-zrvl-type zrvl:quat ()
  (%%disjoint-type 1 (%%vec (%%f 32) 4)))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun %with-zrvl (inputs outputs values? body)
    (flet ((variables-to-parameters (variables storage-class)
             (loop :for variable :in variables
                   :collect (destructuring-bind (name type &key offset row) variable
                              `(%declare-parameter ,name
                                                   ,(process-zrvl-type type)
                                                   ,storage-class
                                                   :offset ,offset
                                                   :row ,row)))))
      (let* ((*id-counter* 1)
             (io (append (and inputs (variables-to-parameters inputs :input))
                         (and outputs (variables-to-parameters outputs :output))))
             (body* (if io
                        `(,@io ,@body)
                        body)))
        (scalar-forms (full-ir-compile body*) values?)))))

(defmacro with-zrvl (inputs outputs &body body)
  (%with-zrvl inputs outputs nil body))

(defmacro with-zrvl* (inputs &body body)
  (%with-zrvl inputs nil nil body))

(defmacro with-zrvl/mv (inputs outputs &body body)
  (%with-zrvl inputs outputs t body))

(defmacro with-zrvl/mv* (inputs &body body)
  (%with-zrvl inputs nil t body))

;;; TODO: priority: extra slot options e.g. :built-in :position, which
;;; will generate the relevant decorations for SPIR-V and any other
;;; required metadata... then gl-per-vertex has gl-per-vertex-position
;;;
;;; TODO: Define the type as a disjoint type when these new types are
;;; added so structs of the same structure and different names are
;;; seen as different from each other.
;;;
;;; TODO: support options list as an alternative to just a name symbol
;;; in name-and-options
;;;
;;; TODO: add a default value for the struct slots
(defmacro define-zrvl-struct (name-and-options &body slots)
  (let* ((name name-and-options)
         (slot-types (mapcar (lambda (slot)
                               (destructuring-bind (slot-name &key type &allow-other-keys)
                                   slot
                                 (declare (ignore slot-name))
                                 type))
                             slots))
         (slot-types* (mapcar #'process-zrvl-type slot-types))
         (conc-slot-names (mapcar (lambda (slot)
                                    (destructuring-bind (slot-name &key &allow-other-keys)
                                        slot
                                      (check-type slot-name symbol)
                                      (intern (concatenate 'string
                                                           (symbol-name name)
                                                           "-"
                                                           (symbol-name slot-name))
                                              (symbol-package name-and-options))))
                                  slots)))
    `(progn
       (define-zrvl-type ,name ()
         (%%struct ,@slot-types*))
       ,@(loop :for reader-name :in conc-slot-names
               ;; TODO: the way to do SETFs on places will change
               :for writer-name := (intern (concatenate 'string
                                                        (symbol-name '#:%setf-)
                                                        (symbol-name reader-name))
                                           (symbol-package name-and-options))
               :for slot-type :in slot-types
               :for slot-type* :in slot-types*
               :for index :from 0
               :collect `(define-zrvl-primitive (,reader-name :return ,slot-type)
                             ((instance ,name))
                           (%read-var id ,slot-type* (%access-chain id ,slot-type* instance ,index)))
                 :into readers
               ;; TODO: unless read-only
               :collect `(define-zrvl-primitive (,writer-name :return zrvl:void)
                             ((instance ,name)
                              (new-value ,slot-type*))
                           (%write-var (%access-chain id ,slot-type* instance ,index)
                                       new-value))
                 :into writers
               :finally (return (append readers writers)))
       ',name)))

;; TODO: the other two slots require array (i.e. list names) support
;; in types to avoid having to provide the low-level type name
(define-zrvl-struct zrvl:gl-per-vertex
  (zrvl::position :type zrvl:vec4 :built-in :position)
  (zrvl::point-size :type zrvl:f32 :built-in :point-size)
  (zrvl::clip-distance :type (%%array 1 (%%f 32)) :built-in :clip-distance)
  (zrvl::cull-distance :type (%%array 1 (%%f 32)) :built-in :cull-distance))

(define-symbol-macro zrvl:gl-position (zrvl::gl-per-vertex-position zrvl:gl-per-vertex))

;;; Functions

;;; TODO: support function parameters and return values
;;;
;;; TODO: turn into the basis of an internal function definition for ZRVL
(defmacro zrvl-int:with-function ((function-type globals main-defines) &body body)
  (let ((body* (if (or globals main-defines)
                   `((zrvl-int:with-defines (,@body)
                       ,@main-defines))
                   body)))
    `(progn
       ,@globals
       (zrvl-int:function-begin ,function-type)
       ;; TODO: a label is really starting a block like let starts a
       ;; %scope... nested (%block ...)s can exist, but they actually
       ;; split the outer block
       (zrvl-int:label)
       ,@body*
       (zrvl-int:return)
       (zrvl-int:function-end))))

;;; Bindings

(defmacro zrvl:let (bindings &body body)
  (let ((bindings (mapcar (lambda (binding)
                            (with-current-source-form (binding)
                              (destructuring-bind (name-and-type form)
                                  binding
                                (multiple-value-bind (name type)
                                    (if (consp name-and-type)
                                        (destructuring-bind (name type) name-and-type
                                          (values name type))
                                        (values name-and-type nil))
                                  `(%initialize-variable ,name ,type ,form)))))
                          bindings)))
    `(progn ,(if bindings
                 `(%scope ,bindings ,@body)
                 `(progn ,@body)))))

(defmacro zrvl:let* (bindings &body body)
  (if bindings
      (loop :for binding :in (reverse bindings)
            :for bindings* := `(zrvl:let (,binding) ,@body)
              :then `(zrvl:let (,binding) ,bindings*)
            :finally (return bindings*))
      `(progn
         ,@body)))

;;; A basic shader has a body that turns forms consisting of
;;; zrvl:define into a LET* that wraps around the "inner-forms" so
;;; that the implicit zrvl-int:setf on the global variables that
;;; represent the shader output can have access to these defined
;;; variables.
;;;
;;; TODO: define a dummy zrvl:define that is a syntax error in every
;;; context outside of this one, and that has an API that SLIME will
;;; recognize.
(defmacro zrvl-int:with-defines (inner-forms &body body)
  `(zrvl:let* ,(loop :for form :in body
                     :collect (with-current-source-form (form)
                                (destructuring-bind (define name-and-type definition) form
                                  (unless (eql define 'zrvl:define)
                                    (error "Currently, only variable definitions are accepted here."))
                                  `(,name-and-type ,definition))))
     ,@inner-forms))

(defmacro define-zrvl-primitives (name lambda-list substitute substitutions &body body)
  `(progn ,@(loop :for substitutions* :in substitutions
                  :collect (loop :for old :in substitute
                                 :for new :in substitutions*
                                 :for result := (subst new
                                                       old
                                                       `(define-zrvl-primitive ,name ,lambda-list ,@body))
                                   :then (subst new old result)
                                 :finally (return result)))))

;;; TODO: Make sure to convert to the type of the vector/matrix in
;;; constructors, e.g. single-float if vec... Avoiding having to write
;;; 0f0 instead of 0 helps with generated code!

;;; TODO: note that ATAN vs ATAN2 is implemented as a second, optional
;;; argument in CL:ATAN and probably should be the same here.

;;; TODO: plusp minusp zerop (derived from <, >, and = to 0)
;;; TODO: evenp oddp (derived from mod and zerop)
;;; TODO: signum (scalar normalize is signum when nonzero; run on each component)

;;; TODO: reverse (built from swizzle when on vecs?)

;;; TODO: isqrt

;;; TODO: floor, ffloor, etc., has a second return value. Derive it at
;;; the higher level on top of the lower-level one-return-value forms
;;; only when the second or both values are needed.

;;; TODO: Derive LOG for non-e, non-2

;;; TODO: (expt 2 x) should be optimized to use the Exp2 instruction
;;; instead of Pow. And, similarly, (expt x 2) should be special cased
;;; into the form (* x x)

;;; TODO: implement access-chain

;;; TODO: every, some, notany, notevery... optimized in particular for
;;; #'identity on boolean vectors. This is a high priority because
;;; every/some seem have equivalents elsewhere.
;;;
;;; TODO: sequence SUBSEQ, MAP, REDUCE, COUNT, LENGTH, CONCATENATE,
;;; etc., except perhaps POSITION and the impure functions

;;; TODO: full texture API, various shader stage specific stuff, etc.

;;; TODO: missing functions useful in CL specification 12.2, various
;;; linear algebra libraries (e.g. Python's numpy and/or "Python array
;;; API standard"; various other shading languages; similar CL linear
;;; algebra libraries (magicl?), etc., etc...
;;;
;;; TODO: Don't forget the bit-related stuff in 12.2, which can be
;;; extended to component-wise
;;;
;;; TODO: Useful stuff SIMD + SPIR-V have in common? Or even just in
;;; the SPIR-V instruction set?

;;; TODO: Higher level constructs? Complex, quaternions, exterior
;;; algebra, duals?, geometric algebra, etc.
;;;
;;; TODO: Can finite integer overflow be detected? Should bignums
;;; exist? Ratios? Stay i32 by default or something else?

;;; TODO: slerp, rk4, euler, etc.

;;; TODO: statistics? mean, var, std, median, etc.

;;; TODO: random numbers?

;;; TODO: fft/ifft/etc.?

;;; TODO: Note: texture uses image-sample-implicit-lod... while
;;; texture-lod by contrast would use image-sample-explicit-lod

;;;; Variadic functions, which become calls to internal-only ZRVL functions

;;; CL-style variadic functions (only cross, for the cross-product,
;;; isn't copied from CL) that are extended to work on linear algebra
;;; objects where they make sense.

;;; TODO: implement the identities

(define-variadic-function zrvl:+ (&rest numbers)
    (zrvl:zero zrvl::%numeric-identity zrvl-int:+))

(define-variadic-function zrvl:* (&rest numbers)
    (zrvl:one zrvl::%numeric-identity zrvl-int:*))

(define-variadic-function zrvl:- (number &rest more-numbers)
    (nil zrvl-int:negate zrvl-int:-))

(define-variadic-function zrvl:/ (number &rest more-numbers)
    (nil zrvl-int:reciprocal zrvl-int:/))

(define-variadic-function zrvl:cross (vector &rest more-vectors)
    (nil zrvl-int:vec-identity zrvl-int:cross))

(define-variadic-function zrvl:min (number &rest more-numbers)
    (nil zrvl::%numeric-identity zrvl-int:min))

(define-variadic-function zrvl:max (number &rest more-numbers)
    (nil zrvl::%numeric-identity zrvl-int:max))

(define-variadic-function zrvl:n-min (number &rest more-numbers)
    (nil zrvl::%numeric-identity zrvl-int:n-min))

(define-variadic-function zrvl:n-max (number &rest more-numbers)
    (nil zrvl::%numeric-identity zrvl-int:n-max))

;;; Element-wise variadic

(define-variadic-function zrvl:.* (sequence &rest more-sequences)
    (nil zrvl::%seq-or-mat-identity zrvl-int:.*))

(define-variadic-function zrvl:./ (sequence &rest more-sequences)
    (nil zrvl-int:seq-or-mat-reciprocal zrvl-int:./))

;;; Comparison variadic

;;; TODO: Note that since the order in the file matters, this means
;;; that AND needs to be defined above this point.

(define-variadic-function zrvl:= (number &rest more-numbers)
    (nil zrvl:true zrvl-int:=)
    :predicate? t)

(define-variadic-function zrvl:/= (number &rest more-numbers)
    (nil zrvl:true zrvl-int:/=)
    :predicate? t)

(define-variadic-function zrvl:< (number &rest more-numbers)
    (nil zrvl:true zrvl-int:<)
    :predicate? t)

(define-variadic-function zrvl:> (number &rest more-numbers)
    (nil zrvl:true zrvl-int:>)
    :predicate? t)

(define-variadic-function zrvl:<= (number &rest more-numbers)
    (nil zrvl:true zrvl-int:<=)
    :predicate? t)

(define-variadic-function zrvl:>= (number &rest more-numbers)
    (nil zrvl:true zrvl-int:>=)
    :predicate? t)

;;; Access variadic

;; TODO: implement 0D-aref?
(define-variadic-function zrvl:aref (array-like-object &rest subscripts)
    (nil zrvl-int:aref-0D zrvl-int:aref))

;;; Macros to help with the definitions below

(defmacro zrvl-int:define-primitive (name lambda-list &body body)
  `(define-zrvl-primitive ,name ,lambda-list ,@body))

(defmacro zrvl-int:define-primitives (name lambda-list substitute substitutions &body body)
  `(define-zrvl-primitives ,name ,lambda-list ,substitute ,substitutions ,@body))

(defmacro zrvl-int:define-unary-elementwise-matrix-primitives
    (vec-op (mx2-substitutions mx3-substitutions mx4-substitutions))
  `(progn
     (zrvl-int:define-primitives (%op :return alpha) ((m alpha))
         (%op alpha)
         ,mx2-substitutions
       (alpha (,vec-op (zrvl:aref m 0))
              (,vec-op (zrvl:aref m 1))))
     (zrvl-int:define-primitives (%op :return alpha) ((m alpha))
         (%op alpha)
         ,mx3-substitutions
       (alpha (,vec-op (zrvl:aref m 0))
              (,vec-op (zrvl:aref m 1))
              (,vec-op (zrvl:aref m 2))))
     (zrvl-int:define-primitives (%op :return alpha) ((m alpha))
         (%op alpha)
         ,mx4-substitutions
       (alpha (,vec-op (zrvl:aref m 0))
              (,vec-op (zrvl:aref m 1))
              (,vec-op (zrvl:aref m 2))
              (,vec-op (zrvl:aref m 3))))))

(defmacro zrvl-int:define-binary-elementwise-matrix-primitives
    (vec-op (mx2-substitutions mx3-substitutions mx4-substitutions))
  `(progn
     (zrvl-int:define-primitives (%op :return alpha) ((m1 alpha) (m2 alpha))
         (%op alpha)
         ,mx2-substitutions
       (alpha (,vec-op (zrvl:aref m1 0) (zrvl:aref m2 0))
              (,vec-op (zrvl:aref m1 1) (zrvl:aref m2 1))))
     (zrvl-int:define-primitives (%op :return alpha) ((m1 alpha) (m2 alpha))
         (%op alpha)
         ,mx3-substitutions
       (alpha (,vec-op (zrvl:aref m1 0) (zrvl:aref m2 0))
              (,vec-op (zrvl:aref m1 1) (zrvl:aref m2 1))
              (,vec-op (zrvl:aref m1 2) (zrvl:aref m2 2))))
     (zrvl-int:define-primitives (%op :return alpha) ((m1 alpha) (m2 alpha))
         (%op alpha)
         ,mx4-substitutions
       (alpha (,vec-op (zrvl:aref m1 0) (zrvl:aref m2 0))
              (,vec-op (zrvl:aref m1 1) (zrvl:aref m2 1))
              (,vec-op (zrvl:aref m1 2) (zrvl:aref m2 2))
              (,vec-op (zrvl:aref m1 3) (zrvl:aref m2 3))))))

(defmacro zrvl-int:define-simple-unary-primitives ((name instruction) &body substitutions)
  `(define-zrvl-primitives (,name :return alpha) ((x alpha))
       (,name alpha)
       ,substitutions
     (,instruction id alpha x)))

(defmacro zrvl-int:define-simple-binary-primitives ((name instruction) &body substitutions)
  `(define-zrvl-primitives (,name :return alpha) ((x alpha) (y alpha))
       (,name alpha)
       ,substitutions
     (,instruction id alpha x y)))

(defmacro zrvl-int:define-simple-ternary-primitives ((name instruction) &body substitutions)
  `(define-zrvl-primitives (,name :return alpha) ((x alpha) (y alpha) (z alpha))
       (,name alpha)
       ,substitutions
     (,instruction id alpha x y z)))

(defmacro zrvl-int:define-simple-unary-predicates ((name instruction) &body substitutions)
  `(define-zrvl-primitives (,name :return beta) ((x alpha))
       (,name alpha beta)
       ,substitutions
     (,instruction id beta x)))

(defmacro zrvl-int:define-simple-binary-predicates ((name instruction) &body substitutions)
  `(define-zrvl-primitives (,name :return beta) ((x alpha) (y alpha))
       (,name alpha beta)
       ,substitutions
     (,instruction id beta x y)))

(defmacro zrvl-int:define-simple-ternary-predicates ((name instruction) &body substitutions)
  `(define-zrvl-primitives (,name :return beta) ((x alpha) (y alpha) (z alpha))
       (,name alpha beta)
       ,substitutions
     (,instruction id beta x y z)))

(defmacro zrvl-int:define-generic (name lambda-list &body monomorphic-signatures)
  `(define-zrvl-generic ,name ,lambda-list ,@monomorphic-signatures))

(defmacro zrvl-int:define-alias (alias-name real-name)
  `(define-zrvl-alias ,alias-name ,real-name))

;;;; ZRVL primitive functions

(defmacro zrvl-int:setf (place new-value)
  `(%setf ,place ,new-value))

(defmacro zrvl-int:function-begin (function-type)
  `(%function-header ,function-type))

(in-package #:zrvl)

(zrvl-int:define-primitive (zrvl-int:label :return void) ()
  (%label id))

;;; TODO: Support non-void return (%return-value)
(zrvl-int:define-primitive (zrvl-int:return :return void) ()
  (%return))

(zrvl-int:define-primitive (zrvl-int:function-end :return void) ()
  (%function-end))

(zrvl-int:define-primitive (zrvl:zero :return i32) () 0)
(zrvl-int:define-primitive (zrvl:one  :return i32) () 1)

(zrvl-int:define-primitive (zrvl:true  :return boolean) () zrvlir:true)
(zrvl-int:define-primitive (zrvl:false :return boolean) () zrvlir:false)

;;; TODO: arrays
(zrvl-int:define-generic zrvl-int:aref (composite subscript)
  (zrvl-mono:f32.2-ref   f32.2   i32)
  (zrvl-mono:f32.3-ref   f32.3   i32)
  (zrvl-mono:f32.4-ref   f32.4   i32)
  (zrvl-mono:quat-ref    quat    i32)
  (zrvl-mono:f32.2x2-ref f32.2x2 i32)
  (zrvl-mono:f32.2x3-ref f32.2x3 i32)
  (zrvl-mono:f32.2x4-ref f32.2x4 i32)
  (zrvl-mono:f32.3x2-ref f32.3x2 i32)
  (zrvl-mono:f32.3x3-ref f32.3x3 i32)
  (zrvl-mono:f32.3x4-ref f32.3x4 i32)
  (zrvl-mono:f32.4x2-ref f32.4x2 i32)
  (zrvl-mono:f32.4x3-ref f32.4x3 i32)
  (zrvl-mono:f32.4x4-ref f32.4x4 i32))

;;; TODO: If not constant/literal, then access-chain must be used
;;; instead of this? Or in-bounds-access-chain if bounds checked? The
;;; exception is for vecs, which can use vector-extract-dynamic
(zrvl-int:define-primitives (zrvl-int:aref :return gamma) ((composite alpha) (subscript beta))
    (zrvl-int:aref alpha beta gamma)
    ((zrvl-mono:f32.2-ref   f32.2   i32 f32)
     (zrvl-mono:f32.3-ref   f32.3   i32 f32)
     (zrvl-mono:f32.4-ref   f32.4   i32 f32)
     (zrvl-mono:f32.2x2-ref f32.2x2 i32 f32.2)
     (zrvl-mono:f32.2x3-ref f32.2x3 i32 f32.2)
     (zrvl-mono:f32.2x4-ref f32.2x4 i32 f32.2)
     (zrvl-mono:f32.3x2-ref f32.3x2 i32 f32.3)
     (zrvl-mono:f32.3x3-ref f32.3x3 i32 f32.3)
     (zrvl-mono:f32.3x4-ref f32.3x4 i32 f32.3)
     (zrvl-mono:f32.4x2-ref f32.4x2 i32 f32.4)
     (zrvl-mono:f32.4x3-ref f32.4x3 i32 f32.4)
     (zrvl-mono:f32.4x4-ref f32.4x4 i32 f32.4))
  ;; Note: A literal must be a constant. It is inlined with the
  ;; instruction instead of being stored as a constant. In other
  ;; words, during codegen, handle (%literal subscript) separately.
  (zombie-raptor/zrvl/a-normal:check-bounds composite subscript)
  (%composite-extract id gamma composite (%literal subscript)))

(in-package #:zombie-raptor/zrvl/zrvl)

;;; A "swizzle", which uses dot notation in GLSL and a keyword here,
;;; constructs a new vector using 1 to 4 the characters from the
;;; character sets XYZQ, RGBA, or STPQ as accessors to the input
;;; vector's elements. For example, the call (swizzle vector :yyy)
;;; creates a new vec3 containing the second element of the input
;;; vector duplicated three times. As in GLSL, you cannot mix
;;; characters from the three character sets.
;;;
;;; Note that because keywords do not currently exist at runtime, they
;;; must be literal, not variables. That is, (swizzle v :zyx) works,
;;; but (swizzle v args) where args is a variable does not.
;;;
;;; TODO: Optimize the case where :xy or :xyz or :xyzw (probably from
;;; generated code) etc. is used on a vector of the same length into a
;;; no-op?
(defmacro zrvl:swizzle (vector swizzle-keyword)
  ;; Validates the swizzle-keyword by the GLSL-style swizzle rules
  (unless (swizzle? swizzle-keyword)
    (error "Invalid swizzle ~A" swizzle-keyword))
  ;; Turns the swizzle into numeric elements.
  (let ((swizzle (map 'list
                      (lambda (char)
                        (ecase char
                          ((#\X #\R #\S) 0)
                          ((#\Y #\G #\T) 1)
                          ((#\Z #\B #\P) 2)
                          ((#\W #\A #\Q) 3)))
                      (symbol-name swizzle-keyword))))
    (ecase= (length swizzle)
      (1 `(zrvl:aref ,vector ,(nth 0 swizzle)))
      (2 `(zrvl-int:swizzle2 ,vector ,@swizzle))
      (3 `(zrvl-int:swizzle3 ,vector ,@swizzle))
      (4 `(zrvl-int:swizzle4 ,vector ,@swizzle)))))

(in-package #:zrvl)

;;; For vec2, vec3, and vec4, any combination of scalars and vectors
;;; that produces a given vector is valid, including shrinking a
;;; larger vector. This last case, shrinking a larger vector, can only
;;; happen when one argument, the vector itself, is provided. In other
;;; words, (vec3 vec4) is permitted, but (vec3 vec2 vec2) is not
;;; permitted.

;;; TODO: Turn into templated versions for vec, dvec, ivec, uvec?, bvec
(zrvl-int:define-primitive (zrvl-int:vec2 :return vec2) ((x f32) (y f32))
  (%composite-construct id vec2 x y))

(zrvl-int:define-primitive (zrvl-int:vec3 :return vec3) ((x f32) (y f32) (z f32))
  (%composite-construct id vec3 x y z))

(zrvl-int:define-primitive (zrvl-int:vec4 :return vec4) ((x f32) (y f32) (z f32) (w f32))
  (%composite-construct id vec4 x y z w))

(zrvl-int:define-primitive (vec2<-3 :return vec2) ((v vec2))
  (zrvl-int:vec2 (aref v 0)
                 (aref v 1)))

(zrvl-int:define-primitive (vec2<-4 :return vec2) ((v vec2))
  (zrvl-int:vec2 (aref v 0)
                 (aref v 1)))

(zrvl-int:define-primitive (vec3<-4 :return vec3) ((v vec4))
  (zrvl-int:vec3 (aref v 0)
                 (aref v 1)
                 (aref v 2)))

(zrvl-int:define-primitive (zrvl-int:vec3<-2 :return vec3) ((v vec2) (z f32))
  (zrvl-int:vec3 (aref v 0)
                 (aref v 1)
                 z))

(zrvl-int:define-primitive (zrvl-int:vec3<-2* :return vec3) ((x f32) (v vec2))
  (zrvl-int:vec3 x
                 (aref v 0)
                 (aref v 1)))

(zrvl-int:define-primitive (zrvl-int:vec3<-2-expand :return vec3) ((v vec2))
  (zrvl-int:vec3<-2 v 0f0))

(zrvl-int:define-primitive (zrvl-int:vec4<-2 :return vec4) ((v vec2) (z f32) (w f32))
  (zrvl-int:vec4 (aref v 0)
                 (aref v 1)
                 z
                 w))

(zrvl-int:define-primitive (zrvl-int:vec4<-2* :return vec4) ((x f32) (v vec2) (w f32))
  (zrvl-int:vec4 x
                 (aref v 0)
                 (aref v 1)
                 w))

(zrvl-int:define-primitive (zrvl-int:vec4<-2** :return vec4) ((x f32) (y f32) (v vec2))
  (zrvl-int:vec4 x
                 y
                 (aref v 0)
                 (aref v 1)))

(zrvl-int:define-primitive (vec4<-2s :return vec4) ((u vec2) (v vec2))
  (zrvl-int:vec4 (aref u 0)
                 (aref u 1)
                 (aref v 0)
                 (aref v 1)))

(zrvl-int:define-primitive (zrvl-int:vec4<-2-expand :return vec4) ((v vec2))
  (zrvl-int:vec4<-2 v
                    0f0
                    1f0))

(zrvl-int:define-primitive (zrvl-int:vec4<-3 :return vec4) ((v vec3) (w f32))
  (zrvl-int:vec4 (aref v 0)
                 (aref v 1)
                 (aref v 2)
                 w))

(zrvl-int:define-primitive (zrvl-int:vec4<-3* :return vec4) ((x f32) (v vec3))
  (zrvl-int:vec4 x
                 (aref v 0)
                 (aref v 1)
                 (aref v 2)))

(zrvl-int:define-primitive (zrvl-int:vec4<-3-expand :return vec4) ((v vec3))
  (zrvl-int:vec4<-3 v 1f0))

(zrvl-int:define-primitive (zrvl-int:vec2<-scalar :return vec2) ((x f32))
  (zrvl-int:vec2 x x))

(zrvl-int:define-primitive (zrvl-int:vec3<-scalar :return vec3) ((x f32))
  (zrvl-int:vec3 x x x))

(zrvl-int:define-primitive (zrvl-int:vec4<-scalar :return vec4) ((x f32))
  (zrvl-int:vec4 x x x x))

(zrvl-int:define-primitive (zrvl-int:vec2-identity :return vec2) ((v vec2)) v)
(zrvl-int:define-primitive (zrvl-int:vec3-identity :return vec3) ((v vec3)) v)
(zrvl-int:define-primitive (zrvl-int:vec4-identity :return vec4) ((v vec4)) v)

;;; TODO: extract matrix diagonals if given a conforming matrix
(zrvl-int:define-generic vec2<- (object)
  (zrvl-int:vec2<-scalar  f32)
  (zrvl-int:vec2-identity f32.2)
  (zrvl:vec2<-3           f32.3)
  (zrvl:vec2<-4           f32.4))

;;; TODO: extract matrix diagonals if given a conforming matrix
(zrvl-int:define-generic vec3<- (object)
  (zrvl-int:vec3<-scalar   f32)
  (zrvl-int:vec3<-2-expand f32.2)
  (zrvl-int:vec3-identity  f32.3)
  (zrvl:vec3<-4            f32.4))

;;; TODO: extract matrix diagonals if given a 4x4 matrix
(zrvl-int:define-generic vec4<- (object)
  (zrvl-int:vec4<-scalar   f32)
  (zrvl-int:vec4<-2-expand f32.2)
  (zrvl-int:vec4<-3-expand f32.3)
  (zrvl-int:vec4-identity  f32.4))

(zrvl-int:define-generic zrvl-int:vec2<-vec (vector)
  (zrvl-int:vec2-identity f32.2)
  (zrvl:vec2<-3           f32.3)
  (zrvl:vec2<-4           f32.4))

(zrvl-int:define-generic zrvl-int:vec3<-vec (vector)
  (zrvl-int:vec3-identity f32.3)
  (zrvl:vec3<-4           f32.4))

(zrvl-int:define-generic vec3<-2 (a b)
  (zrvl-int:vec3<-2  f32.2 f32)
  (zrvl-int:vec3<-2* f32   f32.2))

(zrvl-int:define-generic vec4<-2 (a b c)
  (zrvl-int:vec4<-2   f32.2 f32   f32)
  (zrvl-int:vec4<-2*  f32   f32.2 f32)
  (zrvl-int:vec4<-2** f32   f32   f32.2))

(zrvl-int:define-generic vec4<-3 (a b)
  (zrvl-int:vec4<-3  f32.3 f32)
  (zrvl-int:vec4<-3* f32   f32.3))

(zrvl-int:define-generic zrvl-int:vec4<-ab (a b)
  (zrvl-int:vec4<-3  f32.3 f32)
  (zrvl-int:vec4<-3* f32   f32.3)
  (zrvl:vec4<-2s     f32.2 f32.2))

(zrvl-int:define-generic zrvl-int:vec-identity (v)
  (zrvl-int:vec2-identity f32.2)
  (zrvl-int:vec3-identity f32.3)
  (zrvl-int:vec4-identity f32.4))

;;; TODO: eventually, directly supporting &optional with some kind of
;;; "none" default value as part of an optional type could then turn
;;; these macros into regular generics, with b, etc., possibly being
;;; of type "none" being part of the dispatch mechanism... until then,
;;; the only function that doesn't need to exist otherwise is
;;; zrvl-int:vec4<-ab

(in-package #:zombie-raptor/zrvl/zrvl)

(defmacro zrvl:vec (x y &optional z w)
  (cond (w
         `(zrvl-int:vec4 ,x ,y ,z ,w))
        (z
         `(zrvl-int:vec3 ,x ,y ,z))
        (y
         `(zrvl-int:vec2 ,x ,y))))

(defmacro zrvl:vec2 (a &optional b)
  (if b
      `(zrvl-int:vec2 ,a ,b)
      `(zrvl-int:vec2<-vec ,a)))

(defmacro zrvl:vec3 (a &optional b c)
  (cond (c `(zrvl-int:vec3 ,a ,b ,c))
        (b `(zrvl:vec3<-2 ,a ,b))
        (t `(zrvl-int:vec3<-vec ,a))))

(defmacro zrvl:vec4 (a &optional b c d)
  (cond (d `(zrvl-int:vec4 ,a ,b ,c ,d))
        (c `(zrvl:vec4<-2 ,a ,b ,c))
        (b `(zrvl-int:vec4<-ab ,a ,b))
        (t `(zrvl-int:vec4-identity ,a))))

(in-package #:zrvl)

;;; GLSL-style matrix constructors, except only from vectors
;;;
;;; These matrix constructors either take in a matrix or column
;;; vectors. To construct a matrix from scalars, create column vectors
;;; that will be optimized away.
;;;
;;; If vectors, the column vectors must be the correct size to match
;;; the matrix dimensions and all columns must be provided.
;;;
;;; Example: (mat2<- matrix) resizes matrix to be 2x2. As all matrices
;;; are at least 2x2 in size, this is always valid, but calling mat2
;;; on a mat2 will just return the input mat2.
;;;
;;; Example: (mat2 (vec2 1f0 2f0) (vec2 3f0 4f0)) creates a 2x2 matrix
;;; from scalars.
;;;
;;; Example: (mat2 a b) where a and b are column vectors creates a 2x2
;;; matrix from a and b, with a as the first column and b as the
;;; second column.

;;; TODO: Also dmat

(zrvl-int:define-alias mat2 mat2x2)
(zrvl-int:define-alias mat3 mat3x3)
(zrvl-int:define-alias mat4 mat4x4)

(zrvl-int:define-primitive (mat2x2 :return mat2x2)
    ((col1 vec2) (col2 vec2))
  (%composite-construct id mat2x2 col1 col2))

(zrvl-int:define-primitive (mat2x3 :return mat2x3)
    ((col1 vec2) (col2 vec2) (col3 vec2))
  (%composite-construct id mat2x3 col1 col2 col3))

(zrvl-int:define-primitive (mat2x4 :return mat2x4)
    ((col1 vec2) (col2 vec2) (col3 vec2) (col4 vec2))
  (%composite-construct id mat2x4 col1 col2 col3 col4))

(zrvl-int:define-primitive (mat3x2 :return mat3x2)
    ((col1 vec3) (col2 vec3))
  (%composite-construct id mat3x2 col1 col2))

(zrvl-int:define-primitive (mat3x3 :return mat3x3)
    ((col1 vec3) (col2 vec3) (col3 vec3))
  (%composite-construct id mat3x3 col1 col2 col3))

(zrvl-int:define-primitive (mat3x4 :return mat3x4)
    ((col1 vec3) (col2 vec3) (col3 vec3) (col4 vec3))
  (%composite-construct id mat3x4 col1 col2 col3 col4))

(zrvl-int:define-primitive (mat4x2 :return mat4x2)
    ((col1 vec4) (col2 vec4))
  (%composite-construct id mat4x2 col1 col2))

(zrvl-int:define-primitive (mat4x3 :return mat4x3)
    ((col1 vec4) (col2 vec4) (col3 vec4))
  (%composite-construct id mat4x3 col1 col2 col3))

(zrvl-int:define-primitive (mat4x4 :return mat4x4)
    ((col1 vec4) (col2 vec4) (col3 vec4) (col4 vec4))
  (%composite-construct id mat4x4 col1 col2 col3 col4))

;;; Note: Unlike GLSL, diagonal matrices are not created from the
;;; generic matrix constructor with an argument of one scalar. All
;;; matrix constructors of one argument rather than of columns uses
;;; these special constructors to either resize matrices or set the
;;; diagonal of the matrices.

;;; Then there's the other way around, to get the diagonal of the
;;; matrix. This is a generic for every matrix type with a vector as
;;; output.
;;;
;;; TODO: implement
#+(or)
(zrvl-int:define-primitive diagonal (matrix)
  stub)

;;; TODO: not
;;; TODO: %if if and or cond when unless case

;;; TODO: eventually:
;;; boolean
;;; f16 f32 f64
;;; u8 u16 u32 u64
;;; i8 i16 i32 i64
;;;
;;; TODO: three backends: SB-SIMD, Lisp scalar, and SPIR-V... right now, just SPIR-V

;;; TODO: expand most scalar/vec operations to also include matrices,
;;; which means decomposing them into column vecs, applying to the
;;; column vecs, and then rebuilding a fresh matrix from those column
;;; vecs

;;; TODO: outer-product
;;; TODO: refract inverse-sqrt

;;; TODO: eql?

;;; TODO: pi (constant)

;;; TODO: mod rem
;;; TODO: ash (and the other bit stuff)
;;; TODO: smooth-step
;;; TODO: face-forward
;;; TODO: n-min n-max n-clamp

;;; TODO: the various identity functions, which all return one
;;; low-level identity for a given type from the same set of low-level
;;; identity functions, but which differ in which types they accept as
;;; valid. In particular, one identity applies to all primitive
;;; numeric types (number, vec, matrix) and another (for element-wise
;;; functions) applies to arrays, vecs, and matrices... maybe
;;; nonscalar-numeric-identity? and one just for vectors, too.

(zrvl-int:define-generic zrvl-int:swizzle2 (vec a b)
  (zrvl-mono:f32.2-swizzle2 vec2 i32 i32)
  (zrvl-mono:f32.3-swizzle2 vec3 i32 i32)
  (zrvl-mono:f32.4-swizzle2 vec4 i32 i32)
  (zrvl-mono:quat-swizzle2  quat i32 i32))

(zrvl-int:define-generic zrvl-int:swizzle3 (vec a b c)
  (zrvl-mono:f32.2-swizzle3 vec2 i32 i32 i32)
  (zrvl-mono:f32.3-swizzle3 vec3 i32 i32 i32)
  (zrvl-mono:f32.4-swizzle3 vec4 i32 i32 i32)
  (zrvl-mono:quat-swizzle3  quat i32 i32 i32))

(zrvl-int:define-generic zrvl-int:swizzle4 (vec a b c d)
  (zrvl-mono:f32.2-swizzle4 vec2 i32 i32 i32 i32)
  (zrvl-mono:f32.3-swizzle4 vec3 i32 i32 i32 i32)
  (zrvl-mono:f32.4-swizzle4 vec4 i32 i32 i32 i32)
  (zrvl-mono:quat-swizzle4  quat i32 i32 i32 i32))

(zrvl-int:define-primitives (zrvl-int:swizzle2 :return gamma) ((vec alpha) (a beta) (b beta))
    (zrvl-int:swizzle2 alpha beta gamma)
    ((zrvl-mono:f32.2-swizzle2 vec2 i32 vec2)
     (zrvl-mono:f32.3-swizzle2 vec3 i32 vec2)
     (zrvl-mono:f32.4-swizzle2 vec4 i32 vec2))
  (zombie-raptor/zrvl/a-normal:check-bounds vec (max a b))
  (%vector-shuffle id gamma vec vec (%literal a) (%literal b)))

(zrvl-int:define-primitives (zrvl-int:swizzle3 :return gamma) ((vec alpha) (a beta) (b beta) (c beta))
    (zrvl-int:swizzle3 alpha beta gamma)
    ((zrvl-mono:f32.2-swizzle3 vec2 i32 vec3)
     (zrvl-mono:f32.3-swizzle3 vec3 i32 vec3)
     (zrvl-mono:f32.4-swizzle3 vec4 i32 vec3))
  (zombie-raptor/zrvl/a-normal:check-bounds vec (max a b c))
  (%vector-shuffle id gamma vec vec (%literal a) (%literal b) (%literal c)))

(zrvl-int:define-primitives (zrvl-int:swizzle4 :return gamma) ((vec alpha) (a beta) (b beta) (c beta) (d beta))
    (zrvl-int:swizzle4 alpha beta gamma)
    ((zrvl-mono:f32.2-swizzle4 vec2 i32 vec4)
     (zrvl-mono:f32.3-swizzle4 vec3 i32 vec4)
     (zrvl-mono:f32.4-swizzle4 vec4 i32 vec4))
  (zombie-raptor/zrvl/a-normal:check-bounds vec (max a b c d))
  (%vector-shuffle id gamma vec vec (%literal a) (%literal b) (%literal c) (%literal d)))

(zrvl-int:define-generic zrvl-int:= (x y)
  (zrvl-mono:f32=   f32  f32)
  (zrvl-mono:f32.2= vec2 vec2)
  (zrvl-mono:f32.3= vec3 vec3)
  (zrvl-mono:f32.4= vec4 vec4))

(zrvl-int:define-generic zrvl-int:/= (x y)
  (zrvl-mono:f32/=   f32  f32)
  (zrvl-mono:f32.2/= vec2 vec2)
  (zrvl-mono:f32.3/= vec3 vec3)
  (zrvl-mono:f32.4/= vec4 vec4))

(zrvl-int:define-generic zrvl-int:< (x y)
  (zrvl-mono:f32<   f32  f32)
  (zrvl-mono:f32.2< vec2 vec2)
  (zrvl-mono:f32.3< vec3 vec3)
  (zrvl-mono:f32.4< vec4 vec4))

(zrvl-int:define-generic zrvl-int:<= (x y)
  (zrvl-mono:f32<=   f32  f32)
  (zrvl-mono:f32.2<= vec2 vec2)
  (zrvl-mono:f32.3<= vec3 vec3)
  (zrvl-mono:f32.4<= vec4 vec4))

(zrvl-int:define-generic zrvl-int:> (x y)
  (zrvl-mono:f32>   f32  f32)
  (zrvl-mono:f32.2> vec2 vec2)
  (zrvl-mono:f32.3> vec3 vec3)
  (zrvl-mono:f32.4> vec4 vec4))

(zrvl-int:define-generic zrvl-int:>= (x y)
  (zrvl-mono:f32>=   f32  f32)
  (zrvl-mono:f32.2>= vec2 vec2)
  (zrvl-mono:f32.3>= vec3 vec3)
  (zrvl-mono:f32.4>= vec4 vec4))

(zrvl-int:define-simple-binary-predicates (zrvl-int:= %f-equal)
  (zrvl-mono:f32=   f32  boolean)
  (zrvl-mono:f32.2= vec2 bvec2)
  (zrvl-mono:f32.3= vec3 bvec3)
  (zrvl-mono:f32.4= vec4 bvec4))

(zrvl-int:define-simple-binary-predicates (zrvl-int:/= %f-not-equal)
  (zrvl-mono:f32/=   f32  boolean)
  (zrvl-mono:f32.2/= vec2 bvec2)
  (zrvl-mono:f32.3/= vec3 bvec3)
  (zrvl-mono:f32.4/= vec4 bvec4))

(zrvl-int:define-simple-binary-predicates (zrvl-int:< %f-less-than)
  (zrvl-mono:f32<   f32  boolean)
  (zrvl-mono:f32.2< vec2 bvec2)
  (zrvl-mono:f32.3< vec3 bvec3)
  (zrvl-mono:f32.4< vec4 bvec4))

(zrvl-int:define-simple-binary-predicates (zrvl-int:<= %f-less-than-equal)
  (zrvl-mono:f32<=   f32  boolean)
  (zrvl-mono:f32.2<= vec2 bvec2)
  (zrvl-mono:f32.3<= vec3 bvec3)
  (zrvl-mono:f32.4<= vec4 bvec4))

(zrvl-int:define-simple-binary-predicates (zrvl-int:> %f-greater-than)
  (zrvl-mono:f32>   f32  boolean)
  (zrvl-mono:f32.2> vec2 bvec2)
  (zrvl-mono:f32.3> vec3 bvec3)
  (zrvl-mono:f32.4> vec4 bvec4))

(zrvl-int:define-simple-binary-predicates (zrvl-int:>= %f-greater-than-equal)
  (zrvl-mono:f32>=   f32  boolean)
  (zrvl-mono:f32.2>= vec2 bvec2)
  (zrvl-mono:f32.3>= vec3 bvec3)
  (zrvl-mono:f32.4>= vec4 bvec4))

(zrvl-int:define-generic zrvl-int:+ (x y)
  (zrvl-mono:f32+   f32  f32)
  (zrvl-mono:f32.2+ vec2 vec2)
  (zrvl-mono:f32.3+ vec3 vec3)
  (zrvl-mono:f32.4+ vec4 vec4)
  (zrvl-mono:quat+  quat quat)
  (zrvl-mono:f32.2x2+ f32.2x2 f32.2x2)
  (zrvl-mono:f32.2x3+ f32.2x3 f32.2x3)
  (zrvl-mono:f32.2x4+ f32.2x4 f32.2x4)
  (zrvl-mono:f32.3x2+ f32.3x2 f32.3x2)
  (zrvl-mono:f32.3x3+ f32.3x3 f32.3x3)
  (zrvl-mono:f32.3x4+ f32.3x4 f32.3x4)
  (zrvl-mono:f32.4x2+ f32.4x2 f32.4x2)
  (zrvl-mono:f32.4x3+ f32.4x3 f32.4x3)
  (zrvl-mono:f32.4x4+ f32.4x4 f32.4x4))

(zrvl-int:define-generic zrvl-int:- (x y)
  (zrvl-mono:f32-   f32  f32)
  (zrvl-mono:f32.2- vec2 vec2)
  (zrvl-mono:f32.3- vec3 vec3)
  (zrvl-mono:f32.4- vec4 vec4)
  (zrvl-mono:quat-  quat quat)
  (zrvl-mono:f32.2x2- f32.2x2 f32.2x2)
  (zrvl-mono:f32.2x3- f32.2x3 f32.2x3)
  (zrvl-mono:f32.2x4- f32.2x4 f32.2x4)
  (zrvl-mono:f32.3x2- f32.3x2 f32.3x2)
  (zrvl-mono:f32.3x3- f32.3x3 f32.3x3)
  (zrvl-mono:f32.3x4- f32.3x4 f32.3x4)
  (zrvl-mono:f32.4x2- f32.4x2 f32.4x2)
  (zrvl-mono:f32.4x3- f32.4x3 f32.4x3)
  (zrvl-mono:f32.4x4- f32.4x4 f32.4x4))

(zrvl-int:define-generic *+ (a b c)
  (zrvl-mono:f32*+   f32  f32  f32)
  (zrvl-mono:f32.2*+ vec2 vec2 vec2)
  (zrvl-mono:f32.3*+ vec3 vec3 vec3)
  (zrvl-mono:f32.4*+ vec4 vec4 vec4))

(zrvl-int:define-generic zrvl-int:/ (x y)
  (zrvl-mono:f32/   f32  f32))

(zrvl-int:define-generic zrvl-int:./ (x y)
  (zrvl-mono:f32.2./ vec2 vec2)
  (zrvl-mono:f32.3./ vec3 vec3)
  (zrvl-mono:f32.4./ vec4 vec4)
  (zrvl-mono:f32.2x2./ f32.2x2 f32.2x2)
  (zrvl-mono:f32.2x3./ f32.2x3 f32.2x3)
  (zrvl-mono:f32.2x4./ f32.2x4 f32.2x4)
  (zrvl-mono:f32.3x2./ f32.3x2 f32.3x2)
  (zrvl-mono:f32.3x3./ f32.3x3 f32.3x3)
  (zrvl-mono:f32.3x4./ f32.3x4 f32.3x4)
  (zrvl-mono:f32.4x2./ f32.4x2 f32.4x2)
  (zrvl-mono:f32.4x3./ f32.4x3 f32.4x3)
  (zrvl-mono:f32.4x4./ f32.4x4 f32.4x4))

(zrvl-int:define-generic zrvl-int:negate (x)
  (zrvl-mono:f32-negate   f32)
  (zrvl-mono:f32.2-negate vec2)
  (zrvl-mono:f32.3-negate vec3)
  (zrvl-mono:f32.4-negate vec4)
  (zrvl-mono:quat-negate  quat)
  (zrvl-mono:f32.2x2-negate f32.2x2)
  (zrvl-mono:f32.2x3-negate f32.2x3)
  (zrvl-mono:f32.2x4-negate f32.2x4)
  (zrvl-mono:f32.3x2-negate f32.3x2)
  (zrvl-mono:f32.3x3-negate f32.3x3)
  (zrvl-mono:f32.3x4-negate f32.3x4)
  (zrvl-mono:f32.4x2-negate f32.4x2)
  (zrvl-mono:f32.4x3-negate f32.4x3)
  (zrvl-mono:f32.4x4-negate f32.4x4))

(zrvl-int:define-generic zrvl-int:reciprocal (x)
  (zrvl-mono:f32-recip   f32)
  (zrvl-mono:f32.2-recip vec2)
  (zrvl-mono:f32.3-recip vec3)
  (zrvl-mono:f32.4-recip vec4))

;;; TODO: element-wise matrix reciprocal
(zrvl-int:define-generic zrvl-int:seq-or-mat-reciprocal (x)
  (zrvl-mono:f32.2-recip vec2)
  (zrvl-mono:f32.3-recip vec3)
  (zrvl-mono:f32.4-recip vec4))

(zrvl-int:define-generic dot (x y)
  (zrvl-mono:f32.2-dot vec2 vec2)
  (zrvl-mono:f32.3-dot vec3 vec3)
  (zrvl-mono:f32.4-dot vec4 vec4)
  (zrvl-mono:quat-dot  quat quat))

(zrvl-int:define-simple-binary-primitives (zrvl-int:+ %f+)
  (zrvl-mono:f32+   f32)
  (zrvl-mono:f32.2+ vec2)
  (zrvl-mono:f32.3+ vec3)
  (zrvl-mono:f32.4+ vec4))

(zrvl-int:define-binary-elementwise-matrix-primitives zrvl:+
    (((zrvl-mono:f32.2x2+ mat2x2)
      (zrvl-mono:f32.3x2+ mat3x2)
      (zrvl-mono:f32.4x2+ mat4x2))
     ((zrvl-mono:f32.2x3+ mat2x3)
      (zrvl-mono:f32.3x3+ mat3x3)
      (zrvl-mono:f32.4x3+ mat4x3))
     ((zrvl-mono:f32.2x4+ mat2x4)
      (zrvl-mono:f32.3x4+ mat3x4)
      (zrvl-mono:f32.4x4+ mat4x4))))

(zrvl-int:define-simple-binary-primitives (zrvl-int:- %f-)
  (zrvl-mono:f32-   f32)
  (zrvl-mono:f32.2- vec2)
  (zrvl-mono:f32.3- vec3)
  (zrvl-mono:f32.4- vec4))

(zrvl-int:define-binary-elementwise-matrix-primitives zrvl:-
    (((zrvl-mono:f32.2x2- mat2x2)
      (zrvl-mono:f32.3x2- mat3x2)
      (zrvl-mono:f32.4x2- mat4x2))
     ((zrvl-mono:f32.2x3- mat2x3)
      (zrvl-mono:f32.3x3- mat3x3)
      (zrvl-mono:f32.4x3- mat4x3))
     ((zrvl-mono:f32.2x4- mat2x4)
      (zrvl-mono:f32.3x4- mat3x4)
      (zrvl-mono:f32.4x4- mat4x4))))

(zrvl-int:define-simple-ternary-primitives (*+ %*+)
  (zrvl-mono:f32*+   f32)
  (zrvl-mono:f32.2*+ vec2)
  (zrvl-mono:f32.3*+ vec3)
  (zrvl-mono:f32.4*+ vec4))

(zrvl-int:define-simple-binary-primitives (zrvl-int:/ %f/)
  (zrvl-mono:f32/ f32))

(zrvl-int:define-simple-binary-primitives (zrvl-int:./ %f/)
  (zrvl-mono:f32.2./ vec2)
  (zrvl-mono:f32.3./ vec3)
  (zrvl-mono:f32.4./ vec4))

(zrvl-int:define-binary-elementwise-matrix-primitives zrvl:./
    (((zrvl-mono:f32.2x2./ mat2x2)
      (zrvl-mono:f32.3x2./ mat3x2)
      (zrvl-mono:f32.4x2./ mat4x2))
     ((zrvl-mono:f32.2x3./ mat2x3)
      (zrvl-mono:f32.3x3./ mat3x3)
      (zrvl-mono:f32.4x3./ mat4x3))
     ((zrvl-mono:f32.2x4./ mat2x4)
      (zrvl-mono:f32.3x4./ mat3x4)
      (zrvl-mono:f32.4x4./ mat4x4))))

(zrvl-int:define-simple-unary-primitives (zrvl-int:negate %f-negate)
  (zrvl-mono:f32-negate   f32)
  (zrvl-mono:f32.2-negate vec2)
  (zrvl-mono:f32.3-negate vec3)
  (zrvl-mono:f32.4-negate vec4))

(zrvl-int:define-unary-elementwise-matrix-primitives zrvl:-
    (((zrvl-mono:f32.2x2-negate mat2x2)
      (zrvl-mono:f32.3x2-negate mat3x2)
      (zrvl-mono:f32.4x2-negate mat4x2))
     ((zrvl-mono:f32.2x3-negate mat2x3)
      (zrvl-mono:f32.3x3-negate mat3x3)
      (zrvl-mono:f32.4x3-negate mat4x3))
     ((zrvl-mono:f32.2x4-negate mat2x4)
      (zrvl-mono:f32.3x4-negate mat3x4)
      (zrvl-mono:f32.4x4-negate mat4x4))))

(zrvl-int:define-primitive (zrvl-mono:f32-recip :return f32) ((number f32))
  (/ 1f0 number))

(zrvl-int:define-primitive (zrvl-mono:f32.2-recip :return f32.2) ((number f32.2))
  (./ (vec2 1f0 1f0) number))

(zrvl-int:define-primitive (zrvl-mono:f32.3-recip :return f32.3) ((number f32.3))
  (./ (vec3 1f0 1f0 1f0) number))

(zrvl-int:define-primitive (zrvl-mono:f32.4-recip :return f32.4) ((number f32.4))
  (./ (vec4 1f0 1f0 1f0 1f0) number))

(zrvl-int:define-primitives (dot :return beta) ((x alpha) (y alpha))
    (dot alpha beta)
    ((zrvl-mono:f32.2-dot vec2 f32)
     (zrvl-mono:f32.3-dot vec3 f32)
     (zrvl-mono:f32.4-dot vec4 f32))
  (%dot id beta x y))

(zrvl-int:define-generic abs (x)
  (zrvl-mono:f32-abs   f32)
  (zrvl-mono:f32.2-abs vec2)
  (zrvl-mono:f32.3-abs vec3)
  (zrvl-mono:f32.4-abs vec4))

(zrvl-int:define-generic fround (x)
  (zrvl-mono:f32-fround   f32)
  (zrvl-mono:f32.2-fround vec2)
  (zrvl-mono:f32.3-fround vec3)
  (zrvl-mono:f32.4-fround vec4))

(zrvl-int:define-generic ftruncate (x)
  (zrvl-mono:f32-ftruncate   f32)
  (zrvl-mono:f32.2-ftruncate vec2)
  (zrvl-mono:f32.3-ftruncate vec3)
  (zrvl-mono:f32.4-ftruncate vec4))

(zrvl-int:define-generic ffloor (x)
  (zrvl-mono:f32-ffloor   f32)
  (zrvl-mono:f32.2-ffloor vec2)
  (zrvl-mono:f32.3-ffloor vec3)
  (zrvl-mono:f32.4-ffloor vec4))

(zrvl-int:define-generic fceiling (x)
  (zrvl-mono:f32-fceiling   f32)
  (zrvl-mono:f32.2-fceiling vec2)
  (zrvl-mono:f32.3-fceiling vec3)
  (zrvl-mono:f32.4-fceiling vec4))

(zrvl-int:define-generic round (x)
  (zrvl-mono:f32-round   f32)
  (zrvl-mono:f32.2-round vec2)
  (zrvl-mono:f32.3-round vec3)
  (zrvl-mono:f32.4-round vec4))

(zrvl-int:define-generic truncate (x)
  (zrvl-mono:f32-truncate   f32)
  (zrvl-mono:f32.2-truncate vec2)
  (zrvl-mono:f32.3-truncate vec3)
  (zrvl-mono:f32.4-truncate vec4))

(zrvl-int:define-generic floor (x)
  (zrvl-mono:f32-floor   f32)
  (zrvl-mono:f32.2-floor vec2)
  (zrvl-mono:f32.3-floor vec3)
  (zrvl-mono:f32.4-floor vec4))

(zrvl-int:define-generic ceiling (x)
  (zrvl-mono:f32-ceiling   f32)
  (zrvl-mono:f32.2-ceiling vec2)
  (zrvl-mono:f32.3-ceiling vec3)
  (zrvl-mono:f32.4-ceiling vec4))

(zrvl-int:define-simple-unary-primitives (abs %f-abs)
  (zrvl-mono:f32-abs   f32)
  (zrvl-mono:f32.2-abs vec2)
  (zrvl-mono:f32.3-abs vec3)
  (zrvl-mono:f32.4-abs vec4))

(zrvl-int:define-simple-unary-primitives (fround %round-even)
  (zrvl-mono:f32-fround   f32)
  (zrvl-mono:f32.2-fround vec2)
  (zrvl-mono:f32.3-fround vec3)
  (zrvl-mono:f32.4-fround vec4))

(zrvl-int:define-simple-unary-primitives (ftruncate %trunc)
  (zrvl-mono:f32-ftruncate   f32)
  (zrvl-mono:f32.2-ftruncate vec2)
  (zrvl-mono:f32.3-ftruncate vec3)
  (zrvl-mono:f32.4-ftruncate vec4))

(zrvl-int:define-simple-unary-primitives (ffloor %floor)
  (zrvl-mono:f32-ffloor   f32)
  (zrvl-mono:f32.2-ffloor vec2)
  (zrvl-mono:f32.3-ffloor vec3)
  (zrvl-mono:f32.4-ffloor vec4))

(zrvl-int:define-simple-unary-primitives (fceiling %ceil)
  (zrvl-mono:f32-fceiling   f32)
  (zrvl-mono:f32.2-fceiling vec2)
  (zrvl-mono:f32.3-fceiling vec3)
  (zrvl-mono:f32.4-fceiling vec4))

(zrvl-int:define-primitives (round :return beta) ((x alpha))
    (round alpha beta)
    ((zrvl-mono:f32-round   f32  i32)
     (zrvl-mono:f32.2-round vec2 ivec2)
     (zrvl-mono:f32.3-round vec3 ivec3)
     (zrvl-mono:f32.4-round vec4 ivec4))
  (%convert-f-to-s id beta (fround x)))

(zrvl-int:define-primitives (truncate :return beta) ((x alpha))
    (truncate alpha beta)
    ((zrvl-mono:f32-truncate   f32  i32)
     (zrvl-mono:f32.2-truncate vec2 ivec2)
     (zrvl-mono:f32.3-truncate vec3 ivec3)
     (zrvl-mono:f32.4-truncate vec4 ivec4))
  (%convert-f-to-s id beta (ftruncate x)))

(zrvl-int:define-primitives (floor :return beta) ((x alpha))
    (floor alpha beta)
    ((zrvl-mono:f32-floor   f32  i32)
     (zrvl-mono:f32.2-floor vec2 ivec2)
     (zrvl-mono:f32.3-floor vec3 ivec3)
     (zrvl-mono:f32.4-floor vec4 ivec4))
  (%convert-f-to-s id beta (ffloor x)))

(zrvl-int:define-primitives (ceiling :return beta) ((x alpha))
    (ceiling alpha beta)
    ((zrvl-mono:f32-ceiling   f32  i32)
     (zrvl-mono:f32.2-ceiling vec2 ivec2)
     (zrvl-mono:f32.3-ceiling vec3 ivec3)
     (zrvl-mono:f32.4-ceiling vec4 ivec4))
  (%convert-f-to-s id beta (fceiling x)))

(zrvl-int:define-generic zrvl-int:* (x y)
  (zrvl-mono:f32*                f32  f32)
  (zrvl-mono:f32.2-vector*scalar vec2 f32)
  (zrvl-mono:f32.3-vector*scalar vec3 f32)
  (zrvl-mono:f32.4-vector*scalar vec4 f32)
  (zrvl-mono:f32.2-scalar*vector f32  vec2)
  (zrvl-mono:f32.3-scalar*vector f32  vec3)
  (zrvl-mono:f32.4-scalar*vector f32  vec4)
  (zrvl-mono:quat-vector*scalar quat f32)
  (zrvl-mono:quat-scalar*vector f32  quat)
  (zrvl-mono:quat* quat quat)
  (zrvl-mono:f32.2x2-matrix*scalar f32.2x2 f32)
  (zrvl-mono:f32.2x3-matrix*scalar f32.2x3 f32)
  (zrvl-mono:f32.2x4-matrix*scalar f32.2x4 f32)
  (zrvl-mono:f32.3x2-matrix*scalar f32.3x2 f32)
  (zrvl-mono:f32.3x3-matrix*scalar f32.3x3 f32)
  (zrvl-mono:f32.3x4-matrix*scalar f32.3x4 f32)
  (zrvl-mono:f32.4x2-matrix*scalar f32.4x2 f32)
  (zrvl-mono:f32.4x3-matrix*scalar f32.4x3 f32)
  (zrvl-mono:f32.4x4-matrix*scalar f32.4x4 f32)
  (zrvl-mono:f32.2x2-scalar*matrix f32 f32.2x2)
  (zrvl-mono:f32.2x3-scalar*matrix f32 f32.2x3)
  (zrvl-mono:f32.2x4-scalar*matrix f32 f32.2x4)
  (zrvl-mono:f32.3x2-scalar*matrix f32 f32.3x2)
  (zrvl-mono:f32.3x3-scalar*matrix f32 f32.3x3)
  (zrvl-mono:f32.3x4-scalar*matrix f32 f32.3x4)
  (zrvl-mono:f32.4x2-scalar*matrix f32 f32.4x2)
  (zrvl-mono:f32.4x3-scalar*matrix f32 f32.4x3)
  (zrvl-mono:f32.4x4-scalar*matrix f32 f32.4x4)
  (zrvl-mono:f32.2x2*2-matrix*vector f32.2x2 f32.2)
  (zrvl-mono:f32.2x3*3-matrix*vector f32.2x3 f32.3)
  (zrvl-mono:f32.2x4*4-matrix*vector f32.2x4 f32.4)
  (zrvl-mono:f32.3x2*2-matrix*vector f32.3x2 f32.2)
  (zrvl-mono:f32.3x3*3-matrix*vector f32.3x3 f32.3)
  (zrvl-mono:f32.3x4*4-matrix*vector f32.3x4 f32.4)
  (zrvl-mono:f32.4x2*2-matrix*vector f32.4x2 f32.2)
  (zrvl-mono:f32.4x3*3-matrix*vector f32.4x3 f32.3)
  (zrvl-mono:f32.4x4*4-matrix*vector f32.4x4 f32.4)
  (zrvl-mono:f32.2*2x2-vector*matrix f32.2 f32.2x2)
  (zrvl-mono:f32.2*2x3-vector*matrix f32.2 f32.2x3)
  (zrvl-mono:f32.2*2x4-vector*matrix f32.2 f32.2x4)
  (zrvl-mono:f32.3*3x2-vector*matrix f32.3 f32.3x2)
  (zrvl-mono:f32.3*3x3-vector*matrix f32.3 f32.3x3)
  (zrvl-mono:f32.3*3x4-vector*matrix f32.3 f32.3x4)
  (zrvl-mono:f32.4*4x2-vector*matrix f32.4 f32.4x2)
  (zrvl-mono:f32.4*4x3-vector*matrix f32.4 f32.4x3)
  (zrvl-mono:f32.4*4x4-vector*matrix f32.4 f32.4x4)
  (zrvl-mono:f32.2x2*2x2-matrix*matrix f32.2x2 f32.2x2)
  (zrvl-mono:f32.2x2*2x3-matrix*matrix f32.2x2 f32.2x3)
  (zrvl-mono:f32.2x2*2x4-matrix*matrix f32.2x2 f32.2x4)
  (zrvl-mono:f32.2x3*3x2-matrix*matrix f32.2x3 f32.3x2)
  (zrvl-mono:f32.2x3*3x3-matrix*matrix f32.2x3 f32.3x3)
  (zrvl-mono:f32.2x3*3x4-matrix*matrix f32.2x3 f32.3x4)
  (zrvl-mono:f32.2x4*4x2-matrix*matrix f32.2x4 f32.4x2)
  (zrvl-mono:f32.2x4*4x3-matrix*matrix f32.2x4 f32.4x3)
  (zrvl-mono:f32.2x4*4x4-matrix*matrix f32.2x4 f32.4x4)
  (zrvl-mono:f32.3x2*2x2-matrix*matrix f32.3x2 f32.2x2)
  (zrvl-mono:f32.3x2*2x3-matrix*matrix f32.3x2 f32.2x3)
  (zrvl-mono:f32.3x2*2x4-matrix*matrix f32.3x2 f32.2x4)
  (zrvl-mono:f32.3x3*3x2-matrix*matrix f32.3x3 f32.3x2)
  (zrvl-mono:f32.3x3*3x3-matrix*matrix f32.3x3 f32.3x3)
  (zrvl-mono:f32.3x3*3x4-matrix*matrix f32.3x3 f32.3x4)
  (zrvl-mono:f32.3x4*4x2-matrix*matrix f32.3x4 f32.4x2)
  (zrvl-mono:f32.3x4*4x3-matrix*matrix f32.3x4 f32.4x3)
  (zrvl-mono:f32.3x4*4x4-matrix*matrix f32.3x4 f32.4x4)
  (zrvl-mono:f32.4x2*2x2-matrix*matrix f32.4x2 f32.2x2)
  (zrvl-mono:f32.4x2*2x3-matrix*matrix f32.4x2 f32.2x3)
  (zrvl-mono:f32.4x2*2x4-matrix*matrix f32.4x2 f32.2x4)
  (zrvl-mono:f32.4x3*3x2-matrix*matrix f32.4x3 f32.3x2)
  (zrvl-mono:f32.4x3*3x3-matrix*matrix f32.4x3 f32.3x3)
  (zrvl-mono:f32.4x3*3x4-matrix*matrix f32.4x3 f32.3x4)
  (zrvl-mono:f32.4x4*4x2-matrix*matrix f32.4x4 f32.4x2)
  (zrvl-mono:f32.4x4*4x3-matrix*matrix f32.4x4 f32.4x3)
  (zrvl-mono:f32.4x4*4x4-matrix*matrix f32.4x4 f32.4x4))

(zrvl-int:define-simple-binary-primitives (zrvl-int:* %f*)
  (zrvl-mono:f32*    f32))

(zrvl-int:define-generic zrvl-int:.* (x y)
  (zrvl-mono:f32.2.* vec2 vec2)
  (zrvl-mono:f32.3.* vec3 vec3)
  (zrvl-mono:f32.4.* vec4 vec4)
  (zrvl-mono:f32.2x2.* f32.2x2 f32.2x2)
  (zrvl-mono:f32.2x3.* f32.2x3 f32.2x3)
  (zrvl-mono:f32.2x4.* f32.2x4 f32.2x4)
  (zrvl-mono:f32.3x2.* f32.3x2 f32.3x2)
  (zrvl-mono:f32.3x3.* f32.3x3 f32.3x3)
  (zrvl-mono:f32.3x4.* f32.3x4 f32.3x4)
  (zrvl-mono:f32.4x2.* f32.4x2 f32.4x2)
  (zrvl-mono:f32.4x3.* f32.4x3 f32.4x3)
  (zrvl-mono:f32.4x4.* f32.4x4 f32.4x4))

(zrvl-int:define-simple-binary-primitives (zrvl-int:.* %f*)
  (zrvl-mono:f32.2.* vec2)
  (zrvl-mono:f32.3.* vec3)
  (zrvl-mono:f32.4.* vec4))

(zrvl-int:define-binary-elementwise-matrix-primitives zrvl:.*
    (((zrvl-mono:f32.2x2.* mat2x2)
      (zrvl-mono:f32.3x2.* mat3x2)
      (zrvl-mono:f32.4x2.* mat4x2))
     ((zrvl-mono:f32.2x3.* mat2x3)
      (zrvl-mono:f32.3x3.* mat3x3)
      (zrvl-mono:f32.4x3.* mat4x3))
     ((zrvl-mono:f32.2x4.* mat2x4)
      (zrvl-mono:f32.3x4.* mat3x4)
      (zrvl-mono:f32.4x4.* mat4x4))))

(zrvl-int:define-primitives (zrvl-int:matrix*scalar :return alpha) ((m alpha) (s beta))
    (zrvl-int:matrix*scalar alpha beta)
    ((zrvl-mono:f32.2x2-matrix*scalar f32.2x2 f32)
     (zrvl-mono:f32.2x3-matrix*scalar f32.2x3 f32)
     (zrvl-mono:f32.2x4-matrix*scalar f32.2x4 f32)
     (zrvl-mono:f32.3x2-matrix*scalar f32.3x2 f32)
     (zrvl-mono:f32.3x3-matrix*scalar f32.3x3 f32)
     (zrvl-mono:f32.3x4-matrix*scalar f32.3x4 f32)
     (zrvl-mono:f32.4x2-matrix*scalar f32.4x2 f32)
     (zrvl-mono:f32.4x3-matrix*scalar f32.4x3 f32)
     (zrvl-mono:f32.4x4-matrix*scalar f32.4x4 f32))
  (%matrix*scalar id alpha m s))

(zrvl-int:define-primitives (zrvl-int:scalar*matrix :return alpha) ((s beta) (m alpha))
    (zrvl-int:scalar*matrix alpha beta)
    ((zrvl-mono:f32.2x2-scalar*matrix f32.2x2 f32)
     (zrvl-mono:f32.2x3-scalar*matrix f32.2x3 f32)
     (zrvl-mono:f32.2x4-scalar*matrix f32.2x4 f32)
     (zrvl-mono:f32.3x2-scalar*matrix f32.3x2 f32)
     (zrvl-mono:f32.3x3-scalar*matrix f32.3x3 f32)
     (zrvl-mono:f32.3x4-scalar*matrix f32.3x4 f32)
     (zrvl-mono:f32.4x2-scalar*matrix f32.4x2 f32)
     (zrvl-mono:f32.4x3-scalar*matrix f32.4x3 f32)
     (zrvl-mono:f32.4x4-scalar*matrix f32.4x4 f32))
  (%matrix*scalar id alpha m s))

(zrvl-int:define-primitives (zrvl-int:vector*scalar :return alpha) ((v alpha) (s beta))
    (zrvl-int:vector*scalar alpha beta)
    ((zrvl-mono:f32.2-vector*scalar vec2 f32)
     (zrvl-mono:f32.3-vector*scalar vec3 f32)
     (zrvl-mono:f32.4-vector*scalar vec4 f32))
  (%vector*scalar id alpha v s))

(zrvl-int:define-primitives (zrvl-int:scalar*vector :return alpha) ((s beta) (v alpha))
    (zrvl-int:scalar*vector alpha beta)
    ((zrvl-mono:f32.2-scalar*vector vec2 f32)
     (zrvl-mono:f32.3-scalar*vector vec3 f32)
     (zrvl-mono:f32.4-scalar*vector vec4 f32))
  (%vector*scalar id alpha v s))

(zrvl-int:define-primitives (zrvl-int:matrix*vector :return gamma) ((m alpha) (v beta))
    (zrvl-int:matrix*vector alpha beta gamma)
    ((zrvl-mono:f32.2x2*2-matrix*vector f32.2x2 f32.2 f32.2)
     (zrvl-mono:f32.2x3*3-matrix*vector f32.2x3 f32.3 f32.2)
     (zrvl-mono:f32.2x4*4-matrix*vector f32.2x4 f32.4 f32.2)
     (zrvl-mono:f32.3x2*2-matrix*vector f32.3x2 f32.2 f32.3)
     (zrvl-mono:f32.3x3*3-matrix*vector f32.3x3 f32.3 f32.3)
     (zrvl-mono:f32.3x4*4-matrix*vector f32.3x4 f32.4 f32.3)
     (zrvl-mono:f32.4x2*2-matrix*vector f32.4x2 f32.2 f32.4)
     (zrvl-mono:f32.4x3*3-matrix*vector f32.4x3 f32.3 f32.4)
     (zrvl-mono:f32.4x4*4-matrix*vector f32.4x4 f32.4 f32.4))
  (%matrix*vector id gamma m v))

;;; Note: The vectors in the input and output are row vectors, not
;;; column vectors. These need to be tagged specially in the higher
;;; level type system despite having the same internal representation.
;;; A no-op transpose can convert between row and column vectors when
;;; dealing with vectors as matrices, like in vector*matrix and
;;; matrix*vector. Otherwise, it doesn't matter and they can be
;;; treated like vectors.
(zrvl-int:define-primitives (zrvl-int:vector*matrix :return gamma) ((v alpha) (m beta))
    (zrvl-int:vector*matrix alpha beta gamma)
    ((zrvl-mono:f32.2*2x2-vector*matrix f32.2 f32.2x2 f32.2)
     (zrvl-mono:f32.2*2x3-vector*matrix f32.2 f32.2x3 f32.3)
     (zrvl-mono:f32.2*2x4-vector*matrix f32.2 f32.2x4 f32.4)
     (zrvl-mono:f32.3*3x2-vector*matrix f32.3 f32.3x2 f32.2)
     (zrvl-mono:f32.3*3x3-vector*matrix f32.3 f32.3x3 f32.3)
     (zrvl-mono:f32.3*3x4-vector*matrix f32.3 f32.3x4 f32.4)
     (zrvl-mono:f32.4*4x2-vector*matrix f32.4 f32.4x2 f32.2)
     (zrvl-mono:f32.4*4x3-vector*matrix f32.4 f32.4x3 f32.3)
     (zrvl-mono:f32.4*4x4-vector*matrix f32.4 f32.4x4 f32.4))
  (%vector*matrix id gamma v m))

(zrvl-int:define-primitives (zrvl-int:matrix*matrix :return gamma) ((m1 alpha) (m2 beta))
    (zrvl-int:matrix*matrix alpha beta gamma)
    ((zrvl-mono:f32.2x2*2x2-matrix*matrix f32.2x2 f32.2x2 f32.2x2)
     (zrvl-mono:f32.2x2*2x3-matrix*matrix f32.2x2 f32.2x3 f32.2x3)
     (zrvl-mono:f32.2x2*2x4-matrix*matrix f32.2x2 f32.2x4 f32.2x4)
     (zrvl-mono:f32.2x3*3x2-matrix*matrix f32.2x3 f32.3x2 f32.2x2)
     (zrvl-mono:f32.2x3*3x3-matrix*matrix f32.2x3 f32.3x3 f32.2x3)
     (zrvl-mono:f32.2x3*3x4-matrix*matrix f32.2x3 f32.3x4 f32.2x4)
     (zrvl-mono:f32.2x4*4x2-matrix*matrix f32.2x4 f32.4x2 f32.2x2)
     (zrvl-mono:f32.2x4*4x3-matrix*matrix f32.2x4 f32.4x3 f32.2x3)
     (zrvl-mono:f32.2x4*4x4-matrix*matrix f32.2x4 f32.4x4 f32.2x4)
     (zrvl-mono:f32.3x2*2x2-matrix*matrix f32.3x2 f32.2x2 f32.3x2)
     (zrvl-mono:f32.3x2*2x3-matrix*matrix f32.3x2 f32.2x3 f32.3x3)
     (zrvl-mono:f32.3x2*2x4-matrix*matrix f32.3x2 f32.2x4 f32.3x4)
     (zrvl-mono:f32.3x3*3x2-matrix*matrix f32.3x3 f32.3x2 f32.3x2)
     (zrvl-mono:f32.3x3*3x3-matrix*matrix f32.3x3 f32.3x3 f32.3x3)
     (zrvl-mono:f32.3x3*3x4-matrix*matrix f32.3x3 f32.3x4 f32.3x4)
     (zrvl-mono:f32.3x4*4x2-matrix*matrix f32.3x4 f32.4x2 f32.3x2)
     (zrvl-mono:f32.3x4*4x3-matrix*matrix f32.3x4 f32.4x3 f32.3x3)
     (zrvl-mono:f32.3x4*4x4-matrix*matrix f32.3x4 f32.4x4 f32.3x4)
     (zrvl-mono:f32.4x2*2x2-matrix*matrix f32.4x2 f32.2x2 f32.4x2)
     (zrvl-mono:f32.4x2*2x3-matrix*matrix f32.4x2 f32.2x3 f32.4x3)
     (zrvl-mono:f32.4x2*2x4-matrix*matrix f32.4x2 f32.2x4 f32.4x4)
     (zrvl-mono:f32.4x3*3x2-matrix*matrix f32.4x3 f32.3x2 f32.4x2)
     (zrvl-mono:f32.4x3*3x3-matrix*matrix f32.4x3 f32.3x3 f32.4x3)
     (zrvl-mono:f32.4x3*3x4-matrix*matrix f32.4x3 f32.3x4 f32.4x4)
     (zrvl-mono:f32.4x4*4x2-matrix*matrix f32.4x4 f32.4x2 f32.4x2)
     (zrvl-mono:f32.4x4*4x3-matrix*matrix f32.4x4 f32.4x3 f32.4x3)
     (zrvl-mono:f32.4x4*4x4-matrix*matrix f32.4x4 f32.4x4 f32.4x4))
  (%matrix*matrix id gamma m1 m2))

(zrvl-int:define-generic transpose (matrix)
  (zrvl-mono:f32.2x2-transpose f32.2x2)
  (zrvl-mono:f32.2x3-transpose f32.2x3)
  (zrvl-mono:f32.2x4-transpose f32.2x4)
  (zrvl-mono:f32.3x2-transpose f32.3x2)
  (zrvl-mono:f32.3x3-transpose f32.3x3)
  (zrvl-mono:f32.3x4-transpose f32.3x4)
  (zrvl-mono:f32.4x2-transpose f32.4x2)
  (zrvl-mono:f32.4x3-transpose f32.4x3)
  (zrvl-mono:f32.4x4-transpose f32.4x4))

(zrvl-int:define-primitives (transpose :return beta) ((m alpha))
    (transpose alpha beta)
    ((zrvl-mono:f32.2x2-transpose f32.2x2 f32.2x2)
     (zrvl-mono:f32.2x3-transpose f32.2x3 f32.3x2)
     (zrvl-mono:f32.2x4-transpose f32.2x4 f32.4x2)
     (zrvl-mono:f32.3x2-transpose f32.3x2 f32.2x3)
     (zrvl-mono:f32.3x3-transpose f32.3x3 f32.3x3)
     (zrvl-mono:f32.3x4-transpose f32.3x4 f32.4x3)
     (zrvl-mono:f32.4x2-transpose f32.4x2 f32.2x4)
     (zrvl-mono:f32.4x3-transpose f32.4x3 f32.3x4)
     (zrvl-mono:f32.4x4-transpose f32.4x4 f32.4x4))
  (%transpose id beta m))

(zrvl-int:define-generic clamp (x min max)
  (zrvl-mono:f32-clamp   f32  f32  f32)
  (zrvl-mono:f32.2-clamp vec2 vec2 vec2)
  (zrvl-mono:f32.3-clamp vec3 vec3 vec3)
  (zrvl-mono:f32.4-clamp vec4 vec4 vec4))

(zrvl-int:define-generic sat (x)
  (zrvl-mono:f32-sat   f32)
  (zrvl-mono:f32.2-sat vec2)
  (zrvl-mono:f32.3-sat vec3)
  (zrvl-mono:f32.4-sat vec4))

(zrvl-int:define-generic zrvl-int:max (x y)
  (zrvl-mono:f32-max   f32  f32)
  (zrvl-mono:f32.2-max vec2 vec2)
  (zrvl-mono:f32.3-max vec3 vec3)
  (zrvl-mono:f32.4-max vec4 vec4))

(zrvl-int:define-generic zrvl-int:min (x y)
  (zrvl-mono:f32-min   f32  f32)
  (zrvl-mono:f32.2-min vec2 vec2)
  (zrvl-mono:f32.3-min vec3 vec3)
  (zrvl-mono:f32.4-min vec4 vec4))

(zrvl-int:define-simple-ternary-primitives (clamp %f-clamp)
  (zrvl-mono:f32-clamp   f32)
  (zrvl-mono:f32.2-clamp vec2)
  (zrvl-mono:f32.3-clamp vec3)
  (zrvl-mono:f32.4-clamp vec4))

(zrvl-int:define-primitive (zrvl-mono:f32-sat :return f32) ((x f32))
  (clamp x 0f0 1f0))

(zrvl-int:define-primitive (zrvl-mono:f32.2-sat :return vec2) ((x vec2))
  (clamp x (vec2 0f0 0f0) (vec2 1f0 1f0)))

(zrvl-int:define-primitive (zrvl-mono:f32.3-sat :return vec3) ((x vec3))
  (clamp x (vec3 0f0 0f0 0f0) (vec3 1f0 1f0 1f0)))

(zrvl-int:define-primitive (zrvl-mono:f32.4-sat :return vec4) ((x vec4))
  (clamp x (vec4 0f0 0f0 0f0 0f0) (vec4 1f0 1f0 1f0 1f0)))

(zrvl-int:define-simple-binary-primitives (max %f-max)
  (zrvl-mono:f32-max   f32)
  (zrvl-mono:f32.2-max vec2)
  (zrvl-mono:f32.3-max vec3)
  (zrvl-mono:f32.4-max vec4))

(zrvl-int:define-simple-binary-primitives (min %f-min)
  (zrvl-mono:f32-min   f32)
  (zrvl-mono:f32.2-min vec2)
  (zrvl-mono:f32.3-min vec3)
  (zrvl-mono:f32.4-min vec4))

(zrvl-int:define-generic degrees (x)
  (zrvl-mono:f32-degrees   f32)
  (zrvl-mono:f32.2-degrees vec2)
  (zrvl-mono:f32.3-degrees vec3)
  (zrvl-mono:f32.4-degrees vec4))

(zrvl-int:define-generic radians (x)
  (zrvl-mono:f32-radians   f32)
  (zrvl-mono:f32.2-radians vec2)
  (zrvl-mono:f32.3-radians vec3)
  (zrvl-mono:f32.4-radians vec4))

(zrvl-int:define-generic distance (x y)
  (zrvl-mono:f32-distance   f32  f32)
  (zrvl-mono:f32.2-distance vec2 vec2)
  (zrvl-mono:f32.3-distance vec3 vec3)
  (zrvl-mono:f32.4-distance vec4 vec4))

(zrvl-int:define-generic magnitude (x)
  (zrvl-mono:f32-magnitude   f32)
  (zrvl-mono:f32.2-magnitude vec2)
  (zrvl-mono:f32.3-magnitude vec3)
  (zrvl-mono:f32.4-magnitude vec4)
  (zrvl-mono:quat-magnitude  quat))

(zrvl-int:define-generic normalize (x)
  (zrvl-mono:f32-normalize   f32)
  (zrvl-mono:f32.2-normalize vec2)
  (zrvl-mono:f32.3-normalize vec3)
  (zrvl-mono:f32.4-normalize vec4)
  (zrvl-mono:quat-normalize  quat))

(zrvl-int:define-generic reflect (i n)
  (zrvl-mono:f32-reflect   f32  f32)
  (zrvl-mono:f32.2-reflect vec2 vec2)
  (zrvl-mono:f32.3-reflect vec3 vec3)
  (zrvl-mono:f32.4-reflect vec4 vec4))

(zrvl-int:define-generic zrvl-int:cross (x y)
  (zrvl-mono:f32.3-cross vec3 vec3))

(zrvl-int:define-generic inverse (m)
  (zrvl-mono:f32-inverse     f32)
  (zrvl-mono:f32.2x2-inverse mat2)
  (zrvl-mono:f32.3x3-inverse mat3)
  (zrvl-mono:f32.4x4-inverse mat4))

(zrvl-int:define-generic det (m)
  (zrvl-mono:f32-determinant     f32)
  (zrvl-mono:f32.2x2-determinant mat2)
  (zrvl-mono:f32.3x3-determinant mat3)
  (zrvl-mono:f32.4x4-determinant mat4))

(zrvl-int:define-simple-unary-primitives (degrees %degrees)
  (zrvl-mono:f32-degrees   f32)
  (zrvl-mono:f32.2-degrees vec2)
  (zrvl-mono:f32.3-degrees vec3)
  (zrvl-mono:f32.4-degrees vec4))

(zrvl-int:define-simple-unary-primitives (radians %radians)
  (zrvl-mono:f32-radians   f32)
  (zrvl-mono:f32.2-radians vec2)
  (zrvl-mono:f32.3-radians vec3)
  (zrvl-mono:f32.4-radians vec4))

(zrvl-int:define-primitives (distance :return beta) ((p1 alpha) (p2 alpha))
    (distance alpha beta)
    ((zrvl-mono:f32-distance   f32  f32)
     (zrvl-mono:f32.2-distance vec2 f32)
     (zrvl-mono:f32.3-distance vec3 f32)
     (zrvl-mono:f32.4-distance vec4 f32))
  (%distance id beta p1 p2))

(zrvl-int:define-primitives (magnitude :return beta) ((v alpha))
    (magnitude alpha beta)
    ((zrvl-mono:f32-magnitude   f32  f32)
     (zrvl-mono:f32.2-magnitude vec2 f32)
     (zrvl-mono:f32.3-magnitude vec3 f32)
     (zrvl-mono:f32.4-magnitude vec4 f32))
  (%vec-length id beta v))

(zrvl-int:define-simple-unary-primitives (normalize %normalize)
  (zrvl-mono:f32-normalize   f32)
  (zrvl-mono:f32.2-normalize vec2)
  (zrvl-mono:f32.3-normalize vec3)
  (zrvl-mono:f32.4-normalize vec4))

(zrvl-int:define-simple-binary-primitives (reflect %reflect)
  (zrvl-mono:f32-reflect   f32)
  (zrvl-mono:f32.2-reflect vec2)
  (zrvl-mono:f32.3-reflect vec3)
  (zrvl-mono:f32.4-reflect vec4))

(zrvl-int:define-simple-binary-primitives (cross %cross)
  (zrvl-mono:f32.3-cross vec3))

(zrvl-int:define-primitives (inverse :return alpha) ((x alpha))
    (inverse alpha)
    ((zrvl-mono:f32-inverse f32))
  (/ x))

(zrvl-int:define-simple-unary-primitives (inverse %matrix-inverse)
  (zrvl-mono:f32.2x2-inverse f32.2x2)
  (zrvl-mono:f32.3x3-inverse f32.3x3)
  (zrvl-mono:f32.4x4-inverse f32.4x4))

(zrvl-int:define-primitives (det :return alpha) ((x alpha))
    (det alpha)
    ((zrvl-mono:f32-determinant f32))
  x)

(zrvl-int:define-simple-unary-primitives (det %determinant)
  (zrvl-mono:f32.2x2-determinant mat2)
  (zrvl-mono:f32.3x3-determinant mat3)
  (zrvl-mono:f32.4x4-determinant mat4))

(zrvl-int:define-generic sin (x)
  (zrvl-mono:f32-sin   f32)
  (zrvl-mono:f32.2-sin vec2)
  (zrvl-mono:f32.3-sin vec3)
  (zrvl-mono:f32.4-sin vec4))

(zrvl-int:define-generic cos (x)
  (zrvl-mono:f32-cos   f32)
  (zrvl-mono:f32.2-cos vec2)
  (zrvl-mono:f32.3-cos vec3)
  (zrvl-mono:f32.4-cos vec4))

(zrvl-int:define-generic tan (x)
  (zrvl-mono:f32-tan   f32)
  (zrvl-mono:f32.2-tan vec2)
  (zrvl-mono:f32.3-tan vec3)
  (zrvl-mono:f32.4-tan vec4))

(zrvl-int:define-generic asin (x)
  (zrvl-mono:f32-asin   f32)
  (zrvl-mono:f32.2-asin vec2)
  (zrvl-mono:f32.3-asin vec3)
  (zrvl-mono:f32.4-asin vec4))

(zrvl-int:define-generic acos (x)
  (zrvl-mono:f32-acos   f32)
  (zrvl-mono:f32.2-acos vec2)
  (zrvl-mono:f32.3-acos vec3)
  (zrvl-mono:f32.4-acos vec4))

(zrvl-int:define-generic atan (x)
  (zrvl-mono:f32-atan   f32)
  (zrvl-mono:f32.2-atan vec2)
  (zrvl-mono:f32.3-atan vec3)
  (zrvl-mono:f32.4-atan vec4))

(zrvl-int:define-generic zrvl-int:atan2 (x y)
  (zrvl-mono:f32-atan2   f32  f32)
  (zrvl-mono:f32.2-atan2 vec2 vec2)
  (zrvl-mono:f32.3-atan2 vec3 vec2)
  (zrvl-mono:f32.4-atan2 vec4 vec4))

(zrvl-int:define-generic sinh (x)
  (zrvl-mono:f32-sinh   f32)
  (zrvl-mono:f32.2-sinh vec2)
  (zrvl-mono:f32.3-sinh vec3)
  (zrvl-mono:f32.4-sinh vec4))

(zrvl-int:define-generic cosh (x)
  (zrvl-mono:f32-cosh   f32)
  (zrvl-mono:f32.2-cosh vec2)
  (zrvl-mono:f32.3-cosh vec3)
  (zrvl-mono:f32.4-cosh vec4))

(zrvl-int:define-generic tanh (x)
  (zrvl-mono:f32-tanh   f32)
  (zrvl-mono:f32.2-tanh vec2)
  (zrvl-mono:f32.3-tanh vec3)
  (zrvl-mono:f32.4-tanh vec4))

(zrvl-int:define-generic asinh (x)
  (zrvl-mono:f32-asinh   f32)
  (zrvl-mono:f32.2-asinh vec2)
  (zrvl-mono:f32.3-asinh vec3)
  (zrvl-mono:f32.4-asinh vec4))

(zrvl-int:define-generic acosh (x)
  (zrvl-mono:f32-acosh   f32)
  (zrvl-mono:f32.2-acosh vec2)
  (zrvl-mono:f32.3-acosh vec3)
  (zrvl-mono:f32.4-acosh vec4))

(zrvl-int:define-generic atanh (x)
  (zrvl-mono:f32-atanh   f32)
  (zrvl-mono:f32.2-atanh vec2)
  (zrvl-mono:f32.3-atanh vec3)
  (zrvl-mono:f32.4-atanh vec4))

(zrvl-int:define-simple-unary-primitives (sin %sin)
  (zrvl-mono:f32-sin   f32)
  (zrvl-mono:f32.2-sin vec2)
  (zrvl-mono:f32.3-sin vec3)
  (zrvl-mono:f32.4-sin vec4))

(zrvl-int:define-simple-unary-primitives (cos %cos)
  (zrvl-mono:f32-cos   f32)
  (zrvl-mono:f32.2-cos vec2)
  (zrvl-mono:f32.3-cos vec3)
  (zrvl-mono:f32.4-cos vec4))

(zrvl-int:define-simple-unary-primitives (tan %tan)
  (zrvl-mono:f32-tan   f32)
  (zrvl-mono:f32.2-tan vec2)
  (zrvl-mono:f32.3-tan vec3)
  (zrvl-mono:f32.4-tan vec4))

(zrvl-int:define-simple-unary-primitives (asin %asin)
  (zrvl-mono:f32-asin   f32)
  (zrvl-mono:f32.2-asin vec2)
  (zrvl-mono:f32.3-asin vec3)
  (zrvl-mono:f32.4-asin vec4))

(zrvl-int:define-simple-unary-primitives (acos %acos)
  (zrvl-mono:f32-acos   f32)
  (zrvl-mono:f32.2-acos vec2)
  (zrvl-mono:f32.3-acos vec3)
  (zrvl-mono:f32.4-acos vec4))

(zrvl-int:define-simple-unary-primitives (atan %atan)
  (zrvl-mono:f32-atan   f32)
  (zrvl-mono:f32.2-atan vec2)
  (zrvl-mono:f32.3-atan vec3)
  (zrvl-mono:f32.4-atan vec4))

(zrvl-int:define-simple-binary-primitives (zrvl-int:atan2 %atan2)
  (zrvl-mono:f32-atan2   f32)
  (zrvl-mono:f32.2-atan2 vec2)
  (zrvl-mono:f32.3-atan2 vec3)
  (zrvl-mono:f32.4-atan2 vec4))

(zrvl-int:define-simple-unary-primitives (sinh %sinh)
  (zrvl-mono:f32-sinh   f32)
  (zrvl-mono:f32.2-sinh vec2)
  (zrvl-mono:f32.3-sinh vec3)
  (zrvl-mono:f32.4-sinh vec4))

(zrvl-int:define-simple-unary-primitives (cosh %cosh)
  (zrvl-mono:f32-cosh   f32)
  (zrvl-mono:f32.2-cosh vec2)
  (zrvl-mono:f32.3-cosh vec3)
  (zrvl-mono:f32.4-cosh vec4))

(zrvl-int:define-simple-unary-primitives (tanh %tanh)
  (zrvl-mono:f32-tanh   f32)
  (zrvl-mono:f32.2-tanh vec2)
  (zrvl-mono:f32.3-tanh vec3)
  (zrvl-mono:f32.4-tanh vec4))

(zrvl-int:define-simple-unary-primitives (asinh %asinh)
  (zrvl-mono:f32-asinh   f32)
  (zrvl-mono:f32.2-asinh vec2)
  (zrvl-mono:f32.3-asinh vec3)
  (zrvl-mono:f32.4-asinh vec4))

(zrvl-int:define-simple-unary-primitives (acosh %acosh)
  (zrvl-mono:f32-acosh   f32)
  (zrvl-mono:f32.2-acosh vec2)
  (zrvl-mono:f32.3-acosh vec3)
  (zrvl-mono:f32.4-acosh vec4))

(zrvl-int:define-simple-unary-primitives (atanh %atanh)
  (zrvl-mono:f32-atanh   f32)
  (zrvl-mono:f32.2-atanh vec2)
  (zrvl-mono:f32.3-atanh vec3)
  (zrvl-mono:f32.4-atanh vec4))

;; TODO: Note: Since EXP, EXPT, LN, etc., can be defined meaningfully
;; on certain things with multiplications (e.g. matrices,
;; quaternions), should the elementwise version on vectors use the dot
;; prefix to avoid confusion?

(zrvl-int:define-generic zrvl-int:ln (x)
  (zrvl-mono:f32-ln   f32)
  (zrvl-mono:f32.2-ln vec2)
  (zrvl-mono:f32.3-ln vec3)
  (zrvl-mono:f32.4-ln vec4))

(zrvl-int:define-generic zrvl-int:log2 (x)
  (zrvl-mono:f32-log2   f32)
  (zrvl-mono:f32.2-log2 vec2)
  (zrvl-mono:f32.3-log2 vec3)
  (zrvl-mono:f32.4-log2 vec4))

(zrvl-int:define-generic exp (x)
  (zrvl-mono:f32-exp   f32)
  (zrvl-mono:f32.2-exp vec2)
  (zrvl-mono:f32.3-exp vec3)
  (zrvl-mono:f32.4-exp vec4))

(zrvl-int:define-generic expt (x y)
  (zrvl-mono:f32-expt   f32  f32)
  (zrvl-mono:f32.2-expt vec2 vec2)
  (zrvl-mono:f32.3-expt vec3 vec3)
  (zrvl-mono:f32.4-expt vec4 vec4))

(zrvl-int:define-generic zrvl-int:expt2 (x)
  (zrvl-mono:f32-expt2   f32)
  (zrvl-mono:f32.2-expt2 vec2)
  (zrvl-mono:f32.3-expt2 vec3)
  (zrvl-mono:f32.4-expt2 vec4))

(zrvl-int:define-simple-unary-primitives (zrvl-int:ln %ln)
  (zrvl-mono:f32-ln   f32)
  (zrvl-mono:f32.2-ln vec2)
  (zrvl-mono:f32.3-ln vec3)
  (zrvl-mono:f32.4-ln vec4))

(zrvl-int:define-simple-unary-primitives (zrvl-int:log2 %log2)
  (zrvl-mono:f32-log2   f32)
  (zrvl-mono:f32.2-log2 vec2)
  (zrvl-mono:f32.3-log2 vec3)
  (zrvl-mono:f32.4-log2 vec4))

(zrvl-int:define-simple-unary-primitives (exp %exp)
  (zrvl-mono:f32-exp   f32)
  (zrvl-mono:f32.2-exp vec2)
  (zrvl-mono:f32.3-exp vec3)
  (zrvl-mono:f32.4-exp vec4))

(zrvl-int:define-simple-binary-primitives (expt %expt)
  (zrvl-mono:f32-expt   f32)
  (zrvl-mono:f32.2-expt vec2)
  (zrvl-mono:f32.3-expt vec3)
  (zrvl-mono:f32.4-expt vec4))

(zrvl-int:define-simple-unary-primitives (zrvl-int:expt2 %exp2)
  (zrvl-mono:f32-expt2   f32)
  (zrvl-mono:f32.2-expt2 vec2)
  (zrvl-mono:f32.3-expt2 vec3)
  (zrvl-mono:f32.4-expt2 vec4))

;;; Todo: Lerp should be a x y and Mix should be x y a, and they both
;;; use f-mix. Right now this uses Mix's order.
(zrvl-int:define-generic lerp (x y a)
  (zrvl-mono:f32-lerp   f32  f32  f32)
  (zrvl-mono:f32.2-lerp vec2 vec2 vec2)
  (zrvl-mono:f32.3-lerp vec3 vec3 vec3)
  (zrvl-mono:f32.4-lerp vec4 vec4 vec4))

(zrvl-int:define-simple-ternary-primitives (lerp %f-mix)
  (zrvl-mono:f32-lerp   f32)
  (zrvl-mono:f32.2-lerp vec2)
  (zrvl-mono:f32.3-lerp vec3)
  (zrvl-mono:f32.4-lerp vec4))

(zrvl-int:define-generic mat2<- (object)
  (zrvl-mono:f32.2x2<-f32-diag   f32)
  (zrvl-mono:f32.2x2<-f32.2-diag f32.2)
  (zrvl-mono:f32.2x2<-2x2-conv   f32.2x2)
  (zrvl-mono:f32.2x2<-2x3-conv   f32.2x3)
  (zrvl-mono:f32.2x2<-2x4-conv   f32.2x4)
  (zrvl-mono:f32.2x2<-3x2-conv   f32.3x2)
  (zrvl-mono:f32.2x2<-3x3-conv   f32.3x3)
  (zrvl-mono:f32.2x2<-3x4-conv   f32.3x4)
  (zrvl-mono:f32.2x2<-4x2-conv   f32.4x2)
  (zrvl-mono:f32.2x2<-4x3-conv   f32.4x3)
  (zrvl-mono:f32.2x2<-4x4-conv   f32.4x4))

(zrvl-int:define-alias mat2x2<- mat2<-)

(zrvl-int:define-generic mat2x3<- (object)
  (zrvl-mono:f32.2x3<-f32-diag   f32)
  (zrvl-mono:f32.2x3<-f32.2-diag f32.2)
  (zrvl-mono:f32.2x3<-2x2-conv   f32.2x2)
  (zrvl-mono:f32.2x3<-2x3-conv   f32.2x3)
  (zrvl-mono:f32.2x3<-2x4-conv   f32.2x4)
  (zrvl-mono:f32.2x3<-3x2-conv   f32.3x2)
  (zrvl-mono:f32.2x3<-3x3-conv   f32.3x3)
  (zrvl-mono:f32.2x3<-3x4-conv   f32.3x4)
  (zrvl-mono:f32.2x3<-4x2-conv   f32.4x2)
  (zrvl-mono:f32.2x3<-4x3-conv   f32.4x3)
  (zrvl-mono:f32.2x3<-4x4-conv   f32.4x4))

(zrvl-int:define-generic mat2x4<- (object)
  (zrvl-mono:f32.2x4<-f32-diag   f32)
  (zrvl-mono:f32.2x4<-f32.2-diag f32.2)
  (zrvl-mono:f32.2x4<-2x2-conv   f32.2x2)
  (zrvl-mono:f32.2x4<-2x3-conv   f32.2x3)
  (zrvl-mono:f32.2x4<-2x4-conv   f32.2x4)
  (zrvl-mono:f32.2x4<-3x2-conv   f32.3x2)
  (zrvl-mono:f32.2x4<-3x3-conv   f32.3x3)
  (zrvl-mono:f32.2x4<-3x4-conv   f32.3x4)
  (zrvl-mono:f32.2x4<-4x2-conv   f32.4x2)
  (zrvl-mono:f32.2x4<-4x3-conv   f32.4x3)
  (zrvl-mono:f32.2x4<-4x4-conv   f32.4x4))

(zrvl-int:define-generic mat3x2<- (object)
  (zrvl-mono:f32.3x2<-f32-diag   f32)
  (zrvl-mono:f32.3x2<-f32.2-diag f32.2)
  (zrvl-mono:f32.3x2<-2x2-conv   f32.2x2)
  (zrvl-mono:f32.3x2<-2x3-conv   f32.2x3)
  (zrvl-mono:f32.3x2<-2x4-conv   f32.2x4)
  (zrvl-mono:f32.3x2<-3x2-conv   f32.3x2)
  (zrvl-mono:f32.3x2<-3x3-conv   f32.3x3)
  (zrvl-mono:f32.3x2<-3x4-conv   f32.3x4)
  (zrvl-mono:f32.3x2<-4x2-conv   f32.4x2)
  (zrvl-mono:f32.3x2<-4x3-conv   f32.4x3)
  (zrvl-mono:f32.3x2<-4x4-conv   f32.4x4))

(zrvl-int:define-generic mat3<- (object)
  (zrvl-mono:f32.3x3<-f32-diag   f32)
  (zrvl-mono:f32.3x3<-f32.3-diag f32.3)
  (zrvl-mono:f32.3x3<-2x2-conv   f32.2x2)
  (zrvl-mono:f32.3x3<-2x3-conv   f32.2x3)
  (zrvl-mono:f32.3x3<-2x4-conv   f32.2x4)
  (zrvl-mono:f32.3x3<-3x2-conv   f32.3x2)
  (zrvl-mono:f32.3x3<-3x3-conv   f32.3x3)
  (zrvl-mono:f32.3x3<-3x4-conv   f32.3x4)
  (zrvl-mono:f32.3x3<-4x2-conv   f32.4x2)
  (zrvl-mono:f32.3x3<-4x3-conv   f32.4x3)
  (zrvl-mono:f32.3x3<-4x4-conv   f32.4x4))

(zrvl-int:define-alias mat3x3<- mat3<-)

(zrvl-int:define-generic mat3x4<- (object)
  (zrvl-mono:f32.3x4<-f32-diag   f32)
  (zrvl-mono:f32.3x4<-f32.3-diag f32.3)
  (zrvl-mono:f32.3x4<-2x2-conv   f32.2x2)
  (zrvl-mono:f32.3x4<-2x3-conv   f32.2x3)
  (zrvl-mono:f32.3x4<-2x4-conv   f32.2x4)
  (zrvl-mono:f32.3x4<-3x2-conv   f32.3x2)
  (zrvl-mono:f32.3x4<-3x3-conv   f32.3x3)
  (zrvl-mono:f32.3x4<-3x4-conv   f32.3x4)
  (zrvl-mono:f32.3x4<-4x2-conv   f32.4x2)
  (zrvl-mono:f32.3x4<-4x3-conv   f32.4x3)
  (zrvl-mono:f32.3x4<-4x4-conv   f32.4x4))

(zrvl-int:define-generic mat4x2<- (object)
  (zrvl-mono:f32.4x2<-f32-diag   f32)
  (zrvl-mono:f32.4x2<-f32.2-diag f32.2)
  (zrvl-mono:f32.4x2<-2x2-conv   f32.2x2)
  (zrvl-mono:f32.4x2<-2x3-conv   f32.2x3)
  (zrvl-mono:f32.4x2<-2x4-conv   f32.2x4)
  (zrvl-mono:f32.4x2<-3x2-conv   f32.3x2)
  (zrvl-mono:f32.4x2<-3x3-conv   f32.3x3)
  (zrvl-mono:f32.4x2<-3x4-conv   f32.3x4)
  (zrvl-mono:f32.4x2<-4x2-conv   f32.4x2)
  (zrvl-mono:f32.4x2<-4x3-conv   f32.4x3)
  (zrvl-mono:f32.4x2<-4x4-conv   f32.4x4))

(zrvl-int:define-generic mat4x3<- (object)
  (zrvl-mono:f32.4x3<-f32-diag   f32)
  (zrvl-mono:f32.4x3<-f32.3-diag f32.3)
  (zrvl-mono:f32.4x3<-2x2-conv   f32.2x2)
  (zrvl-mono:f32.4x3<-2x3-conv   f32.2x3)
  (zrvl-mono:f32.4x3<-2x4-conv   f32.2x4)
  (zrvl-mono:f32.4x3<-3x2-conv   f32.3x2)
  (zrvl-mono:f32.4x3<-3x3-conv   f32.3x3)
  (zrvl-mono:f32.4x3<-3x4-conv   f32.3x4)
  (zrvl-mono:f32.4x3<-4x2-conv   f32.4x2)
  (zrvl-mono:f32.4x3<-4x3-conv   f32.4x3)
  (zrvl-mono:f32.4x3<-4x4-conv   f32.4x4))

(zrvl-int:define-generic mat4<- (object)
  (zrvl-mono:f32.4x4<-f32-diag   f32)
  (zrvl-mono:f32.4x4<-f32.4-diag f32.4)
  (zrvl-mono:f32.4x4<-2x2-conv   f32.2x2)
  (zrvl-mono:f32.4x4<-2x3-conv   f32.2x3)
  (zrvl-mono:f32.4x4<-2x4-conv   f32.2x4)
  (zrvl-mono:f32.4x4<-3x2-conv   f32.3x2)
  (zrvl-mono:f32.4x4<-3x3-conv   f32.3x3)
  (zrvl-mono:f32.4x4<-3x4-conv   f32.3x4)
  (zrvl-mono:f32.4x4<-4x2-conv   f32.4x2)
  (zrvl-mono:f32.4x4<-4x3-conv   f32.4x3)
  (zrvl-mono:f32.4x4<-4x4-conv   f32.4x4))

(zrvl-int:define-alias mat4x4<- mat4<-)

;;; Convert to the same matrix type, i.e. do nothing.
(zrvl-int:define-primitives (%convert-matrix :return alpha) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.2x2<-2x2-conv f32.2x2)
     (zrvl-mono:f32.2x3<-2x3-conv f32.2x3)
     (zrvl-mono:f32.2x4<-2x4-conv f32.2x4)
     (zrvl-mono:f32.3x2<-3x2-conv f32.3x2)
     (zrvl-mono:f32.3x3<-3x3-conv f32.3x3)
     (zrvl-mono:f32.3x4<-3x4-conv f32.3x4)
     (zrvl-mono:f32.4x2<-4x2-conv f32.4x2)
     (zrvl-mono:f32.4x3<-4x3-conv f32.4x3)
     (zrvl-mono:f32.4x4<-4x4-conv f32.4x4))
  matrix)

(zrvl-int:define-primitives (%convert-matrix :return f32.2x2) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.2x2<-2x3-conv f32.2x3)
     (zrvl-mono:f32.2x2<-2x4-conv f32.2x4))
  (mat2x2 (aref matrix 0)
          (aref matrix 1)))

(zrvl-int:define-primitives (%convert-matrix :return f32.2x2) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.2x2<-3x2-conv f32.3x2)
     (zrvl-mono:f32.2x2<-3x3-conv f32.3x3)
     (zrvl-mono:f32.2x2<-3x4-conv f32.3x4)
     (zrvl-mono:f32.2x2<-4x2-conv f32.4x2)
     (zrvl-mono:f32.2x2<-4x3-conv f32.4x3)
     (zrvl-mono:f32.2x2<-4x4-conv f32.4x4))
  (mat2x2 (swizzle (aref matrix 0) :xy)
          (swizzle (aref matrix 1) :xy)))

(zrvl-int:define-primitives (%convert-matrix :return f32.2x3) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.2x3<-2x2-conv f32.2x2))
  (mat2x3 (aref matrix 0)
          (aref matrix 1)
          (vec2 0f0 0f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.2x3) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.2x3<-2x4-conv f32.2x4))
  (mat2x3 (aref matrix 0)
          (aref matrix 1)
          (aref matrix 2)))

(zrvl-int:define-primitives (%convert-matrix :return f32.2x3) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.2x3<-3x2-conv f32.3x2)
     (zrvl-mono:f32.2x3<-4x2-conv f32.4x2))
  (mat2x3 (swizzle (aref matrix 0) :xy)
          (swizzle (aref matrix 1) :xy)
          (vec2 0f0 0f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.2x3) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.2x3<-3x3-conv f32.3x3)
     (zrvl-mono:f32.2x3<-3x4-conv f32.3x4)
     (zrvl-mono:f32.2x3<-4x3-conv f32.4x3)
     (zrvl-mono:f32.2x3<-4x4-conv f32.4x4))
  (mat2x3 (swizzle (aref matrix 0) :xy)
          (swizzle (aref matrix 1) :xy)
          (swizzle (aref matrix 2) :xy)))

(zrvl-int:define-primitives (%convert-matrix :return f32.2x4) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.2x4<-2x2-conv f32.2x2))
  (mat2x4 (aref matrix 0)
          (aref matrix 1)
          (vec2 0f0 0f0)
          (vec2 0f0 0f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.2x4) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.2x4<-2x3-conv f32.2x3))
  (mat2x4 (aref matrix 0)
          (aref matrix 1)
          (aref matrix 2)
          (vec2 0f0 0f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.2x4) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.2x4<-3x2-conv f32.3x2)
     (zrvl-mono:f32.2x4<-4x2-conv f32.4x2))
  (mat2x4 (swizzle (aref matrix 0) :xy)
          (swizzle (aref matrix 1) :xy)
          (vec2 0f0 0f0)
          (vec2 0f0 0f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.2x4) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.2x4<-3x3-conv f32.3x3)
     (zrvl-mono:f32.2x4<-4x3-conv f32.4x3))
  (mat2x4 (swizzle (aref matrix 0) :xy)
          (swizzle (aref matrix 1) :xy)
          (swizzle (aref matrix 2) :xy)
          (vec2 0f0 0f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.2x4) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.2x4<-3x4-conv f32.3x4)
     (zrvl-mono:f32.2x4<-4x4-conv f32.4x4))
  (mat2x4 (swizzle (aref matrix 0) :xy)
          (swizzle (aref matrix 1) :xy)
          (swizzle (aref matrix 2) :xy)
          (swizzle (aref matrix 3) :xy)))

(zrvl-int:define-primitives (%convert-matrix :return f32.3x2) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.3x2<-2x2-conv f32.2x2)
     (zrvl-mono:f32.3x2<-2x3-conv f32.2x3)
     (zrvl-mono:f32.3x2<-2x4-conv f32.2x4))
  (mat3x2 (vec3 (aref matrix 0) 0f0)
          (vec3 (aref matrix 1) 0f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.3x2) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.3x2<-3x3-conv f32.3x3)
     (zrvl-mono:f32.3x2<-3x4-conv f32.3x4))
  (mat3x2 (aref matrix 0)
          (aref matrix 1)))

(zrvl-int:define-primitives (%convert-matrix :return f32.3x2) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.3x2<-4x2-conv f32.4x2)
     (zrvl-mono:f32.3x2<-4x3-conv f32.4x3)
     (zrvl-mono:f32.3x2<-4x4-conv f32.4x4))
  (mat3x2 (swizzle (aref matrix 0) :xyz)
          (swizzle (aref matrix 1) :xyz)))

(zrvl-int:define-primitives (%convert-matrix :return f32.3x3) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.3x3<-2x2-conv f32.2x2))
  (mat3x3 (vec3 (aref matrix 0) 0f0)
          (vec3 (aref matrix 1) 0f0)
          (vec3 0f0 0f0 1f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.3x3) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.3x3<-2x3-conv f32.2x3)
     (zrvl-mono:f32.3x3<-2x4-conv f32.2x4))
  (mat3x3 (vec3 (aref matrix 0) 0f0)
          (vec3 (aref matrix 1) 0f0)
          (vec3 (aref matrix 2) 1f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.3x3) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.3x3<-3x2-conv f32.3x2)
     (zrvl-mono:f32.3x3<-4x2-conv f32.4x2))
  (mat3x3 (swizzle (aref matrix 0) :xyz)
          (swizzle (aref matrix 1) :xyz)
          (vec3 0f0 0f0 1f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.3x3) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.3x3<-3x4-conv f32.3x4)
     (zrvl-mono:f32.3x3<-4x3-conv f32.4x3)
     (zrvl-mono:f32.3x3<-4x4-conv f32.4x4))
  (mat3x3 (swizzle (aref matrix 0) :xyz)
          (swizzle (aref matrix 1) :xyz)
          (swizzle (aref matrix 2) :xyz)))

(zrvl-int:define-primitives (%convert-matrix :return f32.3x4) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.3x4<-2x2-conv f32.2x2))
  (mat3x4 (vec3 (aref matrix 0) 0f0)
          (vec3 (aref matrix 1) 0f0)
          (vec3 0f0 0f0 1f0)
          (vec3 0f0 0f0 0f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.3x4) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.3x4<-2x3-conv f32.2x3))
  (mat3x4 (vec3 (aref matrix 0) 0f0)
          (vec3 (aref matrix 1) 0f0)
          (vec3 (aref matrix 2) 1f0)
          (vec3 0f0 0f0 0f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.3x4) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.3x4<-2x4-conv f32.2x4))
  (mat3x4 (vec3 (aref matrix 0) 0f0)
          (vec3 (aref matrix 1) 0f0)
          (vec3 (aref matrix 2) 1f0)
          (vec3 (aref matrix 3) 0f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.3x4) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.3x4<-3x2-conv f32.3x2)
     (zrvl-mono:f32.3x4<-4x2-conv f32.4x2))
  (mat3x4 (swizzle (aref matrix 0) :xyz)
          (swizzle (aref matrix 1) :xyz)
          (vec3 0f0 0f0 1f0)
          (vec3 0f0 0f0 0f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.3x4) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.3x4<-3x3-conv f32.3x3))
  (mat3x4 (aref matrix 0)
          (aref matrix 1)
          (aref matrix 2)
          (vec3 0f0 0f0 0f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.3x4) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.3x4<-4x3-conv f32.4x3))
  (mat3x4 (swizzle (aref matrix 0) :xyz)
          (swizzle (aref matrix 1) :xyz)
          (swizzle (aref matrix 2) :xyz)
          (vec3 0f0 0f0 0f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.3x4) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.3x4<-4x4-conv f32.4x4))
  (mat3x4 (swizzle (aref matrix 0) :xyz)
          (swizzle (aref matrix 1) :xyz)
          (swizzle (aref matrix 2) :xyz)
          (swizzle (aref matrix 3) :xyz)))

(zrvl-int:define-primitives (%convert-matrix :return f32.4x2) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.4x2<-2x2-conv f32.2x2)
     (zrvl-mono:f32.4x2<-2x3-conv f32.2x3)
     (zrvl-mono:f32.4x2<-2x4-conv f32.2x4))
  (mat4x2 (vec4 (aref matrix 0) 0f0 0f0)
          (vec4 (aref matrix 1) 1f0 0f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.4x2) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.4x2<-3x2-conv f32.3x2)
     (zrvl-mono:f32.4x2<-3x3-conv f32.3x3)
     (zrvl-mono:f32.4x2<-3x4-conv f32.3x4))
  (mat4x2 (vec4 (aref matrix 0) 0f0)
          (vec4 (aref matrix 1) 0f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.4x2) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.4x2<-4x3-conv f32.4x3)
     (zrvl-mono:f32.4x2<-4x4-conv f32.4x4))
  (mat4x2 (aref matrix 0)
          (aref matrix 1)))

(zrvl-int:define-primitives (%convert-matrix :return f32.4x3) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.4x3<-2x2-conv f32.2x2))
  (mat4x3 (vec4 (aref matrix 0) 0f0 0f0)
          (vec4 (aref matrix 1) 0f0 0f0)
          (vec4 0f0 0f0 1f0 0f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.4x3) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.4x3<-2x3-conv f32.2x3)
     (zrvl-mono:f32.4x3<-2x4-conv f32.2x4))
  (mat4x3 (vec4 (aref matrix 0) 0f0 0f0)
          (vec4 (aref matrix 1) 0f0 0f0)
          (vec4 (aref matrix 2) 1f0 0f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.4x3) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.4x3<-3x2-conv f32.3x2))
  (mat4x3 (vec4 (aref matrix 0) 0f0)
          (vec4 (aref matrix 1) 0f0)
          (vec4 0f0 0f0 1f0 0f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.4x3) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.4x3<-4x2-conv f32.4x2))
  (mat4x3 (aref matrix 0)
          (aref matrix 1)
          (vec4 0f0 0f0 1f0 0f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.4x3) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.4x3<-3x3-conv f32.3x3)
     (zrvl-mono:f32.4x3<-3x4-conv f32.3x4))
  (mat4x3 (vec4 (aref matrix 0) 0f0)
          (vec4 (aref matrix 1) 0f0)
          (vec4 (aref matrix 2) 0f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.4x3) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.4x3<-4x4-conv f32.4x4))
  (mat4x3 (aref matrix 0)
          (aref matrix 1)
          (aref matrix 2)))

(zrvl-int:define-primitives (%convert-matrix :return f32.4x4) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.4x4<-2x2-conv f32.2x2))
  (mat4x4 (vec4 (aref matrix 0) 0f0 0f0)
          (vec4 (aref matrix 1) 0f0 0f0)
          (vec4 0f0 0f0 1f0 0f0)
          (vec4 0f0 0f0 0f0 1f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.4x4) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.4x4<-2x3-conv f32.2x3))
  (mat4x4 (vec4 (aref matrix 0) 0f0 0f0)
          (vec4 (aref matrix 1) 0f0 0f0)
          (vec4 (aref matrix 2) 1f0 0f0)
          (vec4 0f0 0f0 0f0 1f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.4x4) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.4x4<-2x4-conv f32.2x4))
  (mat4x4 (vec4 (aref matrix 0) 0f0 0f0)
          (vec4 (aref matrix 1) 0f0 0f0)
          (vec4 (aref matrix 2) 1f0 0f0)
          (vec4 (aref matrix 3) 0f0 1f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.4x4) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.4x4<-3x2-conv f32.3x2))
  (mat4x4 (vec4 (aref matrix 0) 0f0)
          (vec4 (aref matrix 1) 0f0)
          (vec4 0f0 0f0 1f0 0f0)
          (vec4 0f0 0f0 0f0 1f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.4x4) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.4x4<-4x2-conv f32.4x2))
  (mat4x4 (aref matrix 0)
          (aref matrix 1)
          (vec4 0f0 0f0 1f0 0f0)
          (vec4 0f0 0f0 0f0 0f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.4x4) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.4x4<-3x3-conv f32.3x3))
  (mat4x4 (vec4 (aref matrix 0) 0f0)
          (vec4 (aref matrix 1) 0f0)
          (vec4 (aref matrix 2) 0f0)
          (vec4 0f0 0f0 0f0 1f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.4x4) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.4x4<-4x3-conv f32.4x3))
  (mat4x4 (aref matrix 0)
          (aref matrix 1)
          (aref matrix 2)
          (vec4 0f0 0f0 0f0 1f0)))

(zrvl-int:define-primitives (%convert-matrix :return f32.4x4) ((matrix alpha))
    (%convert-matrix alpha)
    ((zrvl-mono:f32.4x4<-3x4-conv f32.3x4))
  (mat4x4 (vec4 (aref matrix 0) 0f0)
          (vec4 (aref matrix 1) 0f0)
          (vec4 (aref matrix 2) 0f0)
          (vec4 (aref matrix 3) 1f0)))

(zrvl-int:define-primitives (%diag-matrix :return f32.2x2) ((scalar alpha))
    (%diag-matrix alpha)
    ((zrvl-mono:f32.2x2<-f32-diag f32))
  (mat2x2 (vec2 scalar 0f0)
          (vec2 0f0    scalar)))

(zrvl-int:define-primitives (%diag-matrix :return f32.2x3) ((scalar alpha))
    (%diag-matrix alpha)
    ((zrvl-mono:f32.2x3<-f32-diag f32))
  (mat2x3 (vec2 scalar 0f0)
          (vec2 0f0    scalar)
          (vec2 0f0    0f0)))

(zrvl-int:define-primitives (%diag-matrix :return f32.2x4) ((scalar alpha))
    (%diag-matrix alpha)
    ((zrvl-mono:f32.2x4<-f32-diag f32))
  (mat2x4 (vec2 scalar 0f0)
          (vec2 0f0    scalar)
          (vec2 0f0    0f0)
          (vec2 0f0    0f0)))

(zrvl-int:define-primitives (%diag-matrix :return f32.3x2) ((scalar alpha))
    (%diag-matrix alpha)
    ((zrvl-mono:f32.3x2<-f32-diag f32))
  (mat3x2 (vec3 scalar 0f0    0f0)
          (vec3 0f0    scalar 0f0)))

(zrvl-int:define-primitives (%diag-matrix :return f32.3x3) ((scalar alpha))
    (%diag-matrix alpha)
    ((zrvl-mono:f32.3x3<-f32-diag f32))
  (mat3x3 (vec3 scalar 0f0    0f0)
          (vec3 0f0    scalar 0f0)
          (vec3 0f0    0f0    scalar)))

(zrvl-int:define-primitives (%diag-matrix :return f32.3x4) ((scalar alpha))
    (%diag-matrix alpha)
    ((zrvl-mono:f32.3x4<-f32-diag f32))
  (mat3x4 (vec3 scalar 0f0    0f0)
          (vec3 0f0    scalar 0f0)
          (vec3 0f0    0f0    scalar)
          (vec3 0f0    0f0    0f0)))

(zrvl-int:define-primitives (%diag-matrix :return f32.4x2) ((scalar alpha))
    (%diag-matrix alpha)
    ((zrvl-mono:f32.4x2<-f32-diag f32))
  (mat4x2 (vec4 scalar 0f0    0f0 0f0)
          (vec4 0f0    scalar 0f0 0f0)))

(zrvl-int:define-primitives (%diag-matrix :return f32.4x3) ((scalar alpha))
    (%diag-matrix alpha)
    ((zrvl-mono:f32.4x3<-f32-diag f32))
  (mat4x3 (vec4 scalar 0f0    0f0    0f0)
          (vec4 0f0    scalar 0f0    0f0)
          (vec4 0f0    0f0    scalar 0f0)))

(zrvl-int:define-primitives (%diag-matrix :return f32.4x4) ((scalar alpha))
    (%diag-matrix alpha)
    ((zrvl-mono:f32.4x4<-f32-diag f32))
  (mat4x4 (vec4 scalar 0f0    0f0    0f0)
          (vec4 0f0    scalar 0f0    0f0)
          (vec4 0f0    0f0    scalar 0f0)
          (vec4 0f0    0f0    0f0    scalar)))

(zrvl-int:define-primitives (%diag-matrix :return f32.2x2) ((v alpha))
    (%diag-matrix alpha)
    ((zrvl-mono:f32.2x2<-f32.2-diag f32.2))
  (mat2x2 (vec2 (aref v 0) 0f0)
          (vec2 0f0       (aref v 1))))

(zrvl-int:define-primitives (%diag-matrix :return f32.2x3) ((v alpha))
    (%diag-matrix alpha)
    ((zrvl-mono:f32.2x3<-f32.2-diag f32.2))
  (mat2x3 (vec2 (aref v 0) 0f0)
          (vec2 0f0        (aref v 1))
          (vec2 0f0        0f0)))

(zrvl-int:define-primitives (%diag-matrix :return f32.2x4) ((v alpha))
    (%diag-matrix alpha)
    ((zrvl-mono:f32.2x4<-f32.2-diag f32.2))
  (mat2x4 (vec2 (aref v 0) 0f0)
          (vec2 0f0        (aref v 1))
          (vec2 0f0        0f0)
          (vec2 0f0        0f0)))

(zrvl-int:define-primitives (%diag-matrix :return f32.3x2) ((v alpha))
    (%diag-matrix alpha)
    ((zrvl-mono:f32.3x2<-f32.2-diag f32.2))
  (mat3x2 (vec3 (aref v 0) 0f0       0f0)
          (vec3 0f0       (aref v 1) 0f0)))

(zrvl-int:define-primitives (%diag-matrix :return f32.3x3) ((v alpha))
    (%diag-matrix alpha)
    ((zrvl-mono:f32.3x3<-f32.3-diag f32.3))
  (mat3x3 (vec3 (aref v 0) 0f0        0f0)
          (vec3 0f0        (aref v 1) 0f0)
          (vec3 0f0        0f0        (aref v 2))))

(zrvl-int:define-primitives (%diag-matrix :return f32.3x4) ((v alpha))
    (%diag-matrix alpha)
    ((zrvl-mono:f32.3x4<-f32.3-diag f32.3))
  (mat3x4 (vec3 (aref v 0) 0f0        0f0)
          (vec3 0f0        (aref v 1) 0f0)
          (vec3 0f0        0f0        (aref v 2))
          (vec3 0f0        0f0        0f0)))

(zrvl-int:define-primitives (%diag-matrix :return f32.4x2) ((v alpha))
    (%diag-matrix alpha)
    ((zrvl-mono:f32.4x2<-f32.2-diag f32.2))
  (mat4x2 (vec4 (aref v 0) 0f0        0f0 0f0)
          (vec4 0f0        (aref v 1) 0f0 0f0)))

(zrvl-int:define-primitives (%diag-matrix :return f32.4x3) ((v alpha))
    (%diag-matrix alpha)
    ((zrvl-mono:f32.4x3<-f32.3-diag f32.3))
  (mat4x3 (vec4 (aref v 0) 0f0        0f0        0f0)
          (vec4 0f0        (aref v 1) 0f0        0f0)
          (vec4 0f0        0f0        (aref v 2) 0f0)))

(zrvl-int:define-primitives (%diag-matrix :return f32.4x4) ((v alpha))
    (%diag-matrix alpha)
    ((zrvl-mono:f32.4x4<-f32.4-diag f32.4))
  (mat4x4 (vec4 (aref v 0) 0f0        0f0        0f0)
          (vec4 0f0        (aref v 1) 0f0        0f0)
          (vec4 0f0        0f0        (aref v 2) 0f0)
          (vec4 0f0        0f0        0f0        (aref v 3))))

(zrvl-int:define-generic texture (sampled-image coordinate)
  (zrvl-mono:sampler-2D-texture       sampler-2D       f32.2)
  (zrvl-mono:sampler-2D-array-texture sampler-2D-array f32.3))

(zrvl-int:define-primitives (texture :return gamma) ((sampled-image alpha) (coordinate beta))
    (texture alpha beta gamma)
    ((zrvl-mono:sampler-2D-texture       sampler-2D       f32.2 f32.4)
     (zrvl-mono:sampler-2D-array-texture sampler-2D-array f32.3 f32.4))
  (%image-sample-implicit-lod id gamma sampled-image coordinate))

(zrvl-int:define-generic 1+ (number)
  (zrvl-mono:f32-1+   f32)
  (zrvl-mono:f32.2-1+ f32.2)
  (zrvl-mono:f32.3-1+ f32.3)
  (zrvl-mono:f32.4-1+ f32.4)
  (zrvl-mono:quat-1+  quat)
  (zrvl-mono:f32.2x2-1+ f32.2x2)
  (zrvl-mono:f32.2x3-1+ f32.2x3)
  (zrvl-mono:f32.2x4-1+ f32.2x4)
  (zrvl-mono:f32.3x2-1+ f32.3x2)
  (zrvl-mono:f32.3x3-1+ f32.3x3)
  (zrvl-mono:f32.3x4-1+ f32.3x4)
  (zrvl-mono:f32.4x2-1+ f32.4x2)
  (zrvl-mono:f32.4x3-1+ f32.4x3)
  (zrvl-mono:f32.4x4-1+ f32.4x4))

(zrvl-int:define-primitive (zrvl-mono:f32-1+ :return f32) ((number f32))
  (+ 1f0 number))

(zrvl-int:define-primitive (zrvl-mono:f32.2-1+ :return f32.2) ((number f32.2))
  (+ (vec2 1f0 1f0) number))

(zrvl-int:define-primitive (zrvl-mono:f32.3-1+ :return f32.3) ((number f32.3))
  (+ (vec3 1f0 1f0 1f0) number))

(zrvl-int:define-primitive (zrvl-mono:f32.4-1+ :return f32.4) ((number f32.4))
  (+ (vec4 1f0 1f0 1f0 1f0) number))

(zrvl-int:define-unary-elementwise-matrix-primitives zrvl:1+
    (((zrvl-mono:f32.2x2-1+ mat2x2)
      (zrvl-mono:f32.3x2-1+ mat3x2)
      (zrvl-mono:f32.4x2-1+ mat4x2))
     ((zrvl-mono:f32.2x3-1+ mat2x3)
      (zrvl-mono:f32.3x3-1+ mat3x3)
      (zrvl-mono:f32.4x3-1+ mat4x3))
     ((zrvl-mono:f32.2x4-1+ mat2x4)
      (zrvl-mono:f32.3x4-1+ mat3x4)
      (zrvl-mono:f32.4x4-1+ mat4x4))))

(zrvl-int:define-generic sqrt (number)
  (zrvl-mono:f32-sqrt f32))

(zrvl-int:define-simple-unary-primitives (zrvl-mono:f32-sqrt %sqrt)
  (zrvl-mono:f32-sqrt f32))

;;; Quaternions
;;;
;;; TODO: When this is generated via macro instead of directly, change
;;; how the type conversion works so only the first three forms work.
;;; The rest will have to convert back to quat after they convert to
;;; vec4 to do the operations.
;;;
;;; TODO: Alternatively, make it clear in the return type that it is
;;; quat<-vec4 return and not just a quat.
;;;
;;; TODO: Also change the generics so they're not all in one form
;;; because it doesn't make sense to generate the quat forms up there
;;; instead of down here.

(zrvl-int:define-primitive (quat :return quat) ((x f32) (y f32) (z f32) (w f32))
  (vec4 x y z w))

(zrvl-int:define-primitive (vec4<-quat :return vec4) ((q quat)) q)

(zrvl-int:define-primitive (quat<-vec4 :return quat) ((v vec4)) v)

(zrvl-int:define-primitive (zrvl-mono:quat-ref :return f32) ((q quat) (subscript i32))
  (aref (vec4<-quat q) subscript))

(zrvl-int:define-primitive (zrvl-mono:quat-swizzle2 :return vec2) ((q quat) (a i32) (b i32))
  (zrvl-int:swizzle2 (vec4<-quat q) a b))

(zrvl-int:define-primitive (zrvl-mono:quat-swizzle3 :return vec3) ((q quat) (a i32) (b i32) (c i32))
  (zrvl-int:swizzle3 (vec4<-quat q) a b c))

(zrvl-int:define-primitive (zrvl-mono:quat-swizzle4 :return vec4) ((q quat) (a i32) (b i32) (c i32) (d i32))
  (zrvl-int:swizzle4 (vec4<-quat q) a b c d))

(zrvl-int:define-primitive (vec3<-quat :return vec3) ((q quat))
  (swizzle q :xyz))

;;; TODO: make generic; have an alternate form that provides the scalar, too
(zrvl-int:define-primitive (quat<-vec3 :return quat) ((v vec3))
  (vec4<-3 v 0f0))

(zrvl-int:define-primitive (zrvl-mono:quat+ :return quat) ((q1 quat) (q2 quat))
  (+ (vec4<-quat q1) (vec4<-quat q2)))

(zrvl-int:define-primitive (zrvl-mono:quat- :return quat) ((q1 quat) (q2 quat))
  (- (vec4<-quat q1) (vec4<-quat q2)))

(zrvl-int:define-primitive (zrvl-mono:quat-vector*scalar :return quat) ((q quat) (scalar f32))
  (* (vec4<-quat q) scalar))

(zrvl-int:define-primitive (zrvl-mono:quat-scalar*vector :return quat) ((scalar f32) (q quat))
  (* scalar (vec4<-quat q)))

(zrvl-int:define-primitive (zrvl-mono:quat* :return quat) ((q1 quat) (q2 quat))
  (quat (+ (* (swizzle q1 :w) (swizzle q2 :x))
           (* (swizzle q1 :x) (swizzle q2 :w))
           (* (swizzle q1 :y) (swizzle q2 :z))
           (- (* (swizzle q1 :z) (swizzle q2 :y))))
        (+ (* (swizzle q1 :w) (swizzle q2 :y))
           (- (* (swizzle q1 :x) (swizzle q2 :z)))
           (* (swizzle q1 :y) (swizzle q2 :w))
           (* (swizzle q1 :z) (swizzle q2 :x)))
        (+ (* (swizzle q1 :w) (swizzle q2 :z))
           (* (swizzle q1 :x) (swizzle q2 :y))
           (- (* (swizzle q1 :y) (swizzle q2 :x)))
           (* (swizzle q1 :z) (swizzle q2 :w)))
        (- (* (swizzle q1 :w) (swizzle q2 :w))
           (* (swizzle q1 :x) (swizzle q2 :x))
           (* (swizzle q1 :y) (swizzle q2 :y))
           (* (swizzle q1 :z) (swizzle q2 :z)))))

(zrvl-int:define-generic zrvl:conjugate (x)
  (zrvl-mono::quat-conjugate quat))

(zrvl-int:define-primitive (zrvl-mono:quat-conjugate :return quat) ((q quat))
  (let ((v (vec4<-quat q)))
    (quat (- (aref v 0))
          (- (aref v 1))
          (- (aref v 2))
          (aref v 3))))

(zrvl-int:define-primitive (zrvl-mono:quat-dot :return f32) ((q1 quat) (q2 quat))
  (dot (vec4<-quat q1) (vec4<-quat q2)))

(zrvl-int:define-primitive (zrvl-mono:quat-negate :return quat) ((q quat))
  (- (vec4<-quat q)))

(zrvl-int:define-primitive (zrvl-mono:quat-1+ :return quat) ((q quat))
  (1+ (vec4<-quat q)))

(zrvl-int:define-primitive (zrvl-mono:quat-normalize :return quat) ((q quat))
  (normalize (vec4<-quat q)))

(zrvl-int:define-primitive (zrvl-mono:quat-magnitude :return f32) ((q quat))
  (magnitude (vec4<-quat q)))

;;; Note: The quaternions must be normalized in order for the dot
;;; product to be between -1 and 1 as below, which this function
;;; assumes. The clamp just tells the compiler this. The modified
;;; slerp* further restricts this to be between 0 and 1 by negating q2
;;; if the dot product is negative.
(zrvl-int:define-primitive (zrvl:slerp :return quat) ((q1 quat) (q2 quat) (a f32))
  (let* ((dot (clamp (dot q1 q2) -1f0 1f0))
         (phi (acos dot))
         ;; Note: The denominator is zero if the dot product is +/- 1.
         (denom (sqrt (- 1f0 (expt dot 2f0)))))
    (+ (* (/ (sin (* (- 1f0 a) phi))
             denom)
          q1)
       (* (/ (sin (* a phi))
             denom)
          q2))))

(zrvl-int:define-primitive (zrvl:rotate :return vec3) ((q quat) (v vec3))
  (vec3<-quat (* q (quat<-vec3 v) (conjugate q))))

(zrvl-int:define-primitive (zrvl:rotation :return quat) ((axis vec3) (theta f32))
  (let ((theta/2 (* 0.5f0 theta)))
    (vec4<-3 (* (sin theta/2) axis)
             (cos theta/2))))

(zrvl-int:define-primitive (zrvl:quat<-yaw :return quat) ((theta f32))
  (let ((theta/2 (* 0.5f0 theta)))
    (quat 0f0
          (sin theta/2)
          0f0
          (cos theta/2))))

(zrvl-int:define-primitive (zrvl:quat<-pitch :return quat) ((theta f32))
  (let ((theta/2 (* 0.5f0 theta)))
    (quat (sin theta/2)
          0f0
          0f0
          (cos theta/2))))

(zrvl-int:define-primitive (zrvl:quat<-roll :return quat) ((theta f32))
  (let ((theta/2 (* 0.5f0 theta)))
    (quat 0f0
          0f0
          (sin theta/2)
          (cos theta/2))))

(zrvl-int:define-primitive (zrvl:quat<-euler :return quat) ((euler-angles vec3))
  (* (quat 0f0 0f0 0f0 1f0)
     (quat<-yaw   (aref euler-angles 0))
     (quat<-pitch (aref euler-angles 1))
     (quat<-roll  (aref euler-angles 2))))

;;; TODO:
;;; slerp* (optimal, branching path of slerp)
;;; matrix<-quat (i.e. quaternion-rotation-matrix)

;;; TODO: Make matrices work on the CPU and extend them to work with
;;; +, etc., etc.
