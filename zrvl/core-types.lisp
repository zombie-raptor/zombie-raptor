(defpackage #:zombie-raptor/zrvl/core-types
  (:documentation "Types used by ZRVL.")
  (:import-from #:alexandria)
  (:use #:cl
        ;; #:zombie-raptor/zrvl/a-normal
        #:zombie-raptor/zrvl/spir-v-enums
        #:zr-utils)
  (:export #:%%array
           #:%%array-length
           #:%%boolean
           #:%%disjoint-type
           #:%%disjoint-type-base-type
           #:%%f
           #:%%ftype
           #:%%ftype-output-type
           #:%%i
           #:%%matrix
           #:%%matrix-cols
           #:%%matrix-column-vec-type
           #:%%matrix-rows
           #:%%pointer
           #:%%scalar-p
           #:%%scalar-width
           #:%%spir-v-image
           #:%%spir-v-sampled-image
           #:%%struct
           #:%%struct-type-ref
           #:%%type=
           #:%%typecase
           #:%%typep
           #:%%u
           #:%%vec
           #:%%vec-scalar
           #:%%vec-size
           #:%%void
           #:arg-list-matches-ftype-p
           #:decode-linear-algebra-type
           #:derivative-type-encoding
           #:derivative-type-encoding-p
           #:enumerate-types
           #:primitive-type-encoding
           #:primitive-type-encoding-p
           #:type-erasure
           #:write-linear-algebra-type
           #:zrvl-boolean))

(in-package #:zombie-raptor/zrvl/core-types)

;;; This package is a namespace for the IR's symbols, which are true
;;; and false.
(defpackage #:zrvlir
  (:documentation "Symbols in the intermediate representation.")
  (:use)
  (:import-from #:cl
                #:defpackage
                #:function
                #:in-package
                #:nil)
  (:export #:false
           #:true))

;;; True is just CL:T, while false is a self-evaluating constant
;;; similar to NIL, but distinct from NIL.
(defconstant zrvlir:true t)
(defconstant zrvlir:false 'zrvlir:false)

;;; The boolean is represented by the above two symbols.
(deftype zrvl-boolean ()
  `(member zrvlir:true
           ,zrvlir:true
           ,zrvlir:false))

;;; A primitive type is just encoded as an unsigned integer.
(deftype primitive-type-encoding ()
  `uint32)

;;; A derivative type is encoded as an array of unsigned integers.
;;; This uses the word "derivative" as the antonym of primitive. Note
;;; that this is NOT referring to differential calculus.
(deftype derivative-type-encoding ()
  `(simple-array uint32 (*)))

(deftype type-encoding ()
  `(or primitive-type-encoding
       derivative-type-encoding))

(defconstant +l-separator+ #x3FFFFFFF
  "
A fake (invalid) primitive type encoding used as the left or opening
separator in a derivative type encoding.
")

(defconstant +r-separator+ #x7FFFFFFF
  "
A fake (invalid) primitive type encoding used as the right or closing
separator in a derivative type encoding.
")

(define-function (next-l-separator :inline t)
    ((encoding derivative-type-encoding)
     &optional
     (start 0 alexandria:array-index)
     (end nil (maybe alexandria:array-index)))
  "Finds the next left/opening separator in an encoding."
  (position +l-separator+ encoding :start start :end end))

(define-function (next-r-separator :inline t)
    ((encoding derivative-type-encoding)
     &optional
     (start 0 alexandria:array-index)
     (end nil (maybe alexandria:array-index)))
  "Finds the next right/closing separator in an encoding."
  (position +r-separator+ encoding :start start :end end))

(define-function (next-separator :inline t)
    ((encoding derivative-type-encoding)
     &optional
     (start 0 alexandria:array-index)
     (end nil (maybe alexandria:array-index)))
  "
Finds the next separator of either type in an encoding and returns the
separator itself as the second value so that another function can
change its behavior based on which one was encountered.
"
  (let ((position (position-if (lambda (x)
                                 (or (= +l-separator+ x)
                                     (= +r-separator+ x)))
                               encoding
                               :start start
                               :end end)))
    (values (or position (length encoding))
            (and position (aref encoding position)))))

(define-function (next-encoded-form :return (alexandria:array-index alexandria:array-index))
    ((encoding derivative-type-encoding)
     &optional
     (start 0 alexandria:array-index)
     (end nil (maybe alexandria:array-index)))
  "
Returns two array indices of the encoding. The first is the position
immediately after the left separator's position at the current level
and the second is the corresponding right separator's position. This
start/end syntax matches most sequence functions, such as SUBSEQ. For
instance, this should behave as expected to extract the first
top-level encoded type that is separated by separators:

(multiple-value-call #'subseq encoding (next-encoded-form encoding))
"
  (labels ((next-subform (level start)
             (multiple-value-bind (position separator)
                 (next-separator encoding start end)
               (if separator
                   (if (= separator +l-separator+)
                       (multiple-value-bind (start* end*)
                           (next-subform (1+ level) (1+ position))
                         (declare (ignore start*))
                         (if (zerop level)
                             (values (1+ position) end*)
                             (next-subform (1+ level) (1+ end*))))
                       (if (<= level 0)
                           (error "Type encoding separator was closed, but not opened.")
                           (values start position)))
                   (if (zerop level)
                       (values (or end (length encoding))
                               (or end (length encoding)))
                       (error "Type encoding separator was opened, but not closed."))))))
    (next-subform 0 start)))

(define-function (embedded-type :inline t)
    ((type derivative-type-encoding)
     (start alexandria:array-index)
     (end alexandria:array-index))
  (if (= 1 (- end start))
      (aref type start)
      (subseq type start end)))

(define-function (primitive-type-encoding-p :inline t) (object)
  (typep object 'primitive-type-encoding))

(define-function (derivative-type-encoding-p :inline t) (object)
  (typep object 'derivative-type-encoding))

;;; A derivative type can be made concisely with this constructor.
(define-function (make-derivative-type :inline t) (&rest integers)
  (make-array (length integers)
              :element-type 'uint32
              :initial-contents integers))

;;; This compiler macro optimizes the above constructor, which
;;; normally is not optimized because the length isn't known at
;;; compilation time. However, the length usually is known when it is
;;; called normally, which this compiler macro is saying by inlining
;;; the length at compilation time if possible, thus creating a much
;;; more efficient constructor.
;;;
;;; This still isn't the most efficient way to do it. Putting #. in
;;; front if the array isn't modified to turn it into a literal at
;;; read time will be even more efficient.
(define-compiler-macro make-derivative-type (&whole form &rest integers)
  (declare (ignore form))
  `(make-array ,(length integers)
               :element-type 'primitive-type-encoding
               :initial-contents (list ,@integers)))

(define-function (%%disjoint-type-base-type :inline t)
    ((disjoint-type derivative-type-encoding))
  (if (> (length disjoint-type) 3)
      (subseq disjoint-type 2)
      (aref disjoint-type 2)))

;;; Type encoding predicates

(define-function (%%scalar-p :inline t) ((type primitive-type-encoding))
  (zerop (ldb (byte 1 0) type)))

(define-function (%%vec-p :inline t) ((type primitive-type-encoding))
  (= #b01 (ldb (byte 2 0) type)))

(define-function (%%matrix-p :inline t) ((type primitive-type-encoding))
  (= #b011 (ldb (byte 3 0) type)))

(define-function (%%void-p :inline t) ((type primitive-type-encoding))
  (= #xffffffff type))

(define-function (%%array-p :inline t) ((type derivative-type-encoding))
  (= #b0111 (aref type 0)))

(define-function (%%pointer-p :inline t) ((type derivative-type-encoding))
  (= #b01111 (aref type 0)))

(define-function (%%ftype-p :inline t) ((type derivative-type-encoding))
  (= #b011111 (ldb (byte 6 0) (aref type 0))))

(define-function (%%struct-p :inline t) ((type derivative-type-encoding))
  (= #b0111111 (aref type 0)))

(define-function (%%spir-v-image-p :inline t) ((type derivative-type-encoding))
  (= #b01111111 (aref type 0)))

(define-function (%%spir-v-sampled-image-p :inline t) ((type derivative-type-encoding))
  (= #b011111111 (aref type 0)))

(define-function (%%disjoint-type-p :inline t) ((type derivative-type-encoding))
  (= #b0111111111 (aref type 0)))

(defun %%typep (type-object type)
  (declare (optimize (speed 3) (safety 1) (space 0) (compilation-speed 0)))
  (let ((primitive? (primitive-type-encoding-p type-object))
        (derivative? (and (derivative-type-encoding-p type-object)
                          (plusp (length type-object)))))
    (cond (primitive?
           (case type
             (%%i (= #b100 (ldb (byte 3 0) type-object)))
             (%%u (= #b000 (ldb (byte 3 0) type-object)))
             (%%f (= #b010 (ldb (byte 3 0) type-object)))
             (%%boolean (= #b1110 type-object))
             (%%vec (%%vec-p type-object))
             (%%matrix (%%matrix-p type-object))
             (%%void (%%void-p type-object))))
          (derivative?
           (case type
             (%%array (%%array-p type-object))
             (%%pointer (%%pointer-p type-object))
             (%%ftype (%%ftype-p type-object))
             (%%struct (%%struct-p type-object))
             (%%spir-v-image (%%spir-v-image-p type-object))
             (%%spir-v-sampled-image (%%spir-v-sampled-image-p type-object))
             (%%disjoint-type (%%disjoint-type-p type-object)))))))

(defun %%type= (encoded-type-1 encoded-type-2)
  (if (numberp encoded-type-1)
      (if (numberp encoded-type-2)
          (= encoded-type-1 encoded-type-2)
          nil)
      (and (typep encoded-type-1 'derivative-type-encoding)
           (typep encoded-type-2 'derivative-type-encoding)
           (= (length encoded-type-1)
              (length encoded-type-2))
           (every #'=
                  encoded-type-1
                  encoded-type-2))))

(defmacro %%typecase (keyform &body cases)
  (alexandria:once-only (keyform)
    `(cond ,@(mapcar (lambda (case)
                       `(,(let ((type (car case)))
                            (if (eq type t)
                                t
                                `(%%type= ,keyform ,type)))
                         ,@(cdr case)))
                     cases))))

;;; Useful functions on encoded types

(define-function (%%vec-size :inline t) ((vec-type primitive-type-encoding))
  (1+ (ldb (byte 8 14) vec-type)))

(define-function (%%vec-scalar :inline t) ((vec-type primitive-type-encoding))
  (logand #x00000fff (ash vec-type -1)))

(define-function (%%matrix-rows :inline t) ((matrix-type primitive-type-encoding))
  (1+ (ldb (byte 8 15) matrix-type)))

(define-function (%%matrix-cols :inline t) ((matrix-type primitive-type-encoding))
  (1+ (ldb (byte 8 23) matrix-type)))

(define-function decode-primitive-type ((type primitive-type-encoding))
  "
Turns a bit-based representation of a linear algebra type into
multiple 'decoded' return values that may be useful in debugging or
printing.
"
  (labels ((decode-scalar-type (type)
             (cond ((zerop (ldb (byte 1 0) type))
                    (values :scalar
                            (if (zerop (ldb (byte 1 1) type))
                                :unsigned
                                :signed)
                            (1+ (ldb (byte 10 2) type))))
                   ((zerop (ldb (byte 1 1) type))
                    (values :scalar
                            :float
                            (1+ (ldb (byte 10 2) type))))
                   ((= #b111 type)
                    (values :scalar
                            :boolean
                            nil))
                   (t (error "Invalid scalar type."))))
           (decode-vec-type (type)
             (multiple-value-call #'values
               :vec
               (multiple-value-bind (type scalar-type scalar-width)
                   (decode-scalar-type (ash (%%vec-scalar type) -1))
                 (declare (ignore type))
                 (values scalar-type scalar-width))
               (%%vec-size type)))
           (decode-matrix-type (type)
             (multiple-value-call #'values
               :matrix
               (multiple-value-bind (type scalar-type scalar-width)
                   (decode-scalar-type (logand #x00000fff (ash type -3)))
                 (declare (ignore type))
                 (values scalar-type scalar-width))
               (%%matrix-rows type)
               (%%matrix-cols type))))
    (declare (inline decode-scalar-type
                     decode-vec-type
                     decode-matrix-type))
    (cond ((%%scalar-p type)
           (unless (zerop (ash type -13))
             (error "Invalid scalar type."))
           (decode-scalar-type (ash type -1)))
          ((%%vec-p type)
           (unless (zerop (ash type -20))
             (error "Invalid vector type."))
           (decode-vec-type type))
          ((%%matrix-p type)
           (unless (zerop (ash type -31))
             (error "Invalid matrix type."))
           (decode-matrix-type type))
          ((%%void-p type)
           (values :void))
          (t (error "Invalid type.")))))

;;; TODO: some of these numbers are SPIR-V enums and should be
;;; presented as such
(define-function decode-derivative-type ((type derivative-type-encoding))
  (flet ((decode-multiple-types (index)
           (loop :for (start end) := (multiple-value-list (next-encoded-form type index))
                   :then (multiple-value-list (next-encoded-form type (1+ end)))
                 :until (>= end (length type))
                 :unless (= end start)
                   :append (multiple-value-list (decode-linear-algebra-type (embedded-type type start end))))))
    (cond ((%%array-p type)
           (multiple-value-call #'values
             :array
             (aref type 1)
             (decode-linear-algebra-type (embedded-type type 2 (length type)))))
          ((%%pointer-p type)
           (multiple-value-call #'values
             :pointer
             (%storage-class-enum (aref type 1))
             (decode-linear-algebra-type (embedded-type type 2 (length type)))))
          ((%%ftype-p type)
           (multiple-value-call #'values
             :ftype
             (multiple-value-bind (start end)
                 (next-encoded-form type)
               (values (multiple-value-list (decode-linear-algebra-type (embedded-type type start end)))
                       ;; TODO: validate that it matches the stored length
                       (decode-multiple-types (1+ end))))))
          ;; TODO: store and validate length
          ((%%struct-p type)
           (values :struct
                   (decode-multiple-types 0)))
          ((%%spir-v-image-p type)
           (multiple-value-call #'values
             :spir-v-image
             (decode-primitive-type (aref type 1))
             (aref type 2)
             (aref type 3)
             (aref type 4)
             (aref type 5)
             (aref type 6)
             (aref type 7)
             (if (>= (length type) 9)
                 (aref type 8)
                 nil)))
          ((%%spir-v-sampled-image-p type)
           (multiple-value-call #'values
             :spir-v-sampled-image
             (decode-derivative-type (subseq type 1))))
          ((%%disjoint-type-p type)
           (multiple-value-call #'values
             :disjoint-type
             (aref type 1)
             (decode-linear-algebra-type (%%disjoint-type-base-type type)))))))

(define-function decode-linear-algebra-type ((type type-encoding))
  (etypecase type
    (primitive-type-encoding (decode-primitive-type type))
    (derivative-type-encoding (decode-derivative-type type))))

(define-function %%scalar-width ((type primitive-type-encoding))
  (unless (and (%%scalar-p type)
               (zerop (ash type -13)))
    (error "Invalid scalar type."))
  (cond ((zerop (ldb (byte 1 1) type))
         (1+ (ldb (byte 10 3) type)))
        ((zerop (ldb (byte 1 2) type))
         (1+ (ldb (byte 10 3) type)))
        (t (error "Scalar type has no width."))))

;;; TODO: add support for derivative types and add tests for them
(define-function write-linear-algebra-type
    ((type primitive-type-encoding) &optional (stream *standard-output* stream))
  "
Prints a linear algebra type, represented by an integer, as one of the
constructors, defined below, that can create that linear algebra type.
This function can then be called by `print-object' methods of data
structures that contain these linear algebra types.
"
  (multiple-value-bind (type scalar-type scalar-width m n)
      (decode-linear-algebra-type type)
    (flet ((print-scalar ()
             (let ((scalar-type (ecase scalar-type
                                  (:signed '%%i)
                                  (:unsigned '%%u)
                                  (:float '%%f)
                                  (:boolean '%%boolean)
                                  (:void '%%void))))
               (format stream "(~S~@[ ~A~])" scalar-type scalar-width))))
      (ecase type
        (:scalar
         (print-scalar))
        (:vec
         (format stream "(~S " '%%vec)
         (print-scalar)
         (format stream " ~A)" m))
        (:matrix
         (format stream "(~S " '%%matrix)
         (print-scalar)
         (format stream " ~A ~A)" m n))
        (:void
         (format stream "(~S)" '%%void))))))

;;; Constructors

;;; Creates a signed integer type
(define-function (%%i :inline t) ((width (integer 1 1024)))
  (logior #b100 (ash (1- width) 3)))

;;; Creates an unsigned integer type
(define-function (%%u :inline t) ((width (integer 1 1024)))
  (logior #b000 (ash (1- width) 3)))

;;; Creates a floating point type
(define-function (%%f :inline t) ((width (integer 1 1024)))
  (logior #b010 (ash (1- width) 3)))

;;; Creates the boolean type
(define-function (%%boolean :inline t) ()
  #b1110)

;;; Creates the void type, which is used by functions that return
;;; nothing in SPIR-V, including the main() function
(define-function (%%void :inline t) ()
  #xffffffff)

;;; Creates a vec/vector type that contains one of the scalar types
;;;
;;; e.g. (decode-linear-algebra-type (%%vec (%%i 32) 4))
(define-function (%%vec :inline t)
    ((scalar-type (unsigned-byte 13))
     (size (integer 1 256)))
  (logior #b01
          (ash (ash scalar-type -1) 2)
          (ash (1- size) 14)))

;;; Creates a column vec from a matrix
(define-function (%%matrix-column-vec-type :inline t) ((matrix-type primitive-type-encoding))
  (let ((scalar-type (ash (logand #x00000fff (ash matrix-type -3)) 1)))
    (%%vec scalar-type (%%matrix-rows matrix-type))))

;;; Creates a matrix type that contains one of the scalar types
;;;
;;; e.g. (decode-linear-algebra-type (%%matrix (%%f 32) 4 2))
(define-function (%%matrix :inline t)
    ((scalar-type (unsigned-byte 13))
     (rows (integer 1 256))
     (cols (integer 1 256)))
  (logior #b011
          (ash (ash scalar-type -1) 3)
          (ash (1- rows) 15)
          (ash (1- cols) 23)))

(defun %make-derivative-type* (contents)
  (let ((result (make-array 4
                            :element-type 'primitive-type-encoding
                            :initial-element 0
                            :adjustable t
                            :fill-pointer 0)))
    (dolist (type contents (subseq result 0 (length result)))
      (etypecase type
        (primitive-type-encoding
         (vector-push-extend type result))
        (derivative-type-encoding
         (let ((length (+ (length result) (length type))))
           (when (> length (array-total-size result))
             ;; Very large types means a simple doubling isn't enough!
             ;; Just double until it is enough!
             (let ((new-size (loop :for size := (* 2 (array-total-size result))
                                     :then (* 2 size)
                                   :until (>= size length)
                                   :finally (return size))))
               (setf result (adjust-array result
                                          new-size
                                          :element-type 'primitive-type-encoding)))))
         (let ((start (fill-pointer result)))
           (incf (fill-pointer result) (length type))
           (replace result type :start1 start)))))))

(define-function (%%array :inline t) (length &rest contents-type)
  (%make-derivative-type* (list* #b0111 length contents-type)))

(define-function (%%array-length :inline t) ((array-type derivative-type-encoding))
  (aref array-type 1))

(define-function (%%pointer :inline t) ((storage-class keyword) &rest contents-type)
  (%make-derivative-type* (list* #b01111
                                 (storage-class-enum storage-class)
                                 contents-type)))

;;; Note: Multiple values are encoded as one output here, probably a
;;; reified VALUES type (tuple?) that gets undone to multiple-values
;;; where possible (i.e. in CL) and turned into a struct where not
;;; (i.e. SPIR-V)
;;;
;;; Note: This puts output first, unlike CL's FTYPE, because there's
;;; only one output and possibly many inputs.
(define-function (%%ftype :inline t) (output &rest inputs)
  (%make-derivative-type* (list* (logior (ash (length inputs) 6) #b011111)
                                 +l-separator+
                                 output
                                 +r-separator+
                                 (if inputs
                                     (loop :for input :in inputs
                                           :append (list +l-separator+ input +r-separator+))
                                     (list +l-separator+ +r-separator+)))))

(define-function (%%ftype-arity :inline t) ((ftype derivative-type-encoding))
  (ash (aref ftype 0) -6))

(define-function (%%ftype-output-type :inline t) ((ftype derivative-type-encoding))
  (multiple-value-call #'embedded-type ftype (next-encoded-form ftype)))

(define-function arg-list-matches-ftype-p
    ((args list)
     (ftype derivative-type-encoding)
     &optional function-name)
  (unless (= (%%ftype-arity ftype)
             (length args))
    (error "Arity mismatch in function ~A" function-name))
  (let ((start (multiple-value-bind (start end)
                   (next-encoded-form ftype)
                 (declare (ignore start))
                 (1+ end))))
    (or (endp args)
        (loop :for arg :in args
              :always (multiple-value-bind (start* end*)
                          (next-encoded-form ftype start)
                        (setf start (1+ end*))
                        (and (/= start* end*)
                             (%%type= arg (embedded-type ftype start* end*))))))))

;;; TODO: store size and add a function to retrieve size
(define-function (%%struct :inline t) (&rest values)
  (%make-derivative-type* (list* #b0111111
                                 (if values
                                     (loop :for value :in values
                                           :append (list +l-separator+ value +r-separator+))
                                     (list +l-separator+ +r-separator+)))))

;;; TODO: check index by struct size before iterating
(define-function %%struct-type-ref ((struct-type derivative-type-encoding) (index (integer 0 *)))
  (loop :for (start end) := (multiple-value-list (next-encoded-form struct-type))
          :then (multiple-value-list (next-encoded-form struct-type (1+ end)))
        :repeat index
        :finally (return (embedded-type struct-type start end))))

;;; Note: since this is only consumed by SPIR-V, the numeric values
;;; can be equal to their SPIR-V enums where applicable. Note that
;;; access-qualifier is optional.
(define-function (%%spir-v-image :inline t)
    (sampled-type
     dimension
     depth
     arrayed
     multisampled
     sampled
     image-format
     &optional access-qualifier)
  (if access-qualifier
      (make-derivative-type #b01111111
                            sampled-type
                            dimension
                            depth
                            arrayed
                            multisampled
                            sampled
                            image-format
                            access-qualifier)
      (make-derivative-type #b01111111
                            sampled-type
                            dimension
                            depth
                            arrayed
                            multisampled
                            sampled
                            image-format)))

(define-function (%%spir-v-sampled-image :inline t) (image-type)
  (%make-derivative-type* (list #b011111111 image-type)))

;;; A disjoint type
(define-function (%%disjoint-type :inline t) (type-number base-type)
  (%make-derivative-type* (list #b0111111111 type-number base-type)))

;;; Assigns IDs to types, for SPIR-V

(define-function destructure-primitive-type ((type primitive-type-encoding)
                                             (enumerations hash-table))
  (cond ((%%void-p type)
         (list :void))
        ((%%scalar-p type)
         ;; TODO: turn these low level tests into something that can
         ;; be shared with decode-primitive-type and %%scalar-width
         (cond ((zerop (ldb (byte 1 1) type))
                (list :int
                      (%%scalar-width type)
                      ;; integer sign
                      (ldb (byte 1 2) type)))
               ((zerop (ldb (byte 1 2) type))
                (list :float
                      (%%scalar-width type)))
               ((= #b1110 type)
                :bool)
               (t (error "Invalid scalar type encoding."))))
        ((%%vec-p type)
         (let ((vec-scalar (gethash (%%vec-scalar type) enumerations)))
           (unless vec-scalar
             (error "Cannot find vector scalar component."))
           (list :vector vec-scalar (%%vec-size type))))
        ((%%matrix-p type)
         (let ((matrix-col (gethash (%%matrix-column-vec-type type) enumerations)))
           (unless matrix-col
             (error "Cannot find matrix column vector component."))
           (list :matrix matrix-col (%%matrix-cols type))))
        (t (error "Invalid type encoding."))))

;;; TODO: some of these AREFs and calls to embedded-type can be turned
;;; into an inline function that assigns semantic meaning
(define-function destructure-derivative-type ((type derivative-type-encoding)
                                              (enumerations hash-table))
  (flet ((destructure-multiple-types (index)
           (loop :for (start end) := (multiple-value-list (next-encoded-form type index))
                   :then (multiple-value-list (next-encoded-form type (1+ end)))
                 :until (>= end (length type))
                 :unless (= end start)
                   :collect (let ((type-id (gethash (embedded-type type start end) enumerations)))
                              (unless type-id
                                (error "Cannot find type ID."))
                              type-id))))
    (cond ((%%array-p type)
           (let* ((array-type (gethash (embedded-type type 2 (length type)) enumerations))
                  (index-type (gethash (%%u 32) enumerations))
                  (array-length (gethash (list :length index-type (%%array-length type)) enumerations)))
             (unless array-type
               (error "Cannot find array type."))
             (list :array array-type array-length)))
          ((%%pointer-p type)
           (let ((pointer-type (gethash (embedded-type type 2 (length type)) enumerations)))
             (unless pointer-type
               (error "Cannot find pointer type."))
             (list :pointer (aref type 1) pointer-type)))
          ((%%ftype-p type)
           (cons :function (destructure-multiple-types 0)))
          ((%%struct-p type)
           (cons :struct (destructure-multiple-types 0)))
          ((%%spir-v-image-p type)
           (let ((sampled-type (gethash (aref type 1) enumerations)))
             (unless sampled-type
               (error "Cannot find sampled type."))
             (list* :spir-v-image
                    sampled-type
                    (coerce (subseq type 2) 'list))))
          ((%%spir-v-sampled-image-p type)
           (let ((sampled-image (gethash (subseq type 1) enumerations)))
             (unless sampled-image
               (error "Cannot find sampled image."))
             (list :spir-v-sampled-image sampled-image)))
          (t (error "Invalid type encoding.")))))

(define-function destructure-type ((type type-encoding)
                                   (enumerations hash-table))
  (etypecase type
    (primitive-type-encoding (destructure-primitive-type type
                                                         enumerations))
    (derivative-type-encoding (destructure-derivative-type type
                                                           enumerations))))

;;; Generates a new ID if it doesn't already exist for the type, i.e.
;;; if it isn't already in the hash table.
(define-function (%enumerate-type :inline t)
    (type enumerations type-list id-generator)
  (if (gethash type enumerations)
      type-list
      (cons (list (setf (gethash type enumerations)
                        (funcall id-generator))
                  (if (listp type)
                      type
                      (destructure-type type enumerations)))
            type-list)))

;;; Assign a number to the types expressed as if they were SPIR-V
;;; types, which means that the matrix type in particular requires
;;; creating a column vector type.
(define-function enumerate-primitive-type ((type primitive-type-encoding)
                                           (enumerations hash-table)
                                           (type-list list)
                                           (id-generator function))
  (cond ((or (%%scalar-p type) (%%void-p type))
         (%enumerate-type type
                          enumerations
                          type-list
                          id-generator))
        ((%%vec-p type)
         (%enumerate-type type
                          enumerations
                          (enumerate-primitive-type (%%vec-scalar type)
                                                    enumerations
                                                    type-list
                                                    id-generator)
                          id-generator))
        ((%%matrix-p type)
         (%enumerate-type type
                          enumerations
                          (enumerate-primitive-type (%%matrix-column-vec-type type)
                                                    enumerations
                                                    type-list
                                                    id-generator)
                          id-generator))
        (t (error "Invalid type encoding."))))

;;; Assign a number to the type(s) contained in the derivative type
;;; and then assign a number to the derivative type itself.
(define-function enumerate-derivative-type ((type derivative-type-encoding)
                                            (enumerations hash-table)
                                            (type-list list)
                                            (id-generator function))
  (flet ((enumerate-multiple-types (index type-list)
           (loop :for (start end) := (multiple-value-list (next-encoded-form type index))
                   :then (multiple-value-list (next-encoded-form type (1+ end)))
                 :until (>= end (length type))
                 :unless (= end start)
                   :collect (embedded-type type start end)
                     :into types*
                 :finally (return (loop :for type* :in types*
                                        :for type-list* := (enumerate-types type*
                                                                            enumerations
                                                                            nil
                                                                            id-generator)
                                        :if (numberp types*)
                                          :collect type-list* :into type-list**
                                        :else
                                          :append (reverse type-list*) :into type-list**
                                        :finally (return (append (reverse type-list**) type-list)))))))
    (cond ((%%array-p type)
           (%enumerate-type type
                            enumerations
                            (let ((element-type (enumerate-types (embedded-type type 2 (length type))
                                                                 enumerations
                                                                 type-list
                                                                 id-generator))
                                  (index-type (enumerate-types (%%u 32)
                                                               enumerations
                                                               type-list
                                                               id-generator))
                                  (length-pseudo-type (%enumerate-type (list :length
                                                                             (gethash (%%u 32) enumerations)
                                                                             (%%array-length type))
                                                                       enumerations
                                                                       type-list
                                                                       id-generator)))
                              (append length-pseudo-type
                                      index-type
                                      element-type))
                            id-generator))
          ((%%pointer-p type)
           (%enumerate-type type
                            enumerations
                            (enumerate-types (embedded-type type 2 (length type))
                                             enumerations
                                             type-list
                                             id-generator)
                            id-generator))
          ((%%ftype-p type)
           (%enumerate-type type
                            enumerations
                            (enumerate-multiple-types 0 type-list)
                            id-generator))
          ((%%struct-p type)
           (%enumerate-type type
                            enumerations
                            (enumerate-multiple-types 0 type-list)
                            id-generator))
          ((%%spir-v-image-p type)
           (%enumerate-type type
                            enumerations
                            (enumerate-types (aref type 1)
                                             enumerations
                                             type-list
                                             id-generator)
                            id-generator))
          ((%%spir-v-sampled-image-p type)
           (%enumerate-type type
                            enumerations
                            (enumerate-types (subseq type 1)
                                             enumerations
                                             type-list
                                             id-generator)
                            id-generator))
          ((%%disjoint-type-p type)
           (error "Type erasure should remove disjoint types first because they do not exist in SPIR-V."))
          (t (error "Invalid type encoding.")))))

;;; The actual exported entry point to this section of code.
(define-function enumerate-types ((type type-encoding)
                                  (enumerations hash-table)
                                  (type-list list)
                                  (id-generator function))
  "
Goes through the type encoding as well as all embedded types
recursively encoded in that type in a depth-first fashion. Any type
not already associated with a number will get a new number stored in
the enumerations EQUALP hash table. Scalar and void types are
considered one type, but every other type (even other primitive
types!) may encode more than one type.

This function is used for obtaining SPIR-V IDs for a SPIR-V type and
thus should only be used on the low-level types and not any
refinements or other type abstractions. The id-generator is assumed to
have an implicit counter state that goes up after every call.
"
  (etypecase type
    (primitive-type-encoding (enumerate-primitive-type type
                                                       enumerations
                                                       type-list
                                                       id-generator))
    (derivative-type-encoding (enumerate-derivative-type type
                                                         enumerations
                                                         type-list
                                                         id-generator))))

(define-function type-erasure ((type type-encoding))
  (if (%%typep type '%%disjoint-type)
      (%%disjoint-type-base-type type)
      type))
