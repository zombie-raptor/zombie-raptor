(defpackage #:zombie-raptor/zrvl/spir-v-enums
  (:documentation "Names the various enums used by SPIR-V.")
  (:import-from #:alexandria)
  (:use #:cl
        #:zr-utils)
  (:export #:%storage-class-enum
           #:addressing-model-enum
           #:built-in-enum
           #:capability-type-enum
           #:decoration-enum
           #:execution-mode-enum
           #:execution-model-enum
           #:function-control
           #:function-control-enum
           #:language-enum
           #:memory-model-enum
           #:storage-class-enum))

(in-package #:zombie-raptor/zrvl/spir-v-enums)

;;; TODO: get an ID
(defconstant +zrvl-language-id+ #x00000000)

(defmacro define-spir-v-enum (name &body enums)
  (let ((%name (intern (concatenate 'string "%" (symbol-name name)))))
    (alexandria:with-gensyms (enum)
      `(progn
         (deftype ,name ()
           '(member ,@(loop :for enum :in enums
                            :collect (car enum))))
         (define-function (,name :return uint32) ((,enum keyword))
           (ecase ,enum
             ,@enums))
         (define-function (,%name :return keyword) ((,enum uint32))
           (ecase ,enum
             ,@(remove-duplicates (mapcar (destructuring-lambda (x y)
                                            `(,y ,x))
                                          enums)
                :test #'=
                :key #'car
                :from-end t)))))))

;;; TODO: more execution models (ray tracing)
(define-spir-v-enum execution-model-enum
  (:vertex 0)
  (:tessellation-control 1)
  (:tessellation-evaluation 2)
  (:geometry 3)
  (:fragment 4)
  (:gl-compute 5)
  (:kernel 6)
  (:task-nv 5267)
  (:mesh-nv 5268))

;;; TODO: more execution modes
(define-spir-v-enum execution-mode-enum
  (:origin-lower-left 8))

(define-spir-v-enum addressing-model-enum
  (:logical 0)
  (:physical32 1)
  (:physical64 2)
  (:physical-storage-buffer-64 5348)
  (:physical-storage-buffer-64-ext 5348))

(define-spir-v-enum memory-model-enum
  (:simple 0)
  (:glsl450 1)
  (:opencl 2)
  (:vulkan 3)
  (:vulkan-khr 3))

;;; TODO: Many, many more capabilities
(define-spir-v-enum capability-type-enum
  (:matrix 0)
  (:shader 1)
  (:geometry 2)
  (:tessellation 3)
  (:addresses 4)
  (:linkage 5)
  (:kernel 6)
  (:vector16 7))

(define-spir-v-enum function-control-enum
  (:none 0)
  (:inline 1)
  (:dontinline 2)
  (:notinline 2)
  (:pure 4)
  (:const 8))

;;; TODO: more
(define-spir-v-enum storage-class-enum
  (:uniform-constant 0)
  (:input 1)
  (:uniform 2)
  (:output 3)
  (:workgroup 4)
  (:cross-workgroup 5)
  (:private 6)
  (:function 7)
  (:generic 8)
  (:push-constant 9)
  (:atomic-counter 10)
  (:image 11)
  (:storage-buffer 12))

;;; TODO: more
(define-spir-v-enum decoration-enum
  (:relaxed-precision 00)
  (:spec-id 01)
  (:block 02)
  (:buffer-block 03)
  (:row-major 04)
  (:col-major 05)
  (:array-stride 06)
  (:matrix-stride 07)
  (:glsl-shared 08)
  (:glsl-packed 09)
  (:c-packed 10)
  (:built-in 11)
  (:no-perspective 13)
  (:flat 14)
  (:patch 15)
  (:centroid 16)
  (:sample 17)
  (:invariant 18)
  (:restrict 19)
  (:aliased 20)
  (:volatile 21)
  (:constant 22)
  (:coherent 23)
  (:non-writable 24)
  (:non-readable 25)
  (:uniform 26)
  (:uniform-id 27)
  (:saturated-conversion 28)
  (:stream 29)
  (:location 30)
  (:component 31)
  (:index 32)
  (:binding 33)
  (:descriptor-set 34)
  (:offset 35)
  (:xfb-buffer 36)
  (:xfb-stride 37)
  (:func-param-attr 38)
  (:fp-rounding-mode 39)
  (:fp-fast-math-mode 40)
  (:linkage-attributes 41)
  (:no-contraction 42)
  (:input-attachment-index 43)
  (:alignment 44)
  (:max-byte-offset 45)
  (:alignment-id 46)
  (:max-byte-offset-id 47)
  (:no-signed-wrap 4469)
  (:no-unsigned-wrap 4470))

;;; TODO: more
(define-spir-v-enum built-in-enum
  (:position 00)
  (:point-size 01)
  (:clip-distance 03)
  (:cull-distance 04)
  (:vertex-id 5)
  (:instance-id 6)
  (:primitive-id 7)
  (:invocation-id 8)
  (:layer 9)
  (:viewport-index 10)
  (:tess-level-outer 11)
  (:tess-level-inner 12)
  (:tess-coord 13)
  (:patch-vertices 14)
  (:frag-coord 15)
  (:point-coord 16)
  (:front-facing 17)
  (:sample-id 18)
  (:sample-position 19)
  (:sample-max 20)
  (:frag-depth 22)
  (:helper-invocation 23)
  (:num-workgroups 24)
  (:workgroup-size 25)
  (:workgroup-id 26)
  (:local-invocation-id 27)
  (:global-invocation-id 28)
  (:local-invocation-index 29)
  (:work-dim 30)
  (:global-size 31)
  (:enqueued-workgroup-size 32)
  (:global-offset 33)
  (:global-linear-id 34)
  (:subgroup-size 36)
  (:subgroup-max-size 37)
  (:num-subgroups 38)
  (:num-enqueued-subgroups 39)
  (:subgroup-id 40)
  (:subgroup-local-invocation-id 41)
  (:vertex-index 42)
  (:instance-index 43)
  (:base-vertex 4424)
  (:base-instance 4425)
  (:draw-index 4426))

;;; TODO: the other languages, for disassembly?
(define-spir-v-enum language-enum
  (:zrvl #.+zrvl-language-id+))

(define-function (function-control :inline t) ((controls (or keyword list)))
  (let ((controls (if (listp controls) controls (list controls))))
    (loop :for control :in controls
          :for mask :of-type octet
            := (let ((mask (or mask 0)))
                 (logior mask
                         (function-control-enum control)))
          :finally (return mask))))
