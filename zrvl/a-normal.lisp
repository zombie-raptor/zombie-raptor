(defpackage #:zombie-raptor/zrvl/a-normal
  (:documentation "Generates an intermediate representation for the vector language.")
  (:use #:cl
        #:zombie-raptor/core/conditions
        #:zombie-raptor/zrvl/core-types
        #:zombie-raptor/zrvl/instructions
        #:zombie-raptor/zrvl/scalar
        #:zr-utils)
  (:import-from #:alexandria)
  (:import-from #:float-features)
  (:import-from #:zombie-raptor/math/vector
                #:vec2
                #:vec3
                #:vec4)
  (:import-from #:zombie-raptor/zrvl/spir-v-enums
                #:storage-class-enum)
  (:export #:%%args
           #:%%binding
           #:%%binding-equal
           #:%%binding-equal*
           #:%%binding-p
           #:%%constant
           #:%%constant-composite
           #:%%funcall
           #:%%literal
           #:%%parameter
           #:%%variable
           #:%%variable-binding
           #:%declare-parameter
           #:%function-header
           #:%initialize-variable
           #:%literal
           #:%scope
           #:%setf
           #:*id-counter*
           #:check-bounds
           #:check-matrix-bounds
           #:define-variadic-function
           #:define-zrvl-alias
           #:define-zrvl-generic
           #:define-zrvl-primitive
           #:define-zrvl-type
           #:full-ir-compile
           #:instruction-forms
           #:intermediate-representation
           #:minimal-ir-compile
           #:process-intermediate-forms
           #:process-zrvl-type
           #:rebind-bindings
           #:scalar-forms
           #:spir-v-form
           #:type-ids
           #:zrvl-boolean))

(in-package #:zombie-raptor/zrvl/a-normal)

;;; TODO: Make sure `float-features:with-float-traps-masked' is used
;;; in the initialization of main loop because it is too large to use
;;; at every ZRVL usage on the CPU.

;;; TODO: Generate SPIR-V, scalar CL, and SB-SIMD from the processed
;;; intermediate forms.

;;; TODO: Eventually introspect-environment:variable-type can be used
;;; to create an implicit global for every mentioned variable in the
;;; outside environment that has a declared type, which means these
;;; functions that are currently errors can exist with a parallel
;;; define-compiler-macro that expands to the CPU (scalar or SIMD)
;;; versions of ZRVL. These are in define-variadic-function,
;;; define-zrvl-primitive, define-zrvl-generic, and define-zrvl-alias.
;;; This can replace the math library.

;;; Global hash tables that store function metadata
(defvar *variadic* (make-hash-table))
(defvar *generics* (make-hash-table))
(defvar *signatures* (make-hash-table))
(defvar *aliases* (make-hash-table))
(defvar *functions* (make-hash-table))
(defvar *primitive-functions* (make-hash-table))
(defvar *type-names* (make-hash-table))

;;; "Global" that's used locally.
(defparameter *current-scope* nil)

(defvar *id-counter* 1)

;;; TODO: Add a define-disjoint-zrvl-type variant, which adds an
;;; additional restriction that the distinctly defined types are not
;;; the same even if they otherwise match, e.g. structs of different
;;; names from each other or quaternions vs vec4.
(defmacro define-zrvl-type (name lambda-list &body body)
  (declare (ignore lambda-list))
  (unless (endp (cdr body))
    (error "Invalid syntax."))
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (setf (gethash ',name *type-names*) ,(car body))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun process-zrvl-type (type)
    (cond ((constantp type)
           type)
          ((symbolp type)
           (let ((%type (gethash type *type-names*)))
             (unless %type
               (error "Type ~S does not exist" %type))
             %type))
          (t
           (eval type)))))

;;; A-normal encoding objects
;;;
;;; Bindings can consist of function calls (funcalls), constants, and
;;; variables. Every funcall should only refer to bound identifiers as
;;; their arguments.
;;;
;;; Each object has a concise constructor.

(define-function (new-scope-id :inline t) ()
  (gensym (symbol-name '#:scope-)))

(define-function (new-ir-id :inline t) ()
  (prog1 *id-counter*
    (incf *id-counter*)))

(define-function (constant-bindings :inline t) (forms)
  (car forms))

(define-function (main-bindings :inline t) (forms)
  (cadr forms))

(deftype %%args ()
  `(simple-array uint32 (*)))

(deftype %%literals ()
  `simple-bit-vector)

(define-function (%%args :inline t) (&rest args)
  (make-array (length args)
              :element-type 'uint32
              :initial-contents args))

(define-compiler-macro %%args (&whole form &rest args)
  (declare (ignore form))
  `(make-array ,(length args)
               :element-type 'uint32
               :initial-contents (list ,@args)))

(define-function (%%literals :inline t) (&rest bits)
  (make-array (length bits)
              :element-type 'bit
              :initial-contents bits))

(define-compiler-macro %%literals (&whole form &rest bits)
  (declare (ignore form))
  `(make-array ,(length bits)
               :element-type 'bit
               :initial-contents (list ,@bits)))

(deftype %%binding-value ()
  `(or %%funcall %%constant %%constant-composite %%literal %%variable %%variable-binding %%parameter))

(defstruct %%funcall
  (fun nil :type symbol)
  (args (make-array 0 :element-type 'uint32) :type %%args)
  (literals (make-array 0 :element-type 'bit) :type %%literals))

(define-function (%%funcall :inline t) (fun args literals)
  (make-%%funcall :fun fun :args args :literals literals))

(defmethod print-object ((object %%funcall) stream)
  (write-char #\( stream)
  (format stream "~S" '%%funcall)
  (format stream " '~S " (%%funcall-fun object))
  (write-char #\( stream)
  (format stream "~S" '%%args)
  (loop :for arg :across (%%funcall-args object)
        :do (format stream " ~S" arg))
  (write-char #\) stream)
  (format stream " ~S" (%%funcall-literals object))
  (write-char #\) stream)
  object)

(defstruct %%constant
  (value nil))

(define-function (%%constant :inline t) (value)
  (make-%%constant :value value))

(defmethod print-object ((object %%constant) stream)
  (write-char #\( stream)
  (format stream "~S" '%%constant)
  (format stream " ~S" (%%constant-value object))
  (write-char #\) stream)
  object)

;;; Constant-composites are constructors for certain SPIR-V recognized
;;; data structures that can only reference bindings to other
;;; constants or constant-composites. These, naturally, can only exist
;;; after constant propagation and not in the initial generation of
;;; the IR.
;;;
;;; These can only exist for data structures SPIR-V recognizes or
;;; things implemented in terms of those: structures, arrays, vectors,
;;; or matrices. Initially, these will only support vectors and
;;; matrices.
;;;
;;; The actual type is handled by the type inference system. This only
;;; needs to know the constituents.
(defstruct %%constant-composite
  (constituents nil :type %%args))

(define-function (%%constant-composite :inline t) (constituents)
  (make-%%constant-composite :constituents constituents))

(defmethod print-object ((object %%constant-composite) stream)
  (write-char #\( stream)
  (format stream "~S " '%%constant-composite)
  (write-char #\( stream)
  (format stream "~S" '%%args)
  (loop :for arg :across (%%constant-composite-constituents object)
        :do (format stream " ~S" arg))
  (write-char #\) stream)
  (write-char #\) stream)
  object)

;;; Literals are for constants that must be included in the SPIR-V
;;; instruction instead of indirectly referencing a constant, but
;;; whose value is not yet known until constant propagation.
;;;
;;; In other words, the literal-reference must be determined to be a
;;; constant value, which replaces the reference before generating the
;;; SPIR-V.
(defstruct %%literal
  (reference nil :type uint32))

(define-function (%%literal :inline t) (reference)
  (make-%%literal :reference reference))

(defmethod print-object ((object %%literal) stream)
  (write-char #\( stream)
  (format stream "~S" '%%literal)
  (format stream " ~S" (%%literal-reference object))
  (write-char #\) stream)
  object)

(defstruct %%variable
  (name nil :type symbol)
  (reference nil :type uint32))

(define-function (%%variable :inline t) (name reference)
  (make-%%variable :name name :reference reference))

(defmethod print-object ((object %%variable) stream)
  (write-char #\( stream)
  (format stream "~S" '%%variable)
  (format stream " '~S" (%%variable-name object))
  (format stream " ~S" (%%variable-reference object))
  (write-char #\) stream)
  object)

;;; A known, bound variable that has some scope. Any %%variable that
;;; refers to a %%variable-binding can simply substitute itself for
;;; the binding's value, which could be a constant or some other
;;; binding reference.
(defstruct %%variable-binding
  (name nil :type symbol)
  (item nil :type uint32))

(define-function (%%variable-binding :inline t) (name item)
  (make-%%variable-binding :name name :item item))

(defmethod print-object ((object %%variable-binding) stream)
  (write-char #\( stream)
  (format stream "~S" '%%variable-binding)
  (format stream " '~S" (%%variable-binding-name object))
  (format stream " ~S" (%%variable-binding-item object))
  (write-char #\) stream)
  object)

;;; A parameter whose value is unknown at compilation time. These are
;;; loop, function, or global parameters and they must still have
;;; known types. These are loaded and stored as SPIR-V variables,
;;; except for function parameters, which use OpFunctionParameter in
;;; SPIR-V.
(defstruct %%parameter
  (name nil :type symbol)
  (class nil :type keyword))

(define-function (%%parameter :inline t) (name class)
  (make-%%parameter :name name :class class))

(defmethod print-object ((object %%parameter) stream)
  (write-char #\( stream)
  (format stream "~S" '%%parameter)
  (format stream " '~S" (%%parameter-name object))
  (format stream " ~S" (%%parameter-class object))
  (write-char #\) stream)
  object)

(defstruct %%binding
  (type t)
  (id nil :type uint32)
  (value nil :type %%binding-value)
  (metadata nil :type list))

(define-function (%%binding :inline t) (type id value &optional metadata)
  (make-%%binding :type type :id id :value value :metadata metadata))

(define-function (replace-%%binding-value :inline t) (binding value)
  (%%binding (%%binding-type binding)
             (%%binding-id binding)
             value
             (%%binding-metadata binding)))

(define-function (replace-%%binding-id :inline t) (binding id)
  (%%binding (%%binding-type binding)
             id
             (%%binding-value binding)
             (%%binding-metadata binding)))

(define-function (replace-%%binding-type :inline t) (binding type)
  (%%binding type
             (%%binding-id binding)
             (%%binding-value binding)
             (%%binding-metadata binding)))

;;; Useful functions on A-normal encoding objects, which are mostly
;;; many varieties of equality tests

(defmethod print-object ((object %%binding) stream)
  (write-char #\( stream)
  (format stream "~S " '%%binding)
  (let ((type (%%binding-type object)))
    ;; TODO: also correctly write derivative types
    (if (numberp type)
        (write-linear-algebra-type type stream)
        (format stream "~S" type)))
  (format stream " ~S" (%%binding-id object))
  ;; (format stream " ~:[~;'~]~S" (%%binding-id object) (%%binding-id object))
  (format stream " ~S" (%%binding-value object))
  (format stream " ~:[~;'~]~S" (%%binding-metadata object) (%%binding-metadata object))
  (write-char #\) stream)
  object)

(define-function %%funcall-equal ((x %%funcall) (y %%funcall))
  (and (eql (%%funcall-fun x)
            (%%funcall-fun y))
       (= (length (%%funcall-args x))
          (length (%%funcall-args y)))
       (every #'eql
              (%%funcall-args x)
              (%%funcall-args y))))

;;; Variation of %%function-equal that doesn't check the IDs for
;;; equality.
(define-function %%funcall-equal* ((x %%funcall) (y %%funcall))
  (and (eql (%%funcall-fun x)
            (%%funcall-fun y))
       (= (length (%%funcall-args x))
          (length (%%funcall-args y)))))

(define-function %%constant-equal ((x %%constant) (y %%constant))
  (and (eql (%%constant-value x)
            (%%constant-value y))))

;;; TODO: reference must also equal
(define-function %%variable-equal ((x %%variable) (y %%variable))
  (and (eql (%%variable-name x)
            (%%variable-name y))))

;;; Checks the equality of a binding's values. The optional argument,
;;; if true, tells it to not check for ID equality.
;;;
;;; TODO: add support for constant-composite, literal,
;;; variable-binding, parameter
(defun %%value-equal (x y &optional skip-ids?)
  (and (or (and (%%funcall-p x)
                (%%funcall-p y))
           (and (%%constant-p x)
                (%%constant-p y))
           (and (%%variable-p x)
                (%%variable-p y)))
       (etypecase x
         (%%funcall (if skip-ids?
                        (%%funcall-equal* x y)
                        (%%funcall-equal x y)))
         (%%constant (%%constant-equal x y))
         (%%variable (%%variable-equal x y)))))

(define-function (%%binding-type= :inline t) ((x %%binding) (y %%binding))
  (%%type= (%%binding-type x)
           (%%binding-type y)))

(defun %%binding-equal (x y)
  (and (%%binding-type= x y)
       (eql (%%binding-id x)
            (%%binding-id y))
       (%%value-equal (%%binding-value x)
                      (%%binding-value y))))

;;; Variation of %%binding-equal that doesn't check the IDs for
;;; equality.
(defun %%binding-equal* (x y)
  (and (%%binding-type= x y)
       (%%value-equal (%%binding-value x)
                      (%%binding-value y)
                      t)))

;;; Variadic (arbitrary-length) functions are special cases that need
;;; to be undone at compilation time if possible for maximum
;;; efficiency -- and this is a DSL for numeric so efficiency is
;;; desired! Fortunately, they mostly represent arithmetic operators
;;; and mostly reduce from n-ary to binary/dyadic so there's a clear
;;; pattern to undo.

;;; TODO: mark predicates for transform-variadic-function
(defmacro define-variadic-function (name
                                    lambda-list
                                    (nullary-function unary-function binary-function)
                                    &key predicate?)
  "
Define a variadic (arbitrary-length) function that can take
0 (nullary), 1 (unary), and 2 (binary) arguments. If more than 2
arguments are provided at compilation time, it is reduced down to 2.

The variadic function is defined by providing lower level function
names that this variadic function becomes, either at runtime at the
worst case as a higher-order-function, or at compile time if possible.

If the nullary and/or unary is supposed to be invalid instead of
defined, then provide the invalid function name NIL. For the binary
function form, providing NIL is not a good idea because such a
function is better expressed as a function with one optional argument.

For example, (+ x y z) can become something like (%+ (%+ x y) z) while
also having nullary and unary alternatives, while - can have NIL for
the nullary form because (-) should not be valid despite (+) being
valid.

By contrast, a predicate takes things that are probably not boolean
and returns boolean results so the forms must be combined with AND.

For instance, (= x y z w) might become (and (= x y) (= y z) (= z w))

This is somewhat simplified because care must be taken to not evaluate
y or z twice.
"
  (declare (ignore predicate?))
  (unless (find '&rest lambda-list)
    (error "A ZRVL variadic function must have a lambda-list that looks variadic."))
  (unless binary-function
    (error "A ZRVL variadic function requires a binary-function to expand into."))
  (cond (nullary-function
         (unless unary-function
           (error "A ZRVL variadic function cannot support 0 and 2 arguments without also supporting 1 argument."))
         (unless (and (= 2 (length lambda-list))
                      (eql (nth 0 lambda-list) '&rest)
                      (symbolp (nth 1 lambda-list)))
           (error "The lambda-list does not match the function expansion.")))
        (unary-function
         (unless (and (= 3 (length lambda-list))
                      (eql (nth 1 lambda-list) '&rest)
                      (symbolp (nth 0 lambda-list))
                      (symbolp (nth 2 lambda-list)))
           (error "The lambda-list does not match the function expansion.")))
        (binary-function
         (unless (and (= 4 (length lambda-list))
                      (eql (nth 2 lambda-list) '&rest)
                      (symbolp (nth 0 lambda-list))
                      (symbolp (nth 1 lambda-list))
                      (symbolp (nth 2 lambda-list)))
           (error "The lambda-list does not match the function expansion."))))
  `(progn
     (setf (gethash ',name *variadic*)
           (vector ',nullary-function ',unary-function ',binary-function))
     (defun ,name ,lambda-list
       (declare (ignore ,@(remove '&rest lambda-list)))
       (error "This function must be called through ZRVL."))))

(defun transform-variadic-function (function args)
  (let ((simple-functions (gethash function *variadic*)))
    (case= (length args)
      (0 (let ((nullary-function (aref simple-functions 0)))
           (unless nullary-function
             (error "Invalid argument count 0 for ~A" function))
           `(,nullary-function ,@args)))
      (1 (let ((unary-function (aref simple-functions 1)))
           (unless unary-function
             (error "Invalid argument count 1 for ~A" function))
           `(,unary-function ,@args)))
      ;; TODO: if predicate then use
      ;; TODO: `(and ,@(loop :for args* :on args :while (cdr args*) :collect `(,fun ,(car args*) ,(cadr args*))))
      ;; TODO: but first do a pass to make sure that everything is
      ;; lexically bound so it's only evaluated once
      (t (let ((binary-function (aref simple-functions 2)))
           (reduce (lambda (x y)
                     `(,binary-function ,x ,y))
                   args))))))

(defun variadic-transformation-p (function)
  (and (symbolp function)
       (gethash function *variadic*)))

;;; This is the A-normal transformation itself, minus the parts that
;;; undo the variadic functions, which belong above and are called
;;; first. Everything afterwards is called after this transformation
;;; is done. The exact order is expressed in the function
;;; `intermediate-representation', which returns the intermediate
;;; representation (IR) that eventually becomes SPIR-V (GPU), CL
;;; (CPU), or implementation-and-architecture-specific SIMD CL (CPU).

;;; A literal is left unevaluated so it can be passed through to
;;; SPIR-V and used directly as a literal instead of as a constant.
;;; This is distinct from, but similar to, the concept of CL:QUOTE.
;;;
;;; In terms of this IR, a literal must ultimately reference a
;;; constant, which is placed in the place of the literal instead of
;;; referencing the constant.
;;;
;;; This doesn't actually exist as a function. The `a-normal' function
;;; below uses it.
(defun %literal (object)
  (declare (ignore object))
  (error "The special form ~A is not meant to be called directly." '%literal))

(defun %initialize-variable (name type item)
  (declare (ignore name type item))
  (error "The special form ~A is not meant to be called directly." '%initialize-variable))

(defun %declare-parameter (name type class &rest metadata)
  (declare (ignore name type class metadata))
  (error "The special form ~A is not meant to be called directly." '%declare-parameter))

(defmacro %scope (bindings &body body)
  (declare (ignore bindings body))
  `(error "The special form ~A is not meant to be called directly." '%scope))

(defun %setf (place new-value)
  (declare (ignore place new-value))
  (error "The special form ~A is not meant to be called directly." '%setf))

;;; TODO: support function parameters
(defun %function-header (function-type)
  (declare (ignore function-type))
  (error "The special form ~A is not meant to be called directly." '%function-header))

(defun add-scope (binding parents scopes)
  (let ((table (make-hash-table)))
    (setf (gethash binding table) parents
          (gethash binding scopes) table)
    (values table binding)))

(defun add-variable-to-scope (name binding current-scope scopes)
  (setf (gethash name (gethash current-scope scopes)) binding))

(defun actual-variable-scope (name current-scope scopes)
  (let* ((current-scope* (gethash current-scope scopes))
         (scope-hierarchy (gethash current-scope current-scope*))
         (name-in-current-scope (gethash name current-scope*)))
    (if name-in-current-scope
        (values name-in-current-scope current-scope)
        (loop :for scope :in scope-hierarchy
              :for binding := (gethash name (gethash scope scopes))
              :until binding
              :finally (return binding)))))

(define-condition unused-zrvl-variable (style-warning)
  ((%variable
    :initarg :variable
    :reader error-variable))
  (:report (lambda (condition stream)
             (format stream
                     "You defined an unused variable ~A"
                     (error-variable condition)))))

(defun a-normal (expressions scopes)
  (labels ((%bind-form (binding form)
             (make-%%binding :id binding :value form))
           (%atom (binding item)
             (cond ((keywordp item)
                    (list (%bind-form binding (%%constant item))))
                   ((typep item 'zrvl-boolean)
                    (list (%bind-form binding (%%constant item))))
                   ((symbolp item)
                    (let* ((reference (actual-variable-scope item (car *current-scope*) scopes))
                           (reference* (gethash reference (gethash :all-bindings scopes))))
                      (cond (reference*
                             (incf (car reference*))
                             (list (%bind-form binding (%%variable item reference))))
                            ;; Global symbol macro
                            ((nth-value 1 (macroexpand item))
                             (%bind-list-or-atom binding (macroexpand item)))
                            (t
                             (error "I couldn't find a definition for the variable ~S" item)))))
                   (t
                    (list (%bind-form binding (%%constant item))))))
           (%progn (binding expressions)
             (loop :for expressions* :on expressions
                   :for item := (car expressions*)
                   :for id := (if (endp (cdr expressions*))
                                  binding
                                  (new-ir-id))
                   :append (%bind-list-or-atom id item)))
           (%literal (binding list)
             (unless (= 1 (length list))
               (error "A literal must have exactly one argument."))
             (let* ((item (car list))
                    (id (new-ir-id))
                    (child (%bind-list-or-atom id item)))
               `(,@child ,(%bind-form binding (%%literal id)))))
           (%check-bounds (binding list)
             (declare (ignore binding list))
             ;; TODO: fixme
             nil)
           ;; The return type is special and goes with the binding at
           ;; this stage of the IR. The rest is treated like a special
           ;; form of function call.
           (%inst (binding start list)
             (multiple-value-bind (id type rest)
                 (ecase (gethash start *instructions*)
                   (:a (values 0
                               (%%void)
                               list))
                   (:b (values binding
                               (%%void)
                               (cdr list)))
                   (:bc (values binding
                                (process-zrvl-type (cadr list))
                                (cddr list))))
               (unless (or (zerop id)
                           (string= (car list) '#:id))
                 (error "Instruction ID not defined as ID, but as ~S" (car list)))
               (%function-call id start rest type)))
           (%initialize-variable (binding list)
             (destructuring-bind (name type item new-scope) list
               (let* ((id (new-ir-id))
                      (child (%bind-list-or-atom id item)))
                 (add-variable-to-scope name binding new-scope scopes)
                 (setf (gethash binding (gethash :all-bindings scopes)) (cons 0 list))
                 `(,@child
                   ,(%%binding (if type
                                   (process-zrvl-type type)
                                   ;; NIL means infer later.
                                   type)
                               binding
                               (%%variable-binding name id)
                               nil)))))
           (%parameter (binding list)
             (destructuring-bind (name type class &rest metadata) list
               (add-variable-to-scope name binding (car *current-scope*) scopes)
               (setf (gethash binding (gethash :all-bindings scopes)) (cons 0 list))
               (list (%%binding (process-zrvl-type type)
                                binding
                                (%%parameter name class)
                                metadata))))
           (%function-call (binding start list &optional (type t))
             (loop :for item :in list
                   :for id := (new-ir-id)
                   :append (%bind-list-or-atom id item) :into children
                   :collect id :into current
                   :finally (return `(,@children
                                      ,(%%binding type
                                                  binding
                                                  (%%funcall start
                                                             (apply #'%%args current)
                                                             (make-array (length current)
                                                                         :element-type 'bit
                                                                         :initial-element 0))
                                                  nil)))))
           ;; TODO: Right now, it's hard coded to turn (setf (foo ...)
           ;; val) into (%setf-foo ... val) but instead use another
           ;; hash table for these and look up foo
           (%setf (binding list)
             ;; TODO: Handle place CL-style for SETF and implicitly
             ;; return the value after the set if anything needs to
             ;; use the return value, perhaps by using a new LET.
             (destructuring-bind (place new-value) list
               (if (or (consp place)
                       ;; A global symbol macro not overridden by
                       ;; local lexical scope.
                       (and (symbolp place)
                            (not (gethash (actual-variable-scope place (car *current-scope*) scopes)
                                          (gethash :all-bindings scopes)))
                            (nth-value 1 (macroexpand place))
                            (consp (macroexpand place))))
                   (destructuring-bind (name &rest rest)
                       (if (consp place) place (macroexpand place))
                     (multiple-value-bind (name* package*)
                         (values (symbol-name name)
                                 (symbol-package name))
                       (let ((writer (intern (concatenate 'string
                                                          (symbol-name '#:%setf-)
                                                          name*)
                                             package*)))
                         (%a-normal binding `(,writer ,@rest ,new-value)))))
                   (let* ((place-id (new-ir-id))
                          (value-id (new-ir-id))
                          (place (%bind-list-or-atom place-id place))
                          (value (%bind-list-or-atom value-id new-value)))
                     (unless (and (= 1 (length place)) (%%variable-p (%%binding-value (car place))))
                       (error "Invalid place to set."))
                     `(,@value
                       ;; The void return type should work with the type
                       ;; inference to prevent this from being used
                       ;; directly.
                       ,(%%binding (%%void)
                                   0
                                   (%%funcall '%write-var
                                              (%%args (%%variable-reference (%%binding-value (car place)))
                                                      value-id)
                                              (%%literals 0 0))
                                   nil))))))
           ;; TODO: As currently structured, '%function-begin is a
           ;; special case in generating instructions because it will
           ;; have to run %%ftype-output-type on the ftype and provide
           ;; both that and the ftype when creating the instruction
           (%function-header (binding list)
             (destructuring-bind (function-type) list
               (let* ((function-type* (process-zrvl-type function-type))
                      #+(or)
                      (function-output-type* (%%ftype-output-type function-type*)))
                 (list (%%binding function-type*
                                  binding
                                  (%%funcall '%function-begin
                                             (%%args)
                                             (%%literals))
                                  nil)))))
           (%a-normal (binding list)
             (let ((start (car list)))
               ;; TODO: Handle quote
               ;; TODO: Handle return
               (case start
                 (progn (%progn binding (cdr list)))
                 (%scope (destructuring-bind (bindings &rest expressions) (cdr list)
                           (let ((scope-name (new-scope-id)))
                             (add-scope scope-name *current-scope* scopes)
                             (let* ((let-bindings (mapcar (lambda (binding)
                                                            (unless (eql (car binding) '%initialize-variable)
                                                              (error "Invalid variable initialization ~A" (car binding)))
                                                            (destructuring-bind (name type item) (cdr binding)
                                                              `(%initialize-variable ,name ,type ,item ,scope-name)))
                                                          bindings))
                                    (processed-let-bindings (loop :for binding :in let-bindings
                                                                  :append (%bind-list-or-atom (new-ir-id) binding))))
                               (push scope-name *current-scope*)
                               (prog1 (append processed-let-bindings
                                              (%progn binding expressions))
                                 (pop *current-scope*))))))
                 (%literal (%literal binding (cdr list)))
                 (%initialize-variable (%initialize-variable binding (cdr list)))
                 (%declare-parameter (%parameter binding (cdr list)))
                 (%setf (%setf binding (cdr list)))
                 (%function-header (%function-header binding (cdr list)))
                 (check-bounds (%check-bounds binding (cdr list)))
                 (t (cond ((gethash start *instructions*)
                           (%inst binding start (cdr list)))
                          ((variadic-transformation-p start)
                           (%a-normal binding (transform-variadic-function start (cdr list))))
                          ((macro-function start)
                           (%bind-list-or-atom binding (macroexpand list)))
                          (t (%function-call binding start (cdr list))))))))
           (%bind-list-or-atom (binding expression)
             (if (consp expression)
                 (%a-normal binding expression)
                 (%atom binding expression))))
    (let ((top-scope (new-scope-id))
          (*current-scope* nil))
      (add-scope top-scope *current-scope* scopes)
      (push top-scope *current-scope*)
      (setf (gethash :all-bindings scopes) (make-hash-table))
      (let ((result (loop :for expression :in expressions
                          :append (%bind-list-or-atom (new-ir-id) expression))))
        (let ((top-scope* (pop *current-scope*)))
          (unless (and (eq top-scope top-scope*)
                       (endp *current-scope*))
            (error "Lexical scope error.")))
        ;; TODO: enable style warnings for unused variables again when
        ;; there is a way to DECLARE IGNORE and DECLARE IGNORABLE them
        #+(or)
        (do-hash-table (k v (gethash :all-bindings scopes))
          (declare (ignore k))
          (when (zerop (car v))
            ;; TODO: is there a way to have this location provided to
            ;; the calling macro?
            (warn 'unused-zrvl-variable :variable (cadr v))))
        result))))

;;; Constants

(defun split-and-infer-constants (forms)
  (labels ((split-constants (forms)
             (values (remove-if-not (alexandria:compose #'%%constant-p #'%%binding-value)
                                    forms)
                     (remove-if (alexandria:compose #'%%constant-p #'%%binding-value)
                                forms)))
           (infer-constant-type (constant-value)
             (etypecase constant-value
               ((or int32 uint32)
                (%%i 32))
               (single-float
                (%%f 32))
               (double-float
                (%%f 64))
               (zrvl-boolean
                (%%boolean))))
           (infer-constant-types! (constants)
             (loop :for binding :in constants
                   :do (setf (%%binding-type binding)
                             (infer-constant-type (%%constant-value (%%binding-value binding)))))))
    (multiple-value-bind (constants not-constants)
        (split-constants forms)
      (infer-constant-types! constants)
      (list constants not-constants))))

;;; Substitute constant-composite references if in substitutions
(defun substitute-references-in-constants (constants substitutions remove-constants?)
  (loop :for binding :in constants
        :for id := (%%binding-id binding)
        :for value := (%%binding-value binding)
        :unless (and remove-constants? (gethash id substitutions))
          :collect (cond ((%%constant-p value)
                          binding)
                         ((%%constant-composite-p value)
                          (if (find-if (lambda (x)
                                         (gethash x substitutions))
                                       (%%constant-composite-constituents value))
                              (let ((new-refs (%%constant-composite (map '%%args
                                                                         (lambda (item)
                                                                           (gethash item substitutions item))
                                                                         (%%constant-composite-constituents value)))))
                                (replace-%%binding-value binding new-refs))
                              binding))
                         (t (error "Binding ~A is not constant, but is in with the constants"
                                   binding)))))

;;; Substitute all references to other IDs if in substitutions; only
;;; takes in the main (non-constant bindings)
(defun substitute-references (non-constants substitutions)
  (loop :for binding :in non-constants
        :for value := (%%binding-value binding)
        :collect (cond ((and (%%funcall-p value)
                             (find-if (lambda (x)
                                        (gethash x substitutions))
                                      (%%funcall-args value)))
                        (replace-%%binding-value binding
                                                 (%%funcall (%%funcall-fun value)
                                                            (map '%%args
                                                                 (lambda (arg literal)
                                                                   (let ((sub (if (zerop literal)
                                                                                  (gethash arg substitutions arg)
                                                                                  arg)))
                                                                     (if (consp sub)
                                                                         (nth 1 sub)
                                                                         sub)))
                                                                 (%%funcall-args value)
                                                                 (%%funcall-literals value))
                                                            (map '%%literals
                                                                 (lambda (arg literal)
                                                                   (if (= 1 literal)
                                                                       literal
                                                                       (let ((sub (gethash arg substitutions arg)))
                                                                         (if (consp sub) 1 0))))
                                                                 (%%funcall-args value)
                                                                 (%%funcall-literals value)))))
                       ((and (%%variable-binding-p value)
                             (gethash (%%variable-binding-item value) substitutions))
                        (replace-%%binding-value binding
                                                 (%%variable-binding (%%variable-binding-name value)
                                                                     (gethash (%%variable-binding-item value) substitutions))))
                       ((and (%%literal-p value)
                             (gethash (%%literal-reference value) substitutions))
                        (replace-%%binding-value binding
                                                 (%%literal (gethash (%%literal-reference value) substitutions))))
                       ((and (%%variable-p value)
                             (gethash (%%variable-reference value) substitutions))
                        (replace-%%binding-value binding
                                                 (%%variable (%%variable-name value)
                                                             (gethash (%%variable-reference value) substitutions))))
                       (t binding))))

;;; Substitutes the constants and then the main bindings if any of the
;;; IDs are in the substitutions table
(defun substitute-all-references (bindings substitutions remove-constants?)
  (list (substitute-references-in-constants (constant-bindings bindings) substitutions remove-constants?)
        (substitute-references (main-bindings bindings) substitutions)))

;;; Removes any duplicate constants after the first mention of that
;;; constant and then substitutes all future references. Note that
;;; constant-composites need to come after the constants that they
;;; reference.
;;;
;;; TODO: Right now, it assumes the constants should be numeric.
(defun handle-duplicate-constants (forms)
  (labels ((same-constant-value-p (a b)
             (and (%%constant-p a)
                  (%%constant-p b)
                  (numberp (%%constant-value a))
                  (numberp (%%constant-value b))
                  (= (%%constant-value a)
                     (%%constant-value b))))
           (find-duplicate-constants (constants)
             (loop :for constants* :on constants
                   :for binding := (car constants*)
                   :for id := (%%binding-id binding)
                   :with duplicates := (make-hash-table)
                   :unless (gethash id duplicates)
                     :do (loop :for binding* :in (cdr constants*)
                               :for id* := (%%binding-id binding*)
                               :when (and (%%binding-type= binding binding*)
                                          (same-constant-value-p (%%binding-value binding)
                                                                 (%%binding-value binding*)))
                                 :do (setf (gethash id* duplicates) id))
                   :finally (return duplicates))))
    (substitute-all-references forms
                               (find-duplicate-constants (constant-bindings forms))
                               t)))

;;; Type inference

(eval-when (:compile-toplevel :load-toplevel :execute)
  ;; For generics, the final value is the monomorphic function to
  ;; replace the generic with.
  (defun signature-list-to-table (signature-list table final-value)
    (loop :for signature* :on signature-list
          :for type := (process-zrvl-type (car signature*))
          :for table* := table :then next
          :for next := (if (endp (cdr signature*))
                           (if (and (gethash type table*)
                                    (not (eq (gethash type table*)
                                             final-value)))
                               (error "Invalid generic: multiple generics produce the same result ~A vs ~A."
                                      (gethash type table*)
                                      final-value)
                               final-value)
                           (if (gethash type table*)
                               (progn
                                 (unless (hash-table-p (gethash type table*))
                                   (error "Invalid generic: number of parameters differ."))
                                 (gethash type table*))
                               (make-hash-table :test 'equalp)))
          :do (setf (gethash type table*) next)))

  (defun setup-function-signatures (name monomorphic-signatures)
    (let ((table (or (gethash name *generics*)
                     (make-hash-table :test 'equalp))))
      (unless (gethash name *generics*)
        (setf (gethash name *generics*) table))
      (dolist (signature monomorphic-signatures)
        (destructuring-bind (name &rest type-signature) signature
          (signature-list-to-table type-signature table name))))))

;;; A ZRVL primitive is a function that is _extremely_ aggressively
;;; inlined, to the point where it can be thought of as not existing
;;; at all, except for very complicated higher-order-function
;;; situations that are not currently implemented.
;;;
;;; As a language primarily for arithmetic and linear algebra, most
;;; built-ins that aren't macros should be primitives defined via this
;;; macro, especially since these are often targeting single
;;; instructions in the target platforms like SPIR-V. The rest can
;;; eventually be defined in ZRVL itself if possible.
(defmacro define-zrvl-primitive (name lambda-list &body body)
  ;; TODO: This needs to be more sophisticated to handle &key,
  ;; &optional, &aux, etc., if those are supported later on, but for
  ;; now, it can be a simple mapcar
  (let* ((signature (mapcar (lambda (binding)
                              (unless (consp binding)
                                (error "Binding ~A must be a binding and a type" binding))
                              (assert (= 2 (length binding)))
                              (process-zrvl-type (cadr binding)))
                            lambda-list))
         (body (append (mapcar (lambda (name type)
                                 `(%declare-parameter ,name ,type :function-parameter))
                               (mapcar #'car lambda-list) signature)
                       body)))
    (multiple-value-bind (name* return-type)
        (destructuring-bind (name* &key return) name
          (check-type return (not null))
          (values name* return))
      (let ((return-type (process-zrvl-type return-type)))
        (when (eql (gethash name* *signatures*) :generic)
          (warn "Redefining ZRVL function ~A already defined as a ZRVL generic" name*))
        (let ((variables (mapcar #'car lambda-list)))
          `(progn
             (setf (gethash ',name* *primitive-functions*) t)
             (setf (gethash ',name* *signatures*) (%%ftype ,return-type ,@signature))
             (let ((*id-counter* 1))
               (setf (gethash ',name* *functions*) (minimal-ir-compile ',body)))
             (defun ,name* ,variables
               (declare (ignore ,@variables))
               (error "This function must be called through ZRVL."))))))))

;;; This is achieved through nested hash tables, i.e. the first hash
;;; table represents the first parameter and then the contained hash
;;; table represents the second parameter, etc., until a match is
;;; reached by going through the entire length of the function
;;; arguments.
(defmacro define-zrvl-generic (name lambda-list &body monomorphic-signatures)
  "
Define a generic (arbitrarily typed) function. The function is a
polymorphic function that can be undone into a monomorphic function
through the input types alone.
"
  ;; Validation
  ;; TODO: Make sure the CDRs of the type signatures are valid types.
  ;; TODO: Make sure the lambda-list is simple. No &rest, &optional, etc.
  (let ((length (1+ (length lambda-list))))
    (dolist (signature monomorphic-signatures)
      (unless (consp signature)
        (error "Invalid syntax."))
      (unless (= length (length signature))
        (error "The polymorphic function call must expand into an identical-length monomorphic one."))
      (unless (symbolp (car signature))
        (error "A function name must be provided for each monomorphic function."))))
  (when (multiple-value-bind (signature present?) (gethash name *signatures*)
          (and present? (not (eql signature :generic))))
    (warn "Redefining ZRVL function ~A as a ZRVL generic" name))
  ;; Set up the nested hash tables to represent each argument's type.
  `(progn
     (setf (gethash ',name *signatures*) :generic)
     (setup-function-signatures ',name ',monomorphic-signatures)
     (defun ,name ,lambda-list
       (declare (ignore ,@lambda-list))
       (error "This function must be called through ZRVL."))))

;;; TODO: define a function with real-name's args
(defmacro define-zrvl-alias (alias-name real-name)
  `(progn
     (setf (gethash ',alias-name *aliases*) ',real-name)))

(defun monomorphize (function-name arg-types)
  (let ((table (gethash function-name *generics*)))
    (if table
        (loop :for type :in arg-types
              :for result := (and table (gethash type table))
                :then (and result
                           (if (hash-table-p result)
                               (gethash type result)
                               (error "Too many arguments to function ~A" function-name)))
              :while result
              :finally (cond (result
                              (return result))
                             ((every (lambda (type)
                                       (%%typep type '%%matrix))
                                     arg-types)
                              (error "The matrix dimensions are not conformable for function ~A" function-name))
                             (t
                              (error "No match for function ~A" function-name))))
        function-name)))

(defun function-argument-types (funcall bindings-table)
  (map 'list
       (lambda (arg)
         (if (typep arg 'uint32)
             (let ((binding-type (%%binding-type (gethash arg bindings-table))))
               binding-type)
             (error "Invalid %%funcall arguments")))
       (%%funcall-args funcall)))

(defun function-return-type (function-name args)
  (let ((ftype (gethash function-name *signatures*)))
    (unless ftype
      (error "Function ~A type signature not found" function-name))
    (when (eql ftype :generic)
      (error "Function ~A is generic" function-name))
    (unless (and (derivative-type-encoding-p ftype)
                 (> (length ftype) 1))
      (error "An ftype type must be stored for a given function signature"))
    (unless (= #b011111 (ldb (byte 6 0) (aref ftype 0)))
      (error "Ftype expected, but another type was stored"))
    (unless (arg-list-matches-ftype-p args ftype function-name)
      (error "Invalid types for function ~A" function-name))
    (%%ftype-output-type ftype)))

(defun function-return-type* (funcall bindings-table)
  (let* ((function-name (%%funcall-fun funcall))
         (args (function-argument-types funcall bindings-table))
         (function-name* (if (gethash function-name *aliases*)
                             (gethash function-name *aliases*)
                             function-name))
         (function-name* (monomorphize function-name* args)))
    (values (function-return-type function-name* args)
            (and (not (eql function-name
                           function-name*))
                 function-name*))))

;;; TODO: For now, types must match exactly, but consider having a
;;; simple "numeric tower" of sorts that converts all numeric element
;;; types to the most general, e.g. integer&float->float&float, if
;;; there's no exact match (which means postponing the type error)
(defun infer-types-and-monomorphize (bindings)
  (let ((bindings-table (make-hash-table)))
    (loop :for bindings* :in bindings
          :do (loop :for binding :in bindings*
                    :for id := (%%binding-id binding)
                    :do (unless (zerop id)
                          (when (hash-table-value-present-p id bindings-table)
                            (error "Identifier ~S already exists" id))
                          (setf (gethash id bindings-table)
                                binding))))
    (loop :for binding :in (main-bindings bindings)
          ;; TODO: %%constant-composite? Only if this pass is called
          ;; again after constant propagation, otherwise they
          ;; shouldn't exist yet.
          :do (etypecase (%%binding-value binding)
                (%%constant
                 (error "Constant binding found with non-constant bindings."))
                ;; Parameter types were handled separately.
                (%%parameter nil)
                ;; The type may have been specified; if not, then infer.
                (%%variable-binding
                 (let ((reference (gethash (%%variable-binding-item (%%binding-value binding))
                                           bindings-table)))
                   (unless reference
                     (error "Cannot find ~A in bindings table for ~A"
                            reference
                            (%%variable-binding-name (%%binding-value binding))))
                   (if (%%binding-type binding)
                       ;; match type
                       (unless (%%binding-type= binding reference)
                         (error "Binding ~A has a type mismatch" (%%variable-binding-name (%%binding-value binding))))
                       ;; infer type
                       (setf (%%binding-type binding)
                             (%%binding-type reference)))))
                (%%variable
                 (with-accessors ((reference %%variable-reference))
                     (%%binding-value binding)
                   (let* ((binding* (gethash reference bindings-table)))
                     (setf (%%binding-type binding) (%%binding-type binding*))))
                 nil)
                ;; The literal inherits the type of whatever binding
                ;; that it is referencing and should go away entirely
                ;; at constant propagation.
                (%%literal (let* ((literal-reference (%%literal-reference (%%binding-value binding)))
                                  (referenced-literal (let ((literal (gethash literal-reference bindings-table)))
                                                        (unless literal
                                                          (error "Cannot find ~A in bindings table." literal-reference))
                                                        literal))
                                  (type (%%binding-type referenced-literal)))
                             (setf (%%binding-type binding) type)))
                ;; Instructions do not have type inference and the
                ;; return type has already been provided. Otherwise,
                ;; the funcall objects need type inference.
                (%%funcall (unless (and (gethash (%%funcall-fun (%%binding-value binding))
                                                 *instructions*)
                                        (not (eql (%%funcall-fun (%%binding-value binding)) '%write-var)))
                             (if (eql (%%funcall-fun (%%binding-value binding)) '%write-var)
                                 (multiple-value-bind (place-id value-id)
                                     (array-of-2 (%%funcall-args (%%binding-value binding)))
                                   (unless (%%binding-type= (gethash place-id bindings-table)
                                                            (gethash value-id bindings-table))
                                     (error "Type mismatch when setting place.")))
                                 (multiple-value-bind (return-type name)
                                     (function-return-type* (%%binding-value binding)
                                                            bindings-table)
                                   (setf (%%binding-type binding) return-type)
                                   (when name
                                     (setf (%%funcall-fun (%%binding-value binding))
                                           name)))))))))
  bindings)

;;; Generate the IR step-by-step

(defun intermediate-representation (s-expressions)
  "
Turns s-expressions into an intermediate representation (IR), which is
then processed through later functions. This goes as far as removing
lexical scope, removing duplicate constants, and inferring the types
for everything, but it does not yet turn all primitive functions into
the low level instructions through inlining and it does not eliminate
the immutable variables through substitution with the object that the
variables are referencing.
"
  (let* ((forms (a-normal s-expressions (make-hash-table)))
         (forms (split-and-infer-constants forms))
         (forms (handle-duplicate-constants forms))
         (forms (infer-types-and-monomorphize forms)))
    forms))

;;; TODO: structs must be bounds checked, too
(defun composite-bounds (composite-type)
  (cond ((%%typep composite-type '%%vec)
         (%%vec-size composite-type))
        ((%%typep composite-type '%%matrix)
         (%%matrix-cols composite-type))
        ((%%typep composite-type '%%array)
         (%%array-length composite-type))
        (t (error "Unsupported type for bounds check."))))

(defun %check-bounds (composite-type index)
  (unless (<= 0
              index
              (- (composite-bounds composite-type) 1))
    (error "Index ~A out of bounds for type ~A"
           index
           (with-output-to-string (string)
             (write-linear-algebra-type composite-type string))))
  zrvlir:true)

(defun %check-matrix-bounds (matrix-type m n)
  (unless (%%typep matrix-type '%%matrix)
    (error "Type must be a matrix."))
  (%check-bounds matrix-type n)
  (%check-bounds (%%matrix-column-vec-type matrix-type) m))

;;; TODO: At constant propagation time during compilation time, an
;;; index that is constant or literal can be checked against the known
;;; length of the composite data structure bounds when check-bounds or
;;; check-matrix-bounds are "called". This then disappears before
;;; runtime.
(defmacro check-bounds (composite index)
  (declare (ignore composite index)))

(defmacro check-matrix-bounds (matrix m n)
  (declare (ignore matrix m n)))

(defun rebind-bindings (bindings id-generator)
  (let ((substitutions (make-hash-table)))
    (flet ((rebind (bindings)
             (loop :for binding :in bindings
                   :for old-id := (%%binding-id binding)
                   :for new-id := (if (zerop old-id)
                                      0
                                      (funcall id-generator))
                   :collect (progn
                              (setf (gethash old-id substitutions) new-id)
                              (replace-%%binding-id binding new-id)))))
      (substitute-all-references (list (rebind (constant-bindings bindings))
                                       (rebind (main-bindings bindings)))
                                 substitutions
                                 nil))))

;;; Any called inline functions are merged into the forms if possible.
;;; These functions also exist as their own already-compiled forms.
;;; They may have their own constants, which have to be merged into
;;; the constants section, which requires the constants are unique.
;;; Anything that cannot be inlined will become a function call.
;;;
;;; TODO: Handle NIL in the CADR of a function definition, which means
;;; the last constant is returned instead.
(defun inline-functions (forms)
  (labels ((final-form (form inline-bindings body value)
             (append inline-bindings
                     (butlast body)
                     (let ((last (car (last body))))
                       (if last
                           (list (if (zerop (%%binding-id last))
                                     last
                                     ;; Takes the return type and the ID from the function
                                     ;; rather than from the binding so that type conversion
                                     ;; can take place and later callers know what ID to use.
                                     (let ((declared-function-return-type (%%binding-type form))
                                           (actual-function-return-type (%%binding-type last)))
                                       (unless (%%type= declared-function-return-type
                                                        actual-function-return-type)
                                         (cond ((%%typep declared-function-return-type '%%disjoint-type)
                                                (unless (%%type= (%%disjoint-type-base-type declared-function-return-type)
                                                                 actual-function-return-type)
                                                  (error "Type mismatch when inlining function ~S"
                                                         (%%funcall-fun value))))
                                               ((%%typep actual-function-return-type '%%disjoint-type)
                                                (unless (%%type= (%%disjoint-type-base-type actual-function-return-type)
                                                                 declared-function-return-type)
                                                  (error "Type mismatch when inlining function ~S"
                                                         (%%funcall-fun value))))
                                               (t
                                                (error "Type mismatch when inlining function ~S"
                                                       (%%funcall-fun value)))))
                                       (%%binding declared-function-return-type
                                                  (%%binding-id form)
                                                  (%%binding-value last)
                                                  (%%binding-metadata last)))))
                           nil))))
           (function-parameters-to-variables (form value body)
             (if (zerop (length (%%funcall-args value)))
                 (final-form form nil body value)
                 (loop :for arg :across (%%funcall-args value)
                       :for body* :on body
                       :for parameter := (car body*)
                       :collect (progn
                                  (unless (%%parameter-p (%%binding-value parameter))
                                    (error "Error when inlining the parameters of function ~A"
                                           (%%funcall-fun value)))
                                  (unless (eql (%%parameter-class (%%binding-value parameter))
                                               :function-parameter)
                                    (error "Error when inlining the parameters of function ~A"
                                           (%%funcall-fun value)))
                                  (replace-%%binding-value parameter
                                                           (%%variable-binding (%%parameter-name (%%binding-value parameter))
                                                                               arg)))
                         :into inline-bindings
                       :finally (progn
                                  (when (%%parameter-p (%%binding-value (main-bindings body*)))
                                    (when (eql (%%parameter-class (%%binding-value (main-bindings body*)))
                                               :function-parameter)
                                      (error "Error when inlining the parameters of function ~A"
                                             (%%funcall-fun value))))
                                  (return (final-form form inline-bindings (cdr body*) value)))))))
    (loop :for form :in (main-bindings forms)
          :for value := (%%binding-value form)
          :for inline? := (and (%%funcall-p value)
                               (gethash (%%funcall-fun value) *primitive-functions*))
          :for inline-bindings := (and inline?
                                       (let ((compiled-function (gethash (%%funcall-fun value) *functions*)))
                                         (unless compiled-function
                                           (error "Undefined function ~A" (%%funcall-fun value)))
                                         (rebind-bindings compiled-function #'new-ir-id)))
          :if inline?
            :append (function-parameters-to-variables form value (main-bindings inline-bindings)) :into main-bindings
            :and :append (constant-bindings inline-bindings) :into constant-bindings
          :else
            :collect form :into main-bindings
          :finally (return (handle-duplicate-constants (list (append (constant-bindings forms)
                                                                     constant-bindings)
                                                             main-bindings))))))

;;; Removes any named variables that do not need to exist, including
;;; named variables that were created from the inlining of functions.
;;; This will not remove necessary global variables.
;;;
;;; TODO: If the type of a function argument is a pointer, then do use
;;; %read-var
(defun remove-variables (forms)
  (let ((bindings (make-hash-table))
        (parameters (make-hash-table))
        (substitutions (make-hash-table))
        (maybe-substitute (make-hash-table)))
    ;; Associate binding IDs with their bindings
    (loop :for form :in (main-bindings forms)
          :for value := (%%binding-value form)
          :when (or (%%variable-binding-p value)
                    (%%variable-p value)
                    (%%parameter-p value)
                    (and (%%funcall-p value)
                         (eql (%%funcall-fun value)
                              '%access-chain)))
            :do (setf (gethash (%%binding-id form) bindings)
                      value))
    ;; Build substitutions for every variable except non-function
    ;; parameters, which must actually access the variable.
    (flet ((build-substitution (binding-id starting-id &optional access-chain?)
             (loop :for id := starting-id
                     :then new-id
                   :for new-id := (or (gethash id substitutions)
                                      (and access-chain?
                                           (gethash id maybe-substitute)))
                   :while new-id
                   :when (and access-chain?
                              (gethash id maybe-substitute))
                     :do (progn
                           (setf (gethash id substitutions)
                                 (gethash id maybe-substitute))
                           (remhash id maybe-substitute)
                           (remhash id parameters))
                   :finally (setf (gethash binding-id substitutions) id))))
      (loop :for form :in (main-bindings forms)
            :for value := (%%binding-value form)
            :do (cond ((%%variable-binding-p value)
                       (build-substitution (%%binding-id form) (%%variable-binding-item value)))
                      ((and (%%variable-p value)
                            (or (not (%%parameter-p (gethash (%%variable-reference value) bindings)))
                                (eql :function (%%parameter-class (gethash (%%variable-reference value) bindings)))))
                       (build-substitution (%%binding-id form) (%%variable-reference value)))
                      ((and (%%funcall-p value)
                            (eql (%%funcall-fun value)
                                 '%access-chain))
                       (let* ((variable-id (aref (%%funcall-args value) 0))
                              (variable-reference (%%variable-reference (gethash variable-id bindings))))
                         (build-substitution variable-id variable-reference t)))
                      ((and (%%variable-p value)
                            (%%parameter-p (gethash (%%variable-reference value) bindings)))
                       (setf (gethash (%%binding-id form) maybe-substitute)
                             (%%variable-reference value))
                       (setf (gethash (%%binding-id form) parameters)
                             (%%parameter-class (gethash (%%variable-reference value) bindings)))))))
    ;; Remove most variables and replace their references with
    ;; references to what the variables were referring to, unless
    ;; those variables are an access (turn them into %read-var) or the
    ;; final item in the bindings (so they must exist to be returned).
    (labels ((%remove-variable (form value end?)
               (if (%%variable-p value)
                   (cond ((gethash (%%binding-id form) parameters)
                          (replace-%%binding-value form
                                                   (%%funcall '%read-var
                                                              (%%args (%%variable-reference value))
                                                              (%%literals 0))))
                         (end?
                          (prog1
                              (replace-%%binding-value form
                                                       ;; TODO: handle returns differently?
                                                       (%%funcall 'return
                                                                  (%%args (gethash (%%binding-id form) substitutions))
                                                                  (%%literals 0)))
                            (remhash (%%binding-id form) substitutions)))
                         (t nil))
                   form)))
      (list (constant-bindings forms)
            (substitute-references (loop :for forms* :on (main-bindings forms)
                                         :for form := (car forms*)
                                         :for value := (%%binding-value form)
                                         :for end? := (endp (cdr forms*))
                                         :when (and (not (%%variable-binding-p value))
                                                    (or (not (%%variable-p value))
                                                        end?
                                                        (gethash (%%binding-id form) parameters)))
                                           :collect (%remove-variable form value end?))
                                   substitutions)))))

;;; TODO: Optionally (since it can change the result) constant fold
;;; for floating point. In places where this cannot change the result
;;; (such as constructors), then this is mandatory and cannot
;;; optionally be disabled. This requires the CPU backend.
;;;
;;; TODO: Constant fold everything that is not floating point. Again,
;;; this requires the CPU backend.
(defun propagate-constants (forms)
  ;; Turn %%literals into actual literals comes last.
  (let ((constants (make-hash-table))
        (duplicates (make-hash-table)))
    (loop :for constant :in (constant-bindings forms)
          :do (setf (gethash (%%binding-id constant) constants)
                    (%%constant-value (%%binding-value constant))))
    ;; TODO: Make sure that the data structure is immutable before
    ;; making it composite.
    (flet ((make-constant-composites (forms*)
             (loop :for form :in forms*
                   :for value := (%%binding-value form)
                   :when (and (%%funcall-p value)
                              (eql (%%funcall-fun value)
                                   '%composite-construct)
                              (every (lambda (arg)
                                       (gethash arg constants))
                                     (%%funcall-args value)))
                     :collect (let ((constant-composite (%%constant-composite (%%funcall-args value))))
                                (setf (gethash (%%binding-id form) constants) constant-composite)
                                (replace-%%binding-value form constant-composite))))
           (remove-duplicate-constant-composites (forms*)
             (loop :for bindings* :on forms*
                   :for binding* := (car bindings*)
                   :for id := (%%binding-id binding*)
                   :for composite := (%%binding-value binding*)
                   :for refs := (%%constant-composite-constituents composite)
                   :for dupes := (and (not (gethash id duplicates))
                                      (remove-if-not (lambda (b)
                                                       (and (%%binding-type= binding* b)
                                                            (every #'eql
                                                                   (%%constant-composite-constituents (%%binding-value b))
                                                                   refs)))
                                                     (cdr bindings*)))
                   :when dupes
                     :do (loop :for dupe :in dupes
                               :for dupe-id := (%%binding-id dupe)
                               :do (setf (gethash dupe-id duplicates) id))
                   :unless (gethash id duplicates)
                     :collect (if (find-if (lambda (x)
                                             (gethash x duplicates))
                                           refs)
                                  (replace-%%binding-value binding*
                                                           (%%constant-composite (map '%%args
                                                                                      (lambda (item)
                                                                                        (gethash item duplicates item))
                                                                                      refs)))
                                  binding*)))
           (substitute-constant-composites-and-literals (forms*)
             (substitute-references (loop :with reference-dupes := (make-hash-table)
                                          :for form :in (remove-if (lambda (form)
                                                                     (gethash (%%binding-id form) constants))
                                                                   forms*)
                                          :for value := (%%binding-value form)
                                          :if (%%literal-p value)
                                            :do (let* ((reference (%%literal-reference value)))
                                                  (unless (gethash reference constants)
                                                    (error "Unable to determine literal at compilation time."))
                                                  (setf (gethash (%%binding-id form) duplicates)
                                                        (let ((constant (gethash (%%literal-reference value) constants)))
                                                          ;; TODO: also handle double float
                                                          (list 0
                                                                (if (typep constant 'single-float)
                                                                    (float-features:single-float-bits constant)
                                                                    constant)))))
                                          :when (not (%%literal-p value))
                                            :collect form)
                                    duplicates)))
      ;; The constants part, evaluated first, creates the
      ;; constant-composites. Then, in the remaining forms, any
      ;; literals become actual literals that inline the literal's
      ;; constant value in the instruction call itself.
      (let* ((constant-composites (remove-duplicate-constant-composites (make-constant-composites (main-bindings forms))))
             (constant-forms (append (constant-bindings forms) constant-composites))
             (non-constant-forms (substitute-constant-composites-and-literals (main-bindings forms))))
        (list constant-forms
              (or non-constant-forms
                  (let ((last (car (last constant-forms))))
                    (list (%%binding (%%binding-type last)
                                     (new-ir-id)
                                     (%%funcall 'return
                                                (%%args (%%binding-id last))
                                                (%%literals 0))
                                     nil)))))))))

(defun remove-unused-constants (forms)
  (multiple-value-bind (constants constant-count)
      (loop :with constants := (make-hash-table)
            :with constant-count := (make-hash-table)
            :for constant :in (constant-bindings forms)
            :do (psetf (gethash (%%binding-id constant) constant-count) 0
                       (gethash (%%binding-id constant) constants) (%%binding-value constant))
            :finally (return (values constants constant-count)))
    (labels ((count-constants (args)
               (loop :for arg :across args
                     :when (gethash arg constant-count)
                       :do (progn
                             (incf (gethash arg constant-count))
                             (let ((constant (gethash arg constants)))
                               (when (%%constant-composite-p constant)
                                 (count-constants (%%constant-composite-constituents constant))))))))
      (loop :for binding :in (main-bindings forms)
            :when (%%funcall-p (%%binding-value binding))
              :do (count-constants (%%funcall-args (%%binding-value binding)))))
    (list (remove-if (lambda (x)
                       (zerop (gethash (%%binding-id x) constant-count)))
                     (constant-bindings forms))
          (main-bindings forms))))

;;; TODO: Dead code elimination? Make sure not to remove things with
;;; side effects (things that set or, outside of the GPU, perhaps
;;; things that print)
;;;
;;; TODO: After this, form SPIR-V IDs and SPIR-V type IDs.
(defun process-intermediate-forms (forms)
  "
Takes the basic intermediate representation of bindings and inlines
most of the functions. This produces basic instructions in most
places. Next, it removes the immutable variables and does more
optimizations, preparing the forms for the target architecture.
"
  (let* ((forms (inline-functions forms))
         (forms (remove-variables forms))
         (forms (propagate-constants forms))
         (forms (remove-unused-constants forms)))
    forms))

(define-function (minimal-ir-compile :inline t) (forms)
  (inline-functions (intermediate-representation forms)))

(define-function (full-ir-compile :inline t) (forms)
  (process-intermediate-forms (intermediate-representation forms)))

(defun type-ids (forms &key (start 2) main?)
  "
Generates type IDs from the final form of the binding forms for the
SPIR-V backend. The type IDs can become SPIR-V instructions that are
added ahead of the constants, global variables, and function
definitions, but after the SPIR-V header.

This SPIR-V header is considered to include the part of a SPIR-V
binary containing the debug information and non-debug annotations
after the part that may more properly be called the SPIR-V header.
This is because all of those early sections (the header) ahead of the
types up until and including the type IDs generated here are unique to
the SPIR-V representation. This is also why the type IDs come before
the constants instead of being interspersed with them like in the
example SPIR-V from the specification.

The type IDs themselves, generated here, are not considered to be the
header because they are most naturally generated here, rather than
with the rest of the SPIR-V code. Also, the type IDs are technically
interchangeable with the constants and global variables even if they
are not treated as such by this compiler.

The SPIR-V ID is assumed by default to start at 2 because 0 is an
invalid ID and 1 is assumed to be the ext-inst-import.

The first return value is a list of types in a form close to their
SPIR-V instruction representation. The second return value is the next
unused ID. The third return value is the table of types to find these
IDs when given a certain type encoding.
"
  (labels ((spir-v-pointers (value type)
             (cond ((%%parameter-p value)
                    (%%pointer (%%parameter-class value) type))
                   ((and (%%funcall-p value)
                         (eql (%%funcall-fun value) '%access-chain))
                    ;; TODO: don't hardcode pointer class; derive!
                    (%%pointer :output type))
                   (t
                    type)))
           (enumerate-binding-types (bindings type-ids types)
             (loop :for binding :in bindings
                   ;; Variables are considered by SPIR-V to have a
                   ;; pointer type. Otherwise, just use the type.
                   :for binding-type := (spir-v-pointers (%%binding-value binding)
                                                         (type-erasure (%%binding-type binding)))
                   :for types* := (enumerate-types binding-type
                                                   type-ids
                                                   types
                                                   #'new-spir-v-id)
                     :then (enumerate-types binding-type
                                            type-ids
                                            types*
                                            #'new-spir-v-id)
                   :finally (return types*))))
    (with-spir-v-id (start)
      (let* ((type-ids (make-hash-table :test 'equalp))
             ;; If there is a main function, then put the types of the
             ;; main function ahead of the types for the constants and
             ;; global variables.
             (types (if main?
                        (enumerate-types (%%ftype (%%void))
                                         type-ids
                                         (list)
                                         #'new-spir-v-id)
                        (list)))
             (types (enumerate-binding-types (constant-bindings forms) type-ids types))
             (types (enumerate-binding-types (main-bindings forms) type-ids types)))
        (values (reverse types)
                (spir-v-id)
                type-ids)))))

(defun constant-instruction (constant value)
  (cond ((%%constant-p value)
         (replace-%%binding-value constant
                                  (%%funcall '%constant
                                             (let ((constant-value (%%constant-value value)))
                                               ;; TODO: also handle double float
                                               (%%args (if (typep constant-value 'single-float)
                                                           (float-features:single-float-bits constant-value)
                                                           constant-value)))
                                             (%%literals 1))))
        ((%%constant-composite-p value)
         (replace-%%binding-value constant
                                  (%%funcall '%constant-composite
                                             (%%constant-composite-constituents value)
                                             (make-array (length (%%constant-composite-constituents value))
                                                         :element-type 'bit
                                                         :initial-element 0))))
        (t (error "Invalid constant form ~A" value))))

(defun instruction-forms (forms types)
  (flet ((variable-instruction (binding value)
           (cond ((%%parameter-p value)
                  ;; The type has to switch to a pointer type when it
                  ;; becomes a SPIR-V variable. If the variable was
                  ;; eliminated before this stage, then this doesn't have
                  ;; to be done.
                  (%%binding (%%pointer (%%parameter-class value) (%%binding-type binding))
                             (%%binding-id binding)
                             (%%funcall '%variable
                                        (%%args (storage-class-enum (%%parameter-class value)))
                                        (%%literals 0))
                             (%%binding-metadata binding)))
                 ((and (%%funcall-p value)
                       (eql (%%funcall-fun value)
                            '%access-chain))
                  (let ((variable (find (aref (%%funcall-args value) 0) (main-bindings forms) :key #'%%binding-id)))
                    (unless variable
                      (error "I couldn't find ID ~A" (aref (%%funcall-args value) 0)))
                    (replace-%%binding-type binding
                                            (%%pointer (%%parameter-class (%%binding-value variable))
                                                       (%%binding-type binding)))))
                 (t
                  binding))))
    (multiple-value-call
        #'values
      (list (loop :for constant :in (constant-bindings forms)
                  :for value := (%%binding-value constant)
                  :collect (constant-instruction constant value))
            (loop :for binding :in (main-bindings forms)
                  :for value := (%%binding-value binding)
                  :collect (variable-instruction binding value)))
      (loop :with names := (make-hash-table)
            :for binding :in (main-bindings forms)
            :for value := (%%binding-value binding)
            :when (and (%%parameter-p value)
                       (or (eql (%%parameter-class value) :input)
                           (eql (%%parameter-class value) :output)))
              :collect (list (%%binding-id binding) (%%parameter-name value))
                :into interface
            :when (%%parameter-p value)
              ;; TODO: find some way to handle with the block struct
              ;; type vs pointer ambiguity
              :do (setf (gethash (%%parameter-name value) names)
                        (if (%%typep (%%binding-type binding) '%%struct)
                            (gethash (%%binding-type binding) types)
                            (%%binding-id binding)))
            :when (and (%%funcall-p value)
                       (eql '%function-begin (%%funcall-fun value)))
              :collect (%%binding-id binding)
                :into main-id
            :finally (return (values
                              (prog1 (car main-id)
                                (unless (endp (cdr main-id))
                                  (error "There should only be one main function.")))
                              interface
                              names))))))

;;; Note: This transformation is done in this file to avoid having to
;;; export many struct accessors that are only used in this file.
(defun spir-v-form (form type-ids)
  "
Takes in a binding form and a hash-table from encoded types to SPIR-V
type IDs.

Returns SPIR-V.
"
  (let ((type-id (gethash (%%binding-type form) type-ids))
        (spir-v-id (%%binding-id form)))
    (with-spir-v-id (spir-v-id)
      (let* ((value (%%binding-value form))
             (funcall? (%%funcall-p value)))
        (if funcall?
            (let ((fun (%%funcall-fun value)))
              (unless (gethash fun *instructions*)
                (error "Function ~A was not turned into a SPIR-V instruction." fun))
              ;; TODO: separate instructions into :a :b and :bc again
              (let ((arg-list (coerce (%%funcall-args value) 'list)))
                (case fun
                  (%function-begin
                   (apply fun
                          (spir-v-id)
                          (gethash (%%ftype-output-type (%%binding-type form)) type-ids)
                          type-id
                          arg-list))
                  (t (case (gethash fun *instructions*)
                       (:bc (apply fun (spir-v-id) type-id arg-list))
                       (:b (apply fun (spir-v-id) arg-list))
                       (:a (apply fun arg-list))
                       (t (error "Function ~S is not an instruction." fun)))))))
            (error "Form ~A should not exist in the SPIR-V stage." form))))))

(defmacro with-multiple-value-symbol-macrolet (binding bindings form &body body)
  `(multiple-value-bind ,bindings ,form
     (symbol-macrolet ((,binding (values ,@bindings)))
       ,@body)))

;;; TODO: declarations need to declare matrices dynamic-extent unless
;;; the matrix is the final return value
;;;
;;; TODO: eventually track no-longer-used composite data structures
;;; and reuse them
(defmacro with-ir-bindings (bindings)
  (let ((bindings (loop :for (bindings* built-bindings) := (list bindings nil)
                          :then (let ((binding (car bindings*)))
                                  (if binding
                                      (ecase (car binding)
                                        ((:value :void)
                                         (multiple-value-bind (let*-form bindings**)
                                             (loop :for bindings** :on bindings*
                                                   :for binding** := (car bindings**)
                                                   :for binding-kind := (car binding**)
                                                   :while (or (eql binding-kind :value)
                                                              (eql binding-kind :void))
                                                   :collect (cadr binding**) :into let*-bindings
                                                   :when (eql binding-kind :void)
                                                     :collect `(ignorable ,(caadr binding**)) :into declarations*
                                                   :finally (return (values `(let* ,let*-bindings (declare ,@declarations*))
                                                                            bindings**)))
                                           (list bindings**
                                                 (cons let*-form built-bindings))))
                                        (:values
                                         (list (cdr bindings*)
                                               (destructuring-bind (count (binding-symbol bound-form)) (cdr binding)
                                                 (let ((gensyms (loop :repeat count
                                                                      :collect (gensym))))
                                                   (cons `(with-multiple-value-symbol-macrolet ,binding-symbol ,gensyms
                                                              ,bound-form)
                                                         built-bindings)))))
                                        (:alias
                                         (list (cdr bindings*)
                                               (destructuring-bind ((result-id input)) (cdr binding)
                                                 (cons `(symbol-macrolet ((,result-id ,input)))
                                                       built-bindings)))))
                                      (list bindings* built-bindings)))
                        :while bindings*
                        :finally (return built-bindings))))
    (loop :for binding-form :in bindings
          :for result := (ecase (car binding-form)
                           (let* (append binding-form (list (caar (last (cadr binding-form))))))
                           (with-multiple-value-symbol-macrolet (destructuring-bind (binding bindings form)
                                                                    (cdr binding-form)
                                                                  `(with-multiple-value-symbol-macrolet ,binding ,bindings
                                                                       ,form
                                                                     ,binding)))
                           (symbol-macrolet (destructuring-bind ((out-binding in-binding)) (cadr binding-form)
                                              `(symbol-macrolet ((,out-binding ,in-binding)) ,out-binding))))
            :then (append binding-form (list result))
          :finally (return `(locally (declare (optimize (speed 3)))
                              ,result)))))

(defun scalar-forms (forms &optional values?)
  (with-backend (:scalar)
    (let ((*processed-bindings* (make-hash-table)))
      (flet ((apply-instructions (constants main-bindings)
               (loop :for form :in (append constants main-bindings)
                     :collect (multiple-value-bind (binding binding-type values-count)
                                  (let ((fun (%%funcall-fun (%%binding-value form))))
                                    (if (eql fun 'return)
                                        (let* ((*gensym-counter* (%%binding-id form))
                                               (id (gensym))
                                               (destination-id (aref (%%funcall-args (%%binding-value form)) 0))
                                               (destination-id (processed-binding-id (gethash destination-id *processed-bindings*))))
                                          (values `(,id ,destination-id) :alias nil))
                                        (let* ((type (type-erasure (%%binding-type form)))
                                               (id (case (gethash fun *instructions*)
                                                     ((:bc :b)
                                                      (let* ((*gensym-counter* (%%binding-id form))
                                                             (id (gensym)))
                                                        (setf (gethash (%%binding-id form) *processed-bindings*)
                                                              (make-processed-binding :id id :type type))
                                                        id))
                                                     (:a nil)
                                                     (t (error "Function ~S is not an instruction." fun))))
                                               (arg-list (append (coerce (%%funcall-args (%%binding-value form)) 'list)
                                                                 (if (eql (%%funcall-fun (%%binding-value form))
                                                                          '%variable)
                                                                     (list (%%binding-metadata form))
                                                                     nil))))
                                          (ecase (gethash fun *instructions*)
                                            (:bc (apply fun id type arg-list))
                                            (:b (apply fun id arg-list))
                                            (:a (apply fun arg-list))))))
                                `(,(ecase binding-type
                                     (:object :value)
                                     (:void :void)
                                     (:values :values)
                                     (:alias :alias))
                                  ,@(if values-count
                                        (list values-count)
                                        nil)
                                  ,binding))))
             (instruction-form (binding value)
               (if (%%parameter-p value)
                   (%%binding (%%binding-type binding)
                              (%%binding-id binding)
                              (%%funcall '%variable
                                         (%%args (storage-class-enum (%%parameter-class value)))
                                         (%%literals 0))
                              (append (list :name (%%parameter-name value))
                                      (%%binding-metadata binding)))
                   binding))
             (lisp-type-conversion (result result-type)
               (let ((type (type-erasure result-type)))
                 (if (and (%%typep type '%%vec) (not values?))
                     (cond ((%%type= type (%%vec (%%f 32) 2))
                            `(multiple-value-call #'vec2 ,result))
                           ((%%type= type (%%vec (%%f 32) 3))
                            `(multiple-value-call #'vec3 ,result))
                           ((%%type= type (%%vec (%%f 32) 4))
                            `(multiple-value-call #'vec4 ,result))
                           (t (error "Invalid final result type.")))
                     result))))
        (let ((constants  (loop :for constant :in (constant-bindings forms)
                                :for value := (%%binding-value constant)
                                :collect (constant-instruction constant value)))
              (final-result-type (%%binding-type (car (last (main-bindings forms)))))
              (main-bindings (loop :for binding :in (main-bindings forms)
                                   :for value := (%%binding-value binding)
                                   :collect (instruction-form binding value))))
          (lisp-type-conversion `(with-ir-bindings ,(apply-instructions constants main-bindings))
                                final-result-type))))))
