(defpackage #:zombie-raptor/zrvl/spir-v-instructions
  (:documentation "Instructions used in SPIR-V.")
  (:import-from #:alexandria)
  (:use #:cl
        #:zombie-raptor/zrvl/core-types
        #:zombie-raptor/zrvl/spir-v-enums
        #:zr-utils)
  (:export #:%global-variable
           #:%spir-v-function
           #:constant
           #:constant*
           #:decode-spir-v-instruction
           #:initialize-spir-v-instruction
           #:inst
           #:inst*
           #:inst**
           #:spir-v-instruction
           #:spir-v-instruction-array
           #:spir-v-instruction-type-b-p
           #:spir-v-instruction-type-bc-result-id
           #:spir-v-instruction-type-bc-result-type
           #:spir-v-string))

(in-package #:zombie-raptor/zrvl/spir-v-instructions)

(defpackage #:zrspv
  (:documentation "SPIR-V ops in s-expression form.")
  (:use)
  (:import-from #:cl
                #:defpackage
                #:function
                #:in-package
                #:nil)
  (:import-from #:zrvlir
                #:false
                #:true)
  (:export #:false
           #:true
           ;; new symbols
           #:%access-chain
           #:%acos
           #:%acosh
           #:%array-length
           #:%asin
           #:%asinh
           #:%atan
           #:%atan2
           #:%atanh
           #:%bitwise-and
           #:%bitwise-or
           #:%bitwise-xor
           #:%branch
           #:%branch-conditional
           #:%capability
           #:%ceil
           #:%composite-construct
           #:%composite-extract
           #:%composite-insert
           #:%constant
           #:%constant-composite
           #:%constant-false
           #:%constant-null
           #:%constant-sampler
           #:%constant-true
           #:%convert-f-to-s
           #:%convert-f-to-u
           #:%convert-s-to-f
           #:%convert-u-to-f
           #:%copy-logical
           #:%copy-object
           #:%cos
           #:%cosh
           #:%cross
           #:%decorate
           #:%decorate-id
           #:%degrees
           #:%determinant
           #:%distance
           #:%dot
           #:%entry-point
           #:%execution-mode
           #:%exp
           #:%exp2
           #:%expt
           #:%ext-inst
           #:%ext-inst-import
           #:%extension
           #:%f-abs
           #:%f-add
           #:%f-clamp
           #:%f-convert
           #:%f-div
           #:%f-max
           #:%f-min
           #:%f-mix
           #:%f-mod
           #:%f-mul
           #:%f-negate
           #:%f-ord-equal
           #:%f-ord-greater-than
           #:%f-ord-greater-than-equal
           #:%f-ord-less-than
           #:%f-ord-less-than-equal
           #:%f-ord-not-equal
           #:%f-rem
           #:%f-sign
           #:%f-sub
           #:%f-unord-equal
           #:%f-unord-greater-than
           #:%f-unord-greater-than-equal
           #:%f-unord-less-than
           #:%f-unord-less-than-equal
           #:%f-unord-not-equal
           #:%face-forward
           #:%find-i-lsb
           #:%find-s-msb
           #:%find-u-msb
           #:%floor
           #:%fma
           #:%fr-exp-struct
           #:%fract
           #:%function
           #:%function-call
           #:%function-end
           #:%function-parameter
           #:%generic-ptr-mem-semantics
           #:%i-add
           #:%i-add-carry
           #:%i-equal
           #:%i-mul
           #:%i-not-equal
           #:%i-sub
           #:%i-sub-borrow
           #:%image-sample-implicit-lod
           #:%image-texel-pointer
           #:%in-bounds-access-chain
           #:%in-bounds-ptr-access-chain
           #:%interpolate-at-centroid
           #:%interpolate-at-offset
           #:%interpolate-at-sample
           #:%inverse-sqrt
           #:%kill
           #:%label
           #:%ld-exp
           #:%length
           #:%line
           #:%load
           #:%log
           #:%log2
           #:%logical-and
           #:%logical-equal
           #:%logical-not
           #:%logical-not-equal
           #:%logical-or
           #:%loop-merge
           #:%matrix-inverse
           #:%matrix-times-matrix
           #:%matrix-times-scalar
           #:%matrix-times-vector
           #:%member-decorate
           #:%member-name
           #:%memory-model
           #:%modf-struct
           #:%module-processed
           #:%n-clamp
           #:%n-max
           #:%n-min
           #:%name
           #:%no-line
           #:%no-op
           #:%normalize
           #:%not
           #:%outer-product
           #:%pack-double-2x32
           #:%pack-half-2x16
           #:%pack-snorm-2x16
           #:%pack-snorm-4x8
           #:%pack-unorm-2x16
           #:%pack-unorm-4x8
           #:%phi
           #:%pow
           #:%ptr-access-chain
           #:%ptr-diff
           #:%ptr-equal
           #:%ptr-not-equal
           #:%radians
           #:%reflect
           #:%refract
           #:%return
           #:%return-value
           #:%round
           #:%round-even
           #:%s-abs
           #:%s-clamp
           #:%s-convert
           #:%s-div
           #:%s-greater-than
           #:%s-greater-than-equal
           #:%s-less-than
           #:%s-less-than-equal
           #:%s-max
           #:%s-min
           #:%s-mod
           #:%s-mul-extended
           #:%s-negate
           #:%s-rem
           #:%s-sign
           #:%sampled-image
           #:%selection-merge
           #:%sin
           #:%sinh
           #:%size-of
           #:%smooth-step
           #:%source
           #:%source-continued
           #:%source-extension
           #:%spec-constant
           #:%spec-constant-composite
           #:%spec-constant-false
           #:%spec-constant-op
           #:%spec-constant-true
           #:%sqrt
           #:%step
           #:%store
           #:%string
           #:%switch
           #:%tan
           #:%tanh
           #:%transpose
           #:%trunc
           #:%type-array
           #:%type-bool
           #:%type-device-event
           #:%type-event
           #:%type-float
           #:%type-function
           #:%type-image
           #:%type-int
           #:%type-matrix
           #:%type-named-barrier
           #:%type-opaque
           #:%type-pipe
           #:%type-pipe-storage
           #:%type-pointer
           #:%type-queue
           #:%type-reserve-id
           #:%type-runtime-array
           #:%type-sampled-image
           #:%type-sampler
           #:%type-struct
           #:%type-vector
           #:%type-void
           #:%u-clamp
           #:%u-convert
           #:%u-div
           #:%u-greater-than
           #:%u-greater-than-equal
           #:%u-less-than
           #:%u-less-than-equal
           #:%u-max
           #:%u-min
           #:%u-mod
           #:%u-mul-extended
           #:%undef
           #:%unpack-double-2x32
           #:%unpack-half-2x16
           #:%unpack-snorm-2x16
           #:%unpack-snorm-4x8
           #:%unpack-unorm-2x16
           #:%unpack-unorm-4x8
           #:%unreachable
           #:%variable
           #:%vector-extract-dynamic
           #:%vector-insert-dynamic
           #:%vector-shuffle
           #:%vector-times-matrix
           #:%vector-times-scalar))

(defvar *instruction-names* (make-hash-table))
(defvar *instruction-opcodes* (make-hash-table))
(defvar *instruction-type* (make-hash-table))
(defvar *glsl-instruction-names* (make-hash-table))
(defvar *glsl-instruction-opcodes* (make-hash-table))

(deftype spir-v-binary ()
  `(simple-array uint32 (*)))

;;; TODO: Encode the wide arguments like double-float and strings.
;;; This means determine the word-count, too. It's not just the length
;;; of arguments.
;;;
;;; TODO: Figure out if enums should be encoded here or at the caller.
(defun %inst (inst-type result-id result-type instruction-name arguments)
  (check-type inst-type (member :bc :b :a))
  ;; Note: skip result-type ID and return ID
  (let* ((arg-offset (ecase inst-type
                       (:bc 3)
                       (:b 2)
                       (:a 1)))
         (word-count (+ arg-offset (length arguments)))
         (opcode (gethash instruction-name *instruction-opcodes*))
         (opcode-type (gethash instruction-name *instruction-type*))
         (instruction (make-array word-count :element-type 'uint32 :initial-element 0))
         (arguments (if (eql instruction-name 'zrspv:%ext-inst)
                        (destructuring-bind (ext-inst-id ext-instruction &rest arguments*)
                            arguments
                          (let ((ext-instruction* (gethash ext-instruction *glsl-instruction-opcodes*)))
                            (unless ext-instruction*
                              (error "Invalid extended instruction name: ~A" ext-instruction))
                            (list* ext-inst-id ext-instruction* arguments*)))
                        arguments)))
    (unless opcode
      (error "Invalid instruction name: ~A" instruction-name))
    (unless (eql opcode-type inst-type)
      (error "The opcode ~A is not one that has ~A."
             instruction-name
             (ecase inst-type
               (:bc "a return type and a return ID")
               (:b "a return ID, but no return type")
               (:a "no return type and no return ID"))))
    ;; The first part of the instruction is both the opcode and the
    ;; word-count (length) of the SPIR-V instruction.
    (setf (aref instruction 0) (logior (ash word-count 16) opcode))
    (cond ((eql inst-type :bc)
           (psetf (aref instruction 1) result-type
                  (aref instruction 2) result-id))
          ((eql inst-type :b)
           (setf (aref instruction 1) result-id)))
    (loop :for argument :in arguments
          ;; This is incremented manually because some arguments, such
          ;; as literal strings, will increment things differently.
          :with i := arg-offset
          :do (progn
                (setf (aref instruction i) argument)
                (incf i)))
    instruction))

(define-function (inst :return spir-v-binary) ((result-id uint32)
                                               (result-type uint32)
                                               (instruction-name symbol)
                                               &rest arguments)
  (%inst :bc result-id result-type instruction-name arguments))

(define-function (inst* :return spir-v-binary) ((result-id uint32)
                                                (instruction-name symbol)
                                                &rest arguments)
  (%inst :b result-id nil instruction-name arguments))

(define-function (inst** :return spir-v-binary) ((instruction-name symbol)
                                                 &rest arguments)
  (%inst :a nil nil instruction-name arguments))

(define-function (spir-v-instruction :inline t) ((opcode uint16)
                                                 (word-count uint16))
  (logior (ash word-count 16) opcode))

(define-function (decode-spir-v-opcode :inline t) ((instruction uint32))
  (values (logand instruction #x0000ffff)
          (ash instruction -16)))

(define-function decode-spir-v-instruction ((instruction spir-v-binary))
  (multiple-value-bind (name length inst-type)
      (multiple-value-bind (opcode length)
          (decode-spir-v-opcode (aref instruction 0))
        (let ((name (gethash opcode *instruction-names*)))
          (values name
                  length
                  (gethash name *instruction-type*))))
    (declare (ignore length))
    (list* (ecase inst-type
             (:a (list 0))
             (:b (list (aref instruction 1)))
             (:bc (list (aref instruction 2) (aref instruction 1))))
           name
           (coerce (subseq instruction
                           (ecase inst-type
                             (:a 1)
                             (:b 2)
                             (:bc 3)))
                   'list))))

(defmacro define-spir-v-instruction (op-type op-number op-name lambda-list)
  (declare (ignore lambda-list))
  (let* ((%op-name (concatenate 'string "%" (symbol-name op-name)))
         (%op-name* (intern %op-name '#:zrspv)))
    `(progn
       (setf (gethash ,op-number *instruction-names*) ',%op-name*
             (gethash ',%op-name* *instruction-opcodes*) ,op-number
             (gethash ',%op-name* *instruction-type*) ,op-type))))

(define-function (spir-v-string-length :inline t) (utf8)
  (ceiling (1+ (length utf8)) 4))

;;; Turns a CL string into a SPIR-V string of its word size,
;;; (unsigned-byte 32), which also requires padding the end with some
;;; amount of 0s (up to an entire word). This created in advance as
;;; its own sequence so that the length, which must be provided in
;;; advance, is correct. For simplicity and correctness, it allocates
;;; a UTF-8 "string" of octets at the start.
(define-function spir-v-string ((string simple-string))
  (loop :with utf8 := (string-to-utf8 string)
        :with spir-v-string := (make-array (spir-v-string-length utf8)
                                           :element-type 'uint32
                                           :initial-element 0)
        :for char-code :across utf8
        :for i := 0 :then (mod (1+ i) 4)
        :for j := 0 :then (if (zerop i) (1+ j) j)
        :for shifted-char-code := (ash char-code (* i 8))
        :for partial-word := shifted-char-code
          :then (logior partial-word shifted-char-code)
        ;; Once it's not a partial word anymore, add it to the new
        ;; array and reset it.
        :when (= 3 i) :do
          (setf (aref spir-v-string j) partial-word
                partial-word #x0)
        :finally
           (return
             (progn
               (unless (or (zerop (length string)) (zerop partial-word))
                 ;; Add the leftover word
                 (setf (aref spir-v-string (- (length spir-v-string) 1)) partial-word))
               spir-v-string))))

(define-spir-v-instruction :a  000 no-op ())

(define-spir-v-instruction :bc 001 undef ())

(define-spir-v-instruction :bc 321 size-of
  ((pointer spir-v-result-id)))

(define-spir-v-instruction :a  002 source-continued
  ((continued-source string)))

;;; TODO: optional file (OpString ID)
;;; TODO: optional source (string literal)
(define-spir-v-instruction :a  003 source
  ((language keyword language-enum)
   (version uint32)))

(define-spir-v-instruction :a  004 source-extension
  ((extension string)))

(define-spir-v-instruction :a  005 name
  ((target spir-v-result-id)
   (name string)))

(define-spir-v-instruction :a  006 member-name
  ((type-id spir-v-result-id)
   (member uint32)
   (name string)))

(define-spir-v-instruction :b  007 string
  ((string string)))

(define-spir-v-instruction :a  008 line
  ((file spir-v-result-id)
   (line uint32)
   (column uint32)))

(define-spir-v-instruction :a  317 no-line ())

(define-spir-v-instruction :a  330 module-processed
  ((process string)))

(define-spir-v-instruction :a  010 extension
  ((name string)))

(define-spir-v-instruction :b  011 ext-inst-import
  ((name string)))

(define-spir-v-instruction :bc 012 ext-inst
  ((instruction-set spir-v-result-id)
   (instruction uint32)
   &rest operands))

(define-spir-v-instruction :a  014 memory-model
  ((addressing-model keyword addressing-model-enum)
   (memory-model keyword memory-model-enum)))

(define-spir-v-instruction :a  015 entry-point
  ((execution-model keyword execution-model-enum)
   (entry-point spir-v-result-id)
   (entry-point-literal-name string)
   &rest ids))

(define-spir-v-instruction :a  016 execution-mode
  ((entry-point spir-v-result-id)
   (mode keyword execution-mode-enum)
   &rest extra-operands))

(define-spir-v-instruction :a  017 capability
  ((capability-type keyword capability-type-enum)))

(define-spir-v-instruction :b  019 type-void ())

(define-spir-v-instruction :b  020 type-bool ())

(define-spir-v-instruction :b  021 type-int
  ((width uint32)
   (signedness (integer 0 1))))

(define-spir-v-instruction :b  022 type-float
  (width))

(define-spir-v-instruction :b  023 type-vector
  ((component-type-id spir-v-result-id)
   (component-count (and uint32 (integer 2 *)))))

(define-spir-v-instruction :b  024 type-matrix
  ((column-type spir-v-result-id)
   (columnm-count (and uint32 (integer 2 *)))))

(define-spir-v-instruction :b  025 type-image
  ((sampled-type spir-v-result-id)
   (dimension uint32) ; todo: dim enum
   (depth (integer 0 2))
   (arrayed (integer 0 1))
   (multisampled (integer 0 1))
   (sampled (integer 0 2))
   (image-format uint32) ; todo: image format enum
   ;; TODO: optional access qualifier
   ))

(define-spir-v-instruction :b  026 type-sampler ())

(define-spir-v-instruction :b  027 type-sampled-image
  ((image-type spir-v-result-id)))

(define-spir-v-instruction :b  028 type-array
  ((element-type spir-v-result-id)
   (length spir-v-result-id)))

(define-spir-v-instruction :b  029 type-runtime-array
  ((element-type spir-v-result-id)))

(define-spir-v-instruction :b  030 type-struct
  (&rest member-type-ids))

(define-spir-v-instruction :b  031 type-opaque
  ((name string)))

(define-spir-v-instruction :b  032 type-pointer
  ((storage-class keyword storage-class-enum)
   (type spir-v-result-id)))

(define-spir-v-instruction :b  033 type-function
  ((return-type spir-v-result-id)
   &rest parameter-type-ids))

(define-spir-v-instruction :b  034 type-event ())

(define-spir-v-instruction :b  035 type-device-event ())

(define-spir-v-instruction :b  036 type-reserve-id ())

(define-spir-v-instruction :b  037 type-queue ())

(define-spir-v-instruction :b  038 type-pipe
  ((access-qualifier uint32) ; todo enum
   ))

(define-spir-v-instruction :b  322 type-pipe-storage ())

(define-spir-v-instruction :b  327 type-named-barrier ())

;;; TODO: 39 OpTypeForwardPointer

;;; note: type must be boolean
(define-spir-v-instruction :bc 041 constant-true ())

;;; note: type must be boolean
(define-spir-v-instruction :bc 042 constant-false ())

;;; note: type must be an integer or floating-point type
(define-spir-v-instruction :bc 043 constant
  (&rest value))

(define-spir-v-instruction :bc 044 constant-composite
  (&rest constituents))

(define-spir-v-instruction :bc 045 constant-sampler
  ((sampler-addressing-mode uint32) ; todo: enum
                                    (param uint32)
                                    (sampler-filter-mode uint32) ; todo:enum
                                    ))

(define-spir-v-instruction :bc 046 constant-null ())

(define-spir-v-instruction :bc 048 spec-constant-true ())

(define-spir-v-instruction :bc 049 spec-constant-false ())

(define-spir-v-instruction :bc 049 spec-constant
  (&rest value))

(define-spir-v-instruction :bc 051 spec-constant-composite
  (&rest constituents))

(define-spir-v-instruction :bc 052 spec-constant-op
  ((opcode uint16)
   &rest operand-ids))

(define-spir-v-instruction :bc 054 function
  ((function-control (or keyword list) function-control)
   (function-type spir-v-type)))

(define-spir-v-instruction :bc 055 function-parameter ())

(define-spir-v-instruction :a  056 function-end ())

(define-spir-v-instruction :bc 057 function-call
  ((function spir-v-result-id)
   &rest argument-ids))

;;; TODO: optional initializer
(define-spir-v-instruction :bc 059 variable
  ((storage-class keyword storage-class-enum)))

(define-spir-v-instruction :bc 060 image-texel-pointer
  ((image spir-v-result-id)
   (coordinate spir-v-result-id)
   (sample spir-v-result-id)))

(define-spir-v-instruction :bc 061 load
  ((pointer spir-v-result-id)
   &rest memory-operands))

(define-spir-v-instruction :a  062 store
  ((pointer spir-v-result-id)
   (object spir-v-result-id)
   &rest memory-operands))

;;; TODO: OpCopyMemory
;;; TODO: OpCopyMemorySized

(define-spir-v-instruction :bc 065 access-chain
  ((base spir-v-result-id)
   &rest id-indexes))

(define-spir-v-instruction :bc 066 in-bounds-access-chain
  ((base spir-v-result-id)
   &rest id-indexes))

(define-spir-v-instruction :bc 067 ptr-access-chain
  ((base spir-v-result-id)
   (element spir-v-result-id)
   &rest id-indexes))

(define-spir-v-instruction :bc 068 array-length
  ((structure spir-v-result-id)
   (array-member uint32)))

(define-spir-v-instruction :bc 069 generic-ptr-mem-semantics
  ((pointer spir-v-result-id)))

(define-spir-v-instruction :bc 070 in-bounds-ptr-access-chain
  ((base spir-v-result-id)
   (element spir-v-result-id)
   &rest id-indexes))

(define-spir-v-instruction :a  071 decorate
  ((target spir-v-result-id)
   (decoration keyword decoration-enum)
   &rest decorations))

(define-spir-v-instruction :a  072 member-decorate
  ((structure-type spir-v-result-id)
   (member-number uint32)
   (decoration keyword decoration-enum)
   &rest decorations))

(define-spir-v-instruction :a  332 decorate-id
  ((target spir-v-result-id)
   (decoration keyword decoration-enum)
   &rest decorations))

(define-spir-v-instruction :bc 077 vector-extract-dynamic
  ((vector spir-v-result-id)
   (index spir-v-result-id)))

(define-spir-v-instruction :bc 078 vector-insert-dynamic
  ((vector spir-v-result-id)
   (component spir-v-result-id)
   (index spir-v-result-id)))

;;; Note: a vector can be undef here
(define-spir-v-instruction :bc 079 vector-shuffle
  ((vector-1 spir-v-result-id)
   (vector-2 spir-v-result-id)
   &rest component-literals))

(define-spir-v-instruction :bc 080 composite-construct
  (&rest constituent-ids))

(define-spir-v-instruction :bc 081 composite-extract
  ((composite spir-v-result-id)
   &rest literal-indexes))

(define-spir-v-instruction :bc 082 composite-insert
  ((object spir-v-result-id)
   (composite spir-v-result-id)
   &rest literal-indexes))

(define-spir-v-instruction :bc 083 copy-object
  ((operand spir-v-result-id)))

(define-spir-v-instruction :bc 086 sampled-image
  ((image spir-v-result-id)
   (sampler spir-v-result-id)))

(define-spir-v-instruction :bc 087 image-sample-implicit-lod
  ((sampled-image spir-v-result-id)
   (coordinate spir-v-result-id)
   ;; TODO: optional iamge-operands mask goes here
   &rest ids))

#+(or)
(define-spir-v-instruction :bc 088 image-sample-explicit-lod
  ((sampled-iamge spir-v-result-id)
   (coordinate spir-vid)
   ;; TODO: image-operands mask goes here of type lod and/or grad
   ))

;;; TODO: other image ops

(define-spir-v-instruction :bc 400 copy-logical
  ((operand spir-v-result-id)))

(define-spir-v-instruction :bc 109 convert-f-to-u
  ((float-value spir-v-result-id)))

(define-spir-v-instruction :bc 110 convert-f-to-s
  ((float-value spir-v-result-id)))

(define-spir-v-instruction :bc 111 convert-s-to-f
  ((signed-value spir-v-result-id)))

(define-spir-v-instruction :bc 112 convert-u-to-f
  ((unsigned-value spir-v-result-id)))

(define-spir-v-instruction :bc 113 u-convert
  ((unsigned-value spir-v-result-id)))

(define-spir-v-instruction :bc 114 s-convert
  ((signed-value spir-v-result-id)))

(define-spir-v-instruction :bc 115 f-convert
  ((float-value spir-v-result-id)))

(defmacro define-spir-v-arithmetic-op
    (op-number op-name &optional (binary? t))
  `(define-spir-v-instruction :bc ,op-number ,op-name
     ,(if binary?
          `((operand-1 spir-v-result-id)
            (operand-2 spir-v-result-id))
          `((operand spir-v-result-id)))))

(defmacro define-spir-v-arithmetic-ops (&body definitions)
  `(progn ,@(mapcar (lambda (definition)
                      `(define-spir-v-arithmetic-op ,@definition))
                    definitions)))

(define-spir-v-arithmetic-ops
  (084 transpose nil)
  (126 s-negate nil)
  (127 f-negate nil)
  (128 i-add)
  (129 f-add)
  (130 i-sub)
  (131 f-sub)
  (132 i-mul)
  (133 f-mul)
  (134 u-div)
  (135 s-div)
  (136 f-div)
  (137 u-mod)
  (138 s-rem)
  (139 s-mod)
  (140 f-rem)
  (141 f-mod)
  (142 vector-times-scalar)
  (143 matrix-times-scalar)
  (144 vector-times-matrix)
  (145 matrix-times-vector)
  (146 matrix-times-matrix)
  (147 outer-product)
  (148 dot)
  (149 i-add-carry)
  (150 i-sub-borrow)
  (151 u-mul-extended)
  (152 s-mul-extended))

(define-spir-v-arithmetic-ops
  (164 logical-equal)
  (165 logical-not-equal)
  (166 logical-or)
  (167 logical-and)
  (168 logical-not nil)
  (170 i-equal)
  (171 i-not-equal)
  (172 u-greater-than)
  (173 s-greater-than)
  (174 u-greater-than-equal)
  (175 s-greater-than-equal)
  (176 u-less-than)
  (177 s-less-than)
  (178 u-less-than-equal)
  (179 s-less-than-equal)
  (180 f-ord-equal)
  (181 f-unord-equal)
  (182 f-ord-not-equal)
  (183 f-unord-not-equal)
  (184 f-ord-less-than)
  (185 f-unord-less-than)
  (186 f-ord-greater-than)
  (187 f-unord-greater-than)
  (188 f-ord-less-than-equal)
  (189 f-unord-less-than-equal)
  (190 f-ord-greater-than-equal)
  (191 f-unord-greater-than-equal))

(define-spir-v-arithmetic-ops
  (197 bitwise-or)
  (198 bitwise-xor)
  (199 bitwise-and)
  (200 not nil))

(define-spir-v-arithmetic-ops
  (401 ptr-equal)
  (402 ptr-not-equal)
  (403 ptr-diff))

(define-spir-v-instruction :bc 245 phi
  (&rest variable-parent-id-pairs))

(define-spir-v-instruction :a  246 loop-merge
  ((merge-block spir-v-result-id)
   (continue-target spir-v-result-id)
   (loop-control uint32) ; todo: mask
   &rest loop-control-parameters))

(define-spir-v-instruction :a  247 selection-merge
  ((merge-block spir-v-result-id)
   (selection-control uint32) ; todo mask
   ))

(define-spir-v-instruction :b  248 label ())

(define-spir-v-instruction :a  249 branch
  ((target-label spir-v-result-id)))

;; note: 0 or 2 branch weights
(define-spir-v-instruction :a  250 branch-conditional
  ((condition spir-v-result-id)
   (true-label spir-v-result-id)
   (false-label spir-v-result-id)
   &rest branch-weights))

(define-spir-v-instruction :a  251 switch
  ((selector spir-v-result-id)
   (default spir-v-result-id)
   &rest targets))

(define-spir-v-instruction :a  252 kill ())

(define-spir-v-instruction :a  253 return ())

(define-spir-v-instruction :a  254 return-value
  ((value spir-v-result-id)))

(define-spir-v-instruction :a  255 unreachable ())

(defmacro define-glsl-ext (instruction name args)
  (declare (ignore args))
  (let* ((%name (concatenate 'string "%" (symbol-name name)))
         (%name* (intern %name '#:zrspv)))
    `(progn
       (setf (gethash ,instruction *glsl-instruction-names*) ',%name*
             (gethash ',%name* *glsl-instruction-opcodes*) ,instruction))))

(defmacro define-glsl-ext-arithmetic-op
    (instruction name &optional (binary? t))
  `(define-glsl-ext ,instruction ,name
     ,(if binary?
          `((x spir-v-result-id)
            (y spir-v-result-id))
          `((x spir-v-result-id)))))

(defmacro define-glsl-ext-arithmetic-ops (&body definitions)
  `(progn ,@(mapcar (lambda (definition)
                      `(define-glsl-ext-arithmetic-op ,@definition))
                    definitions)))

(define-glsl-ext-arithmetic-ops
  (001 round nil)
  (002 round-even nil)
  (003 trunc nil)
  (004 f-abs nil)
  (005 s-abs nil)
  (006 f-sign nil)
  (007 s-sign nil)
  (008 floor nil)
  (009 ceil nil)
  (010 fract nil)
  (011 radians nil)
  (012 degrees nil)
  (013 sin nil)
  (014 cos nil)
  (015 tan nil)
  (016 asin nil)
  (017 acos nil)
  (018 atan nil)
  (019 sinh nil)
  (020 cosh nil)
  (021 tanh nil)
  (022 asinh nil)
  (023 acosh nil)
  (024 atanh nil)
  (025 atan2)
  (026 pow)
  (027 exp nil)
  (028 log nil)
  (029 exp2 nil)
  (030 log2 nil)
  (031 sqrt nil)
  (032 inverse-sqrt nil)
  (033 determinant nil)
  (034 matrix-inverse nil)
  (036 modf-struct nil)
  (037 f-min)
  (038 u-min)
  (039 s-min)
  (040 f-max)
  (041 u-max)
  (042 s-max))

(define-glsl-ext 043 f-clamp
  ((x spir-v-result-id)
   (min-val spir-v-result-id)
   (max-val spir-v-result-id)))

(define-glsl-ext 044 u-clamp
  ((x spir-v-result-id)
   (min-val spir-v-result-id)
   (max-val spir-v-result-id)))

(define-glsl-ext 045 s-clamp
  ((x spir-v-result-id)
   (min-val spir-v-result-id)
   (max-val spir-v-result-id)))

(define-glsl-ext 046 f-mix
  ((x spir-v-result-id)
   (y spir-v-result-id)
   (a spir-v-result-id)))

(define-glsl-ext 048 step
  ((edge spir-v-result-id)
   (x spir-v-result-id)))

(define-glsl-ext 049 smooth-step
  ((edge-0 spir-v-result-id)
   (edge-1 spir-v-result-id)
   (x spir-v-result-id)))

(define-glsl-ext 050 fma
  ((a spir-v-result-id)
   (b spir-v-result-id)
   (c spir-v-result-id)))

(define-glsl-ext 052 fr-exp-struct
  ((x spir-v-result-id)))

(define-glsl-ext 053 ld-exp
  ((x spir-v-result-id)
   (exp spir-v-result-id)))

(defmacro define-glsl-ext-pack-unpack (instruction name)
  `(define-glsl-ext ,instruction ,name
     ((id spir-v-result-id))))

(define-glsl-ext-pack-unpack 054 pack-snorm-4x8)
(define-glsl-ext-pack-unpack 055 pack-unorm-4x8)
(define-glsl-ext-pack-unpack 056 pack-snorm-2x16)
(define-glsl-ext-pack-unpack 057 pack-unorm-2x16)
(define-glsl-ext-pack-unpack 058 pack-half-2x16)
(define-glsl-ext-pack-unpack 059 pack-double-2x32)
(define-glsl-ext-pack-unpack 060 unpack-snorm-2x16)
(define-glsl-ext-pack-unpack 061 unpack-unorm-2x16)
(define-glsl-ext-pack-unpack 062 unpack-half-2x16)
(define-glsl-ext-pack-unpack 063 unpack-snorm-4x8)
(define-glsl-ext-pack-unpack 064 unpack-unorm-4x8)
(define-glsl-ext-pack-unpack 065 unpack-double-2x32)

(define-glsl-ext-arithmetic-ops
  (066 length nil)
  (067 distance)
  (068 cross)
  (069 normalize nil)
  (071 reflect))

(define-glsl-ext 070 face-forward
  ((n spir-v-result-id)
   (i spir-v-result-id)
   (n-ref spir-v-result-id)))

(define-glsl-ext 072 refract
  ((i spir-v-result-id)
   (n spir-v-result-id)
   (eta spir-v-result-id)))

(define-glsl-ext-arithmetic-ops
  (073 find-i-lsb nil)
  (074 find-s-msb nil)
  (075 find-u-msb nil)
  (076 interpolate-at-centroid nil)
  (077 interpolate-at-sample)
  (078 interpolate-at-offset)
  (079 n-min)
  (080 n-max))

(define-glsl-ext 081 n-clamp
  ((x spir-v-result-id)
   (min-val spir-v-result-id)
   (max-val spir-v-result-id)))
