ZRVL
====

The Zombie Raptor Vector Language (ZRVL) is an extended subset of
Common Lisp with vector, linear algebra, and numerical computation
capabilities.

ZRVL's goals are:

- To target the GPU through compiling to SPIR-V. In particular, it
  aims to support the shader (OpenGL 4.6 or Vulkan) flavor of SPIR-V,
  rather than the compute kernel flavor. It takes a lot of design cues
  from the OpenGL Shading Language, GLSL, and was originally named
  ZRSL instead of ZRVL. Anyone familiar with both GLSL and CL should
  be able to translate most GLSL into ZRVL.

- To target the CPU, with SIMD where supported, on the more
  computationally intensive and linear algebra oriented portions of
  the game engine. Initially, SIMD support will be limited to SB-SIMD,
  which restricts it to SBCL on the x86-64 architecture. A scalar
  fallback is available otherwise. The idea behind this is to handle
  the entity-component-system (ECS) like compute shaders that run on
  the CPU.

- To target the CPU in an "inline" mode, which will feel like a
  regular Common Lisp linear algebra library, except with full
  tree-walking to optimize away unnecessary allocations at the expense
  of not being able to call most of CL from within this mode.

This is an extended subset because it adds things from GLSL and
similar languages or APIs:

- This includes `clamp`, `dot` (dot-product), `cross` (cross-product),
  `radians` (from degrees), etc.

- ZRVL also has an implementation of a GLSL-style `swizzle`, which
  uses a keyword argument like `(swizzle foo :grb)` instead of dot
  notation like GLSL's `foo.grb`.

- Like GLSL, ZRVL has `vec` and `matrix` built-in types. Mathematical
  vectors are called `vec`s to avoid confusion with programming
  vectors. In Common Lisp, "vectors" are 1D arrays. ZRVL takes other
  types from GLSL as well. For instance, `sampler`, `texture`, etc.,
  types will be available on the GPU in stages where they are
  available in GLSL.

Almost everything commonly used in GLSL and more is here, but the
names may have been Lispified or otherwise made consistent. For
instance, `outerProduct` in GLSL is now just `outer` without "product"
in order to be consistent with `dot` and `cross`.

There are some other extensions:

- Quaternions will be a built-in type.

- Other, more advanced concepts from related fields such as exterior
  algebra and geometric algebra may be added. If geometric algebra is
  added, it will use the Lengyelian formulation of geometric algebra,
  with `wedge-dot`.

- Larger matrix and vector types may be implemented as types that
  become the smaller

- The different, more restricted type system means that verified
  compile-time metadata may be added, such as units of measurement,
  which will not usually have to be retained at runtime.

- The restricted type system may also mean that something like
  typeclasses may be added in the future.

There are some slight differences to have the language fit the
intended domain:

- `T` and `NIL` have been renamed to `zrvl:true` and `zrvl:false`.
  There are a substantial amount of algorithms and equations in the
  domains that this language is intended for that use `T` as a
  variable. With the true constant value renamed, renaming the
  corresponding false value makes sense as well.

- There is no dynamic typing; instead, types are inferred at
  compilation time. This will normally feel the same as dynamic
  typing, but it introduces additional restrictions. Generally
  speaking, where Common Lisp would fall back to generic arithmetic,
  such as a generic `+`, this language does not let you do it. Use
  Common Lisp for generic arithmetic.

There are also some *subtractions* in features to run on the GPU
environment without doing substantial performance-impacting effort to
try to re-implement these features:

- The generalized boolean does not exist, instead strictly relying on
  `zrvl:true` and `zrvl:false` for the booleans. Non-boolean values
  are not truthy. This is because ZRVL is not dynamically typed and is
  primarily used with homogeneous numeric data structures, which
  should homogeneously become boolean values.

- Mutation is strongly discouraged. This makes `dotimes` almost
  useless. On the other hand, `loop` is too powerful and is hard to
  implement in SPIR-V for the GPU. This means that iteration somewhere
  between those two macros in power (and no, `do` doesn't count) is
  needed. Thus, the iteration macros differ. This isn't a big deal
  because iteration is not important on the GPU. The iteration will
  also try to feel like a Lispier (parenthesized) `loop`.

- On the other hand, types are inferred and a substantial amount of
  extra compile-time optimizations and inlining are done. This makes
  any normal performance penalties of higher order functions
  nonexistent, so using things like `map` and `reduce` even with
  currying, etc., should be very efficient. This makes ZRVL generally
  more functional than idiomatic high-performance Common Lisp. So this
  is a case where subtracting features helps make faster code.

- The type system is slightly different, including the numeric tower.
  Right now, bignums and ratios do not exist, but they may be added
  back in a limited, opt-in way in the future if possible. On the
  other hand, short-float (here 16-bit instead of the CL standard's
  18-bit) is probably available on the GPU as a separate type from
  single-float. (Note that short-float literals like `1.0s0` won't
  work because the Common Lisp will turn it into a single-float if it
  has no short-float type.)

- Getting complex numbers is probably going to be opt-in instead of
  something that happens automatically for things like `(sqrt -1)`.

ZRVL isn't designed to be a standalone programming language. It's an
embedded DSL that relies on the host CL for many things. Some things
that could be implemented, like characters and strings, currently
aren't planned.

Arrays must be specialized with no heterogeneous `T` generic fallback.
Symbols and keywords either do not exist or are converted into what
are essentially just enums. These symbols don't have `SYMBOL-PLIST`s,
etc.

Out of necessity, most of CL is missing, including CLOS and conditions
and even (at runtime; macros, written in CL, still exist)
conses/lists. A separate, homogeneous, immutable, and purely
functional cons/list type may be added in the future, using CDR coding
and arrays. This will behave differently than Common Lisp lists, but
may be exposed to Common Lisp on the CPU side via
extensible-sequences.

Also out of necessity, recursion currently does not exist and will be
introduced later in restricted forms that can and must be optimized
away. Perhaps eventually this restriction can be lifted. Shader
languages have not traditionally allowed any recursion.

Similarly, higher order functions and lexical closures are more
restricted than in Common Lisp, again because of the restricted
environment of the GPU. Anything that requires allocating a closure on
the heap is probably not possible here. A later heroic effort might
make more of this more feasible than it seems, but it's not a 1.0
priority for the language and, if it comes at a performance penalty,
it won't make sense in most of the use cases for ZRVL.

Some syntax extensions are planned. These will be printable
representations that are also readable if optional reader macros are
loaded.

Optional reader macros include ones that undo new matrix `#M` and vec
`#V` printable literal representations.

A vec may print as:

```common-lisp
#V(1.0 2.0 3.0)
```

A matrix is always displayed row-major even though a matrix is
configurable between row major and column major, and is by default
column major. (This configuration will not be in the initial
language.) Effectively, a matrix printable representation looks like a
2D array one.

A matrix may print as:

```common-lisp
#M((1.0  2.0  3.0  4.0)
   (5.0  6.0  7.0  8.0)
   (9.0 10.0 11.0 12.0))
```

On the Common Lisp side, the types may or may not be encapsulated in a
struct that preserves the type information. Matrices in particular may
have many underlying implementations since they can be represented as
1D or 2D arrays as well as subsequences in larger 1D or subsections of
larger 2D or 3D arrays. The printable representation can only work if
they show up to the CL as ZRVL matrices.

Note that switching the matrix to row-major will change the
OpTypeMatrix of SPIR-V to reverse the specified order so support for
row-major matrices will have to come at a later time.

Using reader macros to create literals with the simple printable
representations will necessarily lose some information because it is
not fully unambiguous, such as the aforementioned column-major vs
row-major matrix differences. Also, while the floating point type is
unambiguous, the specific integer type will be unclear and will be
assumed to be signed 32-bit.
