(defpackage #:zombie-raptor/zrvl/spir-v
  (:documentation "Generates SPIR-V binaries.")
  (:use #:cl
        #:zombie-raptor/core/conditions
        #:zombie-raptor/data/shader-core
        #:zombie-raptor/zrvl/a-normal
        #:zombie-raptor/zrvl/core-types
        #:zombie-raptor/zrvl/instructions
        #:zombie-raptor/zrvl/spir-v-enums
        #:zombie-raptor/zrvl/spir-v-instructions
        #:zombie-raptor/zrvl/zrvl
        #:zr-utils)
  (:export #:*shader-name*
           #:shader-body*))

(in-package #:zombie-raptor/zrvl/spir-v)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defconstant +magic-number+ #x07230203))

;;; TODO: get an ID
(defconstant +zrvl-generator-id+ #x00000000)

(defconstant +zrvl-source-version+ 0)

;;; The current shader's name is available throughout in order to be
;;; usable in debugging. To avoid passing it in as an argument to
;;; nearly every function, it's a special variable.
(defparameter *shader-name* nil)

(defstruct spir-v-metadata
  ;; alternatively, #x00010600
  (version #x00010000 :type uint32 :read-only t)
  (generator-id +zrvl-generator-id+ :type uint32 :read-only t)
  (schema #x00000000 :type uint32 :read-only t))

(define-accessor-macro with-spir-v-metadata #:spir-v-metadata-)

(define-function (spir-v-metadata :inline t) (spir-v-array)
  (check-type (aref spir-v-array 0)
              (integer #.+magic-number+
                       #.+magic-number+))
  (make-spir-v-metadata :version (aref spir-v-array 1)
                        :generator-id (aref spir-v-array 2)
                        :schema (aref spir-v-array 4)))

;;; Sets the metadata that goes at the start of a SPIR-V file.
(define-function ((setf spir-v-metadata) :inline t)
    ((metadata spir-v-metadata)
     spir-v-array)
  (with-spir-v-metadata (version generator-id schema)
      metadata
    (psetf (array-of-3 spir-v-array) (values +magic-number+
                                             version
                                             generator-id)
           (aref spir-v-array 4) schema)))

;;; Converts the more intuitive-to-me <= upper-bound to the <
;;; upper-bound that the SPIR-V standard actually expects.
(define-function ((setf spir-v-id-upper-bound) :inline t) (value spir-v-array)
  (setf (aref spir-v-array 3) (1+ value)))

;;; Writes to a temporary file so that spir-v-dis and spir-v-val from
;;; spir-v-tools can test the generated output.
;;;
;;; TODO: This probably should move into unit tests for the CI, but
;;; only when `spirv-tools` is detected as installed.
(defun write-spv-file (spir-v &key (path #P"/tmp/tmp.spv"))
  (with-open-file (stream
                   path
                   :if-exists :overwrite
                   :if-does-not-exist :create
                   :element-type 'uint32
                   :direction :output)
    (write-sequence spir-v stream)))

(defmacro generate-decorations (&body decoration-definitions)
  `(append ,@(mapcar (destructuring-lambda (test result-form)
                       `(if ,test (list ,result-form) nil))
                     decoration-definitions)))

;;; TODO: More decorations. Where no arguments, make it a boolean
;;; argument. Where 1 argument, use a regular keyword enum lookup.
;;; Where 2+ arguments, make it a list.
(defun %parse-decoration (name member? keys)
  (destructuring-bind (&key
                         location
                         descriptor-set
                         binding
                         built-in
                         members
                         block?
                         =)
      keys
    (declare (ignore =))
    (generate-decorations
      (location
       `(,name :location ,location))
      (descriptor-set
       `(,name :descriptor-set ,descriptor-set))
      (binding
       `(,name :binding ,binding))
      (built-in
       `(,name :built-in ,(built-in-enum built-in)))
      (members
       (progn
         (when member?
           (error "Nested members-in-members syntax is not allowed."))
         `(,name ,@(parse-decorations* members))))
      (block?
       `(,name :block)))))

(defun parse-decorations* (members)
  (loop :for member :in members
        :for i :from 0
        :append (destructuring-bind (name &rest keys) member
                  (declare (ignore name))
                  (%parse-decoration i t keys))))

(defun parse-decorations (variables)
  (loop :for variable :in variables
        :append (destructuring-bind (name type storage-class &rest keys) variable
                  (declare (ignore type storage-class))
                  (%parse-decoration name nil keys))))

;;; Converts GLSL-style storage classes used in the shader language to
;;; SPIR-V-style storage classes used in the SPIR-V internal
;;; representation.
(define-function (spir-v-storage-class :inline t) (storage-class)
  (ecase storage-class
    (:in :input)
    (:uniform :uniform-constant)
    (:out :output)))

;;; Converts GLSL-style shader stages used in the shader language to
;;; SPIR-V-style execution models used in the SPIR-V internal
;;; representation.
(define-function (shader-stage-to-execution-model :inline t) (shader-stage)
  (ecase shader-stage
    (:vertex-shader :vertex)
    (:fragment-shader :fragment)))

(defun spir-v-style (forms)
  (with-ext-inst-id (1)
    (let ((forms* (full-ir-compile forms)))
      (multiple-value-bind (types next-id type-ids)
          (type-ids forms*
                    :start (1+ (ext-inst-id))
                    :main? t)
        (with-spir-v-id (next-id)
          ;; TODO: Before rebinding, remove constant if the array type
          ;; created a constant of the same type and value for the
          ;; array length.
          (let ((forms** (rebind-bindings forms* #'new-spir-v-id)))
            (multiple-value-bind (forms*** main-id interface names)
                (instruction-forms forms** type-ids)
              (values (append types (apply #'append forms***))
                      type-ids
                      (spir-v-id)
                      (ext-inst-id)
                      main-id
                      interface
                      names))))))))

(defun spir-v-type-instruction (form)
  (destructuring-bind (id (type &rest args)) form
    (if (eql type :length)
        (destructuring-bind (result-type value) args
          (inst id result-type 'zrspv:%constant value))
        (apply #'inst*
               id
               (ecase type
                 (:void 'zrspv:%type-void)
                 (:bool 'zrspv:%type-bool)
                 (:int 'zrspv:%type-int)
                 (:float 'zrspv:%type-float)
                 (:vector 'zrspv:%type-vector)
                 (:matrix 'zrspv:%type-matrix)
                 (:spir-v-image 'zrspv:%type-image)
                 (:spir-v-sampled-image 'zrspv:%type-sampled-image)
                 (:array 'zrspv:%type-array)
                 (:struct 'zrspv:%type-struct)
                 (:pointer 'zrspv:%type-pointer)
                 (:function 'zrspv:%type-function))
               args))))

(defun ir-to-spir (forms type-ids spir-v-id ext-inst-id main-id interface names)
  (with-ext-inst-id (ext-inst-id)
    (values (mapcar (lambda (form)
                      (if (%%binding-p form)
                          (spir-v-form form type-ids)
                          (spir-v-type-instruction form)))
                    forms)
            spir-v-id
            main-id
            interface
            names)))

(defun spir-v-glsl-semantics (&optional (ext-id 1))
  (list (inst** 'zrspv:%capability
                (capability-type-enum :shader))
        (apply #'inst*
               ext-id
               'zrspv:%ext-inst-import
               (coerce (spir-v-string "GLSL.std.450") 'list))
        (inst** 'zrspv:%memory-model
                (addressing-model-enum :logical)
                (memory-model-enum :glsl450))))

(define-function spir-v-introduction ((main-id uint32)
                                      (execution-model execution-model-enum)
                                      (interface-variable-ids list))
  (append (list (apply #'inst**
                       'zrspv:%entry-point
                       (execution-model-enum execution-model)
                       main-id
                       (append (coerce (spir-v-string "main") 'list)
                               interface-variable-ids)))
          (if (eql execution-model :fragment)
              (list (inst** 'zrspv:%execution-mode
                            main-id
                            (execution-mode-enum :origin-lower-left)))
              nil)
          (list (inst** 'zrspv:%source
                        (language-enum :zrvl)
                        +zrvl-source-version+))))

;;; TODO: struct member names and non-interface names
(defun debug-name-instructions (debug-names)
  (loop :for name :in debug-names
        :append (list (apply #'inst**
                             'zrspv:%name
                             (car name)
                             (coerce (spir-v-string (glsl-name (cadr name))) 'list)))))

(defun decoration-instructions (decorations names)
  (flet ((decorate (id decoration-name decoration-value)
           (if (eql decoration-value t)
               (inst** 'zrspv:%decorate
                       id
                       (decoration-enum decoration-name))
               (inst** 'zrspv:%decorate
                       id
                       (decoration-enum decoration-name)
                       decoration-value)))
         (decorate-member (id decoration-value)
           (loop :for member-decoration :in decoration-value
                 :for i :from 0
                 :append (destructuring-bind (member-name &rest member-decorations*)
                             member-decoration
                           (declare (ignore member-name))
                           (loop :for member-decorations** :on member-decorations* :by #'cddr
                                 :for member-decoration-name := (car member-decorations**)
                                 :for member-decoration-value := (cadr member-decorations**)
                                 :unless member-decoration-value
                                   :do (error "Malformed SPIR-V decoration syntax.")
                                 :collect (if (eql member-decoration-value t)
                                              (inst** 'zrspv:%member-decorate
                                                      id
                                                      i
                                                      (decoration-enum member-decoration-name))
                                              (inst** 'zrspv:%member-decorate
                                                      id
                                                      i
                                                      (decoration-enum member-decoration-name)
                                                      (if (eql member-decoration-name :built-in)
                                                          (built-in-enum member-decoration-value)
                                                          member-decoration-value))))))))
    (loop :for decoration :in decorations
          :append (destructuring-bind (%decorate name &rest decorations*)
                      decoration
                    (assert (eql %decorate '%decorate))
                    (let ((id (gethash name names)))
                      (unless id
                        (error "SPIR-V ID for ~A not found" name))
                      (loop :for decorations** :on decorations* :by #'cddr
                            :for decoration-name := (car decorations**)
                            :for decoration-value := (cadr decorations**)
                            :unless decoration-value
                              :do (error "Malformed SPIR-V decoration syntax.")
                            :if (eql decoration-name :members)
                              :append (decorate-member id decoration-value)
                            :else
                              :collect (decorate id decoration-name decoration-value)))))))

(defun compile-to-spir-v (stage forms &optional globals defines decorations)
  (let* ((*id-counter* 1)
        (forms* `((zrvl-int:with-function ((%%ftype (%%void)) ,globals ,defines) ,@forms))))
    (with-backend (:spir-v)
      (multiple-value-bind (spir-list last-id main-id interface names)
          (multiple-value-call #'ir-to-spir (spir-v-style forms*))
        #+(or)
        (when (string= *shader-name* 'simple-2d-vert)
          (map nil
               (lambda (spir)
                 (format t "~A~%" (decode-spir-v-instruction spir)))
               spir-list))
        (let* ((glsl-ext-id 1)
               (spir-list (append (spir-v-glsl-semantics glsl-ext-id)
                                  (spir-v-introduction main-id stage (mapcar #'car interface))
                                  (debug-name-instructions (list* (list main-id 'main)
                                                                  interface))
                                  (decoration-instructions decorations names)
                                  spir-list))
               (metadata-size 5)
               (spir-size (reduce (lambda (x y) (+ x (length y))) spir-list :initial-value metadata-size))
               (spir (make-array spir-size :element-type 'uint32)))
          (setf (spir-v-metadata spir) (make-spir-v-metadata))
          (setf (aref spir 3) last-id)
          (loop :for i := metadata-size :then (+ i (length form))
                :for form :in spir-list
                :do (replace spir form :start1 i)
                :finally (return spir)))))))

;;; Global variables are input, uniform, or output.
(defun parse-zrvl-globals (globals input?)
  (loop :for variable :in globals
        :with set-form := nil
        :collect (destructuring-bind (storage-class
                                      name
                                      type
                                      &key =
                                      &allow-other-keys)
                     variable
                   (if input?
                       (check-type = null)
                       (setf set-form (if =
                                          `(zrvl-int:setf ,name ,=)
                                          nil)))
                   (if input?
                       (check-type storage-class (member :in :uniform))
                       (check-type storage-class (member :out)))
                   `(%declare-parameter
                     ,name
                     ,type
                     ,(spir-v-storage-class storage-class)))
          :into bindings
        :collect (destructuring-bind (storage-class
                                      name
                                      type
                                      &key
                                        location
                                        descriptor-set
                                        binding
                                        built-in
                                        members
                                        block
                                        =)
                     variable
                   (declare (ignore storage-class type =))
                   (append (list '%decorate name)
                           (and location (list :location location))
                           (and descriptor-set (list :descriptor-set descriptor-set))
                           (and binding (list :binding binding))
                           (and built-in (list :built-in built-in))
                           (and members (list :members members))
                           (and block (list :block block))))
          :into decorations
        :when set-form
          :collect set-form :into set-forms
        :finally (return (values bindings set-forms decorations))))

(defun shader-body* (shader-stage inputs outputs &optional defines)
  (multiple-value-bind (inputs* no-outputs in-decorations)
      (parse-zrvl-globals inputs t)
    (when no-outputs
      (error "Inputs should not have outputs."))
    (multiple-value-bind (outputs* set-outputs out-decorations)
        (parse-zrvl-globals outputs nil)
      (let ((stage (shader-stage-to-execution-model shader-stage))
            (globals (append inputs*
                             (if (eql shader-stage :vertex-shader)
                                 (list '(%declare-parameter gl-vertex-id zrvl:i32 :input)
                                       '(%declare-parameter gl-instance-id zrvl:i32 :input))
                                 nil)
                             (substitute-if `(%declare-parameter zrvl:gl-per-vertex
                                                                 zrvl:gl-per-vertex
                                                                 :output)
                                            (lambda (x)
                                              (eql x 'zrvl:gl-position))
                                            outputs*
                                            :key #'cadr)))
            (decorations (append in-decorations
                                 (substitute-if `(%decorate zrvl:gl-per-vertex
                                                            :members ((zrvl::gl-position :built-in :position)
                                                                      (zrvl::gl-point-size :built-in :point-size)
                                                                      (zrvl::gl-clip-distance :built-in :clip-distance)
                                                                      (zrvl::gl-cull-distance :built-in :cull-distance))
                                                            :block t)
                                                (lambda (x)
                                                  (eql x 'zrvl:gl-position))
                                                out-decorations
                                                :key #'cadr))))
        (let ((output (compile-to-spir-v stage set-outputs globals defines decorations)))
          ;; Writes a temporary file so `spirv-val` and `spirv-dis`
          ;; can verify the output.
          (write-spv-file output
                          :path (make-pathname :directory "tmp"
                                               :name (string-downcase (symbol-name *shader-name*))
                                               :type "spv"))
          output)))))
