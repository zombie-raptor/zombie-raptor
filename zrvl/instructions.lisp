(defpackage #:zombie-raptor/zrvl/instructions
  (:documentation "Abstract instructions that become SPIR-V or Lisp code.")
  (:use #:cl
        #:zombie-raptor/zrvl/scalar
        #:zombie-raptor/zrvl/spir-v-enums
        #:zombie-raptor/zrvl/spir-v-instructions
        #:zr-utils)
  (:export #:%*+
           #:%access-chain
           #:%acos
           #:%acosh
           #:%asin
           #:%asinh
           #:%atan
           #:%atan2
           #:%atanh
           #:%ceil
           #:%composite-construct
           #:%composite-extract
           #:%constant
           #:%constant-composite
           #:%convert-f-to-s
           #:%cos
           #:%cosh
           #:%cross
           #:%degrees
           #:%determinant
           #:%distance
           #:%dot
           #:%exp
           #:%exp2
           #:%expt
           #:%f*
           #:%f+
           #:%f-
           #:%f-abs
           #:%f-clamp
           #:%f-equal
           #:%f-greater-than
           #:%f-greater-than-equal
           #:%f-less-than
           #:%f-less-than-equal
           #:%f-max
           #:%f-min
           #:%f-mix
           #:%f-negate
           #:%f-not-equal
           #:%f/
           #:%floor
           #:%function-begin
           #:%function-end
           #:%image-sample-implicit-lod
           #:%label
           #:%ln
           #:%log2
           #:%matrix*matrix
           #:%matrix*scalar
           #:%matrix*vector
           #:%matrix-inverse
           #:%normalize
           #:%radians
           #:%read-var
           #:%reflect
           #:%return
           #:%round-even
           #:%sin
           #:%sinh
           #:%sqrt
           #:%tan
           #:%tanh
           #:%transpose
           #:%trunc
           #:%variable
           #:%vec-length
           #:%vector*matrix
           #:%vector*scalar
           #:%vector-shuffle
           #:%write-var
           #:*instructions*
           #:define-instruction
           #:ext-inst-id
           #:new-spir-v-id
           #:spir-v-id
           #:with-backend
           #:with-ext-inst-id
           #:with-spir-v-id))

(in-package #:zombie-raptor/zrvl/instructions)

(defvar *instructions* (make-hash-table))

(defparameter *spir-v-id* 1)
(defparameter *ext-inst-id* nil)

(defvar *backend* nil)

(defmacro with-spir-v-id ((&optional (id 1)) &body body)
  `(let ((*spir-v-id* ,id))
     ,@body))

(defmacro with-ext-inst-id ((&optional (id 1)) &body body)
  `(let ((*ext-inst-id* ,id))
     ,@body))

;;; TODO: Define a type for the backends?
(defmacro with-backend ((backend) &body body)
  "
Tells the instructions which backend to generate code for.
"
  `(let ((*backend* ,backend))
     ,@body))

(defun spir-v-id ()
  "A monotonically increasing constant representing a new unique integer ID for SPIR-V."
  *spir-v-id*)

;;; Note: This is a dynamically-scoped variable because it is only
;;; used when generating SPIR-V, not in every backend.
(defun ext-inst-id ()
  "The ID, if it exists, of the SPIR-V extended instructions set."
  *ext-inst-id*)

(define-function (new-spir-v-id :inline t) ()
  (prog1 *spir-v-id*
    (incf *spir-v-id*)))

(defmacro define-instruction ((name inst-type) lambda-list &body body)
  "
Defines an instruction, which is similar to a function call, but is
the most basic unit. For instance, this could produce SPIR-V
instructions or a trivial, short chain of SPIR-V instructions if
generating SPIR-V. These could also call CPU implementations of the
equivalent functionality. Type inference doesn't work on these, unlike
everything else so use them sparingly!
"
  ;; TODO: define a type? also used in spir-v-instructions
  (unless (typep inst-type '(member :bc :b :a))
    (error "
A defined instruction needs to be classified as :A, :B, or :BC, where
:A has no result-id or result-type, :B has only a result-id, and :BC
has both a result-id and a result-type. Note that there is no
:C (result-type only)."))
  `(progn
     (setf (gethash ',name *instructions*) ,inst-type)
     (defun ,name ,lambda-list
       (unless (typep *backend* '(member :spir-v :scalar))
         (error "~S is not a valid compiler backend." *backend*))
       ;; TODO: The body should have a clause for every backend or a T
       ;; clause fallback for missing backends. Verify it here.
       (ecase *backend*
         ,@body))))

(define-instruction (%composite-extract :bc) (result-id result-type composite &rest index-literals)
  (:spir-v (apply #'inst result-id result-type 'zrspv:%composite-extract composite index-literals))
  (:scalar (apply #'lisp-cref result-id result-type composite index-literals)))

(define-instruction (%composite-construct :bc) (result-id result-type &rest composite-ids)
  (:spir-v (apply #'inst result-id result-type 'zrspv:%composite-construct composite-ids))
  (:scalar (apply #'lisp-composite-construct result-id result-type composite-ids)))

(define-instruction (%vector-shuffle :bc) (result-id result-type vec1 vec2 &rest components)
  (:spir-v (apply #'inst result-id result-type 'zrspv:%vector-shuffle vec1 vec2 components))
  (:scalar (apply #'lisp-vector-shuffle result-id result-type vec1 vec2 components)))

(define-instruction (%access-chain :bc) (result-id result-type base &rest indexes)
  (:spir-v (apply #'inst result-id result-type 'zrspv:%access-chain base indexes)))

(define-instruction (%read-var :bc) (result-id result-type pointer)
  (:spir-v (inst result-id result-type 'zrspv:%load pointer))
  (:scalar (lisp-var result-id result-type pointer)))

(define-instruction (%write-var :a) (pointer object)
  (:spir-v (inst** 'zrspv:%store pointer object))
  (:scalar (lisp-setf pointer object)))

;;; TODO: Support function parameters
(define-instruction (%function-begin :bc) (result-id result-type function-type)
  (:spir-v (let ((function-control 0))
            (inst result-id result-type 'zrspv:%function function-control function-type))))

(define-instruction (%function-end :a) ()
  (:spir-v (inst** 'zrspv:%function-end)))

;;; TODO: Support non-void return with %return-value instead of %return
(define-instruction (%return :a) ()
  (:spir-v (inst** 'zrspv:%return)))

(define-instruction (%label :b) (result-id)
  (:spir-v (inst* result-id 'zrspv:%label)))

(define-instruction (%variable :bc) (result-id result-type storage-class &optional metadata)
  (:spir-v (inst result-id result-type 'zrspv:%variable storage-class))
  (:scalar (lisp-init-var result-id result-type storage-class metadata)))

(define-instruction (%constant :bc) (result-id result-type value)
  (:spir-v (inst result-id result-type 'zrspv:%constant value))
  (:scalar (lisp-constant result-id result-type value)))

(define-instruction (%constant-composite :bc) (result-id result-type &rest constituents)
  (:spir-v (apply #'inst result-id result-type 'zrspv:%constant-composite constituents))
  (:scalar (apply #'lisp-constant-composite result-id result-type constituents)))

;;; TODO: difference between ord and unord? which should be default?
;;; should there be a way to do both in one program? At the moment,
;;; this assumes e.g. f-equal will always be f-ord-equal, but if I
;;; guessed incorrectly, this will be changed to the -unord- version.

;;; TODO: scalar
(define-instruction (%f-equal :bc) (result-id result-type a b)
  (:spir-v (inst result-id result-type 'zrspv:%f-ord-equal a b)))

;;; TODO: scalar
(define-instruction (%f-not-equal :bc) (result-id result-type a b)
  (:spir-v (inst result-id result-type 'zrspv:%f-ord-not-equal a b)))

;;; TODO: scalar
(define-instruction (%f-less-than :bc) (result-id result-type a b)
  (:spir-v (inst result-id result-type 'zrspv:%f-ord-less-than a b)))

;;; TODO: scalar
(define-instruction (%f-less-than-equal :bc) (result-id result-type a b)
  (:spir-v (inst result-id result-type 'zrspv:%f-ord-less-than-equal a b)))

;;; TODO: scalar
(define-instruction (%f-greater-than :bc) (result-id result-type a b)
  (:spir-v (inst result-id result-type 'zrspv:%f-ord-greater-than a b)))

;;; TODO: scalar
(define-instruction (%f-greater-than-equal :bc) (result-id result-type a b)
  (:spir-v (inst result-id result-type 'zrspv:%f-ord-greater-than-equal a b)))

(define-instruction (%f+ :bc) (result-id result-type a b)
  (:spir-v (inst result-id result-type 'zrspv:%f-add a b))
  (:scalar (lisp-f+ result-id result-type a b)))

(define-instruction (%f- :bc) (result-id result-type a b)
  (:spir-v (inst result-id result-type 'zrspv:%f-sub a b))
  (:scalar (lisp-f- result-id result-type a b)))

(define-instruction (%f* :bc) (result-id result-type a b)
  (:spir-v (inst result-id result-type 'zrspv:%f-mul a b))
  (:scalar (lisp-f* result-id result-type a b)))

(define-instruction (%f-negate :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%f-negate x))
  (:scalar (lisp-negate result-id result-type x)))

(define-instruction (%f/ :bc) (result-id result-type a b)
  (:spir-v (inst result-id result-type 'zrspv:%f-div a b))
  (:scalar (lisp-f/ result-id result-type a b)))

;;; TODO: scalar
(define-instruction (%*+ :bc) (result-id result-type a b c)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%fma a b c)))

(define-instruction (%dot :bc) (result-id result-type vec1 vec2)
  (:spir-v (inst result-id result-type 'zrspv:%dot vec1 vec2))
  (:scalar (lisp-dot result-id result-type vec1 vec2)))

(define-instruction (%f-abs :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%f-abs x))
  (:scalar (lisp-abs result-id result-type x)))

;;; TODO: scalar
(define-instruction (%round-even :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%round-even x)))

;;; TODO: scalar
(define-instruction (%trunc :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%trunc x)))

;;; TODO: scalar
(define-instruction (%floor :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%floor x)))

;;; TODO: scalar
(define-instruction (%ceil :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%ceil x)))

;;; TODO: scalar
(define-instruction (%convert-f-to-s :bc) (result-id result-type float)
  (:spir-v (inst result-id result-type 'zrspv:%convert-f-to-s float)))

;;; TODO: next
(define-instruction (%matrix*scalar :bc) (result-id result-type matrix scalar)
  (:spir-v (inst result-id result-type 'zrspv:%matrix-times-scalar matrix scalar)))

(define-instruction (%vector*scalar :bc) (result-id result-type vector scalar)
  (:spir-v (inst result-id result-type 'zrspv:%vector-times-scalar vector scalar))
  (:scalar (lisp-vector*scalar result-id result-type vector scalar)))

;;; TODO: next
(define-instruction (%matrix*vector :bc) (result-id result-type matrix vector)
  (:spir-v (inst result-id result-type 'zrspv:%matrix-times-vector matrix vector)))

;;; TODO: next (note: row vec)
(define-instruction (%vector*matrix :bc) (result-id result-type vector matrix)
  (:spir-v (inst result-id result-type 'zrspv:%vector-times-matrix vector matrix)))

(define-instruction (%matrix*matrix :bc) (result-id result-type m1 m2)
  (:spir-v (inst result-id result-type 'zrspv:%matrix-times-matrix m1 m2))
  (:scalar (lisp-matmul result-id result-type m1 m2)))

;;; TODO: next
(define-instruction (%transpose :bc) (result-id result-type matrix)
  (:spir-v (inst result-id result-type 'zrspv:%transpose matrix)))

(define-instruction (%f-clamp :bc) (result-id result-type x min max)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%f-clamp x min max))
  (:scalar (lisp-clamp result-id result-type x min max)))

(define-instruction (%f-max :bc) (result-id result-type x y)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%f-max x y))
  (:scalar (lisp-simple-binary result-id result-type 'max x y)))

(define-instruction (%f-min :bc) (result-id result-type x y)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%f-min x y))
  (:scalar (lisp-simple-binary result-id result-type 'min x y)))

(define-instruction (%degrees :bc) (result-id result-type radians)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%degrees radians))
  (:scalar (lisp-degrees result-id result-type radians)))

(define-instruction (%radians :bc) (result-id result-type degrees)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%radians degrees))
  (:scalar (lisp-radians result-id result-type degrees)))

(define-instruction (%distance :bc) (result-id result-type point1 point2)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%distance point1 point2))
  (:scalar (lisp-distance result-id result-type point1 point2)))

(define-instruction (%vec-length :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%length x))
  (:scalar (lisp-magnitude result-id result-type x)))

(define-instruction (%normalize :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%normalize x))
  (:scalar (lisp-normalize result-id result-type x)))

;;; TODO: scalar
(define-instruction (%reflect :bc) (result-id result-type i n)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%reflect i n)))

(define-instruction (%cross :bc) (result-id result-type x y)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%cross x y))
  (:scalar (lisp-cross result-id result-type x y)))

;;; TODO: scalar
(define-instruction (%matrix-inverse :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%matrix-inverse x)))

;;; TODO: scalar
(define-instruction (%determinant :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%determinant x)))

(define-instruction (%sin :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%sin x))
  (:scalar (lisp-simple-unary result-id result-type 'sin x)))

(define-instruction (%cos :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%cos x))
  (:scalar (lisp-simple-unary result-id result-type 'cos x)))

(define-instruction (%tan :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%tan x))
  (:scalar (lisp-simple-unary result-id result-type 'tan x)))

(define-instruction (%asin :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%asin x))
  (:scalar (lisp-simple-unary result-id result-type 'asin x)))

(define-instruction (%acos :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%acos x))
  (:scalar (lisp-simple-unary result-id result-type 'acos x)))

(define-instruction (%atan :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%atan x))
  (:scalar (lisp-simple-unary result-id result-type 'atan x)))

(define-instruction (%atan2 :bc) (result-id result-type y x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%atan2 y x))
  (:scalar (lisp-simple-binary result-id result-type 'atan y x)))

(define-instruction (%sinh :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%sinh x))
  (:scalar (lisp-simple-unary result-id result-type 'sinh x)))

(define-instruction (%cosh :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%cosh x))
  (:scalar (lisp-simple-unary result-id result-type 'cosh x)))

(define-instruction (%tanh :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%tanh x))
  (:scalar (lisp-simple-unary result-id result-type 'tanh x)))

(define-instruction (%asinh :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%asinh x))
  (:scalar (lisp-simple-unary result-id result-type 'asinh x)))

(define-instruction (%acosh :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%acosh x))
  (:scalar (lisp-simple-unary result-id result-type 'acosh x)))

(define-instruction (%atanh :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%atanh x))
  (:scalar (lisp-simple-unary result-id result-type 'atanh x)))

(define-instruction (%ln :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%log x))
  (:scalar (lisp-simple-unary result-id result-type 'log x)))

;;; TODO: scalar
(define-instruction (%log2 :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%log2 x)))

(define-instruction (%exp :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%exp x))
  (:scalar (lisp-simple-unary result-id result-type 'exp x)))

(define-instruction (%expt :bc) (result-id result-type x y)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%pow x y))
  (:scalar (lisp-simple-binary result-id result-type 'expt x y)))

;;; TODO: scalar
(define-instruction (%exp2 :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%exp2 x)))

(define-instruction (%sqrt :bc) (result-id result-type x)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%sqrt x))
  (:scalar (lisp-simple-unary result-id result-type 'sqrt x)))

;;; TODO: next
(define-instruction (%f-mix :bc) (result-id result-type x y a)
  (:spir-v (inst result-id result-type 'zrspv:%ext-inst (ext-inst-id) 'zrspv:%f-mix x y a)))

(define-instruction (%image-sample-implicit-lod :bc) (result-id result-type sampled-image coordinate)
  (:spir-v (inst result-id result-type 'zrspv:%image-sample-implicit-lod sampled-image coordinate))
  (t (error "Textures are only implemented on the GPU.")))
