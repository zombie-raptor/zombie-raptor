Installation
============

Instructions on how to make zombie-raptor a Quicklisp local project
are available
[here](https://www.quicklisp.org/beta/faq.html#local-project) and
[here](http://blog.quicklisp.org/2018/01/the-quicklisp-local-projects-mechanism.html).

**Note:** You can create a symbolic link to the project directory in
the Quicklisp local-projects directory. Quicklisp will follow links.

CL Dependencies
---------------

The dependencies should all be listed in the ASDF file. Any unlisted
dependency is a bug.

Dependencies should be handled by Quicklisp and ASDF. The only
dependency not currently in Quicklisp is
[zr-utils](https://gitlab.com/zombie-raptor/zr-utils), which was
originally part of this repository.

C Dependencies
--------------

- OpenGL 4.6 (for SPIR-V)
- SDL2

CL Implementations
------------------

If it doesn't work on a supported platform, it's a bug. Tests will be
run on all supported CL implementations when the game engine is more
mature.

The main requirements of an implementation are:
- 64-bit implementation
- ASDF 3.1.2 or later
- CFFI
- Threads
- Unicode

| Implementation | Supported | FOSS | CFFI |cl-sdl2| Notes                                       |
| -------------- |:---------:|:----:|:----:|:-----:| ------------------------------------------- |
| SBCL           |    Yes    |  Yes |  Yes |  Yes  | Recommended because it can take full advantage of the static types |
| CCL            |    Yes    |  Yes |  Yes |  Yes  |                                             |
| ECL            |    No     |  Yes |  Yes |  Yes  | problematic FFI performance and the workaround seems to interfere with autowrap |
| ABCL           |    No     |  Yes |  Yes |  No   |                                             |
| CLISP          |    No     |  Yes |  Yes |  No   | Outdated ASDF by default                    |
| CMUCL          |    No     |  Yes |  Yes |  No   | No 64-bit support?                          |
| MKCL           |    No     |  Yes |  Yes |  ???  |                                             |
| Clasp          |    No     |  Yes |  No  |  No   | [Clasp GH Issue #162]                       |
| Mezzano        |    No     |  Yes |  No  |  No   |                                             |
| Allegro        |    No     |  No  |  Yes |  ???  |                                             |
| LispWorks      |    No     |  No  |  Yes |  ???  |                                             |
| Scieneer       |    No     |  No  |  Yes |  ???  |                                             |
| Genera         |    No     |  No  |  No  |  No   |                                             |
| mocl           |    No     |  No  |  No  |  No   |                                             |

Operating Systems
-----------------

Note: Only x86-64 is supported for now. Other 64-bit architectures
might be supported in the future. OSes that would require their own
rendering engine to support are unlikely to be worth the effort.

| OS          | Supported   | Notes                            |
| ----------- | :---------: | -------------------------------- |
| Linux       | Yes         | Recommended; 64-bit only         |
| Windows 10  | Yes         | 64-bit only                      |
| macOS       | No          | No plans to support ARM or Metal |
| FreeBSD     | No          |                                  |
| OpenBSD     | No          |                                  |
| NetBSD      | No          |                                  |

[Clasp GH Issue #162]: https://github.com/drmeister/clasp/issues/162
