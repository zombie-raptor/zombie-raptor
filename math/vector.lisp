(defpackage #:zombie-raptor/math/vector
  (:documentation "The vector part of the game's linear algebra.")
  (:use #:cl
        #:zr-utils)
  (:import-from #:alexandria
                #:lerp)
  (:export #:dvec
           #:dvec2
           #:dvec2=
           #:dvec3
           #:dvec3=
           #:dvec4
           #:dvec4=
           #:make-dvec
           #:make-dvec2
           #:make-dvec3
           #:make-dvec4
           #:make-vec
           #:make-vec2
           #:make-vec3
           #:make-vec4
           #:normalize/mv
           #:normalize/mv4
           #:scalar*vec/mv
           #:vec
           #:vec+/mv2
           #:vec+/mv
           #:vec+/mv4
           #:vec-/mv2
           #:vec-/mv
           #:vec-/mv4
           #:vec-cross/mv
           #:vec-dot/mv
           #:vec-dot/mv4
           #:vec-lerp/mv
           #:vec2
           #:vec2=
           #:vec3
           #:vec3=
           #:vec4
           #:vec4=
           #:vec=))

(in-package #:zombie-raptor/math/vector)

;;;; Types and type constructors

(deftype vec (&optional (length '*))
  `(simple-array single-float (,length)))

(deftype dvec (&optional (length '*))
  `(simple-array double-float (,length)))

(define-array-and-type vec2 (single-float '(2)) (x y))
(define-array-and-type vec3 (single-float '(3)) (x y z))
(define-array-and-type vec4 (single-float '(4)) (x y z w))

(define-array-and-type dvec2 (double-float '(2)) (x y))
(define-array-and-type dvec3 (double-float '(3)) (x y z))
(define-array-and-type dvec4 (double-float '(4)) (x y z w))

(define-function (vec :inline t) (x y &optional z w)
  (cond (z (if w
               (vec4 x y z w)
               (vec3 x y z)))
        ((not w) (vec2 x y))
        (t (error "Invalid vec."))))

(define-function (dvec :inline t) (x y &optional z w)
  (cond (z (if w
               (dvec4 x y z w)
               (dvec3 x y z)))
        ((not w) (dvec2 x y))
        (t (error "Invalid dvec."))))

;;;; Macros

;;;; Functions

(define-function (vec2= :inline t) ((v1 vec2) (v2 vec2))
  (every #'= v1 v2))

(define-function (vec3= :inline t) ((v1 vec3) (v2 vec3))
  (every #'= v1 v2))

(define-function (vec4= :inline t) ((v1 vec4) (v2 vec4))
  (every #'= v1 v2))

(define-function (dvec2= :inline t) ((v1 dvec2) (v2 dvec2))
  (every #'= v1 v2))

(define-function (dvec3= :inline t) ((v1 dvec3) (v2 dvec3))
  (every #'= v1 v2))

(define-function (dvec4= :inline t) ((v1 dvec4) (v2 dvec4))
  (every #'= v1 v2))

(define-function (vec+/mv2 :inline t :default-type single-float)
    (v1.x v1.y v2.x v2.y)
  (values (+ v1.x v2.x)
          (+ v1.y v2.y)))

(define-function (vec+/mv :inline t :default-type single-float)
    (v1.x v1.y v1.z v2.x v2.y v2.z)
  (values (+ v1.x v2.x)
          (+ v1.y v2.y)
          (+ v1.z v2.z)))

(define-function (vec+/mv4 :inline t :default-type single-float)
    (v1.x v1.y v1.z v1.w v2.x v2.y v2.z v2.w)
  (values (+ v1.x v2.x)
          (+ v1.y v2.y)
          (+ v1.z v2.z)
          (+ v1.w v2.w)))

(define-function (vec+/dmv2 :inline t :default-type double-float)
    (v1.x v1.y v2.x v2.y)
  (values (+ v1.x v2.x)
          (+ v1.y v2.y)))

(define-function (vec+/dmv :inline t :default-type double-float)
    (v1.x v1.y v1.z v2.x v2.y v2.z)
  (values (+ v1.x v2.x)
          (+ v1.y v2.y)
          (+ v1.z v2.z)))

(define-function (vec+/dmv4 :inline t :default-type double-float)
    (v1.x v1.y v1.z v1.w v2.x v2.y v2.z v2.w)
  (values (+ v1.x v2.x)
          (+ v1.y v2.y)
          (+ v1.z v2.z)
          (+ v1.w v2.w)))

(define-function (vec-/mv2 :inline t :default-type single-float)
    (v1.x v1.y v2.x v2.y)
  (values (- v1.x v2.x)
          (- v1.y v2.y)))

(define-function (vec-/mv :inline t :default-type single-float)
    (v1.x v1.y v1.z v2.x v2.y v2.z)
  (values (- v1.x v2.x)
          (- v1.y v2.y)
          (- v1.z v2.z)))

(define-function (vec-/mv4 :inline t :default-type single-float)
    (v1.x v1.y v1.z v1.w v2.x v2.y v2.z v2.w)
  (values (- v1.x v2.x)
          (- v1.y v2.y)
          (- v1.z v2.z)
          (- v1.w v2.w)))

(define-function (vec-/dmv2 :inline t :default-type double-float)
    (v1.x v1.y v2.x v2.y)
  (values (- v1.x v2.x)
          (- v1.y v2.y)))

(define-function (vec-/dmv :inline t :default-type double-float)
    (v1.x v1.y v1.z v2.x v2.y v2.z)
  (values (- v1.x v2.x)
          (- v1.y v2.y)
          (- v1.z v2.z)))

(define-function (vec-/dmv4 :inline t :default-type double-float)
    (v1.x v1.y v1.z v1.w v2.x v2.y v2.z v2.w)
  (values (- v1.x v2.x)
          (- v1.y v2.y)
          (- v1.z v2.z)
          (- v1.w v2.w)))

(define-function (scalar*vec/mv2 :inline t :default-type single-float)
    (c x y)
  (values (* c x) (* c y)))

(define-function (scalar*vec/mv :inline t :default-type single-float)
    (c x y z)
  (values (* c x) (* c y) (* c z)))

(define-function (scalar*vec/mv4 :inline t :default-type single-float)
    (c x y z w)
  (values (* c x) (* c y) (* c z) (* c w)))

(define-function (scalar*vec/dmv2 :inline t :default-type double-float)
    (c x y)
  (values (* c x) (* c y)))

(define-function (scalar*vec/dmv :inline t :default-type double-float)
    (c x y z)
  (values (* c x) (* c y) (* c z)))

(define-function (scalar*vec/dmv4 :inline t :default-type double-float)
    (c x y z w)
  (values (* c x) (* c y) (* c z) (* c w)))

(define-function (vec-dot/mv2 :inline t :default-type single-float)
    (v1.x v1.y v2.x v2.y)
  (+ (* v1.x v2.x)
     (* v1.y v2.y)))

(define-function (vec-dot/mv :inline t :default-type single-float)
    (v1.x v1.y v1.z v2.x v2.y v2.z)
  (+ (* v1.x v2.x)
     (* v1.y v2.y)
     (* v1.z v2.z)))

(define-function (vec-dot/mv4 :inline t :default-type single-float)
    (v1.x v1.y v1.z v1.w v2.x v2.y v2.z v2.w)
  (+ (* v1.x v2.x)
     (* v1.y v2.y)
     (* v1.z v2.z)
     (* v1.w v2.w)))

(define-function (vec-dot/dmv2 :inline t :default-type double-float)
    (v1.x v1.y v2.x v2.y)
  (+ (* v1.x v2.x)
     (* v1.y v2.y)))

(define-function (vec-dot/dmv :inline t :default-type double-float)
    (v1.x v1.y v1.z v2.x v2.y v2.z)
  (+ (* v1.x v2.x)
     (* v1.y v2.y)
     (* v1.z v2.z)))

(define-function (vec-dot/dmv4 :inline t :default-type double-float)
    (v1.x v1.y v1.z v1.w v2.x v2.y v2.z v2.w)
  (+ (* v1.x v2.x)
     (* v1.y v2.y)
     (* v1.z v2.z)
     (* v1.w v2.w)))

(define-function (vec-cross/mv :inline t :default-type single-float)
    (v1.x v1.y v1.z v2.x v2.y v2.z)
  (values (- (* v1.y v2.z) (* v1.z v2.y))
          (- (* v1.z v2.x) (* v1.x v2.z))
          (- (* v1.x v2.y) (* v1.y v2.x))))

(define-function (vec-cross/dmv :inline t :default-type double-float)
    (v1.x v1.y v1.z v2.x v2.y v2.z)
  (values (- (* v1.y v2.z) (* v1.z v2.y))
          (- (* v1.z v2.x) (* v1.x v2.z))
          (- (* v1.x v2.y) (* v1.y v2.x))))

(define-function (vec-lerp/mv2 :inline t :default-type single-float)
    (time u.x u.y v.x v.y)
  (values (lerp time u.x v.x)
          (lerp time u.y v.y)))

(define-function (vec-lerp/mv :inline t :default-type single-float)
    (time u.x u.y u.z v.x v.y v.z)
  (values (lerp time u.x v.x)
          (lerp time u.y v.y)
          (lerp time u.z v.z)))

(define-function (vec-lerp/mv4 :inline t :default-type single-float)
    (time u.x u.y u.z u.w v.x v.y v.z v.w)
  (values (lerp time u.x v.x)
          (lerp time u.y v.y)
          (lerp time u.z v.z)
          (lerp time u.w v.w)))

(define-function (vec-lerp/dmv2 :inline t :default-type double-float)
    (time u.x u.y v.x v.y)
  (values (lerp time u.x v.x)
          (lerp time u.y v.y)))

(define-function (vec-lerp/dmv :inline t :default-type double-float)
    (time u.x u.y u.z v.x v.y v.z)
  (values (lerp time u.x v.x)
          (lerp time u.y v.y)
          (lerp time u.z v.z)))

(define-function (vec-lerp/dmv4 :inline t :default-type double-float)
    (time u.x u.y u.z u.w v.x v.y v.z v.w)
  (values (lerp time u.x v.x)
          (lerp time u.y v.y)
          (lerp time u.z v.z)
          (lerp time u.w v.w)))

(define-function (normalize/mv2 :inline t :default-type single-float)
    (x y)
  (let ((length (sqrt (+ (expt x 2) (expt y 2)))))
    (values (/ x length)
            (/ y length))))

(define-function (normalize/mv :inline t :default-type single-float)
    (x y z)
  (let ((length (sqrt (+ (expt x 2) (expt y 2) (expt z 2)))))
    (values (/ x length)
            (/ y length)
            (/ z length))))

(define-function (normalize/mv4 :inline t :default-type single-float)
    (x y z w)
  (let ((length (sqrt (+ (expt x 2) (expt y 2) (expt z 2) (expt w 2)))))
    (values (/ x length)
            (/ y length)
            (/ z length)
            (/ w length))))

(define-function (normalize/dmv2 :inline t :default-type double-float)
    (x y)
  (let ((length (sqrt (+ (expt x 2) (expt y 2)))))
    (values (/ x length)
            (/ y length))))

(define-function (normalize/dmv :inline t :default-type double-float)
    (x y z)
  (let ((length (sqrt (+ (expt x 2) (expt y 2) (expt z 2)))))
    (values (/ x length)
            (/ y length)
            (/ z length))))

(define-function (normalize/dmv4 :inline t :default-type double-float)
    (x y z w)
  (let ((length (sqrt (+ (expt x 2) (expt y 2) (expt z 2) (expt w 2)))))
    (values (/ x length)
            (/ y length)
            (/ z length)
            (/ w length))))
