(defpackage #:zombie-raptor/math/matrix
  (:documentation "The matrices part of the game's linear algebra.")
  (:use #:cl
        #:zombie-raptor/core/constants
        #:zombie-raptor/core/settings
        #:zombie-raptor/math/quaternion
        #:zombie-raptor/math/vector
        #:zombie-raptor/zrvl/zrvl
        #:zr-utils)
  (:import-from #:alexandria
                #:lerp)
  (:import-from #:cffi
                :foreign-pointer)
  (:import-from #:static-vectors
                #:free-static-vector
                #:make-static-vector
                #:static-vector-pointer)
  (:export #:c-matrix
           #:c-quaternion-rotation-matrix
           #:c-yaw-rotation-matrix
           #:camera-2D-matrix
           #:camera-matrix
           #:finite-frustum
           #:foreign-matrices
           #:free-foreign-matrices
           #:identity-matrix
           #:infinite-frustum
           #:inverse-matrix
           #:make-c-matrix-buffer
           #:make-foreign-matrices
           #:make-gl-matrix-storage
           #:make-matrix-buffer
           #:make-ortho
           #:make-projection-matrix
           #:matref
           #:matrix
           #:matrix*
           #:matrix**
           #:matrix*vector/mv
           #:matrix-buffer
           #:matrix-buffer-index
           #:matrix-buffer-ref
           #:matrix=
           #:model-matrix
           #:model-matrix*
           #:normal-matrix
           #:ortho
           #:print-matrix
           #:quaternion-rotation-matrix
           #:scale
           #:translate
           #:transpose-matrix
           #:update-fov
           #:with-foreign-matrices
           #:yaw-rotation-matrix
           #:zero-matrix))

(in-package #:zombie-raptor/math/matrix)

(deftype matrix ()
  "
A default matrix is a 4x4 matrix represented as a length 16 1D array,
which is for ease of use with CFFI.
"
  '(simple-array single-float (16)))

(deftype matrix-buffer ()
  "A matrix buffer is some number of 4x4 matrices in one 1D array."
  `(simple-array single-float (,(* 16 +max-entities+))))

(deftype matrix-buffer-index ()
  "
A matrix buffer index is a valid number for a matrix in the above
matrix-buffer type.
"
  `(integer 0 ,(1- +max-entities+)))

(define-function (matrix :inline t)
    (m11 m12 m13 m14 m21 m22 m23 m24 m31 m32 m33 m34 m41 m42 m43 m44)
  (make-array 16
              :element-type 'single-float
              :initial-contents (list m11 m21 m31 m41
                                      m12 m22 m32 m42
                                      m13 m23 m33 m43
                                      m14 m24 m34 m44)))

(define-function (matrix= :inline t) ((m1 matrix) (m2 matrix))
  "
Is (matref m1 row column) the same number as (matref m2 row column)
for all rows and columns?"
  (every #'= m1 m2))

(defsetf matrix (matrix) (m11 m12 m13 m14 m21 m22 m23 m24 m31 m32 m33 m34 m41 m42 m43 m44)
  `(psetf (aref ,matrix  0) ,m11
          (aref ,matrix  1) ,m21
          (aref ,matrix  2) ,m31
          (aref ,matrix  3) ,m41
          (aref ,matrix  4) ,m12
          (aref ,matrix  5) ,m22
          (aref ,matrix  6) ,m32
          (aref ,matrix  7) ,m42
          (aref ,matrix  8) ,m13
          (aref ,matrix  9) ,m23
          (aref ,matrix 10) ,m33
          (aref ,matrix 11) ,m43
          (aref ,matrix 12) ,m14
          (aref ,matrix 13) ,m24
          (aref ,matrix 14) ,m34
          (aref ,matrix 15) ,m44))

(defmacro matref (matrix row column)
  "
Converts a traditional matrix index (1-based and 2D) to an internal
aref. 1-based indexing is used so it is easier to directly translate
from the math.

Note: This is a macro to avoid hitting compiler inline limits. Because
of this, (setf matref) implicitly exists instead of actually being
defined somewhere."
  `(aref ,matrix (+ (* (1- ,column) 4) (1- ,row))))

(define-function (make-matrix-buffer :inline t) (&key (initial-element 0f0 single-float))
  (make-array (* 16 +max-entities+) :element-type 'single-float :initial-element initial-element))

(define-function (make-c-matrix-buffer :inline t) (&key (initial-element 0f0 single-float))
  (let ((matrix-buffer (make-static-vector (* 16 +max-entities+)
                                           :element-type 'single-float
                                           :initial-element initial-element)))
    (declare (matrix-buffer matrix-buffer))
    matrix-buffer))

(defmacro matrix-buffer-ref (matrices matrix row column)
  `(aref ,matrices (+ (* 16 ,matrix) (* (1- ,column) 4) (1- ,row))))

(define-function print-matrix ((matrix matrix) &optional (stream *standard-output*))
  (dotimes (i 4)
    (write-char #\[ stream)
    (dotimes (j 4)
      (let ((number (matref matrix (1+ i) (1+ j))))
        (format stream "~:[~11,4,2E~;~11,6F~] " (< -1000 number 1000) number)))
    (write-char #\] stream)
    (terpri stream)))

(defun make-gl-matrix-storage (quantity)
  (let ((storage (make-array quantity)))
    (dotimes (i quantity storage)
      (setf (aref storage i) (make-static-vector (* 4 4) :element-type 'single-float)))))

(define-function (c-matrix :inline t)
    (m11 m12 m13 m14 m21 m22 m23 m24 m31 m32 m33 m34 m41 m42 m43 m44)
  (let ((matrix (make-static-vector (* 4 4) :element-type 'single-float)))
    (declare (matrix matrix))
    (setf (matrix matrix) (values m11 m12 m13 m14
                                  m21 m22 m23 m24
                                  m31 m32 m33 m34
                                  m41 m42 m43 m44))
    matrix))

(defmacro define-matrix-constructor-pair ((matrix-constructor c-matrix-constructor) typed-lambda-list &rest values)
  `(progn (define-function (,matrix-constructor :inline t :default-type single-float)
              ,typed-lambda-list
            (matrix ,@values))
          (define-function (,c-matrix-constructor :inline t :default-type single-float)
              ,typed-lambda-list
            (c-matrix ,@values))))

(defmacro define-matrix-function-pair ((matrix-function-name matrix-buffer-function-name &rest options)
                                       matrix-typed-lambda-list
                                       matrix-buffer-typed-lambda-list
                                       (type type-ref)
                                       &body body)
  `(progn (define-function (,matrix-function-name ,@(subst 'matrix type options))
              ,(subst 'matrix type matrix-typed-lambda-list)
            (macrolet ((,type-ref (matrix i row column)
                         (declare (ignore i))
                         `(matref ,matrix ,row ,column)))
              ,@(subst 'matrix type body)))
          (define-function (,matrix-buffer-function-name ,@(subst 'matrix-buffer type options))
              ,(subst 'matrix-buffer type matrix-buffer-typed-lambda-list)
            (macrolet ((,type-ref (matrices matrix row column)
                         `(matrix-buffer-ref ,matrices ,matrix ,row ,column)))
              ,@(subst 'matrix-buffer type body)))))

(define-matrix-function-pair (matrix*vector/mv matrix*vector/elt*mv :default-type single-float)
                               ((m m) x y z &optional (w 1f0))
                               ((m m) (i matrix-buffer-index) x y z &optional (w 1f0))
                               (m mref)
                             (declare (optimize (speed 3)))
                             (let ((x (+ (* (mref m i 1 1) x) (* (mref m i 1 2) y) (* (mref m i 1 3) z) (* (mref m i 1 4) w)))
                                   (y (+ (* (mref m i 2 1) x) (* (mref m i 2 2) y) (* (mref m i 2 3) z) (* (mref m i 2 4) w)))
                                   (z (+ (* (mref m i 3 1) x) (* (mref m i 3 2) y) (* (mref m i 3 3) z) (* (mref m i 3 4) w)))
                                   (w (+ (* (mref m i 4 1) x) (* (mref m i 4 2) y) (* (mref m i 4 3) z) (* (mref m i 4 4) w))))
                               (values x y z w)))

(define-function matrix* ((destination matrix) (m1 matrix) (m2 matrix))
  (declare (optimize (speed 3)))
  (let ((a (+ (* (matref m1 1 1) (matref m2 1 1)) (* (matref m1 1 2) (matref m2 2 1)) (* (matref m1 1 3) (matref m2 3 1)) (* (matref m1 1 4) (matref m2 4 1))))
        (b (+ (* (matref m1 1 1) (matref m2 1 2)) (* (matref m1 1 2) (matref m2 2 2)) (* (matref m1 1 3) (matref m2 3 2)) (* (matref m1 1 4) (matref m2 4 2))))
        (c (+ (* (matref m1 1 1) (matref m2 1 3)) (* (matref m1 1 2) (matref m2 2 3)) (* (matref m1 1 3) (matref m2 3 3)) (* (matref m1 1 4) (matref m2 4 3))))
        (d (+ (* (matref m1 1 1) (matref m2 1 4)) (* (matref m1 1 2) (matref m2 2 4)) (* (matref m1 1 3) (matref m2 3 4)) (* (matref m1 1 4) (matref m2 4 4))))

        (e (+ (* (matref m1 2 1) (matref m2 1 1)) (* (matref m1 2 2) (matref m2 2 1)) (* (matref m1 2 3) (matref m2 3 1)) (* (matref m1 2 4) (matref m2 4 1))))
        (f (+ (* (matref m1 2 1) (matref m2 1 2)) (* (matref m1 2 2) (matref m2 2 2)) (* (matref m1 2 3) (matref m2 3 2)) (* (matref m1 2 4) (matref m2 4 2))))
        (g (+ (* (matref m1 2 1) (matref m2 1 3)) (* (matref m1 2 2) (matref m2 2 3)) (* (matref m1 2 3) (matref m2 3 3)) (* (matref m1 2 4) (matref m2 4 3))))
        (h (+ (* (matref m1 2 1) (matref m2 1 4)) (* (matref m1 2 2) (matref m2 2 4)) (* (matref m1 2 3) (matref m2 3 4)) (* (matref m1 2 4) (matref m2 4 4))))

        (i (+ (* (matref m1 3 1) (matref m2 1 1)) (* (matref m1 3 2) (matref m2 2 1)) (* (matref m1 3 3) (matref m2 3 1)) (* (matref m1 3 4) (matref m2 4 1))))
        (j (+ (* (matref m1 3 1) (matref m2 1 2)) (* (matref m1 3 2) (matref m2 2 2)) (* (matref m1 3 3) (matref m2 3 2)) (* (matref m1 3 4) (matref m2 4 2))))
        (k (+ (* (matref m1 3 1) (matref m2 1 3)) (* (matref m1 3 2) (matref m2 2 3)) (* (matref m1 3 3) (matref m2 3 3)) (* (matref m1 3 4) (matref m2 4 3))))
        (l (+ (* (matref m1 3 1) (matref m2 1 4)) (* (matref m1 3 2) (matref m2 2 4)) (* (matref m1 3 3) (matref m2 3 4)) (* (matref m1 3 4) (matref m2 4 4))))

        (m (+ (* (matref m1 4 1) (matref m2 1 1)) (* (matref m1 4 2) (matref m2 2 1)) (* (matref m1 4 3) (matref m2 3 1)) (* (matref m1 4 4) (matref m2 4 1))))
        (n (+ (* (matref m1 4 1) (matref m2 1 2)) (* (matref m1 4 2) (matref m2 2 2)) (* (matref m1 4 3) (matref m2 3 2)) (* (matref m1 4 4) (matref m2 4 2))))
        (o (+ (* (matref m1 4 1) (matref m2 1 3)) (* (matref m1 4 2) (matref m2 2 3)) (* (matref m1 4 3) (matref m2 3 3)) (* (matref m1 4 4) (matref m2 4 3))))
        (p (+ (* (matref m1 4 1) (matref m2 1 4)) (* (matref m1 4 2) (matref m2 2 4)) (* (matref m1 4 3) (matref m2 3 4)) (* (matref m1 4 4) (matref m2 4 4)))))
    (setf (matrix destination) (values a b c d
                                       e f g h
                                       i j k l
                                       m n o p))))

(defmacro matrix** (destination-matrix &rest matrices)
  (cond ((endp matrices)
         destination-matrix)
        ((endp (cdr matrices))
         `(progn (replace ,destination-matrix ,(car matrices))
                 ,destination-matrix))
        (t
         (let ((matrices (reverse matrices)))
           (destructuring-bind (matrix-2 matrix-1 &rest matrices) matrices
             (let ((first-multiplication `(matrix* ,destination-matrix ,matrix-1 ,matrix-2)))
               (if matrices
                   `(progn
                      ,first-multiplication
                      ,@(loop :for matrix :in matrices
                              :collect `(matrix* ,destination-matrix ,matrix ,destination-matrix)))
                   first-multiplication)))))))

(define-function inverse-matrix ((destination matrix) (matrix matrix))
  (let ((m11 (matref matrix 1 1)) (m12 (matref matrix 1 2)) (m13 (matref matrix 1 3)) (m14 (matref matrix 1 4))
        (m21 (matref matrix 2 1)) (m22 (matref matrix 2 2)) (m23 (matref matrix 2 3)) (m24 (matref matrix 2 4))
        (m31 (matref matrix 3 1)) (m32 (matref matrix 3 2)) (m33 (matref matrix 3 3)) (m34 (matref matrix 3 4))
        (m41 (matref matrix 4 1)) (m42 (matref matrix 4 2)) (m43 (matref matrix 4 3)) (m44 (matref matrix 4 4)))
    (let ((det (- (+ (* m11 m22 m33 m44)
                     (* m11 m23 m34 m42)
                     (* m11 m24 m32 m43)
                     (* m12 m21 m34 m43)
                     (* m12 m23 m31 m44)
                     (* m12 m24 m33 m41)
                     (* m13 m21 m32 m44)
                     (* m13 m22 m34 m41)
                     (* m13 m24 m31 m42)
                     (* m14 m21 m33 m42)
                     (* m14 m22 m31 m43)
                     (* m14 m23 m32 m41))
                  (* m11 m22 m34 m43)
                  (* m11 m23 m32 m44)
                  (* m11 m24 m33 m42)
                  (* m12 m21 m33 m44)
                  (* m12 m23 m34 m41)
                  (* m12 m24 m31 m43)
                  (* m13 m21 m34 m42)
                  (* m13 m22 m31 m44)
                  (* m13 m24 m32 m41)
                  (* m14 m21 m32 m43)
                  (* m14 m22 m33 m41)
                  (* m14 m23 m31 m42))))
      (if (zerop det)
          ;; TODO: replace with custom-defined condition
          (error "Encountered a matrix with a 0 determinant")
          (let ((1/det (/ det)))
            (setf (matrix destination)
                  (values (* 1/det (- (+ (* m22 m33 m44) (* m23 m34 m42) (* m24 m32 m43))
                                      (* m22 m34 m43) (* m23 m32 m44) (* m24 m33 m42)))
                          (* 1/det (- (+ (* m12 m34 m43) (* m13 m32 m44) (* m14 m33 m42))
                                      (* m12 m33 m44) (* m13 m34 m42) (* m14 m32 m43)))
                          (* 1/det (- (+ (* m12 m23 m44) (* m13 m24 m42) (* m14 m22 m43))
                                      (* m12 m24 m43) (* m13 m22 m44) (* m14 m23 m42)))
                          (* 1/det (- (+ (* m12 m24 m33) (* m13 m22 m34) (* m14 m23 m32))
                                      (* m12 m23 m34) (* m13 m24 m32) (* m14 m22 m33)))

                          (* 1/det (- (+ (* m21 m34 m43) (* m23 m31 m44) (* m24 m33 m41))
                                      (* m21 m33 m44) (* m23 m34 m41) (* m24 m31 m43)))
                          (* 1/det (- (+ (* m11 m33 m44) (* m13 m34 m41) (* m14 m31 m43))
                                      (* m11 m34 m43) (* m13 m31 m44) (* m14 m33 m41)))
                          (* 1/det (- (+ (* m11 m24 m43) (* m13 m21 m44) (* m14 m23 m41))
                                      (* m11 m23 m44) (* m13 m24 m41) (* m14 m21 m43)))
                          (* 1/det (- (+ (* m11 m23 m34) (* m13 m24 m31) (* m14 m21 m33))
                                      (* m11 m24 m33) (* m13 m21 m34) (* m14 m23 m31)))

                          (* 1/det (- (+ (* m21 m32 m44) (* m22 m34 m41) (* m24 m31 m42))
                                      (* m21 m34 m42) (* m22 m31 m44) (* m24 m32 m41)))
                          (* 1/det (- (+ (* m11 m34 m42) (* m12 m31 m44) (* m14 m32 m41))
                                      (* m11 m32 m44) (* m12 m34 m41) (* m14 m31 m42)))
                          (* 1/det (- (+ (* m11 m22 m44) (* m12 m24 m41) (* m14 m21 m42))
                                      (* m11 m24 m42) (* m12 m21 m44) (* m14 m22 m41)))
                          (* 1/det (- (+ (* m11 m24 m32) (* m12 m21 m34) (* m14 m22 m31))
                                      (* m11 m22 m34) (* m12 m24 m31) (* m14 m21 m32)))

                          (* 1/det (- (+ (* m21 m33 m42) (* m22 m31 m43) (* m23 m32 m41))
                                      (* m21 m32 m43) (* m22 m33 m41) (* m23 m31 m42)))
                          (* 1/det (- (+ (* m11 m32 m43) (* m12 m33 m41) (* m13 m31 m42))
                                      (* m11 m33 m42) (* m12 m31 m43) (* m13 m32 m41)))
                          (* 1/det (- (+ (* m11 m23 m42) (* m12 m21 m43) (* m13 m22 m41))
                                      (* m11 m22 m43) (* m12 m23 m41) (* m13 m21 m42)))
                          (* 1/det (- (+ (* m11 m22 m33) (* m12 m23 m31) (* m13 m21 m32))
                                      (* m11 m23 m32) (* m12 m21 m33) (* m13 m22 m31))))))))))

;;; Basic matrices

;;; These matrices all come in both CL-native matrix and foreign C
;;; matrix varieties.

(define-function (transpose-matrix :inline t) (destination matrix)
  (setf (matrix destination) (values (matref matrix 1 1) (matref matrix 2 1) (matref matrix 3 1) (matref matrix 4 1)
                                     (matref matrix 1 2) (matref matrix 2 2) (matref matrix 3 2) (matref matrix 4 2)
                                     (matref matrix 1 3) (matref matrix 2 3) (matref matrix 3 3) (matref matrix 4 3)
                                     (matref matrix 1 4) (matref matrix 2 4) (matref matrix 3 4) (matref matrix 4 4))))

(define-matrix-constructor-pair (zero-matrix c-zero-matrix) ()
  0f0 0f0 0f0 0f0
  0f0 0f0 0f0 0f0
  0f0 0f0 0f0 0f0
  0f0 0f0 0f0 0f0)

(define-matrix-constructor-pair (identity-matrix c-identity-matrix) ()
  1f0 0f0 0f0 0f0
  0f0 1f0 0f0 0f0
  0f0 0f0 1f0 0f0
  0f0 0f0 0f0 1f0)

(define-matrix-constructor-pair (scale c-scale) (x y z)
  x 0f0 0f0 0f0
  0f0 y 0f0 0f0
  0f0 0f0 z 0f0
  0f0 0f0 0f0 1f0)

(define-matrix-constructor-pair (translate c-translate) (x y z)
  1f0 0f0 0f0 x
  0f0 1f0 0f0 y
  0f0 0f0 1f0 z
  0f0 0f0 0f0 1f0)

(define-matrix-constructor-pair (quaternion-rotation-matrix c-quaternion-rotation-matrix) (i j k w)
  (- 1f0 (* 2f0 (expt j 2)) (* 2f0 (expt k 2)))
  (* 2f0 (- (* i j) (* k w)))
  (* 2f0 (+ (* i k) (* j w)))
  0f0
  (* 2f0 (+ (* i j) (* k w)))
  (- 1f0 (* 2f0 (expt i 2)) (* 2f0 (expt k 2)))
  (* 2f0 (- (* j k) (* i w)))
  0f0
  (* 2f0 (- (* i k) (* j w)))
  (* 2f0 (+ (* j k) (* i w)))
  (- 1f0 (* 2f0 (expt i 2)) (* 2f0 (expt j 2)))
  0f0
  0f0 0f0 0f0 1f0)

(define-function (yaw-rotation-matrix :inline t) ((angle single-float))
  (multiple-value-call #'quaternion-rotation-matrix (rotate-yaw/mv angle)))

(define-function (c-yaw-rotation-matrix :inline t) ((angle single-float))
  (multiple-value-call #'c-quaternion-rotation-matrix (rotate-yaw/mv angle)))

;;; Fancy matrices

;;; These are all C matrices designed to be used directly with OpenGL

(define-function (finite-frustum :inline t :default-type single-float)
    (left right bottom top near far)
  (c-matrix (/ (* 2f0 near) (- right left)) 0f0 (/ (+ right left) (- right left)) 0f0
            0f0 (/ (* 2f0 near) (- top bottom)) (/ (+ top bottom) (- top bottom)) 0f0
            0f0 0f0 (- (/ (+ far near) (- far near))) (- (/ (* 2 far near) (- far near)))
            0f0 0f0 -1f0 0f0))

(define-function (infinite-frustum :inline t :default-type single-float)
    (left right bottom top near)
  (c-matrix (/ (* 2f0 near) (- right left)) 0f0 (/ (+ right left) (- right left)) 0f0
            0f0 (/ (* 2f0 near) (- top bottom)) (/ (+ top bottom) (- top bottom)) 0f0
            0f0 0f0 -1f0 (* -2f0 near)
            0f0 0f0 -1f0 0f0))

(define-function (ortho :inline t :default-type single-float)
    (left right bottom top near far)
  (c-matrix (/ 2f0 (- right left)) 0f0 0f0 (- (/ (+ right left) (- right left)))
            0f0 (/ 2f0 (- top bottom)) 0f0 (- (/ (+ top bottom) (- top bottom)))
            0f0 0f0 (/ -2f0 (- far near)) (- (/ (+ far near) (- far near)))
            0f0 0f0 0f0 1f0))

(define-function (make-projection-matrix :inline t :default-type single-float)
    (fov aspect-ratio)
  (declare (optimize (speed 3)))
  (let* ((near 0.085d0)
         (edge (tan (* (double-float* fov) 0.5d0 (/ (double-float* pi) 180d0))))
         (top (float* (* aspect-ratio near edge)))
         (right (float* (* near edge))))
    (infinite-frustum (- right) right (- top) top (float* near))))

(define-function (%update-fov :inline t) ((perspective-matrix matrix)
                                          (fov single-float)
                                          (aspect single-float))
  (let ((new-matrix (make-projection-matrix fov aspect)))
    (replace perspective-matrix new-matrix)
    (free-static-vector new-matrix)
    perspective-matrix))

(define-function (make-ortho :inline t :default-type single-float) (width height)
  (ortho (- (* 0.5f0 width))
         (* 0.5f0 width)
         (- (* 0.5f0 height))
         (* 0.5f0 height)
         0.1f0
         10f0))

(define-function (camera-lerp :inline t) (time u.x u.y u.z v.x v.y v.z)
  (vec-lerp/mv time (- u.x) (- u.y) (- u.z) (- v.x) (- v.y) (- v.z)))

;;; Note: The camera transformation requires inverting.
(define-function (camera-matrix :inline t)
    (camera-cache entity-index camera-index old-location new-location old-rotation rotation time)
  (let ((destination (aref camera-cache entity-index))
        (m1 (let ((entity-rotation rotation)
                  (old-entity-rotation old-rotation)
                  (look-rotation rotation)
                  (old-look-rotation old-rotation))
              (multiple-value-call #'quaternion-rotation-matrix
                (multiple-value-call #'quaternion-slerp*/mv time
                  (with-zrvl/mv*
                      ((old-entity-rotation zrvl:vec4 :row entity-index)
                       (old-look-rotation   zrvl:vec4 :row camera-index))
                    (zrvl:normalize (zrvl:conjugate (zrvl:* (zrvl:quat<-vec4 old-entity-rotation)
                                                            (zrvl:quat<-vec4 old-look-rotation)))))
                  (with-zrvl/mv*
                      ((entity-rotation     zrvl:vec4 :row entity-index)
                       (look-rotation       zrvl:vec4 :row camera-index))
                    (zrvl:normalize (zrvl:conjugate (zrvl:* (zrvl:quat<-vec4 entity-rotation)
                                                            (zrvl:quat<-vec4 look-rotation)))))))))
        (m2 (with-array-accessors ((old-location old-location :row-of 3)
                                   (new-location new-location :row-of 3))
                entity-index
              (multiple-value-call #'translate
                (multiple-value-call #'camera-lerp time old-location new-location)))))
    (declare (dynamic-extent m1 m2)
             (matrix m1 m2))
    (matrix* destination m1 m2)
    destination))

(define-function (camera-2D-matrix :inline t) ()
  (c-translate 0f0 0f0 -1f0))

(define-function (model-matrix* :inline t)
    (matrix-cache entity-index old-location new-location old-rotation rotation scale time)
  (with-array-accessors ((old-location old-location :row-of 3)
                         (new-location new-location :row-of 3)
                         (old-rotation old-rotation :row-of 4)
                         (rotation rotation :row-of 4)
                         (scale scale :row-of 3))
      entity-index
    (let ((destination (aref matrix-cache entity-index))
          (m1 (multiple-value-call #'translate
                (multiple-value-call #'vec-lerp/mv time old-location new-location)))
          (m2 (multiple-value-call #'quaternion-rotation-matrix
                (multiple-value-call #'normalize/mv4
                  (multiple-value-call #'quaternion-slerp*/mv time old-rotation rotation))))
          (m3 (multiple-value-call #'scale scale)))
      (declare (dynamic-extent m1 m2 m3)
               (matrix destination m1 m2 m3))
      (matrix* m1 m1 m2)
      (matrix* destination m1 m3)
      destination)))

(define-function (normal-matrix :inline t) (matrix-cache entity-index model-matrix)
  (let ((destination (aref matrix-cache entity-index)))
    (declare (matrix destination))
    (inverse-matrix destination model-matrix)
    (transpose-matrix destination destination)
    destination))

(define-function (model-matrix :inline t)
    (model-matrix-cache normal-matrix-cache entity-index old-location new-location old-rotation rotation scale time)
  (let* ((model-matrix (model-matrix* model-matrix-cache entity-index old-location new-location old-rotation rotation scale time))
         (normal-matrix (normal-matrix normal-matrix-cache entity-index model-matrix)))
    (declare (matrix model-matrix normal-matrix))
    (values model-matrix normal-matrix)))

;;; Matrix allocation and freeing

(deftype matrix-cache-array ()
  `(simple-vector ,+max-entities+))

(defstruct (foreign-matrices (:conc-name nil)
                             (:constructor %make-foreign-matrices))
  (projection-matrix         nil :type matrix)
  (hud-matrix                nil :type matrix)
  (hud-view-matrix           nil :type matrix)
  (camera-cache              nil :type matrix-cache-array)
  (model-cache               nil :type matrix-cache-array)
  (normal-cache              nil :type matrix-cache-array)
  (hud-cache                 nil :type matrix-cache-array)
  (projection-matrix-pointer nil :type foreign-pointer)
  (hud-matrix-pointer        nil :type foreign-pointer)
  (hud-view-matrix-pointer   nil :type foreign-pointer)
  (camera-cache-pointers     nil :type matrix-cache-array)
  (model-cache-pointers      nil :type matrix-cache-array)
  (normal-cache-pointers     nil :type matrix-cache-array)
  (hud-cache-pointers        nil :type matrix-cache-array))

(defmethod print-object :around ((foreign-matrices foreign-matrices) stream)
  ;; The arrays are too large to be reasonably printed
  (let ((*print-array* nil))
    (call-next-method)))

(define-function (make-foreign-matrices :inline t) ((settings settings))
  (with-settings (height fov width)
      settings
    (let* ((storage-matrix-size +max-entities+)
           (projection-matrix (make-projection-matrix fov (float* (/ height width))))
           (hud-matrix (make-ortho (float* width) (float* height)))
           (hud-view-matrix (camera-2D-matrix))
           (camera-cache (make-gl-matrix-storage storage-matrix-size))
           (model-cache (make-gl-matrix-storage storage-matrix-size))
           (normal-cache (make-gl-matrix-storage storage-matrix-size))
           (hud-cache (make-gl-matrix-storage storage-matrix-size))
           (projection-matrix-pointer (static-vector-pointer projection-matrix))
           (hud-matrix-pointer (static-vector-pointer hud-matrix))
           (hud-view-matrix-pointer (static-vector-pointer hud-view-matrix))
           (camera-cache-pointers (map 'vector #'static-vector-pointer camera-cache))
           (model-cache-pointers (map 'vector #'static-vector-pointer model-cache))
           (normal-cache-pointers (map 'vector #'static-vector-pointer normal-cache))
           (hud-cache-pointers (map 'vector #'static-vector-pointer hud-cache)))
      (%make-foreign-matrices :projection-matrix projection-matrix
                              :hud-matrix hud-matrix
                              :hud-view-matrix hud-view-matrix
                              :camera-cache camera-cache
                              :model-cache model-cache
                              :normal-cache normal-cache
                              :hud-cache hud-cache
                              :projection-matrix-pointer projection-matrix-pointer
                              :hud-matrix-pointer hud-matrix-pointer
                              :hud-view-matrix-pointer hud-view-matrix-pointer
                              :camera-cache-pointers camera-cache-pointers
                              :model-cache-pointers model-cache-pointers
                              :normal-cache-pointers normal-cache-pointers
                              :hud-cache-pointers hud-cache-pointers))))

(define-accessor-macro with-foreign-matrices)

(define-function (update-fov :inline t) ((foreign-matrices foreign-matrices)
                                         (fov single-float)
                                         (aspect single-float))
  "
Replace the projection matrix with a new projection matrix at a new
field of view (FOV).
"
  (let ((fov (min (max 5f0 fov) 120f0)))
    (%update-fov (projection-matrix foreign-matrices) fov aspect)
    fov))

(defun free-foreign-matrices (foreign-matrices)
  (check-type foreign-matrices foreign-matrices)
  (with-accessors* (projection-matrix
                    hud-matrix
                    hud-view-matrix
                    camera-cache
                    model-cache
                    normal-cache
                    hud-cache)
      foreign-matrices
    (free-static-vector projection-matrix)
    (free-static-vector hud-matrix)
    (free-static-vector hud-view-matrix)
    (map nil #'free-static-vector camera-cache)
    (map nil #'free-static-vector model-cache)
    (map nil #'free-static-vector normal-cache)
    (map nil #'free-static-vector hud-cache)))

(defmethod cleanup ((object foreign-matrices))
  (free-foreign-matrices object))
