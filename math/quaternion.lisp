(defpackage #:zombie-raptor/math/quaternion
  (:documentation "The quaternion part of the game's linear algebra.")
  (:use #:cl
        #:zombie-raptor/math/vector
        #:zombie-raptor/zrvl/zrvl
        #:zr-utils)
  (:export #:invert-quaternion
           #:invert-quaternion-into!
           #:invert-quaternion/mv
           #:make-quaternion
           #:quaternion
           #:quaternion*
           #:quaternion*-into!
           #:quaternion*/mv
           #:quaternion-rotation-of-vector
           #:quaternion-rotation-of-vector-into!
           #:quaternion-rotation/mv
           #:quaternion-slerp*/mv
           #:quaternion-slerp/mv
           #:rotate-pitch
           #:rotate-pitch/mv
           #:rotate-roll
           #:rotate-roll/mv
           #:rotate-yaw
           #:rotate-yaw/mv
           #:rotation-quaternion
           #:rotation/mv))

(in-package #:zombie-raptor/math/quaternion)

(define-array-and-type quaternion (single-float '(4)) (x y z w))

;;;; Basic quaternion operations

(define-function (invert-quaternion/mv :inline t :default-type single-float)
    (q-x q-y q-z q-w)
  (values (- q-x) (- q-y) (- q-z) q-w))

(define-function (quaternion*/mv :inline t :default-type single-float)
    (q1-x q1-y q1-z q1-w q2-x q2-y q2-z q2-w)
  (with-zrvl/mv*
      ((q1-x zrvl:f32)
       (q1-y zrvl:f32)
       (q1-z zrvl:f32)
       (q1-w zrvl:f32)
       (q2-x zrvl:f32)
       (q2-y zrvl:f32)
       (q2-z zrvl:f32)
       (q2-w zrvl:f32))
    (zrvl:* (zrvl:quat q1-x q1-y q1-z q1-w)
            (zrvl:quat q2-x q2-y q2-z q2-w))))

(define-function (invert-quaternion :inline t) ((q quaternion))
  (multiple-value-call #'quaternion
    (multiple-value-call #'invert-quaternion/mv (array-of-4 q))))

(define-function (invert-quaternion-into! :inline t) ((result-q quaternion) (q quaternion))
  (setf (array-of-4 result-q)
        (multiple-value-call #'invert-quaternion/mv (array-of-4 q))))

(define-function (quaternion-slerp/mv :default-type single-float
                                      :return (single-float
                                               single-float
                                               single-float
                                               single-float))
    (a q1-x q1-y q1-z q1-w q2-x q2-y q2-z q2-w)
  (with-zrvl/mv*
      ((a zrvl:f32)
       (q1-x zrvl:f32)
       (q1-y zrvl:f32)
       (q1-z zrvl:f32)
       (q1-w zrvl:f32)
       (q2-x zrvl:f32)
       (q2-y zrvl:f32)
       (q2-z zrvl:f32)
       (q2-w zrvl:f32))
    (zrvl:slerp (zrvl:quat q1-x q1-y q1-z q1-w)
                (zrvl:quat q2-x q2-y q2-z q2-w)
                a)))

(define-function (quaternion-slerp*/mv :default-type single-float
                                       :return (single-float
                                                single-float
                                                single-float
                                                single-float))
    (a q1-x q1-y q1-z q1-w q2-x q2-y q2-z q2-w)
  (let ((dot-product (with-zrvl/mv*
                         ((q1-x zrvl:f32)
                          (q1-y zrvl:f32)
                          (q1-z zrvl:f32)
                          (q1-w zrvl:f32)
                          (q2-x zrvl:f32)
                          (q2-y zrvl:f32)
                          (q2-z zrvl:f32)
                          (q2-w zrvl:f32))
                       (zrvl:dot (zrvl:quat q1-x q1-y q1-z q1-w)
                                 (zrvl:quat q2-x q2-y q2-z q2-w)))))
    (multiple-value-bind (q2-x q2-y q2-z q2-w)
        (if (minusp dot-product)
            ;; Note: -q, not the conjugate of q
            (values (- q2-x) (- q2-y) (- q2-z) (- q2-w))
            (values q2-x q2-y q2-z q2-w))
      (if (<= (abs dot-product) (- 1f0 1f-7))
          (with-zrvl/mv*
              ((a zrvl:f32)
               (q1-x zrvl:f32)
               (q1-y zrvl:f32)
               (q1-z zrvl:f32)
               (q1-w zrvl:f32)
               (q2-x zrvl:f32)
               (q2-y zrvl:f32)
               (q2-z zrvl:f32)
               (q2-w zrvl:f32))
            (zrvl:slerp (zrvl:quat q1-x q1-y q1-z q1-w)
                        (zrvl:quat q2-x q2-y q2-z q2-w)
                        a))
          (values q1-x q1-y q1-z q1-w)))))

(define-function (quaternion* :inline t) ((q1 quaternion) (q2 quaternion))
  (multiple-value-call #'quaternion
    (multiple-value-call #'quaternion*/mv (array-of-4 q1) (array-of-4 q2))))

(define-function (quaternion*-into! :inline t) ((result-q quaternion) (q1 quaternion) (q2 quaternion))
  (setf (array-of-4 result-q)
        (multiple-value-call #'quaternion*/mv (array-of-4 q1) (array-of-4 q2))))

(define-function (vector-to-quaternion :inline t) ((v vec3))
  (multiple-value-call #'quaternion (array-of-3 v) 0f0))

(define-function (quaternion-to-vector :inline t) ((q quaternion))
  (multiple-value-bind (x y z w)
      (array-of-4 q)
    (declare (ignore w))
    (vec3 x y z)))

;;;; Quaternion rotations

(define-function (rotation/mv :inline t :default-type single-float)
    (u-x u-y u-z theta)
  (declare (optimize (speed 3)))
  (let ((theta/2 (* 0.5f0 theta)))
    (values (* (sin theta/2) u-x)
            (* (sin theta/2) u-y)
            (* (sin theta/2) u-z)
            (cos theta/2))))

;;; Note: yaw, pitch, and roll have to be separate instead of using
;;; rotation/mv to avoid unnecessary multiplications by 0f0 that
;;; cannot be optimized away because of +/- 0f0
(define-function (rotate-yaw/mv :inline t) ((theta single-float))
  (with-zrvl/mv* ((theta zrvl:f32))
    (zrvl:quat<-yaw theta)))

(define-function (rotate-pitch/mv :inline t) ((theta single-float))
  (with-zrvl/mv* ((theta zrvl:f32))
    (zrvl:quat<-pitch theta)))

(define-function (rotate-roll/mv :inline t) ((theta single-float))
  (with-zrvl/mv* ((theta zrvl:f32))
    (zrvl:quat<-roll theta)))

(define-function (rotation-quaternion :inline t :default-type single-float)
    (u-x u-y u-z theta)
  (declare (optimize (speed 3)))
  (multiple-value-call #'quaternion
    (rotation/mv u-x u-y u-z theta)))

(define-function (quaternion-rotation/mv :inline t :default-type single-float)
    (q-x q-y q-z q-w x y z)
  (with-zrvl/mv*
      ((q-x zrvl:f32)
       (q-y zrvl:f32)
       (q-z zrvl:f32)
       (q-w zrvl:f32)
       (x zrvl:f32)
       (y zrvl:f32)
       (z zrvl:f32))
    (zrvl:rotate (zrvl:quat q-x q-y q-z q-w)
                 (zrvl:vec x y z))))

(define-function (quaternion-rotation-of-vector :inline t) ((q quaternion) (v vec3))
  (multiple-value-call #'vec3
    (multiple-value-call #'quaternion-rotation/mv (array-of-4 q) (array-of-3 v))))

(define-function (quaternion-rotation-of-vector-into! :inline t)
    ((result-v vec3) (q quaternion) (v vec3))
  (setf (array-of-3 result-v)
        (multiple-value-call #'quaternion-rotation/mv (array-of-4 q) (array-of-3 v))))

(define-function (rotate-yaw :inline t) ((angle single-float))
  (declare (optimize (speed 3)))
  (multiple-value-call #'quaternion (rotate-yaw/mv angle)))

(define-function (rotate-pitch :inline t) ((angle single-float))
  (declare (optimize (speed 3)))
  (multiple-value-call #'quaternion (rotate-pitch/mv angle)))

(define-function (rotate-roll :inline t) ((angle single-float))
  (declare (optimize (speed 3)))
  (multiple-value-call #'quaternion (rotate-roll/mv angle)))
