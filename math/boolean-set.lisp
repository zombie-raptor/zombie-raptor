(cl:defpackage #:zombie-raptor/math/boolean-set
  (:documentation "Expresses a set of Boolean values as bits in a finite-sized integer.")
  (:use #:cl
        #:zr-utils)
  (:export #:+boolean-set-max-size+
           #:+empty-boolean-set+
           #:boolean-set
           #:boolean-set-difference
           #:boolean-set-index
           #:boolean-set-intersection
           #:boolean-set-member-p
           #:boolean-set-subset-p
           #:boolean-set-symmetric-difference
           #:boolean-set-union
           #:index-to-boolean-set
           #:print-boolean-set))

(cl:in-package #:zombie-raptor/math/boolean-set)

(defconstant +boolean-set-max-size+ 62)

(defconstant +empty-boolean-set+ #b0)

(deftype boolean-set-index ()
  "A number of this size can represent a Boolean set's index."
  `(mod ,+boolean-set-max-size+))

(deftype boolean-set ()
  "A number of this size can represent a Boolean set."
  `(unsigned-byte ,+boolean-set-max-size+))

(define-function (index-to-boolean-set :inline t)
    ((index boolean-set-index))
  (ash 1 index))

(define-function (boolean-set-union :inline t)
    ((set-1 boolean-set)
     (set-2 boolean-set))
  (logior set-1 set-2))

(define-function (boolean-set-intersection :inline t)
    ((set-1 boolean-set)
     (set-2 boolean-set))
  (logand set-1 set-2))

(define-function (boolean-set-difference :inline t)
    ((set-1 boolean-set)
     (set-2 boolean-set))
  (logandc2 set-1 set-2))

(define-function (boolean-set-symmetric-difference :inline t)
    ((set-1 boolean-set)
     (set-2 boolean-set))
  (logxor set-1 set-2))

(define-function (boolean-set-subset-p :inline t)
    ((set-1 boolean-set)
     (set-2 boolean-set))
  (= set-1 (boolean-set-intersection set-1 set-2)))

(define-function (boolean-set-member-p :inline t)
    ((index boolean-set-index)
     (set boolean-set))
  (logbitp index set))

(define-function print-boolean-set ((boolean-set boolean-set))
  (format t "~v,'0b" +boolean-set-max-size+ boolean-set))
