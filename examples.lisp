(uiop:define-package #:zombie-raptor/examples
  (:nicknames #:zr/examples)
  (:use #:cl
        #:zr-utils)
  (:use-reexport #:zombie-raptor/examples/controls
                 #:zombie-raptor/examples/example
                 #:zombie-raptor/examples/scripting
                 #:zombie-raptor/examples/shaders
                 #:zombie-raptor/examples/textures
                 #:zombie-raptor/examples/world))
