(uiop:define-package #:zombie-raptor/data
  (:nicknames #:zr/data)
  (:use #:cl
        #:zr-utils)
  (:use-reexport #:zombie-raptor/data/data-path
                 #:zombie-raptor/data/game-data
                 #:zombie-raptor/data/model
                 #:zombie-raptor/data/shader
                 #:zombie-raptor/data/shader-core
                 #:zombie-raptor/data/terrain
                 #:zombie-raptor/data/texture))
