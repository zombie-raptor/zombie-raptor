Zombie Raptor
=============

A 3D game engine written in Common Lisp, with a focus on getting the
maximum performance possible from Common Lisp.

The official git repository for Zombie Raptor is at
https://gitlab.com/zombie-raptor/zombie-raptor and every other
repository is a mirror or fork. Please put issues [on the issue
tracker there](https://gitlab.com/zombie-raptor/zombie-raptor/issues).

The official website of the game engine is
[zombieraptor.com](https://zombieraptor.com/). Complete documentation
will be there when the game engine is ready.

The official IRC channel is `#zombieraptor` on
[`irc.libera.chat`](https://libera.chat/). Additionally, `#lispgames`
on the same network may be able to help.

Warning
-------

This repository is currently a work-in-progress. The API is not
final. Breaking changes can and will happen all of the time.

Installation
------------

See [INSTALL.md](INSTALL.md) for installation instructions.

Outline
-------

See [OUTLINE.md](OUTLINE.md) for an outline of files and directories.

Contributing
------------
See [CONTRIBUTING.md](CONTRIBUTING.md) for contributing and style guidelines.

License
-------

MIT License. See [LICENSE.txt](LICENSE.txt) for the license text.

Changes
-------

See [CHANGELOG.md](CHANGELOG.md) for the complete log of changes.
There are quite a few changes so a more readable "news" summary might
be added in the near future.
