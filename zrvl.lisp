(uiop:define-package #:zombie-raptor/zrvl
  (:nicknames #:zr/zrvl)
  (:use #:cl
        #:zr-utils
        #:zombie-raptor/zrvl/core-types
        #:zombie-raptor/zrvl/instructions
        #:zombie-raptor/zrvl/scalar
        #:zombie-raptor/zrvl/spir-v-enums
        #:zombie-raptor/zrvl/spir-v-instructions)
  (:use-reexport #:zombie-raptor/zrvl/a-normal
                 #:zombie-raptor/zrvl/spir-v
                 #:zombie-raptor/zrvl/zrvl))
